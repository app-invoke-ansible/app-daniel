import React, { createContext, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';
import LogoFaceops from '../img/LogoFaceops.png';
import { LuLoaderCircle } from "react-icons/lu";

const AuthContext = createContext();

// Componente encargado de manejar la autenticación
export const AuthProvider = ({ children }) => {
    const { keycloak, initialized } = useKeycloak();
    const [token, setToken] = useState(null);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [user, setUser] = useState(null);
    const navigate = useNavigate();

    // Función para verificar si el token es válido
    const isTokenValid = (token) => {
        if (!token) return false;
        try {
            const payload = JSON.parse(atob(token.split('.')[1]));
            const currentTime = Math.floor(Date.now() / 1000);
            return payload.exp > currentTime;
        } catch (e) {
            return false;
        }
    };

    // Hook para actualizar el estado del usuario y autenticación
    useEffect(() => {
        if (initialized && keycloak) {
            if (keycloak.authenticated) {
                setIsAuthenticated(true);
                setToken(keycloak.token);

                // Obtener información del usuario
                const payload = JSON.parse(atob(keycloak.token.split('.')[1]));
                setUser({
                    name: payload.preferred_username || 'Usuario',
                    role: payload.realm_access?.roles?.[0] || 'Rol',
                });
            } else {
                setIsAuthenticated(false);
                setToken(null);
                setUser(null);
            }
        }
    }, [initialized, keycloak]);

    // Función para realizar solicitudes a una API
    const fetchWithAuth = async (url, options = {}) => {
        try {
            // Intenta renovar el token si está a punto de expirar
            const refreshed = await keycloak.updateToken(30); // Renueva si quedan menos de 30 segundos
            if (refreshed) {
                setToken(keycloak.token); // Actualiza el token en el estado
            }
        } catch (error) {
            console.error("Error al renovar el token:", error);
            navigate('/'); // Redirige al usuario al login si no se puede renovar
            throw new Error("Usuario no autenticado");
        }

        // Realiza la solicitud con el token renovado
        const response = await fetch(url, {
            ...options,
            headers: {
                ...options.headers,
                "Authorization": `Bearer ${keycloak.token}`,
            },
        });

        if (response.status === 401) {
            console.error("Usuario no autenticado");
            navigate('/');
            throw new Error("Usuario no autenticado");
        }

        return response;
    };


    // Retorna el contexto con el estado y las funciones necesarias para los componentes hijos
    return (
        <AuthContext.Provider value={{ isAuthenticated, token, user, fetchWithAuth }}>
            {initialized ? (children) : (
                <div className="flex justify-center items-center h-screen">
                    <div className="text-center">
                        <img className="h-32 w-auto mx-auto mb-4" src={LogoFaceops} alt="Logo" />
                        <LuLoaderCircle className="animate-spin mx-auto text-sky-600 text-4xl mb-4" />
                        <p className="text-sky-600 text-md font-medium">Cargando...</p>
                    </div>
                </div>
            )}
        </AuthContext.Provider>
    );
};

// Hook que permite a cualquier componente acceder al estado de autenticación
export const useAuth = () => {
    const context = useContext(AuthContext);
    if (!context) {
        throw new Error('useAuth debe ser utilizado dentro de un AuthProvider');
    }
    return context;
};
