import React, { useMemo } from 'react';
import { ReactKeycloakProvider } from '@react-keycloak/web';
import keycloak from '../config/keycloak';

const KeycloakProvider = ({ children }) => {
    const keycloakMemo = useMemo(() => keycloak, []);

    return (
        <ReactKeycloakProvider
            authClient={keycloakMemo}
            initOptions={{
                onLoad: 'login-required',
                checkLoginIframe: false,
            }}
        >
            {children}
        </ReactKeycloakProvider>
    );
};

export default KeycloakProvider;
