import React, { useState } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline'
import Footer from '../../componentes/Base/Footer';
import Sidebar from '../../componentes/Base/Sidebar';
import SearchForm from '../../componentes/Base/SearchForm';
import Return from '../../componentes/Base/Return';
import ReportarIncidente from '../../componentes/Tickets/ReportarIncidente'
import { Link } from 'react-router-dom';
import MisTickets from '../../componentes/Tickets/MisTickets';
import EstadoServidor from '../../componentes/Tickets/EstadoServidor';
import StackTickets from '../../componentes/Tickets/StackTickets';

export default function Tickets() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [reloadCount, setReloadCount] = useState(0);

    // Función para incrementar el contador de recarga
    const handleReportSuccess = () => {
        setReloadCount(prev => prev + 1);
    };

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8"> {/* Mantener h-16 aquí */}
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                        <Link to="/home" className="flex items-center relative w-full h-full py-0">
                            <div className="flex items-center h-full">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">TICKETS</h1>
                            </div>
                        </Link>
                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </div>

                    <EstadoServidor />
                    <StackTickets />
                    <main className="flex flex-wrap justify-around">
                        <div className="w-full md:w-1/2 lg:w-1/2 p-4">
                            <MisTickets reloadTrigger={reloadCount} />
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/2 p-4">
                            <ReportarIncidente onReportSuccess={handleReportSuccess} />
                        </div>
                    </main>
                </div>
                <Footer />
            </div>
        </>
    );
}