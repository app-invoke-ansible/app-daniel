import React, { useState } from 'react';
import Footer from '../componentes/Base/Footer';
import SearchForm from '../componentes/Base/SearchForm';
import Sidebar from '../componentes/Base/Sidebar';
import { Bars3Icon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

export default function Notificaciones() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <>
      <div className="flex flex-col min-h-screen">
        <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <div className="lg:pl-32 flex-1">
          <header className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
            <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </button>
            <div className="flex items-center h-full space-x-4">
              <Link to="/dashboard" className="flex items-center py-0">
                <h1 className="text-2xl font-bold tracking-tight text-sky-950">NOTIFICACIONES</h1>
              </Link>
            </div>
            <div className="flex items-center ml-auto">
              <SearchForm />
            </div>
          </header>

          <main>
            <div className="flex flex-col md:flex-row p-4">
              <aside className="w-full md:w-1/4 p-4 border-r border-gray-300">
                <h2 className="text-lg font-semibold mb-4">Categorías</h2>
                <ul className="space-y-2">
                  <li>
                    <label className="flex items-center">
                      <input type="checkbox" className="mr-2" />
                      Noticias
                    </label>
                  </li>
                  <li>
                    <label className="flex items-center">
                      <input type="checkbox" className="mr-2" />
                      Nuevos servicios
                    </label>
                  </li>
                  <li>
                    <label className="flex items-center">
                      <input type="checkbox" className="mr-2" />
                      Compras
                    </label>
                  </li>
                  <li>
                    <label className="flex items-center">
                      <input type="checkbox" className="mr-2" />
                      Usuarios nuevos
                    </label>
                  </li>
                  <li>
                    <label className="flex items-center">
                      <input type="checkbox" className="mr-2" />
                      Cotizaciones
                    </label>
                  </li>
                </ul>
              </aside>
              <main className="w-full md:w-3/4 p-4">
                <div className="flex justify-between items-center mb-4">
                  <h1 className="text-2xl font-semibold">Resultados: Noticias nuevos avances</h1>
                  <div className="relative">
                    <input type="text" placeholder="Buscar" className="border-b border-gray-400 focus:outline-none" />
                    <i className="fas fa-search absolute right-0 top-0 mt-1 mr-2 text-gray-400"></i>
                  </div>
                </div>
                <div className="space-y-6">
                  <div>
                    <div className="flex items-center mb-2">
                      <i className="fas fa-file-alt text-blue-500 mr-2"></i>
                      <span>Nuevas tecnologías implementadas en automatización</span>
                    </div>
                    <div className="ml-6">
                      <p>Links: <a href="#" className="text-blue-600">Democrats say Treasury Department guidance led banks to prioritize existing customers for federal paycheck-protection loans</a></p>
                      <span className="inline-block bg-gray-200 text-gray-600 text-xs px-2 py-1 mt-2">NEWS</span>
                    </div>
                  </div>
                  <div>
                    <div className="flex items-center mb-2">
                      <i className="fas fa-file-alt text-blue-500 mr-2"></i>
                      <span>Azure realiza lanzamiento de nuevos implementos para su servidor</span>
                    </div>
                    <div className="ml-6">
                      <p>Links: <a href="#" className="text-blue-600">CBL Properties declares bankruptcy as shopping center manager looks to restructure debt</a></p>
                      <span className="inline-block bg-gray-200 text-gray-600 text-xs px-2 py-1 mt-2">NEWS</span>
                    </div>
                  </div>
                  <div>
                    <div className="flex items-center mb-2">
                      <i className="fas fa-file-alt text-blue-500 mr-2"></i>
                      <span>Nuevos insumos tecnológicos son utilizados en diversas partes de Latinoamérica</span>
                    </div>
                    <div className="ml-6">
                      <p>Source: <a href="#" className="text-blue-600">CBL Properties declares bankruptcy as shopping center manager looks to restructure debt</a></p>
                      <span className="inline-block bg-gray-200 text-gray-600 text-xs px-2 py-1 mt-2">NEWS</span>
                    </div>
                  </div>
                  <div>
                    <div className="flex items-center mb-2">
                      <i className="fas fa-user-plus text-blue-500 mr-2"></i>
                      <span>Usuario 2 (administrador) a creado un nuevo usuario.</span>
                    </div>
                    <div className="ml-6">
                      <p>Redirigir a <a href="#" className="text-blue-600">Usuarios</a></p>
                      <span className="inline-block bg-gray-200 text-gray-600 text-xs px-2 py-1 mt-2">NEWS</span>
                    </div>
                  </div>
                  <div>
                    <div className="flex items-center mb-2">
                      <i className="fas fa-file-alt text-blue-500 mr-2"></i>
                      <span>Usuario 3 (administrador) a solicitado una cotización por Azure</span>
                    </div>
                    <div className="ml-6">
                      <p>Redirigir a <a href="#" className="text-blue-600">Cotizaciones para revisar detalles.</a></p>
                      <span className="inline-block bg-gray-200 text-gray-600 text-xs px-2 py-1 mt-2">NEWS</span>
                    </div>
                  </div>
                </div>
              </main>
            </div>


          </main>

        </div>
        <Footer />
      </div>
    </>
  );
}