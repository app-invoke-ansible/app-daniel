import React, { useState } from 'react';
import Datos from '../../componentes/Configuracion/Datos';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Header from '../../componentes/Configuracion/Header';
import Footer from '../../componentes/Base/Footer';

export default function Ajustes() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [loading, setLoading] = useState(true);

    return (
        <>
            <div className="min-h-screen flex">
                <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className={`flex-1 sm:pl-0 lg:pl-32`}>
                    <Header
                        selectedCard={{ title: 'Mi Perfil' }}
                        sidebarOpen={sidebarOpen}
                        setSidebarOpen={setSidebarOpen}
                    />
                    <div>
                        <Datos loading={loading} setLoading={setLoading} />
                    </div>
                </div>
            </div >
            <Footer />
        </>
    );
}
