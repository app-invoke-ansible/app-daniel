import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Footer from '../../componentes/Base/Footer';
import Header from '../../componentes/Configuracion/Header';
import Loading from '../../componentes/Base/Loading';
import TablaReportes from '../../componentes/Configuracion/TablaReportes';

export default function ReportesModulo() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const { keycloak, initialized } = useKeycloak();

    if (!initialized) {
        return <Loading message="Cargando..." />;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    return (<>
        <div className="min-h-screen flex">
            <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <div className={`flex-1 sm:pl-0 lg:pl-32`}>
                <Header
                    selectedCard={{ title: 'Reportes de Módulos' }}
                    sidebarOpen={sidebarOpen}
                    setSidebarOpen={setSidebarOpen}
                />
                <div className='flex-1'>
                    <div className="rounded-lg overflow-x-auto">
                        <TablaReportes />
                    </div>
                </div>
            </div>
        </div>
        <Footer />
    </>
    );
}