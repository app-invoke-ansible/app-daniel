import React, { useState, useEffect } from 'react';
import UserCard from '../../componentes/Configuracion/UserCard';
import { useAuth } from '../../providers/AuthProvider';
import CrearUsuario from '../../componentes/Configuracion/GestionUsuarios/CrearUsuario';
import BuscarUsuario from '../../componentes/Configuracion/GestionUsuarios/BuscarUsuario';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Header from '../../componentes/Configuracion/Header';
import Footer from '../../componentes/Base/Footer';
import ConfiguracionEmpresa from '../../componentes/Configuracion/GestionUsuarios/ConfiguracionEmpresa';
import Loading from '../../componentes/Base/Loading';

const GestionUsuarios = () => {
  const [usuarios, setUsuarios] = useState([]);
  const [usuariosFiltrados, setUsuariosFiltrados] = useState([]);
  const [loading, setLoading] = useState(false);
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [tipoUsuario, setTipoUsuario] = useState('');
  const [inventarioId, setInventarioId] = useState(null);
  const [empresaId, setEmpresaId] = useState(null);
  const { fetchWithAuth, isAuthenticated, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;

  const obtenerUsuarios = async () => {
    if (!isAuthenticated) return;

    setLoading(true);
    try {
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuariosData = await responseUsuarios.json();
      setUsuarios(usuariosData);
      setUsuariosFiltrados(usuariosData);

      if (user) {
        const usuarioAutenticado = usuariosData.find((usuario) => usuario.username === user?.name);
        if (usuarioAutenticado) {
          setInventarioId(usuarioAutenticado.inventarioId);
          setEmpresaId(usuarioAutenticado.idEmpresa);
          setUsuariosFiltrados(usuariosData.filter((usuario) =>
            usuario.idEmpresa === usuarioAutenticado.idEmpresa && usuario.idEmpresa !== null
          ));
        }
      }
    } catch (error) {
      console.error('Error al obtener los usuarios:', error.message);
      alert('Hubo un problema al cargar los usuarios. Por favor, inténtalo más tarde.');
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    obtenerUsuarios();
  }, [isAuthenticated, user]);

  useEffect(() => {
    let filteredUsers = usuariosFiltrados;

    if (tipoUsuario) {
      filteredUsers = filteredUsers.filter((usuario) => usuario.tipoUsuario === tipoUsuario);
    }

    if (searchTerm.trim() !== '') {
      const term = searchTerm.toLowerCase();
      filteredUsers = filteredUsers.filter((usuario) => usuario.username.toLowerCase().includes(term));
    }
    setUsuariosFiltrados(filteredUsers);
  }, [searchTerm, usuariosFiltrados, tipoUsuario]);

  const handleUserDeleted = (userId) => {
    setUsuarios((prevUsuarios) => prevUsuarios.filter((usuario) => usuario.id !== userId));
    setUsuariosFiltrados((prevUsuariosFiltrados) => prevUsuariosFiltrados.filter((usuario) => usuario.id !== userId));
  };

  const handleUserCreated = async (newUser) => {
    setLoading(true);
    setUsuarios((prevUsuarios) => [...prevUsuarios, newUser]);
    setUsuariosFiltrados((prevUsuariosFiltrados) => [...prevUsuariosFiltrados, newUser]);
    await obtenerUsuarios();
    setLoading(false);
  };

  const handleUserUpdate = (updatedUser) => {
    setUsuarios((prevUsuarios) =>
      prevUsuarios.map((usuario) => (usuario.id === updatedUser.id ? updatedUser : usuario))
    );
    setUsuariosFiltrados((prevUsuariosFiltrados) =>
      prevUsuariosFiltrados.map((usuario) => (usuario.id === updatedUser.id ? updatedUser : usuario))
    );
  };

  return (
    <>
      <div className="min-h-screen">
        <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <div className="flex-1 sm:pl-0 lg:pl-32">
          <Header selectedCard={{ title: 'Gestión de Usuarios' }} sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

          {loading ? (
            <Loading message="Cargando gestión de usuarios..." />
          ) : (
          <div className="flex justify-center items-start h-auto">
            <div className="grid grid-cols-1 lg:grid-cols-3 gap-4 w-full max-w-7xl">
              {/* PRIMERA COLUMNA */}
              <div className="lg:col-span-1  border-r border-gray-300 py-6">
                <ConfiguracionEmpresa />
              </div>

              {/* SEGUNDA COLUMNA  */}
              <div className="lg:col-span-2">
                <h1 className="text-xl font-bold text-center text-sky-950 pb-2 py-6">Usuarios</h1>
                <div>
                  <div className="sm:hidden flex justify-end mb-4">
                    <BuscarUsuario className="w-full max-w-xs" searchTerm={searchTerm} onSearchChange={setSearchTerm} />
                  </div>

                  <div className="flex justify-between items-center mb-6 sm:space-y-0">
                    <CrearUsuario onUserCreate={handleUserCreated} inventarioId={inventarioId} empresaId={empresaId} />

                    <div className="flex space-x-4 items-center">
                      <select
                        className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-xs transition-colors duration-300 transform hover:scale-105"
                        onChange={(e) => setTipoUsuario(e.target.value)}
                      >
                        <option value="">Tipo de Usuario</option>
                        <option value="admin">Administrador</option>
                        <option value="editor">Editor</option>
                        <option value="visualizador">Visualizador</option>
                      </select>

                      <div className="sm:flex relative hidden">
                        <BuscarUsuario className="w-full md:max-w-xs" searchTerm={searchTerm} onSearchChange={setSearchTerm} />
                      </div>
                    </div>
                  </div>


                  <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-8">
                    {usuariosFiltrados.map((usuario) => (
                      <UserCard
                        key={usuario.id}
                        usuario={usuario}
                        onUserDelete={handleUserDeleted}
                        onUserUpdate={handleUserUpdate}
                        onUpdate={obtenerUsuarios}
                      />
                    ))}
                  </div>

                </div>
              </div>
            </div>
          </div>
          )}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default GestionUsuarios;
