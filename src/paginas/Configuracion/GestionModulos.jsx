import React, { useState } from 'react';
import { useKeycloak } from '@react-keycloak/web';
import GestionarModulo from '../../componentes/Configuracion/GestionarModulo';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Header from '../../componentes/Configuracion/Header';
import Footer from '../../componentes/Base/Footer';
import Loading from '../../componentes/Base/Loading';

export default function GestionModulos() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const { keycloak, initialized } = useKeycloak();
    const [isRefreshing, setIsRefreshing] = useState(false); // Estado para deshabilitar boton

    if (!initialized) {
        return <Loading message="Cargando..." />;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    return (
        <>
            <div className="min-h-screen flex">
                <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className={`flex-1 sm:pl-0 lg:pl-32`}>
                    <Header
                        selectedCard={{ title: 'Gestión de Módulos' }}
                        sidebarOpen={sidebarOpen}
                        setSidebarOpen={setSidebarOpen}
                    />
                    <div className='flex-1 mx-4'>
                        <div className="p-2">
                            <GestionarModulo />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}