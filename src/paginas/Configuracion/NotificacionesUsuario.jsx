import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Header from '../../componentes/Configuracion/Header';
import Footer from '../../componentes/Base/Footer';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Notificaciones from '../../componentes/Configuracion/Notificaciones';
import SistemasTickets from '../../componentes/Configuracion/SistemasTickets';

export default function NotificacionesUsuario() {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    return (
        <>
            <div className="min-h-screen flex">
                <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className={`flex-1 sm:pl-0 lg:pl-32`}>
                    <Header
                        selectedCard={{ title: 'Gestión de Notificaciones y Sistemas' }}
                        sidebarOpen={sidebarOpen}
                        setSidebarOpen={setSidebarOpen}
                    />
                    <div className='flex-1 flex'>
                        <div className="w-1/2 p-4">
                            <Notificaciones />
                        </div>
                        <div className="w-1/2 p-4">
                            <SistemasTickets />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}