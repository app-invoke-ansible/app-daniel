import React, { useState, useEffect } from 'react';
import Footer from '../../componentes/Base/Footer';
import { useLocation, useNavigate } from 'react-router-dom';
import { useAuth } from '../../providers/AuthProvider';
import Header from '../../componentes/Configuracion/Header';
import { LuLayoutGrid, LuUsers, LuUserCog, LuBell } from 'react-icons/lu';
import { TbReportSearch } from "react-icons/tb";
import { BanknotesIcon } from '@heroicons/react/24/outline';

import Ajustes from './Ajustes';
import NotificacionesUsuario from './NotificacionesUsuario';
import GestionUsuarios from './GestionUsuarios';
import GestionModulos from './GestionModulos';
import ReportesModulos from './ReportesModulos';
import Facturacion from './Facturacion';

const componentsMap = {
    1: Ajustes,
    2: NotificacionesUsuario,
    3: GestionUsuarios,
    4: GestionModulos,
    5: ReportesModulos,
    6: Facturacion,
};

const componentPathMap = {
    1: "Ajustes",
    2: "NotificacionesUsuario",
    3: "GestionUsuarios",
    4: "GestionModulos",
    5: "ReportesModulos",
    6: "Facturacion",
};

export default function ConfiguracionMenu() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [selectedCard, setSelectedCard] = useState(null);
    const [loading, setLoading] = useState(false);
    const { isAuthenticated, user, fetchWithAuth } = useAuth();
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [activeComponentId, setActiveComponentId] = useState(null);
    const API_URL = process.env.REACT_APP_API_URL;
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchUserData = async () => {
            if (isAuthenticated && user) {
                try {
                    const response = await fetchWithAuth(`${API_URL}/usuario`);
                    const usuarios = await response.json();
                    const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                    if (usuarioAutenticado) {
                        setTipoUsuario(usuarioAutenticado.tipoUsuario);
                    } else {
                        console.warn('Usuario autenticado no encontrado en la API');
                    }
                } catch (error) {
                    console.error('Error al obtener datos de la API:', error);
                }
            }
        };

        fetchUserData();
    }, [isAuthenticated, user, fetchWithAuth, API_URL]);

    useEffect(() => {
        const pathSegments = location.pathname.split('/').filter(segment => segment);
        const componentNameFromUrl = pathSegments[1] || null;

        if (componentNameFromUrl) {
            const componentId = Object.keys(componentPathMap).find(key => componentPathMap[key] === componentNameFromUrl);
            if (componentId) {
                setActiveComponentId(parseInt(componentId, 10));
                setSelectedCard({ id: parseInt(componentId, 10) });
                setSidebarOpen(true);
            } else {
                setActiveComponentId(null);
                setSelectedCard(null);
            }
        }
    }, [location.pathname]);

    const handleCardClick = (card) => {
        setSelectedCard(card);
        setActiveComponentId(card.id);

        const path = componentPathMap[card.id];
        if (path) {
            navigate(`/${path}`);
        }
    };

    const cards = [
        { id: 1, icon: <LuUserCog className="h-6 w-6 mx-auto" />, title: 'Mi Perfil', description: 'Visualiza y edita tu información personal.' },
        { id: 2, icon: <LuBell className="h-6 w-6 mx-auto" />, title: 'Gestión de Notificaciones y Sistemas', description: 'Controla la configuración de tus Notificaciones y Sistemas' },
        { id: 3, icon: <LuUsers className="h-6 w-6 mx-auto" />, title: 'Gestión de Usuarios', description: 'Gestión de usuarios y sus roles en el sistema.' },
        { id: 4, icon: <LuLayoutGrid className="h-6 w-6 mx-auto" />, title: 'Gestión de Módulos', description: 'Administra los modulos adquiridos.' },
        { id: 5, icon: <TbReportSearch className="h-6 w-6 mx-auto" />, title: 'Reportes de Módulos', description: 'Revisa reportes detallados de los módulos.' },
        { id: 6, icon: <BanknotesIcon className="h-6 w-6 mx-auto" />, title: 'Facturación', description: 'Consulta y gestiona cotizaciones realizadas.' }
    ];

    const adminOnlyCards = [3, 4, 5, 6];
    const filteredCards = cards.filter((card) => {
        return adminOnlyCards.includes(card.id) ? tipoUsuario === 'admin' : true;
    });

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <div className="flex-grow flex flex-col">
                    <Header
                        sidebarOpen={sidebarOpen}
                        setSidebarOpen={setSidebarOpen}
                    />
                    <main className="flex-grow flex flex-col p-6">
                        <div className="flex-grow container mx-auto mt-6 mb-4">

                            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-4 sm:gap-6">
                                {filteredCards.filter(card => !adminOnlyCards.includes(card.id)).map((card) => (
                                    <div
                                        key={card.id}
                                        className="group block rounded-lg bg-white shadow-md hover:shadow-lg transition p-2 sm:p-4 border border-sky-950 hover:bg-sky-950 cursor-pointer"
                                        onClick={() => handleCardClick(card)}
                                    >
                                        <div className="flex justify-center text-sky-700 group-hover:text-white">
                                            {React.cloneElement(card.icon, { className: "h-8 w-8 sm:h-12 sm:w-12" })}
                                        </div>
                                        <h3 className="mt-2 sm:mt-4 text-center text-sm sm:text-base font-semibold text-gray-800 group-hover:text-white line-clamp-1">
                                            {card.title}
                                        </h3>
                                        <p className="mt-1 sm:mt-2 text-xs text-center text-gray-400 line-clamp-2 sm:block">{card.description}</p>
                                    </div>
                                ))}
                            </div>

                            {tipoUsuario === 'admin' && (
                                <aside className="bg-white shadow-md border-t border-gray-200 py-4 transition-all my-4">
                                    <div className="mx-auto flex items-center justify-between px-2 sm:px-6">
                                        <div className="flex-grow text-center">
                                            <h3 className="font-bold tracking-tight text-sky-950 text-xl lg:text-2xl">
                                                ADMINISTRACIÓN
                                            </h3>
                                        </div>
                                    </div>
                                </aside>
                            )}

                            {tipoUsuario === 'admin' && (
                                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 sm:gap-6">
                                    {filteredCards.filter(card => adminOnlyCards.includes(card.id)).map((card) => (
                                        <div
                                            key={card.id}
                                            className="group block rounded-lg bg-white shadow-md hover:shadow-lg transition p-2 sm:p-4 border border-sky-950 hover:bg-sky-950 cursor-pointer"
                                            onClick={() => handleCardClick(card)}
                                        >
                                            <div className="flex justify-center text-sky-700 group-hover:text-white">
                                                {React.cloneElement(card.icon, { className: "h-8 w-8 sm:h-12 sm:w-12" })}
                                            </div>
                                            <h3 className="mt-2 sm:mt-4 text-center text-sm sm:text-base font-semibold text-gray-800 group-hover:text-white line-clamp-1">
                                                {card.title}
                                            </h3>
                                            <p className="mt-1 sm:mt-2 text-xs text-center text-gray-400 line-clamp-2 sm:block">
                                                {card.description}
                                            </p>
                                        </div>
                                    ))}
                                </div>
                            )}

                        </div>
                    </main>
                </div>
                <Footer className="mt-auto" />
            </div>
        </>
    );
}
