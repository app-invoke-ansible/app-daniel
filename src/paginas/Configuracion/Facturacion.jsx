import React, { useState } from 'react';
import { useKeycloak } from '@react-keycloak/web';
import ConfiguracionSidebar from './ConfiguracionSidebar';
import Header from '../../componentes/Configuracion/Header';
import Footer from '../../componentes/Base/Footer';
import Loading from '../../componentes/Base/Loading';
import CardFacturacion from '../../componentes/Configuracion/Facturacion/CardFacturacion';

export default function Facturacion() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const { keycloak, initialized } = useKeycloak();
    const [isRefreshing, setIsRefreshing] = useState(false); // Estado para deshabilitar boton

    if (!initialized) {
        return <Loading message="Cargando..." />;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    return (
        <>
            <div className="min-h-screen flex">
                <ConfiguracionSidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className={`flex-1 sm:pl-0 lg:pl-32`}>
                    <Header
                        selectedCard={{ title: 'Facturación' }}
                        sidebarOpen={sidebarOpen}
                        setSidebarOpen={setSidebarOpen}
                    />
                    <div className='sm:flex-1 sm:mx-4'>
                        <div className="sm:p-2">
                            <CardFacturacion />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}