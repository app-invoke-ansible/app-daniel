import React, { Fragment, useState, useEffect, useCallback } from 'react';
import { LuLayoutGrid, LuUsers, LuFile, LuUserCog, LuBell } from 'react-icons/lu';
import { TbReportSearch } from "react-icons/tb";
import { MdOutlineLogin } from "react-icons/md";
import { useAuth } from '../../providers/AuthProvider';
import UserProfilePic from '../../img/perfil/fotoPerfil.jpg';
import ImgInvoke from '../../img/LogoFaceopsVertical.png';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Dialog, Transition } from '@headlessui/react';
import { BanknotesIcon } from '@heroicons/react/24/outline';

const navigation = [
    { id: 1, name: 'Mi Perfil', icon: <LuUserCog />, path: '/Ajustes' },
    { id: 2, name: 'Notificaciones y Sistemas', icon: <LuBell />, path: '/NotificacionesUsuario' },
    { id: 3, name: 'Gestión de Usuarios', icon: <LuUsers />, path: '/GestionUsuarios' },
    { id: 4, name: 'Gestión de Módulos', icon: <LuLayoutGrid />, path: '/GestionModulos' },
    { id: 5, name: 'Reportes de Módulos', icon: <TbReportSearch />, path: '/ReportesModulos' },
    { id: 6, name: 'Facturación', icon: <BanknotesIcon />, path: '/Facturacion' },
];

const classNames = (...classes) => {
    return classes.filter(Boolean).join(' ');
};

const IconoPersonalizado = ({ icon }) => <>{React.cloneElement(icon, { className: "h-6 w-6 shrink-0" })}</>;

export default function ConfiguracionSidebar({ sidebarOpen, setSidebarOpen }) {
    const navigate = useNavigate();
    const { isAuthenticated, user, fetchWithAuth } = useAuth();
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const location = useLocation();
    const [filteredNav, setFilteredNav] = useState([]);
    const [isMobile, setIsMobile] = useState(window.innerWidth < 1024);

    const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
    const REALM_API = process.env.REACT_APP_REALM_API;
    const API_URL = process.env.REACT_APP_API_URL;
    const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;

    useEffect(() => {
        const handleResize = () => {
            setIsMobile(window.innerWidth < 1024);
        };
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    const fetchUserData = useCallback(async () => {
        if (isAuthenticated && user) {
            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await responseUsuarios.json();
            const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

            if (usuarioAutenticado) {
                const userType = usuarioAutenticado.tipoUsuario;
                setTipoUsuario(userType); // Actualizar el estado del tipo de usuario
                filterNavigation(userType); // Filtrar la navegación según el tipo de usuario
            }
        }
    }, [isAuthenticated, user, fetchWithAuth]);

    useEffect(() => {
        fetchUserData();
    }, [fetchUserData]);

    // Función para filtrar la navegación según el tipo de usuario
    const filterNavigation = (userType) => {
        if (userType === 'admin') {
            setFilteredNav(navigation); // Mostrar todas las opciones
        } else {
            setFilteredNav(navigation.filter(item => item.id <= 2)); // Mostrar solo las opciones 1 y 2
        }
    };

    const handleLogout = () => {
        setTipoUsuario(null); // Limpiar el estado del tipo de usuario al cerrar sesión
        const redirectUri = encodeURIComponent(window.location.origin);
        const logoutUrl = `${KEYCLOAK_URL}/realms/${REALM_API}/protocol/openid-connect/logout?post_logout_redirect_uri=${redirectUri}&client_id=${CLIENT_ID}`;
        window.location.href = logoutUrl;
    };

    const handleProfileClick = () => {
        navigate('/ConfiguracionMenu');
    };

    const handleMenuItemClick = (item) => {
        if (item.name === 'Cerrar sesión') {
            handleLogout();
        } else {
            navigate(item.path);
            setSidebarOpen(false);
        }
    };

    return (<>
        {/* Sidebar para dispositivos móviles */}
        <Transition show={sidebarOpen} as={Fragment}>
            <Dialog onClose={() => setSidebarOpen(false)} className="relative z-50 lg:hidden">
                <div className="fixed inset-0 flex">
                    <Transition.Child
                        as={Fragment}
                        enter="transition ease-in-out duration-300 transform"
                        enterFrom="-translate-x-full"
                        enterTo="translate-x-0"
                        leave="transition ease-in-out duration-300 transform"
                        leaveFrom="translate-x-0"
                        leaveTo="-translate-x-full"
                    >
                        <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                            <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-gray-900 px-6 pb-2">
                                <div className="mt-auto flex flex-col items-center">
                                    <img className="h-5 w-auto my-3" src={ImgInvoke} alt="Logo" />
                                    <img className="h-10 w-10 rounded-full cursor-pointer" src={UserProfilePic} alt="User Profile" onClick={handleProfileClick} />
                                    <h3 className="text-xs font-semibold text-white mt-2 cursor-pointer" onClick={handleProfileClick}>{user?.name || 'Usuario Desconocido'}</h3>
                                    <p className="text-xs text-gray-400">{tipoUsuario}</p>
                                </div>
                                <nav className="flex flex-1 flex-col">
                                    <ul role="list" className="-mx-2 flex-1 space-y-1">
                                        {filteredNav.map((item) => (
                                            <li key={item.name}>
                                                <Link to={item.path}
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        handleMenuItemClick(item);
                                                    }}
                                                    className={classNames(
                                                        location.pathname === item.path ? 'bg-gray-800 text-white' : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                                        'group flex items-center gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold'
                                                    )}
                                                >
                                                    <IconoPersonalizado icon={item.icon} />
                                                    <span className="flex-1 text-left">{item.name}</span>
                                                </Link>
                                            </li>
                                        ))}
                                    </ul>
                                    <div className="mt-auto flex flex-col items-center pb-4">
                                        <button
                                            onClick={() => handleMenuItemClick({ name: 'Cerrar sesión' })}
                                            className="flex items-center text-sm text-gray-400 hover:text-white hover:bg-gray-800 rounded-md p-2"
                                        >
                                            <MdOutlineLogin className="h-6 w-6 mr-2" aria-hidden="true" />
                                            <span className='text-gray-400 font-semibold'>Cerrar Sesión</span>
                                        </button>
                                    </div>
                                </nav>
                            </div>
                        </Dialog.Panel>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition>
        {/* Sidebar para escritorio*/}
        <div className={`hidden lg:flex lg:fixed lg:inset-y-0 lg:z-50 lg:w-32 lg:flex-col bg-gray-900`}>
            <div className="flex grow flex-col gap-y-3 overflow-y-auto px-2">
                <div className="mt-auto flex flex-col items-center">
                    <img className="h-5 w-auto my-3" src={ImgInvoke} alt="Logo" />
                    <img className="h-10 w-10 rounded-full cursor-pointer" src={UserProfilePic} alt="User Profile" onClick={handleProfileClick} />
                    <h3 className="text-xs font-semibold text-white mt-2 cursor-pointer" onClick={handleProfileClick}>{user?.name || 'Usuario Desconocido'}</h3>
                    <p className="text-xs text-gray-400">{tipoUsuario}</p>
                </div>
                <nav className="flex flex-1 flex-col">
                    <ul className="flex flex-col items-center space-y-1">
                        {filteredNav.map((item) => (
                            <li key={item.name} className="flex-grow">
                                <Link
                                    to={item.path}
                                    className={classNames(
                                        location.pathname === item.path ? 'bg-gray-800 text-white' : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                        'group flex flex-col items-center justify-center gap-y-1 rounded-md p-2 text-xs leading-5 font-semibold h-full'
                                    )}
                                >
                                    <IconoPersonalizado icon={item.icon} />
                                    <span className="text-xs text-center">{item.name}</span>
                                </Link>
                            </li>
                        ))}
                    </ul>
                </nav>
                <div className="mt-auto flex flex-col items-center pb-4">
                    <button
                        onClick={handleLogout}
                        className="flex items-center text-sm text-gray-400 hover:text-white hover:bg-gray-800 rounded-md p-2"
                    >
                        <MdOutlineLogin className="h-5 w-5" aria-hidden="true" />
                        <span className='text-gray-400 font-semibold'>Cerrar Sesión</span>
                    </button>
                </div>
            </div>
        </div>
    </>
    );
}