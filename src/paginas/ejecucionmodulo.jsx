import React from 'react';
import { useLocation } from 'react-router-dom';
import { useEffect, useState } from 'react';
// import { useKeycloak } from '@react-keycloak/web'; // Import Keycloak

const TabHistorialUltimasEjec = () => {
    const [ejecuciones, setEjecuciones] = useState([]);
    const [moduloSeleccionado, setModuloSeleccionado] = useState('');
    const location = useLocation();
    // const { keycloak, initialized } = useKeycloak(); // Keycloak hook

    useEffect(() => {
        // if (initialized && keycloak.authenticated) { // Check Keycloak authentication
        //     const token = keycloak.token; // Get Keycloak token
            const urlParams = new URLSearchParams(location.search);
            const moduloId = urlParams.get('moduloId'); // Suponiendo que el parámetro en la URL es 'moduloId'

            if (moduloId) {
                setModuloSeleccionado(moduloId);

                fetch(`https://api-v-1-2-git-tdemt.apps.bddfgx0h.eastus.aroapp.io/ejecuciones/modulo/${moduloId}`, { // Cambio en la URL para que coincida con la ruta de la API
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        // 'Authorization': `Bearer ${token}`, // Set authorization header with Keycloak token
                    },
                })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json();
                    })
                    .then(data => {
                        setEjecuciones(data);
                    })
                    .catch(error => {
                        console.error('Error al hacer la solicitud:', error);
                    });
            }
        // }
    }, [location.search]); // Removed Keycloak dependencies
    
    return (
        <div className="">
            <div>
                {/* Aquí puedes agregar algún control visual para mostrar el módulo seleccionado */}
                Módulo Seleccionado: {moduloSeleccionado}
            </div>

            <div className="">
                <div className="">
                    <div className="-mx-4 mt-2 sm:-mx-0">
                        <table className="min-w-full divide-y divide-gray-300">
                            <thead>
                                <tr>
                                    {/* Encabezados de la tabla */}
                                    <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">
                                        Aprobación
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell"
                                    >
                                        Fecha
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Ámbito
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Inicio
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Estado
                                    </th>
                                    <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                        Término
                                    </th>
                                    <th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-0">
                                        <span className="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="divide-y divide-gray-200 bg-white">
                                {/* Renderizado de las filas de la tabla */}
                                {ejecuciones.map(ejecucion => (
                                    <tr key={ejecucion.id}>
                                        <td className="w-full max-w-0 py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:w-auto sm:max-w-none sm:pl-0">
                                            {ejecucion.nombreModulo}
                                            <dl className="font-normal lg:hidden">
                                                <dt className="sr-only">Fecha y hora</dt>
                                                <dd className="mt-1 truncate text-gray-700">{ejecucion.fechaEjecucion}</dd>
                                                <dt className="sr-only sm:hidden">{ejecucion.iDetalles.dominioMaquina}</dt>
                                                <dd className="mt-1 truncate text-gray-500 sm:hidden">{ejecucion.fechaEjecucion}</dd>
                                                <dd className="mt-1 truncate text-gray-700">{ejecucion.estadoEjecucion}</dd>
                                                <dd className="mt-1 truncate text-gray-500 sm:hidden">{ejecucion.fechaEjecucion}</dd>
                                            </dl>
                                        </td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 lg:table-cell">{ejecucion.fechaEjecucion}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 lg:table-cell">{ejecucion.iDetalles.dominioMaquina}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">{ejecucion.fechaEjecucion}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">{ejecucion.estadoEjecucion}</td>
                                        <td className="px-3 py-4 text-sm text-gray-500">{ejecucion.fechaEjecucion}</td>
                                        <td className="py-4 pl-3 pr-4 text-center text-sm font-medium sm:pr-0">
                                            {/* Aquí puedes agregar botones de acción si es necesario */}
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TabHistorialUltimasEjec;