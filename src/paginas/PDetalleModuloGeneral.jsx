import React from 'react';
import { Fragment, useState } from 'react'
import { Dialog, Menu, Transition } from '@headlessui/react';
import { Link } from 'react-router-dom';
import { Description, Field, Label, Switch } from '@headlessui/react'
import {
    Bars3Icon,
    BellIcon,
    ChartPieIcon,
    CloudIcon,
    ComputerDesktopIcon,
    HomeIcon,
    ShoppingCartIcon,
    XMarkIcon,
    ChartBarIcon
} from '@heroicons/react/24/outline'
import { ChevronDownIcon, MagnifyingGlassIcon } from '@heroicons/react/20/solid'
import LogoInvoke from '../img/logoBlanco-invoke.png'
import LogoInvokeColor from '../img/logoColor-Invokes.png'
import ImgSeguridad from '../img/iconos/Iconos_Seguridad.png'
import ImgMonitoreo from '../img/iconos/Iconos_Monitoreo.png'
import ImgInfraestructura from '../img/iconos/Iconos_Infraestructura .png'
import ImgRedes from '../img/iconos/Iconos_Redes.png'
import ImgBD from '../img/iconos/Iconos_BaseDeDatos.png'
import ImgDeployments from '../img/iconos/Iconos_Deployments.png'
import Footer from '../componentes/Base/Footer';
// import { useKeycloak } from '@react-keycloak/web'; // Descomentar para habilitar Keycloak

const navigation = [
    { name: 'Home', href: '/home', icon: HomeIcon, current: true },
    { name: 'Ambito', href: '#', icon: CloudIcon, current: false },
    { name: 'Modulos', href: '/plistamodulos', icon: ChartBarIcon, current: false },
    { name: 'Dashboard', href: '#', icon: ChartPieIcon, current: false },
    { name: 'Marketplace', href: '#', icon: ShoppingCartIcon, current: false },
    { name: 'facturación', href: '#', icon: ComputerDesktopIcon, current: false },
]

const userNavigation = [
    { name: 'Perfil', href: '#' },
    { name: 'Cerrar sesión', href: '#' },
]

const tabs = [
    { name: 'General', href: `/modulos/{modulo.idModulo}`, current: true },
    { name: 'Historial de ejecución', href: '/pdetallemodulohisejecucion', current: false },
    { name: 'Inventario de maquinas', href: '/pdetallemoduloinventariomaquina', current: false },
    { name: 'Circuito', href: '/pdetallemoduloCircuito', current: false },
]

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function PDetalleModuloGeneral() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    // const { keycloak, initialized } = useKeycloak(); // Descomentar para habilitar Keycloak
    const [enabled, setEnabled] = useState(false)

    // if (!initialized) {
    //     return <div>Loading...</div>;
    // }

    // if (!keycloak.authenticated) {
    //     keycloak.login();
    //     return null;
    // }

    const handleLogout = () => {
        // keycloak.logout(); // Descomentar para habilitar logout
    };

    const handleMenuItemClick = (item) => {
        if (item.name === 'Cerrar sesión') {
            handleLogout();
        }
    };

    return (
        <>
            <div>
                <Transition show={sidebarOpen} as={Fragment}>
                    <Dialog onClose={setSidebarOpen}>
                        <Transition
                            as={Fragment}
                            enter="transition-opacity ease-linear duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="transition-opacity ease-linear duration-300"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <div className="fixed inset-0 bg-gray-900/80" />
                        </Transition>
                        <div className="fixed inset-0 flex">
                            <Transition
                                as={Fragment}
                                enter="transition ease-in-out duration-300 transform"
                                enterFrom="-translate-x-full"
                                enterTo="translate-x-0"
                                leave="transition ease-in-out duration-300 transform"
                                leaveFrom="translate-x-0"
                                leaveTo="-translate-x-full"
                            >
                                <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                                    <Transition
                                        as={Fragment}
                                        enter="ease-in-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in-out duration-300"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="absolute left-full top-0 flex w-16 justify-center pt-5">
                                            <button type="button" className="-m-2.5 p-2.5" onClick={() => setSidebarOpen(false)}>
                                                <span className="sr-only">Close sidebar</span>
                                                <XMarkIcon className="h-6 w-6 text-white" aria-hidden="true" />
                                            </button>
                                        </div>
                                    </Transition>
                                    {/* Menu iconos desplegables */}
                                    <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-gray-900 px-6 pb-2 ring-1 ring-white/10">
                                        <div className="flex h-16 shrink-0 items-center">
                                            <img
                                                className="h-8 w-auto"
                                                src={LogoInvoke}
                                                alt="Your aqui"
                                            />
                                        </div>
                                        <nav className="flex flex-1 flex-col">
                                            <ul className="-mx-2 flex-1 space-y-1">
                                                {navigation.map((item) => (
                                                    <li key={item.name}>
                                                        <Link to={item.href}
                                                            className={classNames(
                                                                item.current
                                                                    ? 'bg-gray-800 text-white'
                                                                    : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                                                'group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold'
                                                            )}
                                                        >
                                                            <item.icon className="h-6 w-6 shrink-0" aria-hidden="true" />
                                                            {item.name}
                                                        </Link>
                                                    </li>
                                                ))}
                                            </ul>
                                        </nav>
                                    </div>
                                </Dialog.Panel>
                            </Transition>
                        </div>
                    </Dialog>
                </Transition>
                {/* Static sidebar for desktop */}

                <div className="hidden lg:fixed lg:inset-y-0 lg:left-0 lg:z-50 lg:block lg:w-20 lg:overflow-y-auto lg:bg-gray-900 lg:px-8">
                    <div className="flex h-16 shrink-0 items-center justify-center">
                        <button type="button" className="-m-2.5 p-2.5 text-white" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                    </div>
                    {/*iconos barra lateral izquieda*/}
                    <nav className="mt-8">
                        <ul className="flex flex-col items-center space-y-1">
                            {navigation.map((item) => (
                                <li key={item.name}>
                                    <Link to={item.href}
                                        className={classNames(
                                            item.current ? 'bg-gray-800 text-white' : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                            'group flex gap-x-3 rounded-md p-3 text-sm leading-6 font-semibold'
                                        )}
                                    >
                                        <item.icon className="h-6 w-6 shrink-0" aria-hidden="true" />
                                        <span className="sr-only">{item.name}</span>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </nav>
                </div>


                {/*Version movil*/}
                <div className="lg:pl-20">
                    <div className="sticky top-0 z-40 flex h-16 shrink-0 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        {/* Separator */}
                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                        {/*logo invoke a color navbar*/}
                        <Link to="/home" className="flex h-16 shrink-0 items-center">
                            <img
                                className="h-28 w-auto"
                                src={LogoInvokeColor}
                                alt="Logo"
                            />
                        </Link>
                        <div className="flex flex-1 gap-x-4 self-stretch lg:gap-x-6">
                            {/*barra de busqueda*/}
                            <form className="relative flex flex-1" action="#" method="GET">
                                <label htmlFor="search-field" className="sr-only">
                                    Search
                                </label>
                                <MagnifyingGlassIcon
                                    className="pointer-events-none absolute inset-y-0 left-0 h-full w-5 text-gray-400"
                                    aria-hidden="true"
                                />
                                <input
                                    id="search-field"
                                    className="block h-full w-full border-0 py-0 pl-8 pr-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm"
                                    placeholder="Buscar"
                                    type="search"
                                    name="search"
                                />
                            </form>
                            <div className="flex items-center gap-x-4 lg:gap-x-6">
                                {/*Icono notificaciones*/}
                                <button type="button" className="-m-2.5 p-2.5 text-gray-400 hover:text-gray-500">
                                    <span className="sr-only">View notifications</span>
                                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                                </button>
                                {/* Separator */}
                                <div className="hidden lg:block lg:h-6 lg:w-px lg:bg-gray-900/10" aria-hidden="true" />
                                {/* perfil desplegable */}
                                <Menu as="div" className="relative">
                                    <Menu.Button className="-m-1.5 flex items-center p-1.5">
                                        <span className="sr-only">Open user menu</span>
                                        <img
                                            className="h-8 w-8 rounded-full bg-gray-50"
                                            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                            alt=""
                                        />
                                        <span className="hidden lg:flex lg:items-center">
                                            <span className="ml-4 text-sm font-semibold leading-6 text-gray-900" aria-hidden="true">
                                                Usuario
                                            </span>
                                            <ChevronDownIcon className="ml-2 h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </span>
                                    </Menu.Button>
                                    <Transition
                                        as={Fragment}
                                        enter="transition ease-out duration-100"
                                        enterFrom="transform opacity-0 scale-95"
                                        enterTo="transform opacity-100 scale-100"
                                        leave="transition ease-in duration-75"
                                        leaveFrom="transform opacity-100 scale-100"
                                        leaveTo="transform opacity-0 scale-95"
                                    >
                                        <Menu.Items className="absolute right-0 z-10 mt-2.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                                            {userNavigation.map((item) => (
                                                <Menu.Item key={item.name}>
                                                    {({ active }) => (
                                                        <Link
                                                            to={item.href}
                                                            className={classNames(
                                                                active ? 'bg-gray-50' : '',
                                                                'block px-3 py-1 text-sm leading-6 text-gray-900'
                                                            )}
                                                            onClick={() => handleMenuItemClick(item)}
                                                        >
                                                            {item.name}
                                                        </Link>
                                                    )}
                                                </Menu.Item>
                                            ))}
                                        </Menu.Items>
                                    </Transition>
                                </Menu>
                            </div>
                        </div>
                    </div>
                    <main className="">
                        <div className="">{/* inicio contenido principal */}
                            {/*inicio titulo*/}
                            <div>
                                <header className="bg-white">
                                    <div className="mx-auto px-4 py-6 sm:px-6 lg:px-8">
                                        <h1 className="text-3xl font-bold tracking-tight text-gray-900">Detalle modulo</h1>
                                        <p>Nombre del modulo</p>
                                    </div>
                                </header>
                            </div>
                            {/*fin titulo*/}

                            {/* area main */}
// ... (resto del código)

                            {/*inicio pestañas de navegacion*/}
                            <div>
                                <div className='mx-auto px-4 mb-6 sm:px-6 lg:px-8'>
                                    <div className="sm:hidden">
                                        <label htmlFor="tabs" className="sr-only">
                                            Select a tab
                                        </label>
                                        <select
                                            id="tabs"
                                            name="tabs"
                                            className="block w-full rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500"
                                            defaultValue={tabs.find((tab) => tab.current).name}
                                        >
                                            {tabs.map((tab) => (
                                                <option key={tab.name}>{tab.name}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="hidden sm:block">
                                        <nav className="flex space-x-4" aria-label="Tabs">
                                            {tabs.map((tab) => (
                                                <Link to={tab.href}
                                                    key={tab.name}
                                                    className={classNames(
                                                        tab.current ? 'bg-indigo-100 text-indigo-700' : 'text-gray-500 hover:text-gray-700',
                                                        'rounded-md px-3 py-2 text-sm font-medium'
                                                    )}
                                                    aria-current={tab.current ? 'page' : undefined}
                                                >
                                                    {tab.name}
                                                </Link>
                                            ))}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            {/*fin de pestañas de navegacion*/}

                            <div>
                                <header className="bg-white">
                                    <div className="mx-auto px-4 pb-6 sm:px-6 lg:px-8">
                                        <h1 className="text-2xl font-bold tracking-tight text-gray-900">Healthcheck granjas</h1>
                                    </div>
                                </header>
                            </div>

                            {/*inicio boton de activo/no activo*/}

                            <div className='mx-auto px-4 pb-6 sm:px-6 lg:px-8'>
                                <Field as="div" className="flex items-center">
                                    <Switch
                                        checked={enabled}
                                        onChange={setEnabled}
                                        className={classNames(
                                            enabled ? 'bg-indigo-600' : 'bg-gray-200',
                                            'relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-indigo-600 focus:ring-offset-2'
                                        )}
                                    >
                                        <span
                                            aria-hidden="true"
                                            className={classNames(
                                                enabled ? 'translate-x-5' : 'translate-x-0',
                                                'pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out'
                                            )}
                                        />
                                    </Switch>
                                    <Label as="span" className="ml-3 text-sm">
                                        <span className="font-medium text-gray-900">Activo</span>{' '}
                                    </Label>
                                </Field>
                            </div>
                            {/*fin boton de activo/no activo*/}


                            {/*descripcion del modulo*/}
                            <div>
                                <div className="bg-white shadow sm:rounded-lg">
                                    <Field as="div" className="px-4 py-5 sm:p-6">
                                        <Label as="h3" className="text-xl font-semibold leading-6 text-gray-900" passive>
                                            Descripción general
                                        </Label>
                                        <div className="mt-2 sm:flex sm:items-start sm:justify-between">
                                            <div className="max-w-xl text-sm text-gray-500">
                                                <Description className="mb-2 whitespace-pre-line">
                                                    Ejecutar playbook una vez por granja
                                                </Description>
                                                <Description className="mb-2 whitespace-pre-line">
                                                    Cada playbook de granja tiene una salida "output" que apunta a "ansible.cfg", este script es el encargado de generar una carpeta llamada "documentacion" que genera unos documentos ".adoc"
                                                </Description>
                                                <Description className="whitespace-pre-line">
                                                    Estos documentos ".adoc" se convertirán atreves de ascii-doctor (tener instalado asciidoctor).
                                                </Description>
                                            </div>
                                        </div>
                                    </Field>
                                </div>
                            </div>

                            {/*fin del modulo*/}

                            <div>
                                <div className="bg-white shadow sm:rounded-lg">
                                    <Field as="div" className="px-4 py-5 sm:p-6">
                                        <Label as="h3" className="text-xl font-semibold leading-6 text-gray-900" passive>
                                            Test de conexión
                                        </Label>
                                        <div className="mt-2 sm:flex sm:items-start sm:justify-between">
                                            <div className="max-w-xl text-sm text-gray-500">
                                                <Description className="whitespace-pre-line">
                                                    ansible-playbook -i healthcheck-test-connection-eap.yml -v
                                                </Description>

                                            </div>

                                        </div>
                                    </Field>
                                </div>
                            </div>


                            <div>
                                <div className="bg-white shadow sm:rounded-lg">
                                    <Field as="div" className="px-4 py-5 sm:p-6">
                                        <Label as="h3" className="text-xl font-semibold leading-6 text-gray-900" passive>
                                            Check maquina
                                        </Label>
                                        <div className="mt-2 sm:flex sm:items-start sm:justify-between">
                                            <div className="max-w-xl text-sm text-gray-500">
                                                <Description className="mb-2 whitespace-pre-line">
                                                    ansible-playbook -i healthcheck-gather-facts-eap-6.yml -v
                                                </Description>
                                                <Description className="mb-2 whitespace-pre-line">
                                                    ansible-playbook -i healthcheck-gather-facts-eap-7.yml -v
                                                </Description>
                                                <Description className="mb-2 whitespace-pre-line">
                                                    ansible-playbook -i healthcheck-gather-facts-eap-5.1.yml -v
                                                </Description>
                                            </div>

                                        </div>
                                    </Field>
                                </div>
                            </div>

                            <div>
                                <div className="bg-white shadow sm:rounded-lg">
                                    <Field as="div" className="px-4 py-5 sm:p-6">
                                        <Label as="h3" className="text-xl font-semibold leading-6 text-gray-900" passive>
                                            Acciones a utilizar
                                        </Label>
                                        <div className="mt-2 sm:flex sm:items-start sm:justify-between">
                                            <div className="flex space-x-2 mt-4">
                                                <img src={ImgSeguridad} alt="img1" className="h-20 w-20 rounded-full" />
                                                <img src={ImgMonitoreo} alt="img2" className="h-20 w-20 rounded-full" />
                                                <img src={ImgBD} alt="img3" className="h-20 w-20 rounded-full" />
                                                <img src={ImgDeployments} alt="img4" className="h-20 w-20 rounded-full" />
                                                <img src={ImgInfraestructura} alt="img5" className="h-20 w-20 rounded-full" />
                                                <img src={ImgRedes} alt="img6" className="h-20 w-20 rounded-full" />
                                            </div>
                                        </div>
                                    </Field>
                                </div>
                            </div>







                            {/* inicio contenido principal */}
                            <div>
                                <Footer />
                            </div>
                        </div>
                        {/*fin de contenido principal*/}
                    </main>
                </div>
            </div>
        </>
    )
}
