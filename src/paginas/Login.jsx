import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Footer from "../componentes/Base/Footer";
import ImgFondoLogin from "../img/Imagen-para-web-1-azul.jpg";
import ImgLogoInvoke from "../img/logoColor-InvokeGroup.png"
import LogoFaceops from "../img/iconos/Iconos_Otros.png"
import RegisterForm from "../componentes/RegisterForm";
import { IoCloseCircleSharp } from "react-icons/io5";
import { useAuth } from "../providers/AuthProvider";

export default function Login() {
    const [showRegisterForm, setShowRegisterForm] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const { login } = useAuth();

    // Variables de entorno
    const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
    const REALM_API = process.env.REACT_APP_REALM_API;
    const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;

    // Función para manejar el inicio de sesión
    const handleLogin = async (e) => {
        e.preventDefault();
        setLoading(true);
        setError("");

        try {
            // Solicitud al endpoint de autenticación de Keycloak
            const response = await fetch(
                `${KEYCLOAK_URL}/realms/${REALM_API}/protocol/openid-connect/token`,
                {
                    method: "POST",
                    headers: { "Content-Type": "application/x-www-form-urlencoded" },
                    body: new URLSearchParams({
                        client_id: CLIENT_ID,
                        grant_type: "password",
                        username: username,
                        password: password,
                    }),
                }
            );

            // Verificador de respuesta
            if (!response.ok) {
                if (response.status === 401) {
                    throw new Error("Credenciales inválidas.");
                } else {
                    throw new Error("Error del servidor. Intente más tarde.");
                }
            }

            const data = await response.json();

            // Llamado a la función `login` para establecer el estado global
            login(data.access_token);

            // Almacenar los tokens en localStorage
            localStorage.setItem("token", data.access_token);
            localStorage.setItem("refresh_token", data.refresh_token);

            // Redirigir al home después de iniciar sesión
            navigate("/home");

        } catch (err) {
            console.error("Error al autenticar:", err);
            setError(err.message);
        } finally {
            setLoading(false);
        }
    };

    return (
        <>
            <div className="relative min-h-screen flex items-center justify-center">

                <div className="absolute inset-0">
                    <img
                        className="h-full w-full object-cover opacity-80"
                        src={ImgFondoLogin}
                        alt="Fondo de login"
                    />
                </div>

                <div className="flex flex-row justify-center items h-full w-full pl-52 ml-40">
                    <div className="relative z-10 flex flex-col items-center justify-center w-96 h-full p-8 bg-white rounded-lg shadow-lg ">

                        <div>
                            <img
                                className="h-24 w-auto"
                                src={LogoFaceops}
                                alt="Fondo de login"
                            />
                        </div>

                        <div className="mb-6 w-full">
                            <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">INICIO DE SESIÓN</h2>
                            <form onSubmit={handleLogin} className="space-y-4 mt-6">

                                <div>
                                    <label htmlFor="username" className="block text-sm font-bold leading-6 text-gray-900">Nombre de usuario</label>
                                    <div className="mt-2">
                                        <input
                                            id="username"
                                            name="username"
                                            type="text"
                                            autoComplete="username"
                                            value={username}
                                            onChange={(e) => setUsername(e.target.value)}
                                            required
                                            className="block w-full rounded-md pl-2 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:outline-none sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>

                                <div>
                                    <label htmlFor="password" className="block text-sm font-bold leading-6 text-gray-900">Contraseña</label>
                                    <div className="mt-2">
                                        <input
                                            id="password"
                                            name="password"
                                            type="password"
                                            autoComplete="current-password"
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}
                                            required
                                            className="block w-full rounded-md pl-2 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:outline-none sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>

                                {error && (
                                    <div className="text-red-500 text-sm">{error}</div>
                                )}

                                <button
                                    type="submit"
                                    disabled={loading}
                                    className="flex w-full justify-center rounded-md azul-invoke border border-azul-invoke px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-white hover:text-black hover:border-black focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    {loading ? "Autenticando..." : "INGRESAR"}
                                </button>


                                <div className="text-sm text-center">
                                    <button
                                        onClick={() => alert("Funcionalidad de recuperación de contraseña no implementada")}
                                        className="font-semibold text-azul-invoke hover:text-gray-500"
                                    >
                                        ¿Olvidaste tu contraseña?
                                    </button>
                                </div>

                                <div className="text-sm text-center">
                                    <button
                                        onClick={() => setShowRegisterForm(true)}
                                        className="font-semibold text-azul-invoke hover:text-gray-500"
                                    >
                                        Registrarse
                                    </button>
                                </div>
                                
                            </form>
                        </div>
                    </div>

                    <div className="relative z-10 hidden lg:flex flex-1 items-center justify-center px-20">
                        <img
                            className="h-auto w-auto"
                            src={ImgLogoInvoke}
                            alt="Imagen de reemplazo"
                        />
                    </div>

                </div>
            </div>

            <div>
                <Footer />
            </div>

            {showRegisterForm && (
                <div className="fixed inset-0 flex flex-col items-center justify-center bg-gray-800 bg-opacity-75 z-30">
                    <div className="bg-white p-6 rounded-lg shadow-md w-full max-w-md flex flex-col justify-between">
                        <RegisterForm />
                        <button
                            onClick={() => setShowRegisterForm(false)}
                            className="flex items-center justify-center w-12 h-12 text-red-500 hover:text-red-700 text-3xl focus:outline-none"
                        >
                            <IoCloseCircleSharp />
                        </button>
                    </div>
                </div>
            )}

        </>
    );
}
