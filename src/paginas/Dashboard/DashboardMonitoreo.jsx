import React, { useState, useEffect } from 'react';
import Footer from '../../componentes/Base/Footer';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import { Bars3Icon } from '@heroicons/react/24/outline';
import { IoIosArrowForward } from "react-icons/io";
import { Link } from 'react-router-dom';
import ControladorMonitoreo from '../../componentes/Monitoreo/ControladorMonitoreo';
import MonitoreoHost from '../../componentes/Monitoreo/MonitoreoHost';
import MonitoreoLinux from '../../componentes/Monitoreo/MonitoreoLinux';

export default function DashboardMonitoreo() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [updatedHostIds, setUpdatedHostIds] = useState([]);
  const [moduleType, setModuleType] = useState(null);

  const handleHostIdsUpdate = (hostIds) => {
    console.log("Host IDs:", hostIds);
    setUpdatedHostIds(hostIds);
  };

  const handleModuleTypeChange = (type) => {
    setModuleType(type);
  };

  return (
    <>
      <div className="flex flex-col min-h-screen">
        <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <div className="lg:pl-32 flex-1">
          <header className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
            <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </button>
            <div className="flex items-center h-full space-x-4">
              <Link to="/dashboard" className="flex items-center py-0">
                <h1 className="text-md sm:text-2xl font-bold tracking-tight text-sky-950 ">DASHBOARD</h1>
              </Link>
              <IoIosArrowForward className="text-sm text-gray-400 hidden sm:block" />
              <h1 className="text-sm tracking-tight text-gray-400 hidden sm:block">
                MONITOREO
              </h1>
            </div>
            <div className="flex items-center ml-auto">
              <SearchForm />
            </div>
          </header>

          <main className="flex flex-col">

            <div className="flex items-center justify-start sm:px-10 ">
              <div className="w-full">
              <ControladorMonitoreo onHostIdsUpdate={handleHostIdsUpdate} onModuleTypeChange={handleModuleTypeChange} />
              {moduleType === 'cisco' && (
                <MonitoreoHost updatedHostIds={updatedHostIds} />
              )}

              {moduleType === 'linux' && (
                <MonitoreoLinux updatedHostIds={updatedHostIds} />
              )}
              </div>
            </div>
          </main>
        </div>
        <Footer />
      </div>
    </>
  );
}