import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Footer from '../../componentes/Base/Footer';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import { Bars3Icon } from '@heroicons/react/24/outline';
import { IoIosArrowForward } from "react-icons/io";
import IconSeguridad from '../../img/iconos/Iconos_Seguridad.png';
import IconMonitoreo from '../../img/iconos/Iconos_Monitoreo.png';
import IconInfraestructura from '../../img/iconos/Iconos_Infraestructura .png';
import IconBD from '../../img/iconos/Iconos_BaseDeDatos.png';
import IconRedes from '../../img/iconos/Iconos_Redes.png';
import IconDeployments from '../../img/iconos/Iconos_Deployments.png';
import IconTransversalidad from '../../img/iconos/Iconos_Otros.png';
import TabsAndContent from './TabsAndContent';
import { useAuth } from '../../providers/AuthProvider';
import { Link } from 'react-router-dom';
import Loading from '../../componentes/Base/Loading';

const imgMapping = {
    'Seguridad': IconSeguridad,
    'Monitoreo': IconMonitoreo,
    'Infraestructura': IconInfraestructura,
    'Base Datos': IconBD,
    'Redes': IconRedes,
    'Deployments': IconDeployments,
    'Transversalidad': IconTransversalidad,
};

export default function DashboardCategorias() {
    const { id } = useParams();
    const [categorias, setCategorias] = useState([]);
    const [usuario, setUsuario] = useState(null);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [activeTab, setActiveTab] = useState(null);
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [loading, setLoading] = useState(true);

    // Obtener funciones de autenticación del contexto
    const { isAuthenticated, user, fetchWithAuth } = useAuth();


    // Variables de entorno
    const API_URL = process.env.REACT_APP_API_URL;

    // Función para obtener la información del usuario
    const fetchUserData = async () => {
        setLoading(true);
        try {
            const response = await fetchWithAuth(`${API_URL}/usuario`);
            if (!response.ok) {
              throw new Error(`HTTP error! status: ${response.status}`);
            }
            const usuarios = await response.json();
            const usuarioAutenticado = usuarios.find((u) => u.username === user.name);

            if (usuarioAutenticado) {
                setUsuario(usuarioAutenticado);
                setTipoUsuario(usuarioAutenticado.tipoUsuario);
                // console.log('Usuario autenticado:', usuarioAutenticado);
            } else {
              console.error('Usuario no encontrado');
            }
        } catch (error) {
            console.error('Error al obtener los datos de usuario: ', error);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchUserData();
    }, [fetchWithAuth, user.name]);

    // Función para obtener las categorías y módulos
    const fetchCategoriasAndModules = async () => {
        if (!usuario?.id) return; // No hacer la petición hasta que tengamos usuario
        setLoading(true);
        try {
            // Obtener las categorías
            const categoriasResponse = await fetchWithAuth(`${API_URL}/categorias`);
             if (!categoriasResponse.ok) {
              throw new Error(`HTTP error! status: ${categoriasResponse.status}`);
            }
            const categoriasData = await categoriasResponse.json();
            
            // Obtener los módulos del usuario
            const modulosResponse = await fetchWithAuth(`${API_URL}/usuario/${usuario.id}/modulos`);
             if (!modulosResponse.ok) {
              throw new Error(`HTTP error! status: ${modulosResponse.status}`);
            }
            const modulosData = await modulosResponse.json();
            
            // Extraer las categorías de los módulos del usuario
            const categoriasDeModulos = modulosData.modulos.map(modulo => modulo.categoria);
            
             // Filtrar las categorías para mostrar solo las que están presentes en los módulos del usuario
            const categoriasFiltradas = categoriasData.filter(categoria => categoriasDeModulos.includes(categoria.name));

            setCategorias(categoriasFiltradas);
            setActiveTab(categoriasFiltradas[0]?.id?.toString() || null); // Seleccionar la primera categoría por defecto
            // console.log('Categorías:', categoriasFiltradas);
        } catch (error) {
            console.error("Error al obtener las categorías:", error);
            setCategorias([]);
            setActiveTab(null);
        } finally {
          setLoading(false);
        }
    };

    useEffect(() => {
      fetchCategoriasAndModules();
  }, [fetchWithAuth, API_URL, usuario?.id]); 


    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <header className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="flex items-center h-full space-x-4">
                            <Link to="/dashboard" className="flex items-center py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">DASHBOARD</h1>
                            </Link>
                            <IoIosArrowForward className="text-sm text-gray-400" />
                             <h1 className="text-sm tracking-tight text-gray-400">
                                {categorias.find(categoria => categoria.id?.toString() === activeTab)?.name?.toUpperCase() || "Cargando..."}
                            </h1>
                        </div>
                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </header>

                    <main>
                        {loading ? (
                                        <Loading/>
                                        ) : (
                        <TabsAndContent
                            openTabs={categorias.map(categoria => ({
                                id: categoria.id?.toString(),
                                label: `${categoria.name}`,
                            }))}
                            activeTab={activeTab}
                            setActiveTab={setActiveTab}
                            imgMapping={imgMapping}
                        />
                                        )}
                    </main>
                </div>
                <Footer />
            </div>
        </>
    );
}