import React, { useState, useEffect } from 'react';
import { MdEdit } from "react-icons/md";
import { EstadisticasDashboard } from '../../componentes/Dashboard/EstadisticasDashboard';
import DonutChartDashboard from '../../componentes/Dashboard/DonutChartDashboard';
import StatsTableWidgetDashboard from '../../componentes/Dashboard/StatsTableWidgetDashboard';
import { IoMdClose } from "react-icons/io";
import ListaPlaybooks from '../../componentes/Dashboard/GráficosDashboard/ListaPlaybooks';
import TiempoEjecucion from '../../componentes/Dashboard/GráficosDashboard/TiempoEjecucion';
import GraficoApilado from '../../componentes/Dashboard/GráficosDashboard/GraficoApilado';
import { Link } from 'react-router-dom';
import { useAuth } from '../../providers/AuthProvider';
import Loading from '../../componentes/Base/Loading';

const TabsAndContent = ({ openTabs, activeTab, setActiveTab }) => {
    const { fetchWithAuth, user } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;

    const [isSidebarGraficoOpen, setIsSidebarGraficoOpen] = useState(false);
    const [showDropArea, setShowDropArea] = useState(false);
    const [graficosPorTab, setGraficosPorTab] = useState({});
    const [graficosIniciales, setGraficosIniciales] = useState([
        { id: 1, type: 'GraficoApilado', title: "Ejecuciones por Sistema Operativo" },
        { id: 2, type: 'TiempoEjecucion', title: "Tiempo de Ejecución por Playbooks" },
        { id: 3, type: 'ListaPlaybooks', title: "Lista Playbooks" },
    ]);
    const [dropIndicator, setDropIndicator] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchGraficos = async () => {
            setLoading(true);
            try {
                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();
                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    return;
                }

                const response = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}`);
                const data = await response.json();

                const initialGraficosPorTab = {};

                if (data?.usergrafic && data.usergrafic !== "") {
                    const parsedGraficos = JSON.parse(data.usergrafic);
                    for (const tab of openTabs) {
                        if (parsedGraficos && parsedGraficos[tab.id] && Array.isArray(parsedGraficos[tab.id])) {
                            initialGraficosPorTab[tab.id] = parsedGraficos[tab.id];
                            const hasGraficosEnColumnas = parsedGraficos[tab.id].some(grafico => grafico.status && grafico.status.startsWith('column-'));
                            if (hasGraficosEnColumnas) {
                                setShowDropArea(true);
                            }
                        } else {
                            initialGraficosPorTab[tab.id] = [
                                { id: 1, status: "sidebar" },
                                { id: 2, status: "sidebar" },
                                { id: 3, status: "sidebar" },
                            ];
                        }
                    }
                } else {
                    for (const tab of openTabs) {
                        initialGraficosPorTab[tab.id] = [
                            { id: 1, status: "sidebar" },
                            { id: 2, status: "sidebar" },
                            { id: 3, status: "sidebar" },
                        ];
                    }
                }
                setGraficosPorTab(initialGraficosPorTab);

            } catch (error) {
                console.error("Error al obtener gráficos:", error.message);
            } finally {
                setLoading(false);
            }
        };

        fetchGraficos();
    }, [fetchWithAuth, API_URL, user?.name, openTabs, activeTab]);

    const saveGraficos = async (newGraficos) => {
        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();
        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
            console.error("Usuario autenticado no encontrado en la API.");
            return;
        }
        try {
            const response = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}`);
            const data = await response.json();
            const userGraficData = data?.usergrafic ? JSON.parse(data.usergrafic) : {};
            const updatedUserGraficData = { ...userGraficData, [activeTab]: newGraficos };
            const graficosString = JSON.stringify(updatedUserGraficData);
            await fetchWithAuth(
                `${API_URL}/usuario/${usuarioAutenticado.id}/graficos?graficos=${encodeURIComponent(graficosString)}`,
                { method: 'POST' }
            );

        } catch (error) {
            console.error("Error al guardar gráficos:", error.message);
        }
    };

    const handleTabChange = (newTabId) => setActiveTab(newTabId);
    const handleDragStart = (e, graficoId) => e.dataTransfer.setData("text/plain", graficoId.toString());
    const handleDragEnd = (e) => e.dataTransfer.clearData(); // AÑADIDO e
    const handleDrop = (e, status) => {
        e.preventDefault();
        const graficoId = e.dataTransfer.getData("text/plain");
        setGraficosPorTab((prevGraficosPorTab) => {
            const updatedGraficos = prevGraficosPorTab[activeTab].map((grafico) =>
                grafico.id === +graficoId ? { ...grafico, status } : grafico
            );
            saveGraficos(updatedGraficos);
            return {
                ...prevGraficosPorTab,
                [activeTab]: updatedGraficos
            };
        });
        setDropIndicator(null);
    };
    const handleDragOver = (e) => {
        e.preventDefault();
        setDropIndicator(e.currentTarget.name);
    };

    const renderComponent = (type, id) => {
        switch (type) {
            case 1:
                return <GraficoApilado id={id} />;
            case 2:
                return <TiempoEjecucion id={id} />;
            case 3:
                return <ListaPlaybooks id={id} />;
            default:
                return null;
        }
    };

    const renderGraficos = (status) => {
        if (!graficosPorTab[activeTab]) return <p>No hay gráficos disponibles.</p>;
        return graficosPorTab[activeTab]
            .filter((grafico) => grafico.status === status)
            .map((grafico) => (
                <div
                    key={grafico.id}
                    draggable
                    onDragStart={e => handleDragStart(e, grafico.id)}
                    onDragEnd={handleDragEnd}
                    className="w-full p-2 bg-gray-100 rounded"
                >
                    <div className="text-[#0c4a6e] text-center font-bold text-lg mb-2">
                        {graficosIniciales.find(g => g.id === grafico.id)?.title}
                    </div>
                    {status !== "sidebar" && renderComponent(grafico.id, activeTab)}
                </div>
            ));
        
    };

    const hasGraficosInDropArea = () => graficosPorTab[activeTab]?.some(grafico => grafico.status.startsWith("column-"));

    const createColumns = () => {
        const columns = [];
        for (let i = 1; i <= 6; i++) {
            const columnStatus = `column-${i}`;
            const hasContent = graficosPorTab[activeTab]?.some(grafico => grafico.status === columnStatus);
            const isVisible = isSidebarGraficoOpen || hasContent;
            if (i === 1 || hasContent || isSidebarGraficoOpen) {
                columns.push(
                    <div
                        key={columnStatus}
                        name={columnStatus}
                        onDragOver={handleDragOver}
                        onDrop={e => handleDrop(e, columnStatus)}
                        className={`border border-sky-950 rounded-3xl h-auto p-8 ${hasContent ? "" : "border-dashed"} ${dropIndicator === columnStatus ? "bg-blue-200" : ""} ${isVisible ? "" : "hidden"}`}
                    >
                        {renderGraficos(columnStatus)}
                    </div>
                );
            }
        }
        return (
            <div className="bg-white w-full grid grid-cols-1 md:grid-cols-2 gap-4">
                {columns}
            </div>
        );
    };

    if (!Array.isArray(openTabs) || openTabs.length === 0) {
        return (
            <div>
                <p className="text-center py-4 text-sm text-black">
                    El usuario no tiene módulos asociados para crear.
                </p>
            </div>
        );
    }

    return (
        <>
            <div className="mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex flex-wrap justify-end mr-6">
                    <ul className="flex flex-wrap">
                        {openTabs.map((tab, index) => (
                            <li
                                key={tab.id}
                                className={`bg-white -mb-[1px] -mx-[1px] border border-sky-950 w-56 h-auto
                                ${index === 0 ? 'rounded-tl-md' : ''}
                                ${index === openTabs.length - 1 ? 'rounded-tr-md' : ''}`}
                            >
                                <Link to="#" className="block text-center p-1 w-full no-underline">
                                    <h2
                                        onClick={(e) => {
                                            e.preventDefault();
                                            handleTabChange(tab.id);
                                        }}
                                        className={`text-sky-950 block ${activeTab === tab.id ? 'font-semibold' : 'font-normal'} m-0 w-56 text-sm cursor-pointer`}
                                    >
                                        {tab.label}
                                    </h2>
                                </Link>
                            </li>
                        ))}
                    </ul>
                </div>

                <div className="bg-white border border-sky-950 rounded-3xl p-8 mt-0 mb-8">
                    <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-4">
                        <div className="flex items-center justify-center md:items-start">
                            <div
                                className="w-8 h-8 border border-sky-950 rounded-full flex items-center justify-center cursor-pointer"
                                onClick={() => {
                                    setIsSidebarGraficoOpen(true);
                                    setShowDropArea(true);
                                }}
                            >
                                <MdEdit className="text-sky-950 w-5 h-5" />
                            </div>
                        </div>

                        <div className="bg-white w-full md:w-1/2 rounded-3xl border border-sky-950">
                            <div className="px-6 pt-6">
                                <EstadisticasDashboard id={activeTab} />
                            </div>
                            <DonutChartDashboard id={activeTab} />
                        </div>

                        <div className="bg-white p-6 w-full md:w-1/2 rounded-3xl border border-sky-950">
                            <StatsTableWidgetDashboard id={activeTab} />
                        </div>
                    </div>

                    {showDropArea && (
                        <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-4 mt-4 ml-12">
                            {createColumns()}
                        </div>
                    )}
                </div>

                <div
                    className={`fixed top-0 right-0 w-72 h-full bg-white border border-sky-950 shadow-lg z-50 transform ${isSidebarGraficoOpen ? 'translate-x-0' : 'translate-x-full'} transition-transform duration-300 ease-in-out`}
                >
                    <div className="flex justify-end p-2">
                        <IoMdClose
                            className="text-gray-500 cursor-pointer w-6 h-6"
                            onClick={() => {
                                setIsSidebarGraficoOpen(false);
                                if (!hasGraficosInDropArea()) {
                                    setShowDropArea(false);
                                }
                            }}
                        />
                    </div>
                    <div className="p-4 h-full overflow-y-auto">
                        <h2 className="text-xl font-semibold text-center">Sidebar de Gráficos</h2>
                        <br />
                        <div
                            id="sidebar"
                            onDragOver={handleDragOver}
                            onDrop={(e) => handleDrop(e, "sidebar")}
                            className={`flex flex-col items-center justify-start w-full border rounded-3xl border-sky-950 border-dashed p-8 gap-1 ${dropIndicator === "sidebar" ? "bg-blue-200" : ""}`}
                        >
                            {renderGraficos("sidebar")}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default TabsAndContent;