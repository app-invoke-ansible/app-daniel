import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Footer from '../../componentes/Base/Footer';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import { Bars3Icon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';
import { LuMonitor } from "react-icons/lu";
import { TbCategoryPlus } from "react-icons/tb";

const cards = [
    { id: 1, icon: <TbCategoryPlus className="h-6 w-6 mx-auto" />, title: 'Dashboard Categorias', description: 'Dashboard personalizado donde los módulos se organizan por categorías.' },
    { id: 2, icon: <LuMonitor className="h-6 w-6 mx-auto" />, title: 'Dashboard Monitoreo', description: 'Monitoreo en tiempo real.' },
];

export default function Dashboard() {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <header className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="flex items-center h-full space-x-4">
                            <Link to="/dashboard" className="flex items-center py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">DASHBOARD</h1>
                            </Link>
                        </div>
                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </header>

                    <main>
                        <div className="flex-grow container px-8 py-8 flex justify-center items-center mb-4">
                            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-6">
                                {cards.map((card) => {
                                    // Lógica dinámica para determinar la ruta
                                    const route = card.title === 'Dashboard Categorias'
                                        ? '/dashboardcategorias'
                                        : card.title === 'Dashboard Monitoreo'
                                            ? '/dashboardmonitoreo'
                                            : '#';

                                    return (
                                        <Link
                                            key={card.id}
                                            to={route}
                                            className="group block w-72 mx-auto rounded-lg bg-white shadow-md hover:shadow-lg transition p-4 border border-sky-950 hover:bg-sky-950 cursor-pointer"
                                        >
                                            <div className="flex justify-center text-sky-700 group-hover:text-white">
                                                {React.cloneElement(card.icon, { className: "h-12 w-12" })}
                                            </div>
                                            <h3 className="mt-4 text-center text-base font-semibold text-gray-800 group-hover:text-white">
                                                {card.title}
                                            </h3>
                                            <p className="mt-2 text-sm text-center text-gray-400 group-hover:text-white">
                                                {card.description}
                                            </p>
                                        </Link>
                                    );
                                })}
                            </div>
                        </div>
                    </main>

                </div>
                <Footer />
            </div>
        </>
    );
}
