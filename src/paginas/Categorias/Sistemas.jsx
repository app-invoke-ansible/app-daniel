import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Bars3Icon } from '@heroicons/react/24/outline';
import { IoIosArrowForward } from "react-icons/io";

import Footer from '../../componentes/Base/Footer';
import Sidebar from '../../componentes/Base/Sidebar';
import SearchForm from '../../componentes/Base/SearchForm';
import ListaSistemas from '../../componentes/Categorias/ListaSistemas';
import Return from '../../componentes/Base/Return';
import { Link } from 'react-router-dom';

export default function Sistemas() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [categoriaName, setCategoriaName] = useState('');

    // Obtener parámetros de la URL
    const location = useLocation();
    const query = new URLSearchParams(location.search);
    const categoriaNameFromUrl = query.get('categoria');

    // Cargar categoria
    useEffect(() => {
        if (categoriaNameFromUrl) {
            setCategoriaName(categoriaNameFromUrl);
        } else {
            setCategoriaName("Categoría no encontrada");
        }
    }, [categoriaNameFromUrl]);


    // Render del componente principal
    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                        <button
                            type="button"
                            className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
                            onClick={() => setSidebarOpen(true)}
                        >
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                        <div className="flex items-center h-full space-x-4">
                            <Return />
                            <Link to="/categorias" className="flex items-center py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">
                                    {categoriaName.toUpperCase()}
                                </h1>
                            </Link>
                            <p className="text-sm text-gray-400">
                                <IoIosArrowForward />
                            </p>
                            <Link to="/sistemas" className="flex items-center py-0">
                                <h1 className="text-sm tracking-tight text-gray-400">SISTEMAS</h1>
                            </Link>
                        </div>

                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </div>

                    <main>
                        <ListaSistemas categoriaName={categoriaName} />
                    </main>
                </div>
                <Footer />
            </div>
        </>
    );
}
