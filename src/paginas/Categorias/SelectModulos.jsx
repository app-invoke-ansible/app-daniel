import React, { useState, useEffect } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline';
import Footer from '../../componentes/Base/Footer';
import Sidebar from '../../componentes/Base/Sidebar';
import SearchForm from '../../componentes/Base/SearchForm';
import TablaSelectModulos from '../../componentes/Categorias/TablaSelectModulos';
import Return from '../../componentes/Base/Return';
import { IoIosArrowForward } from "react-icons/io";
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';

const API_URL = process.env.REACT_APP_API_URL;

export default function SelectModulos() {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    const location = useLocation();

    const params = new URLSearchParams(location.search);
    const categoria = params.get('categoria');
    const sistema = params.get('sistema');

    // Fetch de sistema y categoría
    useEffect(() => {
        console.log(`Categoría: ${categoria}, Sistema: ${sistema}`);
    }, [categoria, sistema]);

    return (
        <div className="flex flex-col min-h-screen">
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <div className="lg:pl-32 flex-1">
                <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                    <button
                        type="button"
                        className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
                        onClick={() => setSidebarOpen(true)}
                    >
                        <span className="sr-only">Abrir barra lateral</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                    <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                    {/* Breadcrumb */}
                    <div className="flex items-center h-full space-x-4">
                        <Return />
                        <Link to="/categorias" className="flex items-center py-0">
                            <h1 className="text-sm tracking-tight text-gray-400">{categoria.toUpperCase()}</h1>
                        </Link>
                        <p className="text-sm text-gray-400">
                            <IoIosArrowForward />
                        </p>
                        <h1 className="text-sm tracking-tight text-gray-400">{sistema.toUpperCase()}</h1>
                        <p className="text-sm text-gray-400">
                            <IoIosArrowForward />
                        </p>
                        <h1 className="text-sm tracking-tight text-gray-400">SELECCIÓN DE MÓDULOS</h1>
                    </div>
                    {/* Formulario de búsqueda */}
                    <div className="flex items-center ml-auto">
                        <SearchForm />
                    </div>
                </div>

                {/* Contenido principal */}
                <main>
                    <div className='mx-auto sm:px-6 lg:px-8'>
                        <TablaSelectModulos categoria={categoria} sistema={sistema} />
                    </div>
                </main>
            </div>

            <Footer />
        </div>
    );
}
