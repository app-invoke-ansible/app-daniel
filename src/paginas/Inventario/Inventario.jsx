import React, { useState, useEffect, use } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline';
import Footer from '../../componentes/Base/Footer';
import CardGrupo from '../../componentes/Inventario/CardGrupo';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import BuscarMaquina from '../../componentes/Inventario/BuscarMaquina';
import SubirInventario from '../../componentes/Inventario/SubirInventario';
import AgregarManual from '../../componentes/Inventario/AgregarManual';
import BotonActualizarTodo from '../../componentes/Inventario/BotonActualizarTodo';
import TabInventario from '../../componentes/Inventario/TabInventario';
import { useAuth } from '../../providers/AuthProvider';
import { Link } from 'react-router-dom';
import Loading from '../../componentes/Base/Loading';
import AgregarGrupo from '../../componentes/Inventario/AgregarGrupo';

export default function Inventario() {
    const [data, setData] = useState([]);
    const [inventarioUser, setInventarioUser] = useState([]);
    const [groups, setGroups] = useState([]);
    const [selectedGroup, setSelectedGroup] = useState(null);
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');
    const [loading, setLoading] = useState(true);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const { isAuthenticated, user, fetchWithAuth } = useAuth();
    const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
    const [inventarioId, setInventarioId] = useState(null);
    const [inventarioName, setInventarioName] = useState(null);
    const [username, setUsername] = useState(null);

    const API_URL = process.env.REACT_APP_API_URL;

    useEffect(() => {
        if (isAuthenticated && user) {
            const fetchUserData = async () => {
                setLoading(true);
                try {
                    const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                    const usuarios = await responseUsuarios.json();
                    const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);
                    if (usuarioAutenticado) {
                        setTipoUsuario(usuarioAutenticado.tipoUsuario);
                        setInventarioId(usuarioAutenticado.inventarioId);
                        setUsername(usuarioAutenticado.username);
                    } else {
                        console.warn('Usuario autenticado no encontrado en la API');
                    }
                } catch (error) {
                    console.error('Error al obtener de usuario:', error);
                } finally {
                    setLoadingTipoUsuario(false);
                    setLoading(false);
                }
            };
            fetchUserData();
        }
    }, [isAuthenticated, user, fetchWithAuth, API_URL]);

    useEffect(() => {
        if (inventarioId) {
            fetchInventario();
        }
    }, [inventarioId]);

    const fetchInventario = async () => {
        if (!inventarioId) {
            return;
        }

        setLoading(true);
        try {
            const response = await fetchWithAuth(`${API_URL}/inventarios/${inventarioId}`);
            const contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                const data = await response.json();
                setData(data);
                setInventarioUser(data)
                const allGroups = data.groups.flatMap(group => ({
                    id: group.id,
                    groupName: group.groupName
                }));

                const uniqueGroups = Array.from(new Set(allGroups.map(group => group.groupName)))
                    .map(groupName => allGroups.find(group => group.groupName === groupName));

                setGroups(uniqueGroups);
                // console.log("Grupos asociados al inventario: ",allGroups)
            } else {
                throw new Error("El formato de la respuesta no es JSON");
            }
        } catch (error) {
            setData([]);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchInventario();
    }, [fetchWithAuth, API_URL]);

    const filteredGroupsByGroup = selectedGroup
        ? (data?.groups || []).filter(group => group.groupName === selectedGroup)
        : (data?.groups || []);

    const filteredHostsBySearch = filteredGroupsByGroup.map((group) => ({
        ...group,
        hosts: group.hosts.filter((host) =>
            host.hostname.toLowerCase().includes(searchTerm.toLowerCase())
        ),
    }));

    const handleGroupSelect = (groupName) => {
        setSelectedGroup((prevGroup) => (prevGroup === groupName ? null : groupName));
    };

    const hasHosts = filteredHostsBySearch.some((group) => group.hosts.length > 0);


    return (
        <div className="flex flex-col min-h-screen">
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <div className="lg:pl-32 flex-1">
                <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                    <button
                        type="button"
                        className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
                        onClick={() => setSidebarOpen(true)}
                    >
                        <span className="sr-only">Open sidebar</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                    <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                    <div className="flex items-center h-full">
                        <Link to="/home" className="flex items-center relative w-full h-full py-0">
                            <h1 className="text-2xl font-bold tracking-tight text-sky-950">INVENTARIO</h1>
                        </Link>
                    </div>
                    <div className="flex items-center ml-auto">
                        <SearchForm />
                    </div>
                </div>
                <main>
                    <div>
                        <header className="bg-white">
                            <div className="mx-auto px-4 py-4 sm:px-6 lg:px-8">
                                <h1 className="text-base text-sky-900 font-bold">Grupos</h1>
                            </div>
                        </header>
                        <div className="flex px-4 pb-2 sm:px-6 lg:px-8 space-x-4">

                            <div className="flex-grow flex justify-center">
                                <div className="rounded-lg bg-white px-10 sm:px-1 w-full max-w-7xl">
                                    {loadingTipoUsuario ? (
                                        <div className="col-span-1"></div>
                                    ) : (
                                        <ul
                                            role="list"
                                            className="grid grid-cols-1 gap-5 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5"
                                        >
                                            <div className="border border-sky-950 bg-sky-950 col-span-1 flex flex-col rounded-3xl text-center p-2 relative">
                                                <AgregarGrupo fetchInventario={fetchInventario} inventarioId={inventarioId} />
                                            </div>
                                            {groups.map((group) => (
                                                <CardGrupo
                                                    key={group.id}
                                                    grupo={{ name: group.groupName, id: group.id }}
                                                    isSelected={selectedGroup === group.groupName}
                                                    onSelect={() => handleGroupSelect(group.groupName)}
                                                    fetchInventario={fetchInventario}
                                                />
                                            ))}
                                        </ul>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="grid grid-cols-1 md:grid-cols-3 gap-4 p-4">
                        <div className="col-span-1">
                            <BuscarMaquina
                                setFilteredMaquinas={(term) => setSearchTerm(term)}
                            />
                        </div>
                        {loadingTipoUsuario ? (
                            <div className="col-span-1"></div>
                        ) : (
                            <>
                                {(tipoUsuario === 'admin' || tipoUsuario === 'editor') && (
                                    <div className="col-span-1">
                                        <SubirInventario usuarioAutenticado={username} inventarioId={inventarioId} />
                                    </div>
                                )}
                                <div className="col-span-1 flex items-center justify-center">
                                    {(tipoUsuario === 'admin' || tipoUsuario === 'editor') &&
                                    <AgregarManual fetchInventario={fetchInventario} inventarioUser={inventarioUser} inventarioId={inventarioId} />}
                                    <BotonActualizarTodo onActualizar={fetchInventario} />
                                </div>
                            </>
                        )}
                    </div>
                    <div className="mx-auto px-4 sm:px-6 lg:px-8">
                        {loading ? (
                            <Loading message="Cargando datos..." />
                        ) : (
                            <TabInventario groups={filteredHostsBySearch} selectedGroup={selectedGroup} searchTerm={searchTerm} />
                        )}
                    </div>
                </main>
            </div>
            <Footer />
        </div>
    );
}
