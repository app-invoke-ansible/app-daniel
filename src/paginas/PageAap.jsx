import React from 'react';
import { useState } from 'react'
import { Bars3Icon } from '@heroicons/react/24/outline'
import Footer from '../componentes/Footer';
import SearchForm from '../componentes/SerchForm';
import Sidebar from '../componentes/Sidebar';
import { useKeycloak } from '@react-keycloak/web';
import ProjectCards from '../componentes/ProjectCards';

export default function Tickets() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const { keycloak, initialized } = useKeycloak();

    if (!initialized) {
        return <div>Loading...</div>;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    return (
        <>
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <div className="lg:pl-20">
                <div className="sticky top-0 z-40 flex h-16 shrink-0 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                    <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                        <span className="sr-only">Open sidebar</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                    <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                    <a className="flex h-16 shrink-0 items-center relative w-full py-5">
                        <div className="absolute top-0 left-0 px-1">
                            <h1 className="text-3xl font-bold tracking-tight text-sky-950">Ansible Automation Platform</h1>
                        </div>
                    </a>
                    <SearchForm />
                </div>
                <main>
                    <div>
                        <header className="bg-white">
                            <div className="mx-auto px-4 py-6 sm:px-6 lg:px-8">
                            </div>
                        </header>
                    </div>
                    <div>
                        <ProjectCards/>                  
                    </div>
                </main>
                <div className='pt-96'>
                    <Footer />
                </div>
            </div>
        </>
    );
}
