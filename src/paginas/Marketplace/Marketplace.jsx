import React from 'react';
import { useState } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline';
import Footer from '../../componentes/Base/Footer';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import CardMarket from '../../componentes/Marketplace/CardMarket';
import { Link } from 'react-router-dom';

export default function PHome() {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8"> {/* Mantener h-16 aquí */}
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                        <div className="flex items-center h-full">
                            <Link to="/marketplace" className="flex items-center relative w-full h-full py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">MARKETPLACE</h1>
                            </Link>
                        </div>
                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </div>

                    <main>
                        <div>
                            <section className="pt-4">
                                <CardMarket />
                            </section>
                        </div>
                    </main>
                </div>
                <Footer />
            </div>
        </>
    );
}
