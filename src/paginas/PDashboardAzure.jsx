import React from 'react';
import React, { useState, useEffect } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline';
import Footer from '../componentes/Footer';
import Sidebar from '../componentes/Sidebar';
import SearchForm from '../componentes/SerchForm';
import { useKeycloak } from '@react-keycloak/web';
import GraficasModulos from '../componentes/GraficasModulo';
import { default as GraficoDonut } from '../componentes/GraficoDonut';
import axios from 'axios';
import GraficoDispersion from '../componentes/GraficoDispersion';

export default function PDashboardAzure() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [executionData, setExecutionData] = useState({});
    const [moduloStats, setModuloStats] = useState({
        modulo1Exitos: 0,
        modulo1Fallos: 0,
        modulo2Exitos: 0,
        modulo2Fallos: 0
    });
    const [ejecuciones, setEjecuciones] = useState({
        healthcheck: [],
        cambioZonaHorariaLinux: []
    });
    const [pestañaActiva, setPestañaActiva] = useState('general');
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const { keycloak, initialized } = useKeycloak();

    useEffect(() => {
        if (initialized && keycloak.authenticated) {
            fetchModulos();
        }
    }, [initialized, keycloak.authenticated]);

    const fetchModulos = async () => {
        if (keycloak && keycloak.authenticated) {
            try {
                setLoading(true);
                const token = keycloak.token;
                const response = await axios.get('http://localhost:8080/modulos', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });
                console.log('Datos recibidos:', response.data);
                updateExecutionData(response.data);
                updateModuloStats(response.data);
                prepareEjecucionesData(response.data);
                setLoading(false);
            } catch (error) {
                console.error('Error al obtener los módulos:', error);
                setError(error);
                setLoading(false);
            }
        }
    };

    const updateExecutionData = (modulesData) => {
        const executionCounts = {};
        modulesData.forEach(module => {
            executionCounts[module.name] = module.ejecuciones ? module.ejecuciones.length : 0;
        });
        setExecutionData(executionCounts);
    };

    const updateModuloStats = (modulesData) => {
        const stats = {
            modulo1Exitos: 0,
            modulo1Fallos: 0,
            modulo2Exitos: 0,
            modulo2Fallos: 0
        };

        modulesData.forEach((modulo, index) => {
            if (index === 0) { // Módulo 1
                stats.modulo1Exitos = modulo.ejecuciones?.filter(e => e.status === 'success').length || 0;
                stats.modulo1Fallos = modulo.ejecuciones?.filter(e => e.status === 'failed').length || 0;
            } else if (index === 1) { // Módulo 2
                stats.modulo2Exitos = modulo.ejecuciones?.filter(e => e.status === 'success').length || 0;
                stats.modulo2Fallos = modulo.ejecuciones?.filter(e => e.status === 'failed').length || 0;
            }
        });

        setModuloStats(stats);
    };

    const prepareEjecucionesData = (modulesData) => {
        const ejecucionesData = {
            healthcheck: [],
            cambioZonaHorariaLinux: []
        };

        modulesData.forEach((modulo, index) => {
            const moduloKey = index === 0 ? 'healthcheck' : 'cambioZonaHorariaLinux';
            ejecucionesData[moduloKey] = modulo.ejecuciones.map(e => ({
                tiempoEjecucion: e.tiempoEjecucion || Math.random() * 10, // Asumiendo que tienes este dato, si no, genera un valor aleatorio
                exitoso: e.status === 'success'
            }));
        });

        setEjecuciones(ejecucionesData);
    };

    const renderContenidoPestaña = () => {
        if (loading) {
            return <div>Cargando datos...</div>;
        }

        if (error) {
            return <div>Error al cargar los datos: {error.message}</div>;
        }

        switch (pestañaActiva) {
            case 'general':
                return (
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                        <div className="p-4 border rounded-lg shadow-md">
                            <h3 className="text-lg font-semibold mb-2">Ejecuciones por Módulo</h3>
                            <GraficasModulos executionData={executionData} />
                        </div>
                        <div className="p-4 border rounded-lg shadow-md">
                            <h3 className="text-lg font-semibold mb-2">Éxitos y Fallos por Módulo</h3>
                            <GraficoDonut {...moduloStats} />
                        </div>
                        <div className="col-span-2 p-4 border rounded-lg shadow-md">
                            <h3 className="text-lg font-semibold mb-2">Dispersión de Ejecuciones</h3>
                            <GraficoDispersion 
                                ejecuciones={[...ejecuciones.healthcheck, ...ejecuciones.cambioZonaHorariaLinux]} 
                            />
                        </div>
                    </div>
                );
            case 'modulo-healthcheck':
                return (
                    <div className="p-4 border rounded-lg shadow-md">
                        <h2 className="text-2xl font-bold mb-4">Estadísticas del Módulo Healthcheck</h2>
                        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                            <div className="p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Ejecuciones del Módulo</h3>
                                <GraficasModulos executionData={{ healthcheck: executionData.healthcheck }} />
                            </div>
                            <div className="p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Éxitos y Fallos</h3>
                                <GraficoDonut 
                                    modulo1Exitos={moduloStats.modulo1Exitos}
                                    modulo1Fallos={moduloStats.modulo1Fallos}
                                />
                            </div>
                            <div className="col-span-2 p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Dispersión de Ejecuciones</h3>
                                <GraficoDispersion ejecuciones={ejecuciones.healthcheck} />
                            </div>
                        </div>
                    </div>
                );
            case 'modulo-cambio-zona-horaria-linux':
                return (
                    <div className="p-4 border rounded-lg shadow-md">
                        <h2 className="text-2xl font-bold mb-4">Estadísticas del Módulo Cambio Zona Horaria Linux</h2>
                        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                            <div className="p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Ejecuciones del Módulo</h3>
                                <GraficasModulos executionData={{ cambioZonaHorariaLinux: executionData.cambioZonaHorariaLinux }} />
                            </div>
                            <div className="p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Éxitos y Fallos</h3>
                                <GraficoDonut 
                                    modulo2Exitos={moduloStats.modulo2Exitos}
                                    modulo2Fallos={moduloStats.modulo2Fallos}
                                />
                            </div>
                            <div className="col-span-2 p-4 border rounded-lg shadow-md">
                                <h3 className="text-lg font-semibold mb-2">Dispersión de Ejecuciones</h3>
                                <GraficoDispersion ejecuciones={ejecuciones.cambioZonaHorariaLinux} />
                            </div>
                        </div>
                    </div>
                );
            default:
                return null;
        }
    };

    if (!initialized) {
        return <div>Cargando...</div>;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    return (
        <>
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <div className="lg:pl-20">
                <div className="sticky top-0 z-40 flex h-16 shrink-0 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
                    <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                        <span className="sr-only">Abrir barra lateral</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                    <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                    <div className="flex h-16 shrink-0 items-center relative w-full py-5">
                        <div className="absolute top-0 left-0 px-1">
                            <h1 className="text-3xl font-bold tracking-tight text-sky-950">Dashboard</h1>
                        </div>
                    </div>
                    <SearchForm />
                </div>
                <main className="p-8">
                    <div className="mb-4">
                        <nav className="flex space-x-4 border-b" aria-label="Tabs">
                            {[
                                { key: 'general', label: 'General' },
                                { key: 'modulo-healthcheck', label: 'Módulo Healthcheck' },
                                { key: 'modulo-cambio-zona-horaria-linux', label: 'Módulo Cambio Zona Horaria Linux' }
                            ].map((tab) => (
                                <button
                                    key={tab.key}
                                    onClick={() => setPestañaActiva(tab.key)}
                                    className={`${
                                        pestañaActiva === tab.key
                                            ? 'border-b-2 border-sky-500 text-sky-600'
                                            : 'text-gray-500 hover:text-gray-700 hover:border-gray-300'
                                    } py-4 px-1 font-medium text-sm`}
                                >
                                    {tab.label}
                                </button>
                            ))}
                        </nav>
                    </div>
                    <div className="mt-4">
                        {renderContenidoPestaña()}
                    </div>
                </main>
                <div className='pt-10'>
                    <Footer />
                </div>
            </div>
        </>
    );
}
