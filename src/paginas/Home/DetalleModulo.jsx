import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Sidebar from '../../componentes/Base/Sidebar';
import Footer from '../../componentes/Base/Footer';
import Return from '../../componentes/Base/Return';
import SearchForm from '../../componentes/Base/SearchForm';
import { IoIosArrowForward } from "react-icons/io";
import { Bars3Icon } from '@heroicons/react/24/outline';
import Tabs from '../../componentes/Home/Tabs';
import TabContent from '../../componentes/Home/TabContent';
import { useAuth } from '../../providers/AuthProvider';
import LogoFaceops from '../../img/LogoFaceops.png';
import { LuLoaderCircle } from "react-icons/lu";
import { Link } from 'react-router-dom';

const DetalleModulo = () => {
    const { id } = useParams();
    const [modulo, setModulo] = useState(null);
    const [loading, setLoading] = useState(true);
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [activeTab, setActiveTab] = useState('General');
    const [isOn, setIsOn] = useState(true);

    // Obtener funciones de autenticación del contexto
    const { fetchWithAuth } = useAuth();

    // Variables de entorno
    const API_URL = process.env.REACT_APP_API_URL;

    // Definición de las pestañas disponibles
    const tabs = [
        { id: 'General', label: 'General' },
        { id: 'Historial de ejecución', label: 'Historial de ejecución' },
        { id: 'Segmento', label: 'Segmento' },
        { id: 'Circuito', label: 'Circuito' },
    ];

    // Obtener los datos del módulo al montar el componente
    useEffect(() => {
        const fetchModulo = async () => {
            setLoading(true);
            try {
                const response = await fetchWithAuth(`${API_URL}/modulos/${id}`);

                // Validar el tipo de contenido de la respuesta
                const contentType = response.headers.get("content-type");
                if (contentType && contentType.includes("application/json")) {
                    const data = await response.json();
                    setModulo(data);
                } else {
                    throw new Error("El formato de la respuesta no es JSON");
                }
            } catch (error) {
                console.error("Error al obtener el módulo:", error.message);
                setModulo(null); // Vaciar el módulo en caso de error
            } finally {
                setLoading(false);
            }
        };

        fetchModulo();
    }, [fetchWithAuth, API_URL, id]);

    // Mostrar indicador de carga mientras se obtienen los datos
    if (loading) {
        return <div className="flex justify-center items-center h-screen">
            <div className="text-center">
                <img className="h-32 w-auto mx-auto mb-4" src={LogoFaceops} alt="Logo" />
                <LuLoaderCircle className="animate-spin mx-auto text-sky-600 text-4xl mb-4" />
                <p className="text-sky-600 text-md font-medium">Cargando...</p>
            </div>
        </div>;
    }
    if (!modulo) return <div>No se encontró el módulo.</div>;

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8 justify-center">
                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />
                        <div className="flex items-center h-full space-x-4">
                            <Return />
                            <Link to="/home" className="flex items-center py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">HOME</h1>
                            </Link>
                            <div className="hidden sm:flex items-center space-x-4">
                                <p className="text-sm text-gray-400">
                                    <IoIosArrowForward />
                                </p>
                                <h1 className="text-sm tracking-tight text-gray-400">DETALLE MÓDULO</h1>
                                <p className="text-sm text-gray-400">
                                    <IoIosArrowForward />
                                </p>
                                <h1 className="text-sm tracking-tight text-gray-400">{activeTab.toUpperCase()}</h1>
                            </div>
                        </div>
                        <div className="ml-auto">
                            <SearchForm />
                        </div>
                    </div>

                    <main>
                        <Tabs activeTab={activeTab} setActiveTab={setActiveTab} tabs={tabs} />
                        <TabContent activeTab={activeTab} modulo={modulo} isOn={isOn} setIsOn={setIsOn} />
                    </main>
                </div>
                <Footer />
            </div>
        </>
    );
};


export default DetalleModulo;
