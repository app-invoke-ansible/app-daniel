import React from 'react';
import { useState } from 'react'
import { Bars3Icon } from '@heroicons/react/24/outline'
import ImgSeguridad from '../../img/iconos/Iconos_Seguridad.png';
import ImgMonitoreo from '../../img/iconos/Iconos_Monitoreo.png';
import ImgInfraestructura from '../../img/iconos/Iconos_Infraestructura .png';
import ImgRedes from '../../img/iconos/Iconos_Redes.png';
import ImgBD from '../../img/iconos/Iconos_BaseDeDatos.png';
import ImgDeployments from '../../img/iconos/Iconos_Deployments.png';
import ImgOtros from '../../img/iconos/Iconos_Otros.png';
import Footer from '../../componentes/Base/Footer';
import TabUltEjec from '../../componentes/Home/TabUltEjec';
import CardCategorias from '../../componentes/Home/CardCategorias';
import SearchForm from '../../componentes/Base/SearchForm';
import Sidebar from '../../componentes/Base/Sidebar';
import { Link } from 'react-router-dom';

const scope = [
    { name: 'Seguridad', imageUrl: ImgSeguridad },
    { name: 'Monitoreo', imageUrl: ImgMonitoreo },
    { name: 'Infraestructura', imageUrl: ImgInfraestructura },
    { name: 'Redes', imageUrl: ImgRedes },
    { name: 'Base de datos', imageUrl: ImgBD },
    //    { name: 'Deployments', imageUrl: ImgDeployments },
    { name: 'Aprovisionamiento', imageUrl: ImgOtros },
];

export default function Home() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState(null);

    // Función para manejar la selección de categoría
    const handleCategorySelect = (category) => {
        if (selectedCategory === category) {
            setSelectedCategory(null);
        } else {
            setSelectedCategory(category);
        }
    };

    return (
        <>
            <div className="flex flex-col min-h-screen">
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <div className="lg:pl-32 flex-1">
                    <div className="sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8"> {/* Mantener h-16 aquí */}

                        <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(true)}>
                            <span className="sr-only">Open sidebar</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>

                        <div className="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true" />

                        <div className="flex items-center h-full">
                            <Link to="/home" className="flex items-center relative w-full h-full py-0">
                                <h1 className="text-2xl font-bold tracking-tight text-sky-950">HOME</h1>
                            </Link>
                        </div>

                        <div className="flex items-center ml-auto">
                            <SearchForm />
                        </div>
                    </div>

                    <main>
                        <div>
                            <header className="bg-white">
                                <div className="mx-auto px-4 py-4 sm:px-6 lg:px-8">
                                    <h1 className="text-base text-sky-900 font-bold">Categorías</h1>
                                </div>
                            </header>
                        </div>

                        <div>
                            <div className="mx-auto max-w-7xl px-4 pb-2 sm:px-6 lg:px-8">
                                <div className="rounded-lg bg-white px-10 sm:px-1">
                                    <div>
                                        <ul role="list" className="grid grid-cols-2 gap-3 sm:grid-cols-3 md:grid-cols-3 lg:grid-cols-6">
                                            {scope.map((item) => (
                                                <CardCategorias
                                                    key={item.name}
                                                    categoria={item}
                                                    isSelected={selectedCategory === item.name}
                                                    onSelect={() => handleCategorySelect(item.name)}
                                                />
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div className="mx-auto px-4 py-4 sm:px-6 lg:px-8">
                                <h1 className="text-base text-sky-900 font-bold">Últimas Ejecuciones</h1>
                            </div>
                            <div className='mx-auto px-4 sm:px-6 lg:px-8'>
                                <TabUltEjec selectedCategory={selectedCategory} />
                            </div>
                            <div className='mt-5 mx-9'>
                            </div>
                        </div>

                    </main>
                </div>

                <Footer />
            </div>
        </>
    );
}
