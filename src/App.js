// Dependencias principales
import React, { useState } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import './App.css';

// Componentes principales
import MainApp from './paginas/MainApp';

// Páginas - Home
import Home from './paginas/Home/Home';
import DetalleModulo from './paginas/Home/DetalleModulo';

// Páginas - Categorías
import Categorias from './paginas/Categorias/Categorias';
import Sistemas from './paginas/Categorias/Sistemas';
import SelectModulos from './paginas/Categorias/SelectModulos';

// Páginas - Modulos
import Modulos from './paginas/Modulos/Modulos';

// Páginas - Dashboard
import Dashboard from './paginas/Dashboard/Dashboard';
import DashboardCategorias from './paginas/Dashboard/DashboardCategorias';
import DashboardMonitoreo from './paginas/Dashboard/DashboardMonitoreo';

// Páginas - Inventario
import Inventario from './paginas/Inventario/Inventario';

// Páginas - Marketplace
import Marketplace from './paginas/Marketplace/Marketplace';

// Páginas - Tickets
import Tickets from './paginas/Tickets/Tickets';

// Páginas - Configuración
import ConfiguracionMenu from './paginas/Configuracion/ConfiguracionMenu';
import Ajustes from './paginas/Configuracion/Ajustes';
import GestionModulos from './paginas/Configuracion/GestionModulos';
import GestionUsuarios from './paginas/Configuracion/GestionUsuarios';
import ReportesModulos from './paginas/Configuracion/ReportesModulos';
import NotificacionesUsuario from './paginas/Configuracion/NotificacionesUsuario';
import Facturacion from './paginas/Configuracion/Facturacion';

function App() {

        return (
                <div>
                        <header>
                                <MainApp />
                        </header>
                        <Routes>
                                {/* Rutas - Home */}
                                <Route path="/" element={<Home />} />
                                <Route path="/detallemodulo" element={<DetalleModulo />} />
                                <Route path="/detallemodulo/:id" element={<DetalleModulo />} />

                                {/* Rutas - Categorías */}
                                <Route path="/categorias" element={<Categorias />} />
                                <Route path="/sistemas" element={<Sistemas />} />
                                <Route path="/selectmodulos" element={<SelectModulos />} />

                                {/* Rutas - Modulos */}
                                <Route path="/modulos" element={<Modulos />} />

                                {/* Rutas - Dashboard */}
                                <Route path="/dashboard" element={<Dashboard />} />
                                <Route path="/dashboardcategorias" element={<DashboardCategorias />} />
                                <Route path="/dashboardmonitoreo" element={<DashboardMonitoreo />} />

                                {/* Rutas - Inventario */}
                                <Route path="/inventario" element={<Inventario />} />

                                {/* Rutas - Marketplace */}
                                <Route path="/marketplace" element={<Marketplace />} />

                                {/* Rutas - Tickets */}
                                <Route path="/tickets" element={<Tickets />} />

                                {/* Rutas - Configuración */}
                                <Route path="/configuracionmenu" element={<ConfiguracionMenu />} />
                                <Route path="/ajustes" element={<Ajustes />} />
                                <Route path="/gestionmodulos" element={<GestionModulos />} />
                                <Route path="/gestionusuarios" element={<GestionUsuarios />} />
                                <Route path="/reportesmodulos" element={<ReportesModulos />} />
                                <Route path="/notificacionesusuario" element={<NotificacionesUsuario />} />
                                <Route path="/facturacion" element={<Facturacion />} />


                                {/* Redirección para rutas no coincidentes */}
                                <Route path="/*" element={<Navigate to="/" />} />
                        </Routes>
                </div>
        );
}

export default App;
