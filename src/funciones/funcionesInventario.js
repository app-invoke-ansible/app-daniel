const API_URL = process.env.REACT_APP_API_URL;

// Función para obtener inventario especifico
export const fetchInventario = async (fetchWithAuth, idInventario) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/inventarios/${idInventario}`);
    if (response.ok) {
      const inventario = await response.json();
      return inventario;
    } else {
      console.error('Error al obtener inventario especifico');
      return [];
    }
  } catch (error) {
    console.error('Error en la petición de obtener inventario especifico:', error);
    return [];
  }
};