const API_URL = process.env.REACT_APP_API_URL;

// Función para obtener datos del servidor
export const fetchServidor = async (fetchWithAuth) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/zammad/status`);
    if (response.ok) {
      const servidor = await response.json();
      return servidor;
    } else {
      console.error('Error al obtener los datos del servidor');
      return [];
    }
  } catch (error) {
    console.error("Error en la petición de obtener datos del servidor:", error);
    return [];
  }
};

// Función para obtener tickets
export const fetchTickets = async (fetchWithAuth) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/zammad/tickets`);
    if (response.ok) {
      const tickets = await response.json();
      return tickets;
    } else {
      console.error('Error al obtener los tickets');
      return [];
    }
  } catch (error) {
    console.error("Error en la petición de obtener tickets:", error);
    return [];
  }
};

// Función para obtener id del usuario en Zammad por el username
export const fetchZammadIdByUsername = async (fetchWithAuth, username) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/zammad/users/username/${username}`);
    if (response.ok) {
      const ticketsUserId = await response.json();
      return ticketsUserId;
    } else {
      console.error('Error al obtener el id del usuario Zammad');
      return [];
    }
  } catch (error) {
    console.error("Error en la petición de obtener id del usuario Zammad:", error);
    return [];
  }
};

// Función para crear un nuevo ticket
export const createTicket = async (fetchWithAuth, ticketData) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/zammad/tickets`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(ticketData)
      }
    );

    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(`Error ${response.status}: ${errorText}`);
    }

    return await response.json();
  } catch (error) {
    console.error('Error en crearTicket:', error);
    throw error;
  }
};