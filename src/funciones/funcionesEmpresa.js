const API_URL = process.env.REACT_APP_API_URL;

// Función para obtener la lista de empresas
export const fetchEmpresas = async (fetchWithAuth) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/empresas`);
    if (response.ok) {
      const empresas = await response.json();
      return empresas;
    } else {
      console.error('Error al obtener las empresas');
      return [];
    }
  } catch (error) {
    console.error('Error en la petición de obtener empresas:', error);
    return [];
  }
};

// Función para obtener una empresa especifica
export const fetchEmpresa = async (fetchWithAuth, idEmpresa) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/empresas/${idEmpresa}`);
    if (response.ok) {
      const empresa = await response.json();
      return empresa;
    } else {
      console.error('Error al obtener las empresa');
      return [];
    }
  } catch (error) {
    console.error('Error en la petición de obtener empresa:', error);
    return [];
  }
};