const API_URL = process.env.REACT_APP_API_URL;

// Función para obtener la lista de módulos
export const fetchModulos = async (fetchWithAuth) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/modulos`);
    if (response.ok) {
      const modulos = await response.json();
      return modulos;
    } else {
      console.error('Error al obtener los módulos');
      return [];
    }
  } catch (error) {
    console.error("Error en la petición de obtener módulos:", error);
    return [];
  }
};

// Función para obtener la lista de módulos del usuario
export const fetchModulosUsuario = async (fetchWithAuth, idUsuario) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
    if (response.ok) {
      const modulosUsuario = await response.json();
      return modulosUsuario;
    } else {
      console.error('Error al obtener módulos del usuario');
      return [];
    }
  } catch (error) {
    console.error('Error en la petición de obtener módulos del usuario:', error);
    return [];
  }
};

// Función para obtener la lista de módulos de monitoreo
export const fetchModulosMonitoreo = async (fetchWithAuth, idUsuario) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/modulos/${idUsuario}/monitoreo`);
    if (response.ok) {
      const modulosMonitoreo = await response.json();
      return modulosMonitoreo;
    } else {
      console.error('Error al obtener módulos de monitoreo');
      return [];
    }
  } catch (error) {
    console.error('Error en la petición de obtener módulos de monitoreo:', error);
    return [];
  }
};

// Función para ejecutar el módulo
export const ejecutarModulo = async (moduleId, usuarioId, inventarioId, grupoId, hostIds, fetchWithAuth, extraVars = {}) => {
  const params = new URLSearchParams({
    usuarioId: usuarioId,
    inventarioId: inventarioId,
    grupoId: grupoId || '',
    hostId: hostIds.join(',')
  });

  const url = `${API_URL}/modulos/ejecutar/${moduleId}?${params}`;

  // Realizar petición
  try {
    const response = await fetchWithAuth(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        extra_vars: extraVars, // Se usa extraVars en lugar de un objeto vacío
        metadata: {
          origin: 'web-ui',
          timestamp: new Date().toISOString()
        }
      })
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData.message || 'Error desconocido');
    }

    return response;
  } catch (error) {
    throw new Error(`Error en ejecución: ${error.message}`);
  }
};


// Función para ejecutar el monitoreo
export const ejecutarMonitoreo = async (moduleId, usuarioId, inventarioId, grupoId, hostIds, fetchWithAuth) => {
  // Construir URL con parámetros codificados
  const params = new URLSearchParams({
    usuarioId: usuarioId,
    inventarioId: inventarioId,
    grupoId: grupoId || '',
    hostId: hostIds.join(',')
  });

  const url = `${API_URL}/modulos/ejecutar-periodicamente/${moduleId}?${params}`;

  // Realizar petición
  try {
    const response = await fetchWithAuth(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        extra_vars: {},
        metadata: {
          origin: 'web-ui',
          timestamp: new Date().toISOString()
        }
      })
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData.message || 'Error desconocido');
    }

    return response;
  } catch (error) {
    throw new Error(`Error en ejecución: ${error.message}`);
  }
};

// Función para detener el monitoreo
export const detenerMonitoreo = async (idUsuario, fetchWithAuth) => {
  return fetchWithAuth(
    `${API_URL}/modulos/detener-ejecucion-periodica/${idUsuario}`,
    { method: 'POST' }
  );
};