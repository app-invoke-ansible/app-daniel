const API_URL = process.env.REACT_APP_API_URL;

// Función para obtener la lista de usuarios
export const fetchUsuarios = async (fetchWithAuth) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/usuario`);
    if (response.ok) {
      const usuarios = await response.json();
      return usuarios;
    } else {
      console.error('Error al obtener los usuarios');
      return [];
    }
  } catch (error) {
    console.error("Error en la petición de obtener usuarios:", error);
    return [];
  }
};

// Función para crear un nuevo usuario
export const createUsuario = async (fetchWithAuth, userData, inventarioId) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/usuario?inventarioId=${inventarioId}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(userData)
      }
    );

    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(`Error ${response.status}: ${errorText}`);
    }

    return await response.json();
  } catch (error) {
    console.error('Error en createUsuario:', error);
    throw error;
  }
};

// Función para eliminar un usuario
export const deleteUsuario = async (fetchWithAuth, userId) => {
  try {
    const response = await fetchWithAuth(`${API_URL}/usuario/${userId}`,
      { method: 'DELETE' }
    );

    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(`Error ${response.status}: ${errorText}`);
    }

    return true;
  } catch (error) {
    console.error('Error en deleteUsuario:', error);
    throw error;
  }
};

// Función para eliminar un módulo de un usuario
export const deleteModulo = async (fetchWithAuth, userId, moduleId) => {
  try {
    const response = await fetchWithAuth(
      `${API_URL}/usuario/${userId}/modulos/${moduleId}`,
      { method: 'DELETE' }
    );

    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(`Error ${response.status}: ${errorText}`);
    }

    return true;
  } catch (error) {
    console.error('Error en deleteModulo:', error);
    throw error;
  }
};