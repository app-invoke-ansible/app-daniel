import Keycloak from 'keycloak-js';

const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
const REALM_API = process.env.REACT_APP_REALM_API;
const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;

const keycloakInstance = new Keycloak({
  url: KEYCLOAK_URL,
  realm: REALM_API,
  clientId: CLIENT_ID,
});

export default keycloakInstance;
