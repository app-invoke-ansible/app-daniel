import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend, ChartDataLabels);

function GraficasModulos({ executionData }) {
    const chartData = {
        labels: Object.keys(executionData),
        datasets: [
            {
                label: 'Número de ejecuciones',
                data: Object.values(executionData),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.8)',
                    'rgba(54, 162, 235, 0.8)',
                    'rgba(255, 206, 86, 0.8)',
                    'rgba(75, 192, 192, 0.8)',
                    'rgba(153, 102, 255, 0.8)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                ],
                borderWidth: 2,
                borderSkipped: false,
                barThickness: 40,
            },
        ],
    };

    const chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Ejecuciones por módulo',
                font: {
                    size: 20,
                    weight: 'bold'
                }
            },
            datalabels: {
                anchor: 'end',
                align: 'top',
                formatter: Math.round,
                font: {
                    weight: 'bold',
                    size: 14
                },
                color: '#333'
            }
        },
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    stepSize: 5,
                    font: {
                        size: 12
                    }
                },
                grid: {
                    display: true,
                    color: 'rgba(0, 0, 0, 0.1)'
                }
            },
            x: {
                ticks: {
                    font: {
                        size: 12
                    }
                }
            }
        },
        animation: {
            duration: 2000,
            easing: 'easeInOutQuart'
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        onHover: (event, chartElement) => {
            event.native.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
            if (chartElement[0]) {
                chartElement[0].element.options.borderWidth = 4;
                event.chart.update();
            }
        },
        onClick: (event, chartElement) => {
            if (chartElement[0]) {
                alert(`Módulo: ${chartData.labels[chartElement[0].index]}\nEjecuciones: ${chartData.datasets[0].data[chartElement[0].index]}`);
            }
        }
    };

    return (
        <div className="mt-8 p-4 bg-gradient-to-r from-white to-white  shadow-lg" style={{ height: '400px', width: '100%', margin: 'auto' }}>
            <Bar data={chartData} options={chartOptions} />
        </div>
    );
}

export default GraficasModulos;
