import React from 'react';
import { Line } from 'react-chartjs-2';

const LineChart = () => {
  const data = {
    labels: ['0', '02', '03', '04', '05', '06', '07', '08', '09', '1'],
    datasets: [
      {
        label: 'Total Earnings',
        data: [500, 700, 800, 600, 900, 700, 800, 1000, 600, 700],
        fill: false,
        backgroundColor: 'rgb(75, 192, 192)',
        borderColor: 'rgba(75, 192, 192, 0.2)',
        tension: 0.1,
      },
    ],
  };

  const options = {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  };

  return <Line data={data} options={options} />;
};

export default LineChart;
