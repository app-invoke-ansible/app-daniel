import React from 'react';
import React, { useState } from 'react';

const Formulario = () => {
    const [formData, setFormData] = useState({
        nombre: '',
        apellido: '',
        fechaCreacion: '',
        ultimaConexion: '',
        rut: '',
        genero: '',
        fechaNacimiento: '',
        correoElectronico: '',
        cargo: '',
        unidadAsignada: '',
        contrasena: '',
        confirmarContrasena: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Datos del formulario:', formData);
    };

    return (
        <div className="flex items-center justify-center min-h-screen bg-gray-100 p-4">
            <form onSubmit={handleSubmit} className="bg-white p-10 rounded-lg shadow-lg w-full max-w-6xl">
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">Nombre</label>
                        <input
                            type="text"
                            name="nombre"
                            value={formData.nombre}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Apellido</label>
                        <input
                            type="text"
                            name="apellido"
                            value={formData.apellido}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">Fecha de Creación</label>
                        <input
                            type="date"
                            name="fechaCreacion"
                            value={formData.fechaCreacion}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Última Conexión</label>
                        <input
                            type="date"
                            name="ultimaConexion"
                            value={formData.ultimaConexion}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">RUT</label>
                        <input
                            type="text"
                            name="rut"
                            value={formData.rut}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Género</label>
                        <input
                            type="text"
                            name="genero"
                            value={formData.genero}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">Fecha de Nacimiento</label>
                        <input
                            type="date"
                            name="fechaNacimiento"
                            value={formData.fechaNacimiento}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Correo Electrónico</label>
                        <input
                            type="email"
                            name="correoElectronico"
                            value={formData.correoElectronico}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">Cargo</label>
                        <input
                            type="text"
                            name="cargo"
                            value={formData.cargo}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Unidad Asignada</label>
                        <input
                            type="text"
                            name="unidadAsignada"
                            value={formData.unidadAsignada}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-6 mb-6">
                    <div>
                        <label className="block text-sm font-semibold mb-2">Contraseña</label>
                        <input
                            type="password"
                            name="contrasena"
                            value={formData.contrasena}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-semibold mb-2">Confirmar Contraseña</label>
                        <input
                            type="password"
                            name="confirmarContrasena"
                            value={formData.confirmarContrasena}
                            onChange={handleChange}
                            className="w-full p-3 border border-gray-300 rounded"
                        />
                    </div>
                </div>
                <button type="submit" className="w-full bg-blue-500 text-white py-3 rounded-lg hover:bg-blue-600">Guardar</button>
            </form>
        </div>
    );
};

export default Formulario;
