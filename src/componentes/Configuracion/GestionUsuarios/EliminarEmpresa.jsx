import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';

export default function EliminarEmpresa() {
    const { fetchWithAuth, user } = useAuth();
    const [usuario, setUsuario] = useState(null);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [loading, setLoading] = useState(false);

    const API_URL = process.env.REACT_APP_API_URL;
    const handleDelete = async () => {
        setLoading(true);
        try {
            const userResponse = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await userResponse.json();
            const usuarioLogueado = usuarios.find((u) => u.username === user.name);

            if (usuarioLogueado) {
                setUsuario(usuarioLogueado);
                setTipoUsuario(usuarioLogueado.tipoUsuario);
                console.log("id de empresa asociada al usuario", usuarioLogueado.idEmpresa);
            }

            const response = await fetchWithAuth(`${API_URL}/empresas/${usuarioLogueado.idEmpresa}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                alert('Empresa eliminada exitosamente.');
            } else {
                alert('Error al eliminar la empresa.');
            }
        } catch (error) {
            console.error('Error:', error);
            alert('Ocurrió un error al intentar eliminar la empresa.');
        } finally {
            setLoading(false);
            setShowModal(false);
        }
    };
    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (showModal) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [showModal]);

    return (
        <div>
            <button
                onClick={() => setShowModal(true)}
                className="bg-red-600 text-white hover:bg-red-700 block mt-2 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold "
            >
                Eliminar
            </button>

            {showModal && (
                <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center z-50">
                    <div className="bg-white rounded-lg shadow-lg p-6 sm:w-96 sm:h-auto w-full h-full">
                        <h2 className="text-lg font-semibold text-gray-700 mb-4">Confirmar Eliminación</h2>
                        <p className="text-gray-600 mb-6">Eliminar una empresa borrara todos los usuarios asociados a esta, ¿Estás seguro de que desea eliminar la empresa?</p>

                        <div className="flex justify-center gap-4 mt-4">
                            <button
                                type="button"
                                onClick={() => setShowModal(false)}
                                className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                disabled={loading}
                            >
                                Cancelar
                            </button>
                            <button
                                onClick={handleDelete}
                                className="px-4 py-2 bg-red-600 text-white hover:bg-red-500 border border-transparent hover:border-red-700 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                disabled={loading}
                            >
                                {loading ? 'Eliminando...' : 'Eliminar'}
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
