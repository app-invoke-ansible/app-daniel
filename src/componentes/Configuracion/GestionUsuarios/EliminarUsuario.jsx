import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';
import { LuTrash2, LuLoaderCircle } from 'react-icons/lu';
import { deleteUsuario } from '../../../funciones/funcionesUsuario';

const EliminarUsuario = ({ userId, onUserDelete }) => {
    const { fetchWithAuth } = useAuth();
    const [isOpen, setIsOpen] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    const handleOpen = () => setIsOpen(true);
    const handleClose = () => {
        setIsOpen(false);
    };

    const handleSubmit = async () => {
        setIsDeleting(true);
        try {
            await deleteUsuario(fetchWithAuth, userId);
            onUserDelete(userId);
            handleClose();
        } catch (error) {
            console.error('Error eliminando el usuario:', error);
        } finally {
            setIsDeleting(false);
        }
    };
    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    return (
        <div>
            <button
                className="text-sky-950 hover:text-white hover:bg-sky-950 rounded-full border border-sky-950 flex items-center justify-center w-7 h-7 transition-colors duration-300 transform hover:scale-105"
                onClick={handleOpen}
            >
                <LuTrash2 className="w-4 h-4" />
            </button>

            {isOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white w-full h-full sm:h-auto sm:w-11/12 sm:max-w-2xl p-4 sm:p-6 rounded-lg shadow-lg flex flex-col">
                        <h2 className="text-xl font-bold text-sky-950 text-center mb-2">Eliminar usuario</h2>
                        <p className="text-center text-sm">¿Estás seguro de que deseas eliminar este usuario?</p>
                        <div className="flex flex-row justify-center mt-4 gap-4">
                            <button
                                type="button"
                                onClick={handleClose}
                                className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105">
                                Cancelar
                            </button>
                            <button
                                onClick={handleSubmit}
                                disabled={isDeleting}
                                className="px-4 py-2 bg-red-600 text-white hover:bg-red-500 border border-transparent hover:border-red-700 rounded text-sm transition-colors duration-300 transform hover:scale-105"

                            >
                                {isDeleting ? 'Eliminando...' : 'Eliminar'}
                            </button>
                        </div>

                    </div>
                </div>
            )}
        </div>
    );
};

export default EliminarUsuario;