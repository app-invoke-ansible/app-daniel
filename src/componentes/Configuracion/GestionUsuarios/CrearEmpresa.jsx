import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';

const CrearEmpresa = ({ fetchData }) => {
    const { fetchWithAuth, user } = useAuth();
    const [usuario, setUsuario] = useState(null);
    const [isOpen, setIsOpen] = useState(false);
    const [formData, setFormData] = useState({
        nombre: '',
        rut: '',
        fechaCreacion: '',
        correoElectronico: '',
        actividad: ''
    });

    const API_URL = process.env.REACT_APP_API_URL;

    const [errorMessage, setErrorMessage] = useState({
        nombre: '',
        rut: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({ ...prevData, [name]: value.trim() }));

        if (name === 'nombre' || name === 'rut') {
            validateField(name, value);
        }
    };

    const validateField = (field, value) => {
        let error = '';

        if (field === 'nombre' && value.length < 3) {
            error = 'El nombre debe tener al menos 3 caracteres.';
        } else if (field === 'rut' && !/^\d{7,8}-[0-9kK]$/.test(value)) {
            error = 'El RUT debe tener un formato válido (ejemplo: 12345678-9).';
        }

        setErrorMessage((prev) => ({ ...prev, [field]: error }));
    };

    useEffect(() => {
        const fetchUsuarios = async () => {
            try {
                // Solicita todos los usuarios.
                const response = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await response.json();

                // Filtra el usuario que coincide con el name autenticado.
                const usuarioLogueado = usuarios.find((u) => u.username === user.name);

                setUsuario(usuarioLogueado);
                // console.log("Datos del usuario autenticado:",usuarioLogueado)
                console.log("Id extraida del usuario autenticado:", usuarioLogueado.id)
            } catch (error) {
                console.error("Error al obtener los datos del usuario:", error);
            }
        };

        fetchUsuarios();
    }, [user.name, fetchWithAuth]);

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            if (errorMessage.nombre || errorMessage.rut) {
                alert('Corrige los errores antes de agregar la empresa.');
                return;
            }

            const userId = usuario.id; // Obtener ID del usuario autenticado

            if (!userId) {
                alert('No se pudo determinar el ID del usuario.');
                return;
            }

            const response = await fetchWithAuth(`${API_URL}/empresas/crear/${userId}`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });

            if (response.ok) {
                alert('Empresa agregada exitosamente.');
                setFormData({
                    nombre: '',
                    rut: '',
                    fechaCreacion: '',
                    correoElectronico: '',
                    actividad: ''
                });
                fetchData();
                setIsOpen(false);
            } else {
                alert('Hubo un problema al agregar la empresa.', error.message);
            }
        } catch (error) {
            console.error('Error al agregar empresa:', error.message);
        }
    };

    const handleCancel = () => {
        setIsOpen(false);
        setFormData({
            nombre: '',
            rut: '',
            fechaCreacion: '',
            correoElectronico: '',
            actividad: ''
        });
        setErrorMessage({ nombre: '', rut: '' });
    };

    const handleOpen = () => {
        setIsOpen(true);
    };
    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    return (
        <div className="flex justify-center h-full">
            <button
                className="block mt-2 bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white"
                onClick={handleOpen}>
                Crear Empresa
            </button>
            {isOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white p-4 w-full h-full sm:h-auto sm:p-6 rounded-lg shadow-lg sm:w-11/12 sm:max-w-2xl">
                        <h2 className="text-xl font-bold mb-4 text-sky-950">Agregar Empresa</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="mb-4">
                                <label className="block text-m font-medium text-gray-700">Nombre</label>
                                <input
                                    type="text"
                                    name="nombre"
                                    value={formData.nombre}
                                    onChange={handleChange}
                                    className={`mt-1 p-1 border ${errorMessage.nombre ? 'border-red-500' : 'border-gray-300'} rounded-md w-full text-m`}
                                    required
                                />
                                {errorMessage.nombre && (
                                    <p className="text-red-600 text-sm mt-1">{errorMessage.nombre}</p>
                                )}
                            </div>
                            <div className="mb-4">
                                <label className="block text-m font-medium text-gray-700">RUT</label>
                                <input
                                    type="text"
                                    name="rut"
                                    value={formData.rut}
                                    onChange={handleChange}
                                    className={`mt-1 p-1 border ${errorMessage.rut ? 'border-red-500' : 'border-gray-300'} rounded-md w-full text-m`}
                                    required
                                />
                                {errorMessage.rut && (
                                    <p className="text-red-600 text-sm mt-1">{errorMessage.rut}</p>
                                )}
                            </div>
                            <div className="mb-4">
                                <label className="block text-m font-medium text-gray-700">Fecha de Creación</label>
                                <input
                                    type="date"
                                    name="fechaCreacion"
                                    value={formData.fechaCreacion}
                                    onChange={handleChange}
                                    max={new Date().toISOString().split("T")[0]}
                                    className="mt-1 p-1 border border-gray-300 rounded-md w-full text-m"
                                    required
                                />
                            </div>
                            <div className="mb-4">
                                <label className="block text-m font-medium text-gray-700">Correo Electrónico</label>
                                <input
                                    type="email"
                                    name="correoElectronico"
                                    value={formData.correoElectronico}
                                    onChange={handleChange}
                                    className="mt-1 p-1 border border-gray-300 rounded-md w-full text-m"
                                    required
                                />
                            </div>
                            <div className="mb-4">
                                <label className="block text-m font-medium text-gray-700">Actividad</label>
                                <input
                                    type="text"
                                    name="actividad"
                                    value={formData.actividad}
                                    onChange={handleChange}
                                    className="mt-1 p-1 border border-gray-300 rounded-md w-full text-m"
                                    required
                                />
                            </div>
                            <div className="flex justify-end">
                                <button
                                    type="button"
                                    className="mr-2 bg-gray-300 hover:bg-gray-400 rounded-md px-3 py-1 text-m"
                                    onClick={handleCancel}>
                                    Cancelar
                                </button>
                                <button
                                    type="submit"
                                    className="bg-sky-950 hover:bg-gray-600 rounded-md px-3 py-1 text-m text-white">
                                    Crear
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
};

export default CrearEmpresa;
