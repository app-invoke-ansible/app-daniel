import React, { useState, useEffect, useCallback } from 'react';
import Empresa from './Empresa';
import CrearEmpresa from './CrearEmpresa';
import { useAuth } from '../../../providers/AuthProvider';
import { fetchUsuarios } from '../../../funciones/funcionesUsuario';
import { fetchEmpresa } from '../../../funciones/funcionesEmpresa';

export default function ConfiguracionEmpresa() {
    const { fetchWithAuth, user } = useAuth();
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [usuario, setUsuario] = useState(null);
    const [loading, setLoading] = useState(true);
    const [formData, setFormData] = useState(null);
    const [hasEmpresa, setHasEmpresa] = useState(true);

    // Función para resetear todos los estados
    const resetStates = () => {
        setUsuario(null);
        setTipoUsuario(null);
        setFormData(null);
        setHasEmpresa(false);
    };

    // Función principal para cargar datos
    const fetchData = useCallback(async () => {
        setLoading(true);
        try {
            const usuarios = await fetchUsuarios(fetchWithAuth);
            const usuarioLogueado = usuarios.find((u) => u.username === user.name);

            if (!usuarioLogueado) {
                resetStates();
                return;
            }

            // Actualizar estados base del usuario
            setUsuario(usuarioLogueado);
            setTipoUsuario(usuarioLogueado.tipoUsuario);

            // Manejo de empresa asociada
            if (usuarioLogueado.idEmpresa) {
                setHasEmpresa(true);

                // Obtener datos de empresa
                const empresaData = await fetchEmpresa(
                    fetchWithAuth, usuarioLogueado.idEmpresa
                );

                // Actualizar datos solo si la respuesta es exitosa
                empresaData ? setFormData(empresaData) : setFormData(null);
            } else {
                // Usuario sin empresa asociada
                setHasEmpresa(false);
                setFormData(null);
            }
        } catch (error) {
            console.error('Error en carga de datos:', error);
            setFormData(null);
        } finally {
            setLoading(false);
        }
    }, [fetchWithAuth, user.name]);

    // Cargar datos al montar el componente
    useEffect(() => {
        fetchData();
    }, [fetchData]);

    return (
        <div className="w-full">
            <h1 className="text-xl font-bold text-center text-sky-950">Empresa</h1>

            {/* Mostrar formulario de creación solo para admins sin empresa */}
            {tipoUsuario === 'admin' && !usuario?.idEmpresa && (
                <div className="flex justify-center">
                    <CrearEmpresa fetchData={fetchData} />
                </div>
            )}

            {/* Componente principal con protección de carga */}
            <Empresa
                formData={formData}
                loading={loading}
                tipoUsuario={tipoUsuario}
                onActualizar={fetchData}
            />
        </div>
    );
}