import React, { useState, useEffect } from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { useAuth } from '../../../providers/AuthProvider';
import Loading from '../../Base/Loading';
import { LuPen, LuSave, LuX } from "react-icons/lu";
import EliminarEmpresa from './EliminarEmpresa';

export default function Empresa() {
    const { fetchWithAuth, user } = useAuth();
    const [usuario, setUsuario] = useState(null);
    const [formData, setFormData] = useState({
        nombre: '',
        rut: '',
        fechaCreacion: '',
        correoElectronico: '',
        actividad: ''
    });

    const [isEditable, setIsEditable] = useState(false);
    // const [eliminarVisible, setEliminarVisible] = useState(false);
    const { keycloak, initialized } = useKeycloak();
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
    const [loadingEmpresa, setLoadingEmpresa] = useState(true);
    const [hasChanges, setHasChanges] = useState(false);
    const [hasEmpresa, setHasEmpresa] = useState(false);

    const API_URL = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await response.json();
                const usuarioLogueado = usuarios.find((u) => u.username === user.name);

                if (usuarioLogueado) {
                    setUsuario(usuarioLogueado);
                    setTipoUsuario(usuarioLogueado.tipoUsuario);
                }

                if (usuarioLogueado && usuarioLogueado.idEmpresa) {
                    const empresaResponse = await fetchWithAuth(`${API_URL}/empresas/${usuarioLogueado.idEmpresa}`);
                    if (!empresaResponse.ok) throw new Error('Error al obtener los datos de la empresa');

                    const empresaData = await empresaResponse.json();
                    setFormData({
                        nombre: empresaData.nombre || 'N/A',
                        rut: empresaData.rut || 'N/A',
                        fechaCreacion: empresaData.fechaCreacion || 'N/A',
                        correoElectronico: empresaData.correoElectronico || 'N/A',
                        actividad: empresaData.actividad || 'N/A',
                    });
                    setHasEmpresa(true); // se habilita el hasEmpresa para mostrar editar y eliminar si existe empresa 
                } else {
                    setHasEmpresa(false);
                }

                setLoadingTipoUsuario(false);
                setLoadingEmpresa(false);
            } catch (error) {
                console.error('Error al obtener los datos:', error);
                setHasEmpresa(false);
            }
        };

        fetchData();
    }, [user.name, fetchWithAuth, API_URL]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });

        setHasChanges(true);
    };

    const handleEdit = () => {
        setIsEditable(true);
        // setEliminarVisible(true);
    };

    const handleCancel = () => {
        setIsEditable(false);
        setHasChanges(false);
        // setEliminarVisible(false);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetchWithAuth(`${API_URL}/empresas/${usuario.idEmpresa}`, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData),
            });

            if (!response.ok) throw new Error('Error al actualizar los datos de la empresa');

            const data = await response.json();
            console.log('Empresa actualizada con éxito:', data);
            setIsEditable(false);
            setHasChanges(false);
            // setEliminarVisible(false);
        } catch (error) {
            console.error('Error al actualizar los datos:', error);
        }
    };

    return (
        <div className="w-full max-w-full px-2 flex flex-col items-center">
            <main className="w-full flex justify-center items-center">
                <div className="relative w-full">
                    <form
                        onSubmit={handleSubmit}
                        className="bg-white p-2 rounded-lg w-full"
                    >
                        <div className="flex justify-end space-x-2">
                            {hasEmpresa && tipoUsuario !== 'visualizador' && !isEditable && (
                                <button
                                    type="button"
                                    onClick={handleEdit}
                                    className="bg-yellow-500 text-white rounded-full p-2 hover:bg-yellow-600"
                                >
                                    <LuPen size={15} />
                                </button>
                            )}
                            {isEditable && (
                                <>
                                    <button
                                        type="submit"
                                        disabled={!hasChanges}
                                        className="bg-sky-900 text-white rounded-full p-2 hover:bg-sky-700"
                                    >
                                        <LuSave size={20} />
                                    </button>
                                    <button
                                        type="button"
                                        onClick={handleCancel}
                                        className="bg-gray-500 text-white rounded-full p-2 hover:bg-gray-600"
                                    >
                                        <LuX size={20} />
                                    </button>
                                </>
                            )}
                        </div>

                        <div className="grid grid-cols-1 mb-2">
                            <div>
                                <label className="block text-sm font-semibold text-gray-700">Nombre</label>
                                <input
                                    type="text"
                                    name="nombre"
                                    value={formData.nombre}
                                    onChange={handleChange}
                                    disabled={!isEditable}
                                    className={`w-full px-3 py-2 border rounded text-xs  mb-2 ${isEditable ? 'border-gray-300 focus:ring focus:ring-sky-500' : 'bg-gray-200'}`}
                                />
                            </div>
                            <div>
                                <label className="block text-sm  font-semibold text-gray-700">RUT</label>
                                <input
                                    type="text"
                                    name="rut"
                                    value={formData.rut}
                                    onChange={handleChange}
                                    disabled={!isEditable}
                                    className={`w-full px-3 py-2 border rounded text-xs  mb-2 ${isEditable ? 'border-gray-300 focus:ring focus:ring-sky-500' : 'bg-gray-200'}`}
                                />
                            </div>
                        </div>

                        <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mb-2">
                            <div>
                                <label className="block text-sm font-semibold text-gray-700">Fecha de Creación</label>
                                <input
                                    type="date"
                                    name="fechaCreacion"
                                    value={formData.fechaCreacion}
                                    onChange={handleChange}
                                    disabled={!isEditable}
                                    max={new Date().toISOString().split("T")[0]}
                                    className={`w-full px-3 py-2 border rounded text-xs mb-2 ${isEditable ? 'border-gray-300 focus:ring focus:ring-sky-500' : 'bg-gray-200'}`}
                                />
                            </div>
                            <div>
                                <label className="block text-sm font-semibold text-gray-700">Correo Electrónico</label>
                                <input
                                    type="email"
                                    name="correoElectronico"
                                    value={formData.correoElectronico}
                                    onChange={handleChange}
                                    disabled={!isEditable}
                                    className={`w-full px-3 py-2 border rounded text-xs ${isEditable ? 'border-gray-300 focus:ring focus:ring-sky-500' : 'bg-gray-200'}`}
                                />
                            </div>
                        </div>

                        <div className="mb-2">
                            <label className="block text-sm font-semibold text-gray-700">Actividad</label>
                            <input
                                type="text"
                                name="actividad"
                                value={formData.actividad}
                                onChange={handleChange}
                                disabled={!isEditable}
                                className={`w-full px-3 py-2 border rounded text-xs ${isEditable ? 'border-gray-300 focus:ring focus:ring-sky-500' : 'bg-gray-200'}`}
                            />
                        </div>
                    </form>
                    <div className="flex items-center justify-center mt-2">
                        {hasEmpresa && (
                            <div>
                                <EliminarEmpresa />
                            </div>
                        )}
                    </div>
                </div>
            </main>
        </div>
    );
}
