import React, { useState, useEffect } from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { useAuth } from '../../../providers/AuthProvider';
import { LuEye, LuLoaderCircle } from 'react-icons/lu';
import Loading from '../../Base/Loading';

const VerUsuario = ({ usuario, userId }) => {
    const { fetchWithAuth } = useAuth();
    const [isOpen, setIsOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [userData, setUserData] = useState(null);
    const [userKeycloakData, setUserKeycloakData] = useState(null);

    const API_URL = process.env.REACT_APP_API_URL;
    const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
    const REALM_API = process.env.REACT_APP_REALM_API;

    const handleOpen = async () => {
        setIsOpen(true);
        setLoading(true);
        setError(null);

        try {
            // Llamada a la API local
            const userResponse = await fetchWithAuth(`${API_URL}/usuario/${userId}`);
            if (!userResponse.ok) throw new Error('Error al cargar los datos del usuario.');
            const userData = await userResponse.json();
            setUserData(userData);

            // Llamada a Keycloak
            const keycloakResponse = await fetchWithAuth(
                `${KEYCLOAK_URL}/admin/realms/${REALM_API}/users?username=${userData.username}`
            );
            if (!keycloakResponse.ok) throw new Error('Error al cargar los datos de Keycloak.');
            const keycloakData = await keycloakResponse.json();
            setUserKeycloakData(keycloakData[0]);
        } catch (err) {
            console.error('Error:', err.message);
            setError('Hubo un problema al cargar los datos. Por favor, inténtalo más tarde.');
        } finally {
            setLoading(false);
        }
    };

    const handleClose = () => {
        setIsOpen(false);
        setUserData(null);
        setUserKeycloakData(null);
        setError(null);
    };
    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);
    return (
        <div>
            <button
                className="text-sky-950 hover:text-white hover:bg-sky-950 rounded-full border border-sky-950 flex items-center justify-center w-7 h-7 transition-colors duration-300 transform hover:scale-105"
                onClick={handleOpen}
            >
                <LuEye className="w-4 h-4" />
            </button>

            {isOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white w-full h-full sm:h-auto sm:w-11/12 sm:max-w-2xl p-4 sm:p-6 rounded-lg shadow-lg flex flex-col overflow-y-auto">
                        <h2 className="text-xl font-bold text-sky-950 text-center mb-2">Detalles del usuario</h2>
                        {loading ? (
                            <Loading message="Cargando..." />
                        ) : error ? (
                            <p className="text-red-500 text-center">{error}</p>
                        ) : userData && userKeycloakData ? (
                            <div>
                                <p><strong>Nombre:</strong> {userData.nombre} {userData.apellido}</p>
                                <p><strong>Email:</strong> {userData.email}</p>
                                <p><strong>Estado:</strong> {userData.enabled ? 'Activo' : 'Inactivo'}</p>
                                <p>
                                    <strong>Fecha de creacion:</strong>{' '}
                                    {new Date(userKeycloakData.createdTimestamp).toLocaleString()}
                                </p>
                            </div>
                        ) : (
                            <p>No se encontraron datos del usuario.</p>
                        )}
                        <div className="flex justify-center mt-4">
                            <button
                                onClick={handleClose}
                                className="px-4 py-2 bg-sky-950 text-white hover:bg-gray-600 rounded"
                            >
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default VerUsuario;
