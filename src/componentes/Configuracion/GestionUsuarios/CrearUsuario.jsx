import React, { useState, useEffect, useMemo } from 'react';
import { useAuth } from '../../../providers/AuthProvider';
import { LuUserPlus, LuEye, LuEyeClosed, LuLoaderCircle } from 'react-icons/lu';
import { fetchUsuarios, createUsuario } from '../../../funciones/funcionesUsuario';
import { fetchEmpresas } from '../../../funciones/funcionesEmpresa';

const CrearUsuario = ({ onUserCreate, inventarioId, empresaId }) => {
  const { fetchWithAuth } = useAuth();
  const [isOpen, setIsOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState({});
  const [empresas, setEmpresas] = useState([]);
  const [usuarios, setUsuarios] = useState([]);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
  const [isCreating, setIsCreating] = useState(false);

  // Estado principal del formulario
  const [formData, setFormData] = useState({
    nombre: '',
    apellido: '',
    nombreUsuario: '',
    fechaCreacion: '',
    correoElectronico: '',
    cargo: '',
    tipoUsuario: '',
    unidadAsignada: '',
    contrasena: '',
    confirmarContrasena: '',
    idEmpresa: '',
  });

  // Obtener y actualizar datos iniciales
  useEffect(() => {
    const fetchDatosIniciales = async () => {
      try {
        const [usuariosData, empresasData] = await Promise.all([
          fetchUsuarios(fetchWithAuth),
          fetchEmpresas(fetchWithAuth)
        ]);
        setUsuarios(usuariosData);
        setEmpresas(empresasData);
      } catch (error) {
        console.error('Error cargando datos iniciales:', error);
      }
    };
    fetchDatosIniciales();
  }, [fetchWithAuth]);

  // Actualizar empresaId en el formulario
  useEffect(() => {
    if (empresaId) {
      setFormData(prev => ({ ...prev, idEmpresa: empresaId }));
    }
  }, [empresaId]);

  // Detectar cambios en el tamaño de pantalla
  useEffect(() => {
    const handleResize = () => setIsMobile(window.innerWidth < 768);
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isOpen]);

  // Memorizar el nombre de la empresa para optimizar renderizado
  const empresaNombre = useMemo(() =>
    empresas.find(e => e.id === formData.idEmpresa)?.nombre || "Empresa Desconocida",
    [empresas, formData.idEmpresa]
  );

  // Handlers para visibilidad de contraseñas
  const togglePasswordVisibility = () => setShowPassword(!showPassword);
  const toggleConfirmPasswordVisibility = () => setShowConfirmPassword(!showConfirmPassword);

  // Manejar apertura del modal con fecha actual
  const handleOpen = () => {
    setIsOpen(true);
    setFormData(prev => ({
      ...prev,
      fechaCreacion: new Date().toISOString().split('T')[0]
    }));
  };

  // Resetear estado al cerrar el modal
  const handleClose = () => {
    setIsOpen(false);
    setFormData({
      nombre: '',
      apellido: '',
      nombreUsuario: '',
      fechaCreacion: '',
      correoElectronico: '',
      cargo: '',
      tipoUsuario: '',
      unidadAsignada: '',
      contrasena: '',
      confirmarContrasena: '',
      idEmpresa: empresaId || '',
    });
    setErrorMessage({});
  };

  // Actualizar estado del formulario
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData(prev => ({ ...prev, [name]: value }));
  };

  // Validaciones del formulario
  const validateForm = () => {
    const errors = {};
    const requiredFields = [
      'nombre', 'apellido', 'nombreUsuario', 'correoElectronico',
      'cargo', 'tipoUsuario', 'unidadAsignada', 'contrasena', 'confirmarContrasena'
    ];

    requiredFields.forEach(field => {
      if (!formData[field]) errors[field] = 'Este campo es obligatorio.';
    });

    if (formData.contrasena !== formData.confirmarContrasena) {
      errors.confirmarContrasena = 'Las contraseñas no coinciden.';
    }

    if (usuarios.some(u => u.username === formData.nombreUsuario)) {
      errors.nombreUsuario = 'Este nombre de usuario ya existe.';
    }

    if (usuarios.some(u => u.email === formData.correoElectronico)) {
      errors.correoElectronico = 'Este email ya existe.';
    }

    setErrorMessage(errors);
    return Object.keys(errors).length === 0;
  };

  // Manejar envío del formulario
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!validateForm()) return;

    setIsCreating(true);

    const userData = {
      username: formData.nombreUsuario,
      enabled: true,
      email: formData.correoElectronico,
      firstName: formData.nombre,
      lastName: formData.apellido,
      realmRoles: [formData.tipoUsuario],
      ultimaConexion: new Date().toISOString().split('.')[0],
      cargo: formData.cargo,
      unidadAsignada: formData.unidadAsignada,
      idEmpresa: formData.idEmpresa ? parseInt(formData.idEmpresa, 10) : null,
      credentials: [{
        type: 'password',
        value: formData.contrasena,
        temporary: false,
      }],
    };

    try {
      // Crear usuario principal
      const newUser = await createUsuario(fetchWithAuth, userData, inventarioId);

      onUserCreate(newUser);
      handleClose();
    } catch (error) {
      console.error('Error en el proceso de creación:', error);
      setErrorMessage({
        general: error.message || 'Error al crear el usuario. Por favor intente nuevamente.'
      });
    } finally {
      setIsCreating(false);
    }
  };

  return (
    <div className="flex justify-center h-full">
      <button
        className="px-4 py-2 bg-sky-950 text-white hover:bg-sky-900 rounded text-xs flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105"
        onClick={handleOpen}
      >
        <LuUserPlus /> Crear Usuario
      </button>

      {isOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <div className="bg-white w-full h-full sm:h-auto sm:w-11/12 sm:max-w-2xl p-4 sm:p-6 rounded-lg shadow-lg flex flex-col overflow-y-auto">
            <h2 className="text-xl font-bold text-sky-950 text-center mb-2">Crear nuevo usuario</h2>
            <p className="text-center text-sm">Por favor, ingresa la información requerida para registrar un nuevo usuario.</p>
            <div className="flex-1">
              <form onSubmit={handleSubmit}>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 py-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Nombre</label>
                    <input
                      type="text"
                      name="nombre"
                      value={formData.nombre}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Ingrese su nombre"
                    />
                    {errorMessage.nombre && <p className="text-red-500 text-xs">{errorMessage.nombre}</p>}
                  </div>
                  <div>
                    <label className="block text-sm font-semibold mb-1">Apellido</label>
                    <input
                      type="text"
                      name="apellido"
                      value={formData.apellido}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Ingrese su apellido"
                    />
                    {errorMessage.apellido && <p className="text-red-500 text-xs">{errorMessage.apellido}</p>}
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 pb-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Nombre de Usuario</label>
                    <input
                      type="text"
                      name="nombreUsuario"
                      value={formData.nombreUsuario}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Ingrese nombre de usuario"
                    />
                    {errorMessage.nombreUsuario && <p className="text-red-500 text-xs">{errorMessage.nombreUsuario}</p>}
                  </div>
                  <div>
                    <label className="block text-sm font-semibold mb-1">Fecha de Creación</label>
                    <input
                      type="date"
                      name="fechaCreacion"
                      value={formData.fechaCreacion}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none cursor-not-allowed"
                      disabled
                    />
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 pb-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Correo Electrónico</label>
                    <input
                      type="email"
                      name="correoElectronico"
                      value={formData.correoElectronico}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Correo electrónico"
                    />
                    {errorMessage.correoElectronico && <p className="text-red-500 text-xs">{errorMessage.correoElectronico}</p>}
                  </div>
                  <div>
                    <label className="block text-sm font-semibold mb-1">Tipo de Usuario</label>
                    <select
                      name="tipoUsuario"
                      value={formData.tipoUsuario}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                    >
                      <option value="">Seleccione...</option>
                      <option value="admin">Administrador</option>
                      <option value="editor">Editor</option>
                      <option value="visualizador">Visualizador</option>
                    </select>
                    {errorMessage.tipoUsuario && <p className="text-red-500 text-xs">{errorMessage.tipoUsuario}</p>}
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 pb-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Cargo</label>
                    <input
                      type="text"
                      name="cargo"
                      value={formData.cargo}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Ingrese cargo"
                    />
                    {errorMessage.cargo && <p className="text-red-500 text-xs">{errorMessage.cargo}</p>}
                  </div>
                  <div>
                    <label className="block text-sm font-semibold mb-1">Unidad Asignada</label>
                    <input
                      type="text"
                      name="unidadAsignada"
                      value={formData.unidadAsignada}
                      onChange={handleChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder="Ingrese unidad asignada"
                    />
                    {errorMessage.unidadAsignada && <p className="text-red-500 text-xs">{errorMessage.unidadAsignada}</p>}
                  </div>
                </div>

                <div className="grid grid-cols-1 gap-4 pb-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Empresa</label>
                    <select
                      name="idEmpresa"
                      value={formData.idEmpresa || ''}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none cursor-not-allowed"
                      disabled
                    >
                      <option value={formData.idEmpresa}>{empresaNombre}</option>
                    </select>
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 pb-3">
                  <div>
                    <label className="block text-sm font-semibold mb-1">Contraseña</label>
                    <div className="relative">
                      <input
                        type={showPassword ? "text" : "password"}
                        name="contrasena"
                        value={formData.contrasena}
                        onChange={handleChange}
                        className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                        placeholder="Ingrese contraseña"
                      />
                      <button
                        type="button"
                        onClick={togglePasswordVisibility}
                        className="absolute inset-y-0 right-0 flex items-center pr-3 text-gray-500"
                      >
                        {showPassword ? <LuEyeClosed /> : <LuEye />}
                      </button>
                    </div>
                    {errorMessage.contrasena && <p className="text-red-500 text-xs">{errorMessage.contrasena}</p>}
                  </div>
                  <div>
                    <label className="block text-sm font-semibold mb-1">Confirmar Contraseña</label>
                    <div className="relative">
                      <input
                        type={showConfirmPassword ? "text" : "password"}
                        name="confirmarContrasena"
                        value={formData.confirmarContrasena}
                        onChange={handleChange}
                        className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                        placeholder="Confirme contraseña"
                      />
                      <button
                        type="button"
                        onClick={toggleConfirmPasswordVisibility}
                        className="absolute inset-y-0 right-0 flex items-center pr-3 text-gray-500"
                      >
                        {showConfirmPassword ? <LuEyeClosed /> : <LuEye />}
                      </button>
                    </div>
                    {errorMessage.confirmarContrasena && <p className="text-red-500 text-xs">{errorMessage.confirmarContrasena}</p>}
                  </div>
                </div>

                {/* Botones de acción */}
                <div className="flex flex-row justify-center mt-4 gap-4">
                  <button
                    type="button"
                    onClick={handleClose}
                    className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                  >
                    Cancelar
                  </button>
                  <button
                    type="submit"
                    disabled={isCreating}
                    className="px-4 py-2 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105"
                  >
                    {isCreating ? (
                      <>
                        <LuLoaderCircle className="animate-spin" />Creando...
                      </>
                    ) : (
                      <>
                        <LuUserPlus />Crear usuario
                      </>
                    )}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CrearUsuario;