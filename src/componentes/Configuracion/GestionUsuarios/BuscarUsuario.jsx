import React from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';

const BuscarUsuario = ({ searchTerm, onSearchChange }) => {
    const handleSearchChange = (e) => {
        onSearchChange(e.target.value); // Actualizar el término de búsqueda
    };

    return (
        <div className="flex items-center justify-end w-full px-4 sm:px-6">
            <div className="relative w-full max-w-xs">
                <input
                    type="search"
                    name="search"
                    id="searchUsuario"
                    className="pl-2 pr-10 block w-full border-b-2 border-sky-950 py-1.5 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
                    placeholder="Buscar usuario"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />
            </div>
        </div>
    );
};

export default BuscarUsuario;