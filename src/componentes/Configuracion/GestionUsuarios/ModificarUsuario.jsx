import React, { useState, useEffect } from "react";
import { LuPen } from "react-icons/lu";
import { useAuth } from "../../../providers/AuthProvider";

const ModificarUsuario = ({ usuario, onUpdate }) => {
    const { fetchWithAuth } = useAuth();
    const [isModalOpen, setModalOpen] = useState(false);
    const [modalData, setModalData] = useState(usuario);
    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState({});
    const [empresas, setEmpresas] = useState([]);
    const [usuarios, setUsuarios] = useState([]);
    const [initialUsuario, setInitialUsuario] = useState(usuario); // Guardar el usuario original

    const API_URL = process.env.REACT_APP_API_URL;
    const [isEnabled, setIsEnabled] = useState(usuario.enabled || false);
    const [isMobile, setIsMobile] = useState(window.innerWidth < 768);


    const fetchEmpresas = async () => {
        try {
            const response = await fetchWithAuth(`${API_URL}/empresas`);
            if (response.ok) {
                const data = await response.json();
                setEmpresas(data);
            } else {
                console.error("Error al obtener las empresas");
            }
        } catch (error) {
            console.error("Error en la petición de empresas:", error);
        }
    };
    useEffect(() => {
        const handleResize = () => {
            setIsMobile(window.innerWidth < 768);
        };

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);


    // Obtener la lista de usuarios
    useEffect(() => {
        const fetchUsuarios = async () => {
            try {
                const response = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await response.json();
                setUsuarios(usuarios);
                // console.log(usuarios)
            } catch (error) {
                console.error("Error al obtener los datos del usuario:", error);
            }
        };

        fetchUsuarios();
    }, [fetchWithAuth]);

    const openModal = () => setModalOpen(true);
    const closeModal = () => {
        setModalOpen(false);
        setErrorMessage({});
        setModalData(initialUsuario); // Restablecer modalData al estado inicial
        setIsEnabled(initialUsuario.enabled || false); // Restablecer el estado del toggle
    };

    const handleChange = ({ target: { name, value } }) => {
        setModalData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleIdEmpresaChange = (value) => {
        const selectedEmpresa = empresas.find(empresa => empresa.id === Number(value));
        setModalData((prevData) => ({
            ...prevData,
            idEmpresa: selectedEmpresa ? selectedEmpresa.id : null,
        }));
    };

    const handleToggle = () => setIsEnabled((prev) => !prev);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        setErrorMessage({});

        const errors = {};

        // Validar que el email no esté repetido (omitir si es el mismo email)
        const emailExistente = usuarios.find(
            (u) => u.email === modalData.email && u.id !== modalData.id
        );
        if (emailExistente) {
            errors.email = 'Este email ya está en uso.';
            setErrorMessage(errors); // Establece los errores para que se muestren
            setLoading(false); // Detener el loading
            return; // Detiene el envío del formulario
        }

        try {
            const bodyData = {
                username: modalData.username,
                enabled: isEnabled,
                email: modalData.email,
                firstName: modalData.nombre,
                lastName: modalData.apellido,
                realmRoles: [modalData.tipoUsuario],
                cargo: modalData.cargo,
                unidadAsignada: modalData.unidadAsignada,
                idEmpresa: modalData.idEmpresa,
                credentials: [

                ],
            };


            const response = await fetchWithAuth(`${API_URL}/usuario/${modalData.id}`, {
                method: "PUT",
                body: JSON.stringify(bodyData),
                headers: { "Content-Type": "application/json" },
            });

            if (response.ok) {
                const responseText = await response.text();
                const updatedUser = responseText ? JSON.parse(responseText) : {};
                onUpdate(updatedUser);
                closeModal();
            } else {
                const errorText = await response.text();
                console.error("Error del servidor:", errorText);
                setErrorMessage({ general: errorText || "Error al actualizar el usuario." });
            }
        } catch (error) {
            console.error("Error al actualizar el usuario:", error.message);
            setErrorMessage({ general: "Error de red. Intente nuevamente." });
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        setModalData(usuario);
        setIsEnabled(usuario.enabled || false);
        fetchEmpresas();
    }, [usuario]);

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isModalOpen]);

    return (
        <div className="flex flex-col items-center justify-center">
            <button
                onClick={openModal}
                className="text-sky-950 hover:text-white hover:bg-sky-950 rounded-full border border-sky-950 flex items-center justify-center w-7 h-7 transition-colors duration-300 transform hover:scale-105"
            >
                <LuPen className="w-4 h-4" />
            </button>
            {isModalOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50 overflow-y-auto">
                    <div className="bg-white p-4 rounded-lg shadow-lg sm:w-11/12 sm:max-w-2xl w-full h-full sm:h-auto sm:p-6 ">
                        <div className="flex justify-between mb-4">
                            <h2 className="text-xl font-bold text-sky-950">Modificar Usuario</h2>
                            <div className="flex items-center">
                                <h2 className="text-sm font-bold text-sky-950 mr-3">Estado</h2>
                                <button
                                    type="button"
                                    onClick={handleToggle}
                                    className={`w-12 h-6 flex items-center rounded-full p-1 duration-300 ease-in-out ${isEnabled ? 'bg-green-400' : 'bg-red-500'}`}
                                >
                                    <div className={`bg-white w-4 h-4 rounded-full shadow-md transform duration-300 ease-in-out ${isEnabled ? 'translate-x-6' : ''}`}></div>
                                </button>
                                <span className="ml-2 text-gray-700 text-sm">{isEnabled ? 'ON' : 'OFF'}</span>
                            </div>
                        </div>
                        <div>
                            <form onSubmit={handleSubmit}>
                                <div className="grid grid-cols-1 sm:grid-cols-2 gap-2">
                                    {["nombre", "apellido", "cargo", "unidadAsignada"].map((field, index) => (
                                        <div className="mb-4" key={index}>
                                            <label className="block text-sm text-left font-semibold capitalize mb-1">{field}</label>
                                            <input
                                                type="text"
                                                name={field}
                                                value={modalData[field] || ""}
                                                onChange={handleChange}
                                                placeholder={`Ej: ${field}`}
                                                className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                                            />
                                        </div>
                                    ))}
                                    <div className="mb-4">
                                        <label className="block text-sm text-left font-semibold capitalize mb-1">Tipo de Usuario</label>
                                        <select
                                            name="tipoUsuario"
                                            value={modalData.tipoUsuario || ""}
                                            onChange={handleChange}
                                            className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                                            required
                                        >
                                            <option value="">Seleccione un tipo</option>
                                            <option value="admin">Admin</option>
                                            <option value="editor">Editor</option>
                                            <option value="visualizador">Visualizador</option>
                                        </select>
                                    </div>
                                    <div className="mb-4">
                                        <label className="block text-sm text-left font-semibold capitalize mb-1">Email</label>
                                        <input
                                            type="email"
                                            name="email"
                                            value={modalData.email || ""}
                                            onChange={handleChange}
                                            placeholder="Ej: email@example.com"
                                            className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                                            required
                                        />
                                        {errorMessage.email && (
                                            <p className="text-red-500 text-xs mt-1">{errorMessage.email}</p>
                                        )}
                                    </div>
                                    <div className="mb-4">
                                        <label className="block text-sm text-left font-semibold capitalize mb-1">Empresa</label>
                                        <select
                                            name="idEmpresa"
                                            value={modalData.idEmpresa || ''}
                                            className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none cursor-not-allowed"
                                            disabled
                                        >
                                            {modalData.idEmpresa ? (
                                                <option value={modalData.idEmpresa}>
                                                    {empresas?.find(empresa => empresa.id === modalData.idEmpresa)?.nombre || "Empresa Desconocida"}
                                                </option>
                                            ) : (
                                                <option value="" disabled>No hay empresa asociada</option>
                                            )}
                                        </select>
                                    </div>
                                </div>
                                {errorMessage.general && (
                                    <p className="text-red-500 text-xs mt-2">{errorMessage.general}</p>
                                )}
                                <div className="flex justify-center gap-2 mt-4">
                                    <button
                                        type="button"
                                        className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                        onClick={closeModal}
                                    >
                                        Cancelar
                                    </button>
                                    <button
                                        type="submit"
                                        className={`px-4 py-2 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105 ${loading ? "bg-gray-400 cursor-not-allowed" : "bg-sky-950 hover:bg-gray-600"}`}
                                        disabled={loading}
                                    >
                                        {loading ? "Actualizando..." : "Actualizar"}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default ModificarUsuario;