import React, { useState, useEffect } from 'react';
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io';
import { useAuth } from '../../providers/AuthProvider';
import { useKeycloak } from '@react-keycloak/web';
import Loading from '../Base/Loading';

export default function MisNotificaciones() {
  const [isOn, setIsOn] = useState({});
  const [expandedChannel, setExpandedChannel] = useState(null);
  const [modulos, setModulos] = useState([]);
  const [selectedModules, setSelectedModules] = useState({});
  const [usuariosPorModulo, setUsuariosPorModulo] = useState({});
  const [loading, setLoading] = useState(false);
  const [usuarioAutenticado, setUsuarioAutenticado] = useState(null);

  const { initialized } = useKeycloak();
  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;

  const channels = {
    email: 'Correo Electrónico',
    whatsapp: 'WhatsApp',
    teams: 'Microsoft Teams',
    slack: 'Slack',
    sms: 'SMS',
    pushNotifications: 'Notificaciones Push',
  };

  useEffect(() => {
    if (initialized) {
      fetchModulosYUsuarios();
    }
  }, [initialized]);

  const fetchModulosYUsuarios = async () => {
    setLoading(true);
    try {
      // Obtener datos del usuario
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuariosData = await responseUsuarios.json();
      const usuarioAutenticado = usuariosData.find((u) => u.username === user.name);
      setUsuarioAutenticado(usuarioAutenticado);

      const isAdminUser = usuarioAutenticado && usuarioAutenticado.rol === 'admin';

      // Obtener estado del correo del usuario en el módulo
      const modulosUsuarioResponse = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
      const modulosUsuarioData = await modulosUsuarioResponse.json();

      const initialIsOn = {};

      modulosUsuarioData.modulos.forEach(modulo => {
        initialIsOn[modulo.id] = modulo.estadoCorreo === 1;
      });

      //console.log(initialIsOn);
      setIsOn(initialIsOn);

      const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
      const modulosData = await responseModulos.json();

      let modulosFiltrados = modulosData;
      if (!isAdminUser) {
        const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
        const modulosUsuarioData = await responseModulosUsuario.json();
        if (modulosUsuarioData && modulosUsuarioData.modulos) {
          modulosFiltrados = modulosData.filter(modulo => modulosUsuarioData.modulos.some(mod => mod.id === modulo.id));
        } else {
          modulosFiltrados = [];
        }
      }

      setModulos(modulosFiltrados);
      const initialSelected = {};
      modulosFiltrados.forEach(modulo => {
        initialSelected[modulo.id] = false;
      });
      setSelectedModules(initialSelected);

      const usuariosPorModuloInicial = {};
      modulosData.forEach(modulo => {
        usuariosPorModuloInicial[modulo.id] = [];
      });

      for (const usuario of usuariosData) {
        const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuario.id}/modulos`);
        const modulosUsuarioData = await responseModulosUsuario.json();

        if (modulosUsuarioData && modulosUsuarioData.modulos) {
          modulosUsuarioData.modulos.forEach(modulo => {
            if (usuariosPorModuloInicial[modulo.id]) {
              usuariosPorModuloInicial[modulo.id].push(usuario);
            }
          });
        }
      }
      setUsuariosPorModulo(usuariosPorModuloInicial);

    } catch (err) {
      console.error("Error al obtener los datos:", err);
    } finally {
      setLoading(false);
    }
  };

  const handleChannelToggle = async (channel) => {
    //console.log("Ejecutando handleChannelToggle para el canal:", channel);

    // Determinar si el canal está activado o desactivado
    const newEstadoCorreo = !isOn[channel] ? 1 : 0;
    //console.log("Nuevo estado de correo:", newEstadoCorreo);

    // Actualizar el estado
    setIsOn((prevState) => {
      const newState = { ...prevState };
      newState[channel] = !prevState[channel];

      // Actualizar el estado de cada módulo en base al estado del canal
      modulos.forEach(modulo => {
        newState[modulo.id] = newEstadoCorreo === 1;
      });

      return newState;
    });

    // Enviar la solicitud PUT al backend para actualizar todos los módulos
    try {

      await Promise.all(
        modulos.map(async (modulo) => {
          const response = await fetchWithAuth(
            `${API_URL}/usuario/modulos/${modulo.id}/estado-correo?estadoCorreo=${newEstadoCorreo}&usuarioId=${usuarioAutenticado.id}`,
            { method: 'PUT' }
          );

        })
      );

    } catch (error) {
      console.error("Error al hacer la solicitud de actualización:", error);
    }
  };


  const handleToggle = async (moduloId) => {

    const newEstadoCorreo = isOn[moduloId] ? 0 : 1;

    setIsOn((prevState) => ({
      ...prevState,
      [moduloId]: newEstadoCorreo === 1,
    }));

    // Enviar la solicitud PUT al backend para actualizar el módulo específico
    try {
      const response = await fetchWithAuth(
        `${API_URL}/usuario/modulos/${moduloId}/estado-correo?estadoCorreo=${newEstadoCorreo}&usuarioId=${usuarioAutenticado.id}`,
        { method: 'PUT' }
      );

      if (response.ok) {
        //console.log(`Estado del módulo ${moduloId} actualizado correctamente.`);
      } else {
        console.error(`Error al actualizar el estado del módulo ${moduloId}.`);
      }
    } catch (error) {
      console.error("Error al hacer la solicitud de actualización:", error);
    }
  };

  const handleChannelExpand = (channel) => {
    if (channel !== 'email') {
      alert("Funcionalidad no implementada para este canal");
    } else {
      setExpandedChannel((prev) => (prev === channel ? null : channel));
    }
  };

  return (
    <div className="flex flex-col">
      <h1 className="text-xl font-bold text-center text-sky-950 pb-4">Notificaciones</h1>
      {Object.keys(channels).map((channel) => (
        <div key={channel} className="mb-2">
          {/* PRIMERA FILA */}
          <div className="flex items-center px-2">
            <div className="bg-sky-950 h-12 p-2 flex items-center justify-center rounded-tl-xl rounded-bl-xl">
              {/* Toggle */}
              <button
                onClick={() => handleChannelToggle(channel)}
                className={`w-12 h-6 flex items-center bg-gray-300 rounded-full p-1 duration-300 ease-in-out ${isOn[channel] ? 'bg-green-400' : 'bg-gray-300'}`}
              >
                <div
                  className={`bg-white w-4 h-4 rounded-full shadow-md transform duration-300 ease-in-out ${isOn[channel] ? 'translate-x-6' : ''}`}
                ></div>
              </button>
            </div>

            <div
              className="flex-1 bg-sky-950 h-12 flex items-center justify-between px-4 rounded-tr-xl rounded-br-xl"
              onClick={() => handleChannelExpand(channel)}
            >
              <span className="text-white">{channels[channel]}</span>

              <button
                onClick={(e) => {
                  e.stopPropagation();
                  handleChannelExpand(channel);
                }}
                className={`bg-white border border-white rounded-full p-1 text-sky-950 hover:text-sky-800 shadow-md ${channel !== 'email' ? 'cursor-not-allowed' : ''}`}
                disabled={channel !== 'email'}
              >
                {expandedChannel === channel ? (
                  <IoIosArrowUp size={20} />
                ) : (
                  <IoIosArrowDown size={20} />
                )}
              </button>
            </div>
          </div>

          {/* SEGUNDA FILA */}
          {expandedChannel === channel && channel === 'email' && (
            <div className="bg-gray-200 border border-gray-300 rounded-xl mx-2">
              <div className="overflow-y-auto max-h-96">
                <ul className="divide-y divide-gray-300">
                  {loading ? (
                    <Loading message="Cargando módulos..." />
                  ) : Array.isArray(modulos) && modulos.length > 0 ? (
                    modulos.map((modulo, index) => (
                      <li
                        key={modulo.id}
                        className={`flex items-center pl-6 py-2 x-2`}
                      >
                        <button
                          onClick={() => handleToggle(modulo.id, modulo.estadoCorreo)}
                          className={`w-12 h-6 flex items-center rounded-full p-1 mx-2 duration-300 ease-in-out ${isOn[modulo.id] ? 'bg-green-400' : 'bg-gray-300'}`}
                        >
                          <div
                            className={`bg-white w-4 h-4 rounded-full shadow-md transform duration-300 ease-in-out ${isOn[modulo.id] ? 'translate-x-6' : ''}`}
                          ></div>
                        </button>
                        <span className="text-sm font-medium">{modulo.name}</span>
                      </li>
                    ))
                  ) : (
                    <li className="text-center py-2">No hay módulos disponibles.</li>
                  )}
                </ul>
              </div>
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
