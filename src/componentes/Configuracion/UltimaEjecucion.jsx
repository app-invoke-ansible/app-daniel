import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import CategoriaImagen from '../Base/CategoriaImagen';

const UltimaEjecucion = ({ usuarioId }) => {
  const [ejecuciones, setEjecuciones] = useState(null);
  const [loading, setLoading] = useState(true);
  const API_URL = process.env.REACT_APP_API_URL;
  const { fetchWithAuth, user } = useAuth();

  useEffect(() => {
    const fetchEjecuciones = async () => {
      try {
        setLoading(true);
        const response = await fetchWithAuth(`${API_URL}/ejecuciones/ultimas/${usuarioId}`);

        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
          const data = await response.json();
          const sortedData = Array.isArray(data)
            ? [...data].sort((a, b) => new Date(b.fechaHoraLanzamiento) - new Date(a.fechaHoraLanzamiento))
            : [];
          setEjecuciones(sortedData[0] || null);
        } else {
          throw new Error('El formato de la respuesta no es JSON');
        }
      } catch (error) {
        console.error('Error al obtener las últimas ejecuciones:', error);
        setEjecuciones(null);
      } finally {
        setLoading(false);
      }
    };

    if (usuarioId) {
      fetchEjecuciones();
    }
  }, [usuarioId]);

  const noEjecuciones = !ejecuciones || ejecuciones === 0 || (Array.isArray(ejecuciones) && ejecuciones.length === 0);

  return (
    <div className="p-4">
      <h1 className="text-base text-sky-950 font-bold text-center mb-2">Última Ejecución</h1>
      {noEjecuciones ? (
        <p className='text-xs'>No hay ejecuciones disponibles.</p>
      ) : (
        <div className="border p-4 rounded-lg flex border-gray-300 rounded-xl">
          <div className="w-1/4 flex justify-center items-center">
            <CategoriaImagen categoria={ejecuciones.categoria} />
          </div>

          <div className="w-3/4 flex flex-col">
            <div className="flex flex-col">
              <h6 className="text-xs text-gray-800">{ejecuciones.categoria}</h6>
              <h2 className="text-sm text-sky-900 font-bold">{ejecuciones.modulo}</h2>
              <h6 className="text-xs text-gray-800">Fecha de Lanzamiento:<br /><strong>{new Date(ejecuciones.fechaHoraLanzamiento).toLocaleString()}</strong></h6>
              <h6 className="text-xs text-gray-800">Duración:<strong> {ejecuciones.duracion}</strong> </h6>
              <h6 className="text-xs text-gray-800">Sistema:<strong> {ejecuciones.sistema}</strong> </h6>
              <h6 className="text-xs text-gray-800">Estado:<strong> {ejecuciones.estado}</strong> </h6>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default UltimaEjecucion;
