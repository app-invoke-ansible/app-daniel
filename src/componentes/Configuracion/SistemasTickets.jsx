import React, { useState } from "react";
import ServiceNow from "../../img/tickets/ServiceNow.png";
import JiraService from "../../img/tickets/Jira.png";
import Zendesk from "../../img/tickets/Zendesk.png";

const SistemaTickets = () => {
  const [selectedSystem, setSelectedSystem] = useState(null);
  const [formData, setFormData] = useState({
    serviceURL: "",
    apiKey: "",
    instanceURL: "",
    username: "",
    password: ""
  });

  const systems = [
    {
      name: "ServiceNow",
      img: ServiceNow,
      fields: [
        { name: "instanceURL", label: "URL de instancia", type: "text" },
        { name: "username", label: "Usuario", type: "text" },
        { name: "password", label: "Contraseña", type: "password" }
      ]
    },
    {
      name: "Jira",
      img: JiraService,
      fields: [
        { name: "serviceURL", label: "URL del servicio", type: "text" },
        { name: "apiKey", label: "Clave API", type: "password" },
        { name: "email", label: "Correo electrónico del usuario Jira", description: "(Usuario que creó la API Key)", type: "text" }
      ]
    },
    {
      name: "Zendesk",
      img: Zendesk,
      fields: [
        { name: "serviceURL", label: "Subdominio", type: "text" },
        { name: "apiKey", label: "Token API", type: "password" },
        { name: "email", label: "Correo electrónico del usuario Zendesk", description: "(Usuario que creó la API Key)", type: "text" }
      ]
    }
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Datos guardados:", formData);
    // Lógica para guardar la configuración
  };

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  return (
    <div className="flex flex-col">
      <h1 className="text-xl font-bold text-center text-sky-950 pb-4">Sistema de Tickets</h1>
      <div className="flex flex-row gap-8">
        {/* Columna izquierda para las tarjetas */}
        <div className="w-1/3 flex flex-col border-l border-gray-200 pl-8">

          <div className="flex flex-col gap-2">
            {systems.map((system) => (
              <div
                key={system.name}
                onClick={() => setSelectedSystem(system)}
                className={`cursor-pointer p-4 border-2 rounded-lg transition-all ${selectedSystem?.name === system.name
                  ? "border-sky-900 bg-sky-50"
                  : "border-gray-200 hover:border-sky-300"
                  }`}
              >
                <div className="flex flex-col items-center">
                  <img
                    src={system.img}
                    alt={system.name}
                    className="h-8 w-8 mb-3"
                  />
                  <span className="text-xs font-medium text-sky-900">
                    {system.name}
                  </span>
                </div>
              </div>
            ))}
          </div>
        </div>

        {/* Columna derecha para el formulario */}
        <div className="w-2/3 pr-8">
          {selectedSystem ? (
            <div className="max-w-xl">
              <h2 className="text-sm text-center font-semibold text-sky-900 mb-2">
                Configurar {selectedSystem.name}
              </h2>
              <form onSubmit={handleSubmit} className="space-y-4">
                {selectedSystem.fields.map((field) => (
                  <div>
                    <div key={field.name} className="flex items-center gap-2">
                      <label className="text-sm font-semibold">{field.label}</label>
                      <p className="text-gray-400 italic text-xs">{field.description}</p>
                    </div>
                    <input
                      type={field.type}
                      name={field.name}
                      value={formData[field.name]}
                      onChange={handleInputChange}
                      className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
                      placeholder={`Ingrese ${field.label.toLowerCase()}`}
                      required
                    />
                  </div>
                ))}
                <div className="flex justify-center mt-8">
                  <button
                    type="submit"
                    className="bg-sky-900 text-xs text-white px-6 py-2 rounded-md hover:bg-sky-800 transition-colors"
                  >
                    Guardar Configuración
                  </button>
                </div>
              </form>
            </div>
          ) : (
            <div className="text-gray-500 text-center flex items-center justify-center">
              <p>Seleccione un sistema de tickets para configurar</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SistemaTickets;