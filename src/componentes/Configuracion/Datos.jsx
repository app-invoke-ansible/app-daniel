import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { LuPencil, LuCheck, LuX } from 'react-icons/lu';
import { FaKey } from "react-icons/fa";
import CambiarContrasena from '../../componentes/Configuracion/CambiarContrasena';
import fotoperfil from '../../img/perfil/fotoPerfil.jpg';
import { LuLoaderCircle } from "react-icons/lu";
import Loading from '../Base/Loading';
import UltimaEjecucion from './UltimaEjecucion';
import { TbRubberStampOff } from 'react-icons/tb';

export default function Datos() {
    const { isAuthenticated, user, fetchWithAuth } = useAuth();
    const [isEditable, setIsEditable] = useState(false);
    const [usuario, setUsuario] = useState(null);
    const [usuarioId, setUsuarioId] = useState(null);
    const [usuarioKeyloack, setUsuarioKeyloack] = useState(null);
    const [empresa, setEmpresa] = useState(null);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
    const [formData, setFormData] = useState({
        usuario: '',
        nombre: '',
        apellido: '',
        fechaCreacion: '',
        ultimaConexion: '',
        correoElectronico: '',
        cargo: '',
        unidadAsignada: '',
        empresa: '',
    });
    const [originalFormData, setOriginalFormData] = useState({ ...formData });
    const [loading, setLoading] = useState(true);
    const API_URL = process.env.REACT_APP_API_URL;
    const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
    const REALM_API = process.env.REACT_APP_REALM_API;

    useEffect(() => {
        if (isAuthenticated && user) {
            const fetchUserData = async () => {
                setLoading(true);
                try {
                    const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                    const usuarios = await responseUsuarios.json();
                    const usuarioAutenticado = usuarios.find((u) => u.username === user?.name);
                    if (!usuarioAutenticado) throw new Error('Usuario no encontrado en la API');

                    const responseKeyloack = await fetchWithAuth(`${KEYCLOAK_URL}/admin/realms/${REALM_API}/users?username=${usuarioAutenticado.username}`);
                    const keyloackData = await responseKeyloack.json();
                    if (responseKeyloack.ok) {
                        setUsuarioKeyloack(keyloackData[0]);
                    }

                    if (usuarioAutenticado.idEmpresa) {
                        try {
                            const empresaResponse = await fetchWithAuth(`${API_URL}/empresas/${usuarioAutenticado.idEmpresa}`);
                            if (!empresaResponse.ok) {
                                throw new Error('Error al obtener los datos de la empresa');
                            }
                            const empresaData = await empresaResponse.json();
                            setEmpresa(empresaData);
                        } catch (error) {
                            console.error('Error al obtener los datos de la empresa:', error);
                            setEmpresa(null);
                        }
                    } else {
                        setEmpresa(null);
                    }

                    setUsuarioId(usuarioAutenticado.id);
                    setUsuario(usuarioAutenticado);
                    setTipoUsuario(usuarioAutenticado.tipoUsuario || 'guest');


                } catch (error) {
                    console.error('Error al obtener datos de la API:', error);
                } finally {
                    setLoadingTipoUsuario(false);
                    setLoading(false);
                }
            };

            fetchUserData();
        }

    }, [isAuthenticated, user, fetchWithAuth]);


    const convertirTimestamp = (timestamp) => {
        const date = new Date(timestamp);
        return date.toLocaleString();
    };


    useEffect(() => {
        if (usuario && usuarioKeyloack) {
            setLoading(true);
            setFormData({
                usuario: usuario.username || '',
                nombre: usuario.nombre || '',
                apellido: usuario.apellido || '',
                correoElectronico: usuario.email || '',
                cargo: usuario.cargo || '',
                unidadAsignada: usuario.unidadAsignada || '',
                fechaCreacion: usuarioKeyloack.createdTimestamp ? convertirTimestamp(usuarioKeyloack.createdTimestamp) : 'N/A',
                ultimaConexion: usuario.lastAccess ? convertirTimestamp(usuario.lastAccess) : 'N/A',
                empresa: empresa ? (empresa.nombre || 'No hay empresa asociada') : 'No hay empresa asociada',
            });
            setLoading(false);
        }

    }, [usuario, usuarioKeyloack, empresa]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleEditClick = () => {
        setIsEditable(true);
        setOriginalFormData({ ...formData });
    };

    const handleCancelClick = () => {
        setIsEditable(false);
        setFormData(originalFormData);
    };

    const handleSaveClick = async (e) => {
        setLoading(true);
        e.preventDefault();

        try {
            const bodyData = {
                username: formData.usuario,
                enabled: true,
                email: formData.correoElectronico,
                firstName: formData.nombre,
                lastName: formData.apellido,
                realmRoles: [tipoUsuario],
                cargo: formData.cargo,
                unidadAsignada: formData.unidadAsignada,
                idEmpresa: usuario.idEmpresa ? usuario.idEmpresa : null,
                credentials: [
                ],
            };

            const response = await fetchWithAuth(`${API_URL}/usuario/${usuario.id}`, {
                method: "PUT",
                body: JSON.stringify(bodyData),
                headers: { "Content-Type": "application/json" },
            });

            if (response.ok) {
                console.log('Usuario actualizado correctamente.');
                setOriginalFormData({ ...formData });
            } else {
                const errorText = await response.text();
                console.error("Error del servidor:", errorText);
            }
        } catch (error) {
            console.error("Error al actualizar el usuario:", error.message);
        } finally {
            setIsEditable(false);
            setLoading(false);
        }
    };

    if (loading) {
        return <Loading message="Cargando Datos..." />;
    }

    return (
        <div className="flex flex-col sm:flex-row items-center space-y-4 sm:space-y-0 sm:space-x-4 border border-gray-300 min-h-full">
            {/* PRIMERA COLUMNA */}
            <div className="w-full sm:w-1/3 text-center p-6 mb-4 sm:mb-0 flex flex-col justify-between min-h-full border-r border-gray-300">
                <div className="flex-grow">
                    <img
                        className="h-20 w-auto rounded-full mx-auto mb-2"
                        src={fotoperfil}
                        alt="Foto de perfil"
                    />
                    <div className="mb-1 text-lg text-sky-950 font-bold">{formData.usuario}</div>
                    <div className="mb-1 text-md text-gray-800 font-semibold">{formData.nombre} {formData.apellido}</div>
                    <div className="mb-1 text-xs text-gray-500">{formData.empresa}</div>
                    <div className="text-sm text-sky-600">Última conexión: <br/><strong>{formData.ultimaConexion}</strong></div>
                    <UltimaEjecucion usuarioId={usuarioId}/>
                </div>
            </div>

            {/* SEGUNDA COLUMNA */}
            <div className="w-full sm:w-2/3 p-6 flex flex-col justify-between min-h-full">
                <form onSubmit={handleSaveClick} className="bg-white w-full">
                    <div className="relative mb-4">
                        <h2 className="text-xl text-sky-950 font-bold text-center">Mis datos</h2>
                        {loadingTipoUsuario ? (
                            <div></div>
                        ) : (
                            (tipoUsuario === 'admin' || tipoUsuario === 'editor') && (
                                !isEditable ? (
                                    <button
                                        type="button"
                                        onClick={handleEditClick}
                                        className={`absolute top-0 right-0 h-8 w-8 text-white rounded-full flex items-center justify-center bg-orange-500 hover:bg-orange-600`}
                                    >
                                        <LuPencil className="h-5 w-5" />
                                    </button>
                                ) : (
                                    <div className="absolute top-0 right-0 flex space-x-2">
                                        <button
                                            type="submit"
                                            className="h-8 w-8 text-white rounded-full flex items-center justify-center bg-sky-950 hover:bg-sky-700"
                                        >
                                            <LuCheck className="h-5 w-5" />
                                        </button>
                                        <button
                                            type="button"
                                            onClick={handleCancelClick}
                                            className="h-8 w-8 text-white rounded-full flex items-center justify-center bg-gray-400 hover:bg-gray-500"
                                        >
                                            <LuX className="h-5 w-5" />
                                        </button>
                                    </div>
                                )
                            )
                        )}
                    </div>

                    <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-4">
                        {Object.keys(formData).map((key) => (
                            key !== 'usuario' && key !== 'empresa' && key !== 'ultimaConexion' && (
                                <div key={key} className="mb-2">
                                    <label className="block text-sm font-semibold mb-1 capitalize">{key.replace(/([A-Z])/g, ' $1')}</label>
                                    <input
                                        type="text"
                                        name={key}
                                        value={formData[key] || ''}
                                        onChange={handleChange}
                                        className={`w-full px-3 py-2 text-xs border rounded ${['fechaCreacion', 'ultimaConexion', 'usuario', 'empresa'].includes(key)
                                            ? 'bg-gray-200 cursor-not-allowed'
                                            : isEditable
                                                ? 'border-gray-300 focus:ring focus:ring-sky-500'
                                                : 'bg-gray-200'
                                            }`}
                                        disabled={!isEditable || ['fechaCreacion', 'ultimaConexion', 'usuario', 'empresa'].includes(key)}
                                    />
                                </div>
                            )
                        ))}
                    </div>
                </form>
                <CambiarContrasena />
            </div>
        </div>

    );
}