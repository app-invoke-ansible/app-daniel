import React, { useMemo } from 'react';
import { MdOutlineSignalCellularAlt1Bar, MdOutlineSignalCellularAlt2Bar, MdOutlineSignalCellularAlt } from "react-icons/md";

// Importa las imágenes
import ImgSeguridad from '../../img/iconos/Iconos_Seguridad.png';
import ImgMonitoreo from '../../img/iconos/Iconos_Monitoreo.png';
import ImgInfraestructura from '../../img/iconos/Iconos_Infraestructura .png';
import ImgRedes from '../../img/iconos/Iconos_Redes.png';
import ImgBD from '../../img/iconos/Iconos_BaseDeDatos.png';
import ImgDeployments from '../../img/iconos/Iconos_Deployments.png';
import ImgOtros from '../../img/iconos/Iconos_Otros.png';

// Define las categorías como una constante fuera del componente para evitar recreaciones innecesarias
const CATEGORIAS = [
    { name: 'Seguridad', imageUrl: ImgSeguridad },
    { name: 'Monitoreo', imageUrl: ImgMonitoreo },
    { name: 'Infraestructura', imageUrl: ImgInfraestructura },
    { name: 'Redes', imageUrl: ImgRedes },
    { name: 'Basededatos', imageUrl: ImgBD },
    //  { name: 'Deployments', imageUrl: ImgDeployments },
    { name: 'Aprovisionamiento', imageUrl: ImgOtros },
];

// Componente reutilizable para renderizar los iconos de impacto
const ImpactoIcon = ({ nivelImpacto }) => {
    switch (nivelImpacto) {
        case 1: return <MdOutlineSignalCellularAlt1Bar className="text-sky-500 text-base font-bold" />;
        case 2: return <MdOutlineSignalCellularAlt2Bar className="text-sky-500 text-base font-bold" />;
        case 3: return <MdOutlineSignalCellularAlt className="text-sky-500 text-base font-bold" />;
        default: return null;
    }
};

const DetalleCotizacion = ({ isOpen, onClose, modulo }) => {
    if (!isOpen || !modulo) return null;

    const imageUrl = useMemo(() => {
        const categoria = CATEGORIAS.find((cat) => cat.name === modulo.categoria?.name);
        return categoria?.imageUrl || ImgOtros;
    }, [modulo.categoria?.name]);

    const calcularPrecioTotal = () => {
      let precio = modulo.precio;
      if (modulo.totalEjecuciones > 0) {
          precio *= modulo.totalEjecuciones;
      }
      return precio.toFixed(2);
    };

    const precioTotal = useMemo(() => calcularPrecioTotal(), [modulo.precio, modulo.totalEjecuciones]);

    return (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 overflow-y-auto h-full w-full flex items-center justify-center">
            <div className="relative bg-white rounded-lg shadow-xl max-w-md w-full p-6">
                <button onClick={onClose} className="absolute top-2 right-2 text-gray-500 hover:text-gray-800">
                    <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>

                <div className="flex items-center mb-4 border-b border-gray-200 pb-2">
                    <img src={imageUrl} alt={modulo.name} className="w-16 h-16 object-cover rounded mr-4" />
                    <h2 className="text-xl font-bold text-sky-900">{modulo.name}</h2>
                </div>

                <div>
                    <h3 className="text-lg font-semibold text-gray-800 mb-2">Cálculo del Precio Total</h3>
                    <div className="overflow-x-auto">
                        <table className="min-w-full leading-normal shadow-md rounded-lg">
                            <thead>
                                <tr className="bg-gray-100">
                                    <th className="px-4 py-3 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        Concepto
                                    </th>
                                    <th className="px-4 py-3 text-right text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        Valor
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white">
                                <tr>
                                    <td className="px-4 py-3 border-b border-gray-200 text-sm">Nivel de Impacto</td>
                                    <td className="px-4 py-3 border-b border-gray-200 text-sm text-right">${modulo.precio}</td>
                                </tr>
                                {modulo.totalEjecuciones > 0 && (
                                    <tr>
                                        <td className="px-4 py-3 border-b border-gray-200 text-sm">Ejecuciones</td>
                                        <td className="px-4 py-3 border-b border-gray-200 text-sm text-right">x {modulo.totalEjecuciones}</td>
                                    </tr>
                                )}
                                <tr className="font-semibold">
                                    <td className="px-4 py-3 border-b border-gray-200 text-sm">Precio Total</td>
                                    <td className="px-4 py-3 border-b border-gray-200 text-sm text-right">${precioTotal}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DetalleCotizacion;