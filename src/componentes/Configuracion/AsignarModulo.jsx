import React, { useEffect, useState } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { useKeycloak } from '@react-keycloak/web';
import { FaUserPlus, FaUserMinus } from "react-icons/fa";
import CategoriaImagen from '../Base/CategoriaImagen';

export default function AsignarModulo({ onUpdate, moduloId, usuariosPorModulo }) {
    const [modulos, setModulos] = useState([]);
    const [usuarios, setUsuarios] = useState([]);
    const [listaUsuarios, setListaUsuarios] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [selectedUsuarios, setSelectedUsuarios] = useState([]);
    const [modulosAdmin, setModulosAdmin] = useState([]);
    const [usuarioAutenticado, setUsuarioAutenticado] = useState(null);

    const { keycloak, initialized } = useKeycloak();
    const { fetchWithAuth, user } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;

    const fetchModulosYUsuarios = async () => {
        try {
            const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
            const modulosData = await responseModulos.json();

            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuariosData = await responseUsuarios.json();

            const usuarioAutenticado = usuariosData.find((u) => u.username === user.name);

            if (usuarioAutenticado) {
                setUsuarioAutenticado(usuarioAutenticado)
                setUsuarios(usuarioAutenticado)
            }

            // Filtrar usuarios que tengan el mismo idEmpresa que el usuario autenticado
            const filteredUsuarios = usuariosData.filter(usuario =>
                usuario.idEmpresa === usuarioAutenticado?.idEmpresa && usuario.id !== usuarioAutenticado?.id
            );

            setListaUsuarios(filteredUsuarios);

            // Fetch de los modulos del admin (ID 1)
            const responseModulosAdmin = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
            const modulosAdminData = await responseModulosAdmin.json();

            // Extraemos solo los ids de los modulos
            const modulosAdminIds = modulosAdminData?.modulos?.map(modulo => modulo.id) || [];

            // Filtrar modulos que el admin ya tiene
            const filteredModulos = modulosData.filter(modulo => modulosAdminIds.includes(modulo.id));

            setModulos(filteredModulos);

        } catch (err) {
            console.error('Error al obtener los datos:', err);
        }
    };

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
        setSelectedUsuarios([]);
    };
    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isModalOpen]);

    const handleOtorgarModulo = async () => {
        if (selectedUsuarios.length === 0) {
            alert('Por favor, selecciona al menos un usuario.');
            return;
        }

        try {
            for (const usuarioId of selectedUsuarios) {
                // Obtener los módulos que el usuario ya tiene
                const responseUsuarioModulos = await fetchWithAuth(`${API_URL}/usuario/${usuarioId}/modulos`);
                const usuarioModulosData = await responseUsuarioModulos.json();
                const usuarioModulosIds = usuarioModulosData?.modulos?.map(modulo => modulo.id) || [];

                // Verificar si el usuario ya tiene el módulo
                const selectedModuloId = parseInt(moduloId);
                if (usuarioModulosIds.includes(selectedModuloId)) {
                    alert(`El usuario ${usuarioId} ya tiene este módulo asignado.`);
                    continue; // No se otorga el módulo si ya lo tiene
                }

                const response = await fetchWithAuth(
                    `${API_URL}/usuario/${usuarioId}/modulos/${selectedModuloId}/otorgar?adminId=${usuarios.id}`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    }
                );
                if (response.ok) {
                    alert('Módulo otorgado exitosamente.');
                } else {
                    const errorData = await response.json();
                    console.error('Error al otorgar módulo:', errorData);
                    alert(`Error: ${errorData.message || 'No se pudo otorgar el módulo.'}`);
                }
            }

            onUpdate();
        } catch (err) {
            console.error('Error al realizar la solicitud:', err);
            alert('Ocurrió un error al intentar otorgar el módulo.');
        }

        handleCloseModal();
    };

    useEffect(() => {
        fetchModulosYUsuarios();
    }, [fetchWithAuth, API_URL]);

    if (!initialized) {
        return <div>Loading...</div>;
    }

    if (!keycloak.authenticated) {
        keycloak.login();
        return null;
    }

    // Filtramos el módulo con el moduloId recibido como prop
    const moduloSeleccionado = modulos.find(modulo => modulo.id === parseInt(moduloId));

    // Filtrar los usuarios que ya tienen asignado el módulo
    const usuariosConModulo = usuariosPorModulo[moduloId] || [];
    const usuariosDisponibles = listaUsuarios.filter(usuario =>
        !usuariosConModulo.some(u => u.id === usuario.id) // Excluir usuarios que ya tienen este módulo
    );

    return (
        <>
            <div>
                <button className="p-2 border text-green-600 border-green-600 hover:text-white hover:bg-green-600 rounded-full transition-colors duration-300 transform hover:scale-105" onClick={handleOpenModal}>
                    <FaUserPlus className="h-4 w-4" />
                </button>
            </div>

            {/* Modal */}
            {isModalOpen && (
                <div className="fixed inset-0 bg-gray-600 bg-opacity-50 overflow-y-auto sm:h-full sm:w-full flex items-center justify-center z-50">
                    <div className="bg-white p-6 rounded-lg shadow-lg sm:w-96 w-full sm:h-auto h-full sm:mt-0">
                        <h2 className="text-lg text-center text-sky-900 font-bold ">Asignar Módulo</h2>

                        {/* Módulo ya seleccionado, solo mostrar el nombre */}
                        {moduloSeleccionado && (
                            <div className="mb-4">
                                <div className="flex items-center">
                                    <div className="w-1/6 flex justify-center items-center">
                                        <CategoriaImagen categoriaName={moduloSeleccionado.categoria?.name} className="w-8 h-auto object-cover mr-4" />
                                    </div>
                                    <div className="flex flex-col ml-4">
                                        <h2 className="text-xs text-gray-800">Categoría {moduloSeleccionado.categoria?.name}</h2>
                                        <h1 className="text-sm text-sky-900 font-bold">{moduloSeleccionado.name}</h1>
                                    </div>
                                </div>
                            </div>
                        )}

                        <div className="mb-4">
                            <label htmlFor="usuarios" className="block text-sm font-medium text-gray-700 mb-1">Seleccionar Usuarios:</label>
                            <div className="space-y-2">
                                {usuariosDisponibles.map((usuario) => (
                                    <div key={usuario.id} className="flex items-center">
                                        <input
                                            type="checkbox"
                                            id={`usuario-${usuario.id}`}
                                            value={usuario.id}
                                            onChange={(e) => {
                                                setSelectedUsuarios(prevState => {
                                                    if (e.target.checked) {
                                                        return [...prevState, usuario.id];
                                                    } else {
                                                        return prevState.filter(id => id !== usuario.id);
                                                    }
                                                });
                                            }}
                                            checked={selectedUsuarios.includes(usuario.id)}
                                            className="mr-2"
                                        />
                                        <label htmlFor={`usuario-${usuario.id}`} className="text-sm text-gray-800">{usuario.username}</label>
                                    </div>
                                ))}
                            </div>
                        </div>

                        <div className="flex items-center justify-center space-x-4">
                            <button
                                onClick={handleCloseModal}
                                className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105">
                                Cancelar
                            </button>
                            <button
                                onClick={handleOtorgarModulo}
                                className="px-4 py-2 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105">
                                Otorgar Módulo
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
