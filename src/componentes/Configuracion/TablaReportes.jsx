import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import Loading from '../Base/Loading';
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';
import LogModal from '../Home/LogModal';
import { FiTerminal } from "react-icons/fi";

const TablaReportes = () => {
  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;
  const [data, setData] = useState([]);
  const [nombreEmpresa, setNombreEmpresa] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(10);
  const [filters, setFilters] = useState({ sistema: '', categoria: '' });
  const [loading, setLoading] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedJobId, setSelectedJobId] = useState(null);
  const [selectedModulo, setSelectedModulo] = useState(null);

  // Función para formatear la fecha
  const formatFecha = (fechaISO) => {
    const fecha = new Date(fechaISO);
    const año = fecha.getFullYear();
    const mes = String(fecha.getMonth() + 1).padStart(2, '0');
    const dia = String(fecha.getDate()).padStart(2, '0');
    const horas = String(fecha.getHours()).padStart(2, '0');
    const minutos = String(fecha.getMinutes()).padStart(2, '0');
    const segundos = String(fecha.getSeconds()).padStart(2, '0');
    return `${año}/${mes}/${dia} ${horas}:${minutos}:${segundos}`;
  };

  // Extraer sistemas y categorías únicos de los datos
  const sistemasUnicos = [...new Set(data.map((row) => row.sistema))];
  const categoriasUnicas = [...new Set(data.map((row) => row.categoria))];

  // Calcular métricas
  const totalEjecuciones = data.length;

  const categoriaMasEjecutada = () => {
    const categoriasCount = data.reduce((acc, row) => {
      acc[row.categoria] = (acc[row.categoria] || 0) + 1;
      return acc;
    }, {});
    return Object.keys(categoriasCount).reduce((a, b) => categoriasCount[a] > categoriasCount[b] ? a : b);
  };

  const sistemaMasUtilizado = () => {
    const sistemasCount = data.reduce((acc, row) => {
      acc[row.sistema] = (acc[row.sistema] || 0) + 1;
      return acc;
    }, {});
    return Object.keys(sistemasCount).reduce((a, b) => sistemasCount[a] > sistemasCount[b] ? a : b);
  };

  const porcentajeExito = () => {
    const ejecucionesExitosas = data.filter((row) => row.estado === 'success').length;
    return ((ejecucionesExitosas / totalEjecuciones) * 100).toFixed(2);
  };

  const topUsuarios = () => {
    const usuariosCount = data.reduce((acc, row) => {
      acc[row.nombreUsuario] = (acc[row.nombreUsuario] || 0) + 1;
      return acc;
    }, {});
    return Object.entries(usuariosCount)
      .sort((a, b) => b[1] - a[1])
      .slice(0, 3)
      .map(([usuario]) => usuario);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await response.json();
        const usuarioAutenticado = usuarios.find((u) => u.username === user.name);

        if (usuarioAutenticado) {
          const ejecucionesResponse = await fetchWithAuth(`${API_URL}/ejecuciones/empresa/${usuarioAutenticado.idEmpresa}`);
          const ejecucionesData = await ejecucionesResponse.json();

          setNombreEmpresa(ejecucionesData.nombre);
          // Ordenar las ejecuciones por fechaHoraLanzamiento en orden descendente
          const ejecucionesOrdenadas = ejecucionesData.ejecuciones.sort((a, b) =>
            new Date(b.fechaHoraLanzamiento) - new Date(a.fechaHoraLanzamiento)
          );
          setData(ejecucionesOrdenadas);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [fetchWithAuth, user, API_URL]);

  const handleFilterChange = (e) => {
    const { name, value } = e.target;
    setFilters({ ...filters, [name]: value });
  };

  const filteredData = data.filter((row) => {
    return (filters.sistema === '' || row.sistema === filters.sistema) &&
      (filters.categoria === '' || row.categoria === filters.categoria);
  });

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredData.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const openLogModal = (jobId, modulo) => {
    setSelectedJobId(jobId);
    setSelectedModulo(modulo);
    setIsModalOpen(true);
  };

  const closeLogModal = () => {
    setIsModalOpen(false);
    setSelectedJobId(null);
    setSelectedModulo(null);
  };

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  return (
    <div className="p-4">
      {loading ? (
        <Loading message="Cargando datos..." />
      ) : (
        <>
          <h1 className="text-xl font-bold mb-4 text-center">{nombreEmpresa}</h1>
          {/* Métricas */}
          {data.length === 0 ? (
            <div className="text-center text-gray-600">
              <p>La empresa no tiene ejecuciones.</p>
            </div>
          ) : (
            <>
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 gap-4 mb-4 mx-2">
                <div className="border border-sky-900 rounded-xl flex flex-col items-center justify-center p-2">
                  <p className="text-sm font-semibold">Total de ejecuciones</p>
                  <p className="text-sm font-semibold">de la empresa</p>
                  <div className="mt-1 w-10 h-10 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                    <div className="text-sky-500 text-base font-bold bg-white px-1">
                      {totalEjecuciones}
                    </div>
                  </div>
                </div>
                <div className="border border-sky-900 rounded-xl flex flex-col items-center justify-center p-2">
                  <p className="text-sm font-semibold text-center">Categoría más ejecutada</p>
                  <div className='w-16'><CategoriaImagen categoriaName={categoriaMasEjecutada()} /></div>
                  <p className="text-sky-900 text-xs font-semibold mt-2 text-center">{categoriaMasEjecutada()}</p>
                </div>
                <div className="border border-sky-900 rounded-xl flex flex-col items-center justify-center p-2">
                  <p className="text-sm font-semibold">Sistema más utilizado</p>
                  <div className='w-16'><SistemaImagen sistemaName={sistemaMasUtilizado()} /></div>
                  <p className="text-sky-900 text-xs font-semibold mt-2 text-center">{sistemaMasUtilizado()}</p>
                </div>
                <div className="border border-sky-900 rounded-xl flex flex-col items-center justify-center p-2">
                  <p className="text-sm font-semibold">Porcentaje de éxito</p>
                  <p className="text-sm font-semibold">de las ejecuciones</p>
                  <div className="mt-1 w-10 h-10 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                    <div className="text-sky-500 text-base font-bold bg-white px-1">
                      {porcentajeExito()}%
                    </div>
                  </div>
                </div>
                <div className="border border-sky-900 rounded-xl flex flex-col items-center justify-center p-2">
                  <p className="text-sm font-semibold">Top 3 usuarios</p>
                  <p className="text-sm font-semibold">que más han ejecutado</p>
                  <ul>
                    {topUsuarios().map((usuario, index) => (
                      <li key={index} className="text-sky-900 text-xs font-semibold mt-2 text-center">• {usuario}</li>
                    ))}
                  </ul>
                </div>
              </div>

              {/* Tabla de ejecuciones */}
              <div className="border-sky-950 border rounded-lg mb-4 mx-1">
                <div className="mx-auto lg:px-0 rounded-lg overflow-x-auto">
                  <table className="min-w-full divide-y divide-gray-300">
                    <thead className="bg-sky-950">
                      <tr>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-3 text-center font-semibold text-xs text-white sm:table-cell">Módulo</th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-3 text-center font-semibold text-xs text-white hidden sm:table-cell">
                          <select name="sistema" onChange={handleFilterChange} className="bg-sky-950 text-white">
                            <option value="">Sistema</option>
                            {sistemasUnicos.map((sistema, index) => (
                              <option key={index} value={sistema}>{sistema}</option>
                            ))}
                          </select>
                        </th>
                        <th className="px-3 py-3.5 text-center font-semibold text-xs text-white hidden sm:table-cell">
                          <select name="categoria" onChange={handleFilterChange} className="bg-sky-950 text-white">
                            <option value="">Categoría</option>
                            {categoriasUnicas.map((categoria, index) => (
                              <option key={index} value={categoria}>{categoria}</option>
                            ))}
                          </select>
                        </th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-4 text-center font-semibold text-xs text-white sm:table-cell">Fecha y Hora</th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-4 text-center font-semibold text-xs text-white hidden sm:table-cell">Duración</th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-4 text-center font-semibold text-xs text-white sm:table-cell">Estado</th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-4 text-center font-semibold text-xs text-white sm:table-cell">Usuario</th>
                        <th className="sm:px-3 sm:py-3.5 px-2 py-4 text-center font-semibold text-xs text-white sm:table-cell">Log</th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentItems.map((row, index) => (
                        <tr key={index} className={index % 2 === 1 ? 'bg-white' : 'bg-gray-200'}>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center sm:table-cell">{row.modulo}</td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center hidden sm:table-cell">{row.sistema}</td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center hidden sm:table-cell">{row.categoria}</td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center sm:table-cell">
                            {formatFecha(row.fechaHoraLanzamiento)}
                          </td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center hidden sm:table-cell">{row.duracion}</td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center sm:table-cell">{row.estado}</td>
                          <td className="sm:px-3 sm:py-4 px-2 py-4 text-xs text-center sm:table-cell">{row.nombreUsuario}</td>
                          <td>
                            <div
                              className="flex items-center justify-center cursor-pointer"
                              onClick={() => openLogModal(row.jobId, row)}
                            >
                              <div className="flex items-center justify-center rounded-full border-2 border-sky-950 
                              h-8 w-8 mb-1  text-sky-950 hover:text-white hover:bg-sky-950 transition-colors duration-100 transform hover:scale-110">
                                <FiTerminal className=" h-4 w-4" />
                              </div>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="flex justify-center">
                {Array.from({ length: Math.ceil(filteredData.length / itemsPerPage) }, (_, i) => (
                  <button
                    key={i + 1}
                    onClick={() => paginate(i + 1)}
                    className={`mx-1 px-3 py-1 rounded ${currentPage === i + 1 ? 'bg-sky-950 border rounded-xl text-white border-sky-950' : 'bg-gray-200 border rounded-xl border-sky-950'}`}
                  >
                    {i + 1}
                  </button>
                ))}
              </div>
            </>
          )}
        </>
      )}
      {/* Modal de Log */}
      <LogModal
        isOpen={isModalOpen}
        onClose={closeLogModal}
        jobId={selectedJobId}
        modulo={selectedModulo}
      />
    </div>
  );
};

export default TablaReportes;