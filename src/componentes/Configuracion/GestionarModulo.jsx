import React, { useEffect, useState } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { useKeycloak } from '@react-keycloak/web';
import { LuEye } from 'react-icons/lu';
import { FaUserMinus } from "react-icons/fa";
import AsignarModulo from './AsignarModulo';
import Loading from '../Base/Loading';
import EliminarUnidad from './EliminarModuloAsignado';
import DetalleModulo from './DetalleModulo';
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';
import BuscarModulo from '../Modulo/BuscarModulo';

export default function GestionarModulo() {
    const [modulos, setModulos] = useState([]);
    const [filteredModules, setFilteredModules] = useState([]);
    const [usuariosPorModulo, setUsuariosPorModulo] = useState({});
    const { keycloak, initialized } = useKeycloak();
    const { fetchWithAuth, user } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;
    const [loading, setLoading] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [selectedModulo, setSelectedModulo] = useState(null);
    const [isEliminarModalOpen, setIsEliminarModalOpen] = useState(false);
    const [moduloAEliminar, setModuloAEliminar] = useState(null);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [usuarioAutenticado, setUsuarioAutenticado] = useState(null);

    const fetchModulosYUsuarios = async () => {
        setLoading(true);
        try {
            const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
            const modulosData = await responseModulos.json();

            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuariosData = await responseUsuarios.json();

            const usuarioAutenticado = usuariosData.find((u) => u.username === user.name);
            setUsuarioAutenticado(usuarioAutenticado);

            const isAdminUser = usuarioAutenticado && usuarioAutenticado.rol === 'admin';

            let modulosFiltrados = modulosData;
            if (!isAdminUser) {
                const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
                const modulosUsuarioData = await responseModulosUsuario.json();
                if (modulosUsuarioData && modulosUsuarioData.modulos) {
                    modulosFiltrados = modulosData.filter(modulo => modulosUsuarioData.modulos.some(mod => mod.id === modulo.id));
                } else {
                    modulosFiltrados = [];
                }
            }
            // Aquí agregamos el console.log para los módulos filtrados
            console.log("Módulos filtrados:", modulosFiltrados);
            setModulos(modulosFiltrados);
            setFilteredModules(modulosFiltrados);

            const usuariosPorModuloInicial = {};
            modulosData.forEach(modulo => {
                usuariosPorModuloInicial[modulo.id] = [];
            });


            for (const usuario of usuariosData) {
                const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuario.id}/modulos`);
                const modulosUsuarioData = await responseModulosUsuario.json();

                if (modulosUsuarioData && modulosUsuarioData.modulos) {
                    modulosUsuarioData.modulos.forEach(modulo => {
                        if (usuariosPorModuloInicial[modulo.id]) {
                            usuariosPorModuloInicial[modulo.id].push(usuario);
                        }
                    });
                }
            }
            setUsuariosPorModulo(usuariosPorModuloInicial);

        } catch (err) {
            console.error("Error al obtener los datos:", err);
        } finally {
            setLoading(false);
        }
    };

    const handleOpenModal = (modulo) => {
        setSelectedModulo(modulo);
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
        setSelectedModulo(null);
    };

    const handleOpenEliminarModal = (modulo) => {
        setModuloAEliminar(modulo);
        setIsEliminarModalOpen(true);
    };

    const handleCloseEliminarModal = () => {
        setIsEliminarModalOpen(false);
        setModuloAEliminar(null);
    };

    const onModuloEliminado = () => {
        fetchModulosYUsuarios();
        handleCloseEliminarModal();
    };

    const handleRefresh = async () => {
        setIsRefreshing(true);
        setRefresh(prev => !prev);
        await new Promise(resolve => setTimeout(resolve, 500))
        setIsRefreshing(false);
    };

    useEffect(() => {
        fetchModulosYUsuarios();
    }, [fetchWithAuth, API_URL, refresh, user]);

    if (loading || !initialized) {
        return <Loading message="Cargando módulos..." />;
    }

    return (
        <>
            <div>
                <div className="mb-4 w-full flex justify-end">
                    <BuscarModulo modules={modulos} setFilteredModules={setFilteredModules} />
                </div>
                <div className="mx-auto grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                    {Array.isArray(filteredModules) && filteredModules.length > 0 ? (
                        filteredModules.map((modulo) => {
                            const {
                                name,
                                categoria,
                                sistema,
                                status,
                                lastJobRun,
                                precio,
                                nivelImpacto,
                            } = modulo;

                            return (
                                <div
                                    key={modulo.id}
                                    className="border border-sky-950 rounded-lg shadow-md hover:shadow-lg transition-shadow duration-300 ease-in-out max-w-sm w-full flex-col justify-between"
                                >
                                    <div className="flex items-center p-4">
                                        <div className="w-1/4 flex justify-center items-center">
                                            <CategoriaImagen categoriaName={categoria?.name} className="w-8 h-auto object-cover mr-4" />
                                        </div>
                                        <div className="flex flex-col ml-4">
                                            <h2 className="text-xs text-gray-800">Categoría {categoria?.name}</h2>
                                            <h1 className="text-sm text-sky-900 font-bold">{name}</h1>
                                            <h2 className="text-xs text-gray-800 pt-2">Última ejecución:<br />
                                                <strong>{lastJobRun ? new Date(lastJobRun).toLocaleString() : "Sin ejecuciones"}</strong>
                                            </h2>
                                        </div>
                                    </div>

                                    <div className="border-t border-b border-sky-950 py-2 flex justify-between items-center px-12">
                                        <div className="flex flex-col items-center justify-center">
                                            <span className="text-sky-900 text-xs font-semibold">Nivel de impacto</span>
                                            <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                                                <div className="text-sky-500 text-xs font-bold bg-white px-1">
                                                    {nivelImpacto}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-1/6 flex justify-center items-center mx-6">
                                            <SistemaImagen sistemaName={sistema} />
                                        </div>
                                    </div>

                                    <p className="text-xs text-center text-gray-700 mt-2">
                                        {usuariosPorModulo[modulo.id] ? (
                                            // Filtrar usuarios según la condición
                                            usuariosPorModulo[modulo.id]
                                                .filter(
                                                    (user) =>
                                                        !(usuarioAutenticado && user.tipoUsuario === 'admin' && user.username === usuarioAutenticado.username)
                                                )
                                                .length > 0 ? (
                                                `${usuariosPorModulo[modulo.id]
                                                    .filter(
                                                        (user) =>
                                                            !(usuarioAutenticado && user.tipoUsuario === 'admin' && user.username === usuarioAutenticado.username)
                                                    )
                                                    .length} usuario(s) poseen este módulo`
                                            ) : (
                                                'Ningún usuario posee este módulo'
                                            )
                                        ) : (
                                            'Ningún usuario posee este módulo'
                                        )}
                                    </p>

                                    <div className="flex justify-center items-center space-x-2 p-2">
                                        <AsignarModulo onUpdate={handleRefresh} moduloId={modulo.id} usuariosPorModulo={usuariosPorModulo} />
                                        <button
                                            className="p-2 border text-sky-900 border-sky-900 rounded-full hover:bg-sky-950 hover:text-white transition-colors duration-100 transform hover:scale-105"
                                            onClick={() => handleOpenModal(modulo)}
                                        >
                                            <LuEye className="h-4 w-4 " />
                                        </button>
                                        <button
                                            className="p-2 border  text-red-600 border-red-600 rounded-full hover:bg-red-600 hover:text-white transition-colors duration-100 transform hover:scale-105 "
                                            onClick={() => handleOpenEliminarModal(modulo)}
                                        >
                                            <FaUserMinus className="h-4 w-4" />
                                        </button>
                                    </div>

                                </div>
                            );
                        })
                    ) : (
                        <div className="col-span-full text-center">No hay módulos disponibles.</div>
                    )}
                </div>
            </div>

            {/* Modal descripción */}
            <DetalleModulo
                isOpen={isModalOpen}
                onClose={handleCloseModal}
                modulo={selectedModulo}
                usuarios={selectedModulo ? usuariosPorModulo[selectedModulo.id] : []}
                usuarioAutenticado={usuarioAutenticado} // Pasa el usuario autenticado
            />


            {/* Modal Eliminar */}
            {isEliminarModalOpen && (
                <EliminarUnidad
                    isOpen={isEliminarModalOpen}
                    onClose={handleCloseEliminarModal}
                    modulo={moduloAEliminar}
                    usuariosPorModulo={usuariosPorModulo}
                    usuarioAutenticado={usuarioAutenticado} // Pasar el usuario autenticado completo
                    onModuloEliminado={onModuloEliminado}
                />
            )}
        </>
    );
}