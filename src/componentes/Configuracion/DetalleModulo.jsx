import React, { useMemo, useEffect } from 'react';
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';
import { IoIosClose } from "react-icons/io";

const DetalleModulo = ({ isOpen, onClose, modulo, usuarios, usuarioAutenticado }) => {
  if (!isOpen || !modulo) return null;

  const usuariosFiltrados = useMemo(() => {
    if (!usuarios || !usuarioAutenticado) {
      return [];
    }
    return usuarios.filter(user => user.id !== usuarioAutenticado.id);
  }, [usuarios, usuarioAutenticado]);

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isOpen]);

  return (
    <div className="fixed inset-0 bg-gray-600 bg-opacity-50 overflow-y-auto h-full w-full flex items-center justify-center z-50">
      <div className="flex flex-col relative bg-white rounded-lg shadow-xl max-w-3xl sm:w-full sm:h-auto w-full h-full p-6">

        <div className="flex items-center justify-between mb-6 border-b border-gray-200 pb-2 ">
          <h2 className="sm:items-start sm:text-2xl text-xl font-bold text-sky-900 sm:text-center">{modulo.name}</h2>
          <button
            onClick={onClose}
            className="items-end text-gray-600  rounded-lg hover:scale-105 hover:bg-red-500 hover:text-white"
          >
            <IoIosClose className="w-10 h-10" />
          </button>
        </div>

        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-4">
          {/* Primer rectángulo */}
          <div className="border border-sky-900 rounded-xl w-32 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <div className="w-16">
              <CategoriaImagen categoriaName={modulo.categoria.name} />
            </div>
            <span className="text-sky-900 text-xs font-semibold mt-2 text-center">
              {modulo.categoria.name}
            </span>
          </div>

          {/* Segundo rectángulo */}
          <div className="border border-sky-900 rounded-xl w-32 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <div className="w-16">
              <SistemaImagen sistemaName={modulo.sistema} />
            </div>
          </div>

          {/* Tercer rectángulo */}
          <div className="border border-sky-900 rounded-xl w-32 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">Nivel de impacto</span>
            <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
              <div className="text-sky-500 text-xs font-bold bg-white px-1">
                {modulo.nivelImpacto}
              </div>
            </div>
          </div>

          {/* Cuarto rectángulo */}
          <div className="border border-sky-900 rounded-xl w-32 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">Total Ejecuciones</span>
            <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
              <div className="text-sky-500 text-xs font-bold bg-white px-1">
                {modulo.ejecuciones?.length || 0}
              </div>
            </div>
          </div>

          {/* Quinto rectángulo */}
          <div className="border border-sky-900 rounded-xl w-32 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">
              Última vez<br />ejecutado
            </span>
            {modulo.lastJobRun ? (
              <div className="text-sky-500 text-xs font-bold bg-white px-1 text-center">
                {new Date(modulo.lastJobRun).toLocaleDateString()} <br />
                {new Date(modulo.lastJobRun).toLocaleTimeString()}
              </div>
            ) : (
              <div className="text-sky-500 text-xs font-bold bg-white px-1">Nunca</div>
            )}
          </div>
        </div>

        <div className="mt-6">
          <h3 className="block text-sm font-semibold text-gray-700 mb-1">Usuarios Asignados</h3>
          {usuariosFiltrados && usuariosFiltrados.length > 0 ? (
            <ul className="grid grid-cols-1 sm:grid-cols-2 gap-2">
              {usuariosFiltrados.map((user) => (
                <li
                  key={user.id}
                  className="px-4 py-2 bg-gray-100 rounded-md text-xs text-gray-700"
                >
                  {user.username}
                </li>
              ))}
            </ul>
          ) : (
            <p className="text-gray-700 text-xs">
              Ningún usuario asignado a este módulo.
            </p>
          )}
        </div>
      </div>
    </div>
  );
};

export default DetalleModulo;