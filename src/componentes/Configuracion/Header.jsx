import React, { useEffect, useState } from 'react';
import { Bars3Icon } from '@heroicons/react/24/outline';
import SearchForm from '../../componentes/Base/SearchForm';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import Return from '../../componentes/Base/Return';
import { IoIosArrowForward } from "react-icons/io";
import { useAuth } from '../../providers/AuthProvider';

const Header = ({ selectedCard, sidebarOpen, setSidebarOpen }) => {
    const navigate = useNavigate();
    const location = useLocation();
    const { isAuthenticated, user, fetchWithAuth } = useAuth();
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [breadcrumb, setBreadcrumb] = useState(
        <>
            <div className="lg:flex items-center hidden">
                <span className="cursor-pointer text-sm "
                    onClick={() => navigate('/ConfiguracionPage')}>
                    CONFIGURACIÓN
                </span>
                <IoIosArrowForward className="mx-2" />
                <span>{selectedCard?.title?.toUpperCase() || ''}</span>
            </div>
        </>
    );

    useEffect(() => {
        const fetchUserData = async () => {
            if (isAuthenticated && user) {
                try {
                    const response = await fetchWithAuth(`${process.env.REACT_APP_API_URL}/usuario`);
                    const usuarios = await response.json();
                    const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                    if (usuarioAutenticado) {
                        setTipoUsuario(usuarioAutenticado.tipoUsuario);
                    } else {
                        console.warn('Usuario autenticado no encontrado en la API');
                    }
                } catch (error) {
                    console.error('Error al obtener datos de la API:', error);
                }
            }
        };

        fetchUserData();
    }, [isAuthenticated, user, fetchWithAuth]);

    useEffect(() => {
        if (selectedCard) {
            setBreadcrumb(
                <>
                    <div className="md:flex items-center hidden">
                        <span className="cursor-pointer" onClick={() => navigate('/ConfiguracionMenu')}>
                            CONFIGURACIÓN
                        </span>
                        <IoIosArrowForward className="mx-2 " />
                        <span>{selectedCard.title.toUpperCase()}</span>
                    </div>
                </>
            );
        }
    }, [selectedCard, navigate]);

    const handleToggleSidebar = () => {
        setSidebarOpen(!sidebarOpen);
    };

    const isConfigPage = location.pathname === '/ConfiguracionMenu';


    return (
        <>
            <div className={`sticky top-0 z-40 flex h-16 items-center gap-x-4 border-b border-gray-200 bg-white
                px-2 sm:px-4 shadow-sm transition-all ${sidebarOpen ? 'ml-32' : ''}`}>
                {!isConfigPage && (
                    <button
                        type="button"
                        className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
                        onClick={handleToggleSidebar}
                    >
                        <span className="sr-only">Open sidebar</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                )}
                <div className="flex items-center h-full space-x-2 sm:space-x-4">
                    <Return />
                    <Link to="/home" className="flex items-center py-0">
                        <h1 className="font-bold tracking-tight text-sky-950 sm:text-xl lg:text-2xl">HOME</h1>
                    </Link>
                    <p className="text-sm text-gray-400 hidden sm:flex">
                        <IoIosArrowForward />
                    </p>
                    <div className="flex items-center tracking-tight text-gray-400 ">
                        {breadcrumb}
                    </div>
                </div>

                <div className="flex items-center ml-auto lg:flex">
                    <div className="flex items-center p-4">
                        <SearchForm className="m-4" />
                    </div>
                </div>
            </div>

            <aside className={`bg-white shadow-md border-t border-gray-200 py-4 transition-all
                ${sidebarOpen ? 'ml-32' : ''}
                `}>
                <div className="mx-auto flex items-center justify-between px-2 sm:px-6">

                    <div className="flex-grow text-center">
                        <h3 className="font-bold tracking-tight text-sky-950 text-xl  lg:text-2xl">
                            {selectedCard ? selectedCard.title : 'CONFIGURACIÓN'}</h3>
                    </div>

                </div>
            </aside>
        </>
    );
};

export default Header;