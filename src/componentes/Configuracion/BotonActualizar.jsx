import React, {useState} from 'react';
import { LuRefreshCcw } from "react-icons/lu";

const BotonActualizar = ({onActualizar}) => {
    const [loading, setLoading] = useState(false);

    const handleActualizar = async () => {
        setLoading(true);
        await onActualizar(); // Llama a la función del padre para actualizar el inventario
        setLoading(false);
    };
    return (
        <div className="flex justify-center h-full">
            <button onClick={handleActualizar}
                className="flex items-center gap-2 mt-2 bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white"
                disabled={loading} // Deshabilita el botón mientras se carga
            >
                <LuRefreshCcw />
                {loading ? 'Cargando...' : 'Actualizar'}
            </button>
        </div>
    );
};


export default BotonActualizar;