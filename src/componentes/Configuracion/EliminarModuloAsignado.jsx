import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import CategoriaImagen from '../Base/CategoriaImagen';

const EliminarModuloAsignado = ({ isOpen, onClose, modulo, usuariosPorModulo, usuarioAutenticado, onModuloEliminado }) => {
    const [selectedUser, setSelectedUser] = useState('');
    const { fetchWithAuth } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;
    const [usuariosDelModulo, setUsuariosDelModulo] = useState([]);
    const [usuariosFiltrados, setUsuariosFiltrados] = useState([]);

    useEffect(() => {
        if (isOpen && modulo && usuariosPorModulo && usuarioAutenticado) {
            const usuarios = usuariosPorModulo[modulo.id] || [];

            // Filtrar los usuarios, excluyendo al admin y asegurándose de que pertenezcan al mismo módulo
            const filteredUsers = usuarios.filter(usuario =>
                usuario.id !== usuarioAutenticado.id // Excluye al usuario autenticado
            );
            setUsuariosDelModulo(usuarios);
            setUsuariosFiltrados(filteredUsers);
        } else {
            setUsuariosDelModulo([]);
            setUsuariosFiltrados([]);
        }
    }, [isOpen, modulo, usuariosPorModulo, usuarioAutenticado]);

    const handleEliminarModulo = async () => {
        if (!selectedUser) {
            alert('Por favor, selecciona un usuario.');
            return;
        }

        try {
            const response = await fetchWithAuth(
                `${API_URL}/usuario/${selectedUser}/modulos/${modulo.id}/otorgar?adminId=${usuarioAutenticado.id}`,
                {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
            );
            if (response.ok) {
                alert('Módulo eliminado exitosamente.');
                onModuloEliminado();
            } else {
                const errorData = await response.json();
                console.error('Error al eliminar módulo:', errorData);
                alert(`Error: ${errorData.message || 'No se pudo eliminar el módulo.'}`);
            }

        } catch (err) {
            console.error('Error al realizar la solicitud:', err);
            alert('Ocurrió un error al intentar eliminar el módulo.');
        }

        onClose();
    };

    if (!isOpen) return null;

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    return (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 overflow-y-auto h-full w-full flex items-center justify-center z-50">
            <div className="bg-white rounded-lg shadow-lg sm:max-w-sm sm:h-auto w-full h-full p-6 ">
                <h2 className="text-lg text-center text-sky-900 font-bold">Eliminar Módulo de Usuario</h2>

                {modulo && (
                    <div className="mb-4">
                        <div className="flex items-center">
                            <div className="w-1/6 flex justify-center items-center">
                                <CategoriaImagen categoriaName={modulo.categoria?.name} className="w-8 h-auto object-cover mr-4" />
                            </div>
                            <div className="flex flex-col ml-4">
                                <h2 className="text-xs text-gray-800">Categoría {modulo.categoria?.name}</h2>
                                <h1 className="text-sm text-sky-900 font-bold">{modulo.name}</h1>
                            </div>
                        </div>
                    </div>
                )}

                {usuariosFiltrados.length > 0 ? (
                    <div className="m-4">
                        <label htmlFor="usuario" className="block text-sm font-medium text-gray-700 mb-1">Seleccionar Usuario:</label>
                        <select
                            id="usuario"
                            value={selectedUser}
                            onChange={(e) => setSelectedUser(e.target.value)}
                            className="w-full border-gray-300 rounded-md shadow-sm py-2 px-3 text-sm text-gray-700"
                        >
                            <option value="">-- Seleccionar --</option>
                            {usuariosFiltrados.map((usuario) => (
                                <option key={usuario.id} value={usuario.id}>
                                    {usuario.username}
                                </option>
                            ))}
                        </select>
                    </div>
                ) : (
                    <p className="text-gray-700 text-sm m-4">No hay usuarios con este módulo.</p>
                )}

                <div className="flex items-center justify-center space-x-4">
                    {usuariosFiltrados.length > 0 ? (
                        <>
                            <button
                                onClick={onClose}
                                className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105">
                                Cancelar
                            </button>
                            <button
                                onClick={handleEliminarModulo}
                                className="px-4 py-2 bg-red-600 text-white hover:bg-red-500 border border-transparent hover:border-red-700 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                disabled={usuariosFiltrados.length === 0}
                            >
                                Eliminar
                            </button>
                        </>
                    ) : (
                        <button
                            onClick={onClose}
                            className="px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105">
                            Cancelar
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};

export default EliminarModuloAsignado;