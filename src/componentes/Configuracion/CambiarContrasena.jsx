import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { LuPencil, LuCheck, LuX } from "react-icons/lu";

export default function CambiarContrasena() {
  const { isAuthenticated, user, fetchWithAuth } = useAuth();
  const [isEditable, setIsEditable] = useState(false);
  const [usuario, setUsuario] = useState(null);
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
  const [formData, setFormData] = useState({
    contrasena: '',
    confirmarContrasena: '',
    nuevaContrasena: ''
  });
  const [formErrors, setFormErrors] = useState({});
  const [originalFormData, setOriginalFormData] = useState({ ...formData });
  const [updateSuccess, setUpdateSuccess] = useState(null);

  const API_URL = process.env.REACT_APP_API_URL;

  useEffect(() => {
    if (isAuthenticated && user) {
      const fetchUserData = async () => {
        try {
          const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
          const usuarios = await responseUsuarios.json();
          const usuarioAutenticado = usuarios.find(u => u.username === user?.name);

          if (usuarioAutenticado) {
            setUsuario(usuarioAutenticado);
          } else {
            console.warn('Usuario autenticado no encontrado en la API');
          }
        } catch (error) {
          console.error('Error al obtener datos de la API:', error);
        } finally {
          setLoadingTipoUsuario(false);
        }
      };

      fetchUserData();
    }
  }, [isAuthenticated, user, fetchWithAuth]);

  useEffect(() => {
    if (usuario) {
      setFormData({
        contrasena: '',
        confirmarContrasena: '',
        nuevaContrasena: ''
      });
      setFormErrors({});
    }
  }, [usuario]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value
    }));
    setFormErrors(prevErrors => ({ ...prevErrors, [name]: null }));
  };


  const handleEditClick = () => {
    setIsEditable(true);
    setOriginalFormData({ ...formData });
    setUpdateSuccess(null);
  };


  const validateForm = () => {
    let errors = {};
    if (isEditable) {
      if (!formData.nuevaContrasena) {
        errors.nuevaContrasena = "La nueva contraseña es requerida";
      }
      if (formData.nuevaContrasena !== formData.confirmarContrasena) {
        errors.confirmarContrasena = "Las contraseñas no coinciden";
      }
    }
    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const handleCancelClick = () => {
    setIsEditable(false);
    setFormData(originalFormData);
    setFormErrors({});
    setUpdateSuccess(null);
  };

  const handleSaveClick = async (e) => {
    e.preventDefault();
    if (!validateForm()) {
      return;
    }

    try {
      const bodyData = {
        username: user.name,
        enabled: true,
        email: usuario.email,
        firstName: usuario.firstName,
        lastName: usuario.lastName,
        realmRoles: [usuario.tipoUsuario],
        cargo: usuario.cargo,
        unidadAsignada: usuario.unidadAsignada,
        idEmpresa: usuario.idEmpresa ? usuario.idEmpresa : null,
        credentials: [
          {
            "type": "password",
            "value": formData.nuevaContrasena,
            "temporary": false
          }
        ],
      };

      const response = await fetchWithAuth(`${API_URL}/usuario/${usuario.id}`, {
        method: "PUT",
        body: JSON.stringify(bodyData),
        headers: { "Content-Type": "application/json" },
      });

      if (response.ok) {
        console.log('Usuario actualizado correctamente.');
        setUpdateSuccess('Contraseña actualizada correctamente.');
        setOriginalFormData({ ...formData });
        setFormData((prevState) => ({
          ...prevState,
          contrasena: formData.nuevaContrasena,
          nuevaContrasena: '',
          confirmarContrasena: ''
        }));

      } else {
        const errorText = await response.text();
        console.error("Error del servidor:", errorText);
        setUpdateSuccess('Error al actualizar la contraseña.');
      }
    } catch (error) {
      console.error("Error al actualizar el usuario:", error.message);
      setUpdateSuccess('Error al actualizar la contraseña.');
    } finally {
      setIsEditable(false);
    }
  };

  return (
    <div>
      <form
        onSubmit={handleSaveClick}
        className="bg-white rounded-lg w-full h-full  flex flex-col"
      >
        <div className="relative my-4">
          <h1 className="text-xl text-sky-950 font-bold text-center">Contraseña</h1>
          {!isEditable ? (
            <button
              type="button"
              onClick={handleEditClick}
              className={`absolute top-0 right-0 h-8 w-8 text-white rounded-full flex items-center justify-center bg-orange-500 hover:bg-orange-600`}
            >
              <LuPencil className="h-5 w-5" />
            </button>
          ) : (
            <div className="absolute top-0 right-0 flex space-x-2">
              <button
                type="submit"
                className="h-8 w-8 text-white rounded-full flex items-center justify-center bg-sky-950 hover:bg-sky-700"
              >
                <LuCheck className="h-5 w-5" />
              </button>
              <button
                type="button"
                onClick={handleCancelClick}
                className="h-8 w-8 text-white rounded-full flex items-center justify-center bg-gray-400 hover:bg-gray-500"
              >
                <LuX className="h-5 w-5" />
              </button>
            </div>
          )}
        </div>

        {updateSuccess && (
          <div
            className={`mb-4 p-2 rounded ${updateSuccess.startsWith('Error')
              ? 'bg-red-100 text-red-700 border border-red-400'
              : 'bg-green-100 text-green-700 border border-green-400'
              }`}
          >
            {updateSuccess}
          </div>
        )}

        {/* Campo de Contraseña */}
        <div className="mb-4">
          <label className="block text-sm font-semibold mb-1">Contraseña</label>
          <input
            type="password"
            name="contrasena"
            onChange={handleChange}
            className="w-full px-3 py-2 border rounded bg-gray-200 text-xs"
            placeholder="*********"
            disabled
          />
        </div>

        {isEditable && (
          <div className="grid grid-cols-1 gap-4 mb-4">
            <div>
              <label className="block text-sm font-semibold mb-1">Nueva Contraseña</label>
              <input
                type="password"
                name="nuevaContrasena"
                value={formData.nuevaContrasena}
                onChange={handleChange}
                className={`w-full px-3 py-2 border rounded text-xs ${formErrors.nuevaContrasena ? 'border-red-500' : ''}`}
                placeholder="Ingrese nueva contraseña"
              />
              {formErrors.nuevaContrasena && <p className="text-red-500 text-sm">{formErrors.nuevaContrasena}</p>}
            </div>
            <div>
              <label className="block text-sm font-semibold mb-1">Confirmar Contraseña</label>
              <input
                type="password"
                name="confirmarContrasena"
                value={formData.confirmarContrasena}
                onChange={handleChange}
                className={`w-full px-3 py-2 border rounded text-xs  ${formErrors.confirmarContrasena ? 'border-red-500' : ''}`}
                placeholder="Confirme nueva contraseña"
              />
              {formErrors.confirmarContrasena && <p className="text-red-500 text-sm">{formErrors.confirmarContrasena}</p>}

            </div>
          </div>
        )}
      </form>
    </div>
  );
}