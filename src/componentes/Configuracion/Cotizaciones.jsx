import React, { useEffect, useState } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { LuTrash2, LuEye, LuPencil } from 'react-icons/lu';
import Loading from '../../componentes/Base/Loading';
import DetalleCotizacion from './DetalleCotizacion';

const MisCotizaciones = ({ initialized }) => {
    const [modulos, setModulos] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const { fetchWithAuth, user } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;
    const [selectedModulo, setSelectedModulo] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const fetchModulos = async () => {
        setLoading(true);
        try {
            const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
            const modulosData = await responseModulos.json();

            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuariosData = await responseUsuarios.json();

            const usuarioAutenticado = usuariosData.find((u) => u.username === user.name);

            const isAdminUser = usuarioAutenticado && usuarioAutenticado.rol === 'admin';

            let modulosFiltrados = modulosData;
            if (!isAdminUser) {
                const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
                const modulosUsuarioData = await responseModulosUsuario.json();
                if (modulosUsuarioData && modulosUsuarioData.modulos) {
                    modulosFiltrados = modulosData.filter(modulo => modulosUsuarioData.modulos.some(mod => mod.id === modulo.id));
                } else {
                    modulosFiltrados = [];
                }
            }

            // Obtener los modulos filtrados con su precio
            const modulosConPrecioTotal = await Promise.all(modulosFiltrados.map(async modulo => {
                try {
                    const response = await fetchWithAuth(`${API_URL}/modulos/${modulo.id}/ejecuciones`);
                    let totalEjecuciones = 0;
                    if (response.ok) {
                        const data = await response.json();
                        totalEjecuciones = data;
                    }

                    let precioFinal = modulo.precio
                    if (totalEjecuciones > 0) {
                        precioFinal = precioFinal * totalEjecuciones;
                    }

                    return {
                        ...modulo,
                        precioTotal: precioFinal,
                        totalEjecuciones: totalEjecuciones
                    };
                } catch (error) {
                    console.error("Error al obtener las ejecuciones:", error);
                    return {
                        ...modulo,
                        precioTotal: modulo.precio,
                        totalEjecuciones: 0,
                    };
                }
            }));

            setModulos(modulosConPrecioTotal);

        } catch (err) {
            console.error("Error al obtener los datos:", err.message);
            setError(new Error(err.message));
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (initialized) {
            fetchModulos();
        }
    }, [initialized, fetchWithAuth, API_URL, user]);

    const handleOpenModal = (modulo) => {
        setSelectedModulo(modulo);
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setSelectedModulo(null);
        setIsModalOpen(false);
    };

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isModalOpen]);
    
    if (loading) {
        return <Loading message="Cargando datos..." />;
    }

    if (error) {
        return <div className="text-center text-red-500">Error: {error.message}</div>;
    }

    return (
        <>
            <div className='flex p-2'>
                <div className="flex-grow rounded-lg overflow-x-auto border-sky-950 border my-4">
                    <table className="min-w-full divide-y divide-gray-300">
                        <thead>
                            <tr>
                                <th className="px-2 py-2 text-center font-bold text-xs text-gray-900 sm:table-cell">Módulo</th>
                                <th className="px-2 py-2 text-center font-semibold text-xs text-gray-900 sm:table-cell">Vigente</th>
                                <th className="px-2 py-2 text-center font-semibold text-xs text-gray-900 sm:table-cell">Precio (USD)</th>
                                {/* <th className="px-2 py-2 text-center font-semibold text-xs text-gray-900 sm:table-cell">Total Ejecuciones</th> */}
                                <th className="px-2 py-2 text-center font-semibold text-xs text-gray-900 sm:table-cell">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {Array.isArray(modulos) && modulos.length > 0 ? (
                                modulos.map((modulo, index) => (
                                    <tr key={modulo.id} className={`${index % 2 === 1 ? 'bg-white' : 'bg-gray-200'} items-center`}>
                                        <td className="px-2 py-2 text-xs font-bold text-sky-950 text-center sm:table-cell">
                                            {modulo.name}
                                        </td>
                                        <td className="px-2 py-2 text-xs text-center sm:table-cell">Vigente</td>
                                        <td className="px-2 py-2 text-xs text-center sm:table-cell">${modulo.precioTotal}</td>
                                        {/* <td className="px-2 py-2 text-xs text-center sm:table-cell">{modulo.totalEjecuciones}</td> */}
                                        <td className="px-2 py-2 text-xs text-center sm:table-cell">
                                            <div className="flex justify-center items-center space-x-2 sm:space-x-1">
                                                <button className="bg-transparent border-2 border-sky-900 rounded-full p-2 sm:p-1" onClick={() => handleOpenModal(modulo)}>
                                                    <LuEye className="lg:h-4 lg:w-4 text-sky-900 sm:h-3 sm:w-3" />
                                                </button>
                                                <button className="bg-transparent border-2 border-sky-900 rounded-full p-2 sm:p-1">
                                                    <LuPencil className="lg:h-4 lg:w-4 text-sky-900 sm:h-3 sm:w-3" />
                                                </button>
                                                <button className="bg-transparent border-2 border-sky-900 rounded-full p-2 sm:p-1">
                                                    <LuTrash2 className="lg:h-4 lg:w-4 text-sky-900 sm:h-3 sm:w-3" />
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="5" className="text-center">No hay módulos disponibles.</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>

            {/* Renderizar el modal */}
            {isModalOpen && (
                <DetalleCotizacion
                    isOpen={isModalOpen}
                    onClose={handleCloseModal}
                    modulo={selectedModulo}
                    usuario={user}
                />
            )}
        </>
    );
};

export default MisCotizaciones;