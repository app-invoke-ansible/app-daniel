import React, { useState, useEffect } from 'react';
import { LuUser } from 'react-icons/lu';
import logoInvokeRojo from '../../img/logoInvokeIcono.png';
import logoInvokeAzul from '../../img/logoInvokeIcono_azul.png';
import logoInvokeNaranjo from '../../img/logoInvokeIcono_naranjo.png';
import EliminarUsuario from './GestionUsuarios/EliminarUsuario';
import ModificarUsuario from './GestionUsuarios/ModificarUsuario';
import VerUsuario from './GestionUsuarios/VerUsuario';
import { useAuth } from '../../providers/AuthProvider';

const UserCard = ({ usuario, onUserDelete, onUserUpdate, onUpdate }) => {
  const [loading, setLoading] = useState(false);
  const { user } = useAuth(); // traer usuario actual
  const [currentUserId, setCurrentUserId] = useState(null);

  useEffect(() => {
    if (user && user.name === usuario.username) {
      setCurrentUserId(usuario.id);
    }
  }, [user, usuario.username, usuario.id]);

  // console.log(usuario.id, usuario.username);

  const handleUserUpdate = async (updatedUser) => {
    setLoading(true);
    await onUserUpdate(updatedUser);
    onUpdate(); // Llama a la función para obtener usuarios actualizados
    setLoading(false);
  };

  const handleUserDelete = async () => {
    setLoading(true);
    await onUserDelete(usuario.id);
    onUpdate(); // Llama a la función para obtener usuarios actualizados
    setLoading(false);
  };

  // Logo según el tipo de usuario
  const tipoLogo = () => {
    if (usuario.tipoUsuario === 'admin') {
      return logoInvokeRojo;
    } else if (usuario.tipoUsuario === 'visualizador') {
      return logoInvokeAzul;
    } else if (usuario.tipoUsuario === 'editor') {
      return logoInvokeNaranjo;
    }
    return logoInvokeRojo;
  };

  const isCurrentUser = currentUserId === usuario.id; // comparar usuario actual con la id del usuario a mostrar
  if (isCurrentUser) { // Si es el usuario actual, no renderizar nada
    return null;
  }
  return (
    <div className="bg-white border border-sky-950 shadow-md rounded-lg p-4 flex items-center relative w-full mb-6">
      <img src={tipoLogo()} alt="Logo Invoke" className="absolute top-2 right-2 w-8 h-8" />
      <div className="w-16 h-16 flex items-center justify-center rounded-full border border-sky-950 mr-4">
        <LuUser className="w-12 h-12 text-gray-600" />
      </div>
      <div className="flex-grow">
        <h2 className="text-sm text-sky-900 font-bold">{usuario.username}</h2>
        <p className="text-xs text-gray-800">{usuario.tipoUsuario}</p>
        <p className="text-xs text-gray-800">Estado: {usuario.enabled ? 'Activo' : 'Inactivo'}</p>
        <div className="flex items-center mt-2">
          <span className={`h-2 rounded-3xl transition-all duration-300 ${usuario.enabled ? 'bg-green-500' : 'bg-red-500'} w-full`} style={{ maxWidth: '80%' }}></span>
        </div>
      </div>
      <div className="flex flex-col items-end mt-14">
        <div className="flex space-x-2">
          <VerUsuario userId={usuario.id} usuario={usuario} />
          <ModificarUsuario
            usuario={usuario}
            onUpdate={handleUserUpdate}
            loading={loading}
          />
          <EliminarUsuario
            userId={usuario.id}
            onUserDelete={handleUserDelete}
            loading={loading}
          />
        </div>
      </div>
    </div>
  );
};

export default UserCard;