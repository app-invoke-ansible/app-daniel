import { useState, useEffect, useCallback } from 'react';
import { useAuth } from '../../../providers/AuthProvider';

export const BalanceData = () => {
  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;
  const [nombreEmpresa, setNombreEmpresa] = useState(''); // Nuevo estado para el nombre de la empresa
  const [data, setData] = useState([]);
  const [modulos, setModulos] = useState([]);
  const [ejecuciones, setEjecuciones] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [costosMensuales, setCostosMensuales] = useState([]);
  const [dataGrafico, setDataGrafico] = useState([]);
  const [selectedMonth, setSelectedMonth] = useState(null);
  const [availableMonths, setAvailableMonths] = useState([]);
  const [histogramData, setHistogramData] = useState([]);
  const [usuariosData, setUsuariosData] = useState([]);
  const [selectedModule, setSelectedModule] = useState(null);
  const [allUsersWithExecutions, setAllUsersWithExecutions] = useState([]); // Nuevo estado

  const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
  ];

  const agruparCostosMensuales = useCallback((ejecuciones) => {
    const costosMensuales = {};

    ejecuciones.forEach(ejecucion => {
      if (ejecucion.fechaHoraLanzamiento) {
        const fecha = new Date(ejecucion.fechaHoraLanzamiento);
        const mes = String(fecha.getMonth() + 1).padStart(2, '0');
        const anio = fecha.getFullYear();
        const mesAnio = `${anio}-${mes}`;

        if (!costosMensuales[mesAnio]) {
          costosMensuales[mesAnio] = {
            mes: mesAnio,
            costoTotal: 0,
            anio: anio,
            mesNumber: parseInt(mes)
          };
        }
        costosMensuales[mesAnio].costoTotal += ejecucion.precioTotal;
      }
    });
    return Object.values(costosMensuales).sort((a, b) => new Date(a.mes) - new Date(b.mes));
  }, []);

  const obtenerCostoDelMes = useCallback((costosMensuales, mesAnio) => {
    const mesData = costosMensuales.find(mes => mes.mes === mesAnio);
    return mesData ? mesData.costoTotal : 0;
  }, []);

  const fetchModulosAndEjecuciones = useCallback(async (idEmpresa) => { // Recibe idEmpresa
    setLoading(true);
    setError(null);
    try {
      const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
      const modulosData = await responseModulos.json();

      // 1. Obtener la información del usuario autenticado.
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      if (!responseUsuarios.ok) {
        throw new Error(`HTTP error! status: ${responseUsuarios.status}`);
      }
      const usuariosData = await responseUsuarios.json();

      setUsuariosData(usuariosData);

      const usuarioAutenticado = usuariosData.find((u) => u.username === user.name);

      // Verificar si el usuario autenticado existe y tiene un idEmpresa
      if (!usuarioAutenticado || !usuarioAutenticado.idEmpresa) {
        throw new Error("Usuario autenticado no encontrado o sin idEmpresa.");
      }

      const isAdminUser = usuarioAutenticado && usuarioAutenticado.rol === 'admin';

      let modulosFiltrados = modulosData;
      if (!isAdminUser) {
        const responseModulosUsuario = await fetchWithAuth(`${API_URL}/usuario/${usuarioAutenticado.id}/modulos`);
        const modulosUsuarioData = await responseModulosUsuario.json();

        if (modulosUsuarioData && modulosUsuarioData.modulos) {
          modulosFiltrados = modulosData.filter(modulo => modulosUsuarioData.modulos.some(mod => mod.id === modulo.id));
        } else {
          modulosFiltrados = [];
        }
      }
      // Calcular el precio total de los módulos
      const modulosConPrecioTotal = modulosFiltrados.map(modulo => ({
        ...modulo,
        precioTotal: modulo.precio * modulo.nivelImpacto,
      }));

      // 2. Obtener todas las ejecuciones de la empresa
      const responseEjecucionesEmpresa = await fetchWithAuth(`${API_URL}/ejecuciones/empresa/${idEmpresa}`); // Usa idEmpresa
      if (!responseEjecucionesEmpresa.ok) {
        throw new Error(`HTTP error fetching ejecuciones de la empresa! status: ${responseEjecucionesEmpresa.status}`);
      }
      const ejecucionesDataEmpresa = await responseEjecucionesEmpresa.json();

        //Set nombre de la empresa
        setNombreEmpresa(ejecucionesDataEmpresa.nombre);

      // 3. Asociar precioTotal y nombreUsuario a cada ejecución
      const ejecucionesConPrecio = ejecucionesDataEmpresa.ejecuciones.map(ejecucion => {
        const modulo = modulosConPrecioTotal.find(modulo => modulo.id === ejecucion.moduloId);
        const usuarioEjecucion = usuariosData.find(usuario => usuario.id === ejecucion.usuarioId);

        return {
          ...ejecucion,
          precioTotal: modulo ? modulo.precioTotal : 0,
          nombreUsuario: usuarioEjecucion ? usuarioEjecucion.username : 'Usuario Desconocido' //Asignar el nombre de usuario aqui
        };
      });

      // Agrupar ejecuciones por mes y calcular el costo total mensual
      const costosMensualesCalculados = agruparCostosMensuales(ejecucionesConPrecio);
      setCostosMensuales(costosMensualesCalculados);

      // Calculate histogram data here
      const currentMonth = new Date().getMonth();
      const histogramDataCalculated = Array(currentMonth + 1).fill(0);

      ejecucionesConPrecio.forEach(ejecucion => {
        const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
        const mesEjecucion = fechaEjecucion.getMonth();  // 0-indexado
        if (mesEjecucion <= currentMonth) {
          histogramDataCalculated[mesEjecucion] += ejecucion.precioTotal;
        }
      });
      setHistogramData(histogramDataCalculated);

      // Set available months for the select
      const availableMonthsOptions = costosMensualesCalculados.map(item => {
        const date = new Date(item.anio, item.mesNumber - 1);
        return {
          value: item.mes,
          label: `${monthNames[date.getMonth()]} ${date.getFullYear()}`
        };
      });
      setAvailableMonths(availableMonthsOptions);

      // Set initial selected month to current month
      const fechaActual = new Date();
      const anioActual = fechaActual.getFullYear();
      const mesActual = String(fechaActual.getMonth() + 1).padStart(2, '0');
      const mesAnioActual = `${anioActual}-${mesActual}`;
      setSelectedMonth(mesAnioActual);

      // Set Modulos
      setModulos(modulosConPrecioTotal);
      setEjecuciones(ejecucionesConPrecio);

      //  Obtener todos los usuarios que han realizado ejecuciones
      const uniqueUserIds = [...new Set(ejecucionesConPrecio.map(ejecucion => ejecucion.usuarioId))];
      const usersWithExecutions = usuariosData.filter(usuario => uniqueUserIds.includes(usuario.id));
      setAllUsersWithExecutions(usersWithExecutions);

    } catch (err) {
      setError(new Error(err.message));
      console.error("Error fetching data:", err);
    } finally {
      setLoading(false);
    }
  }, [fetchWithAuth, API_URL, user, agruparCostosMensuales]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await response.json();
        const usuarioAutenticado = usuarios.find((u) => u.username === user.name);

        if (usuarioAutenticado) {
          fetchModulosAndEjecuciones(usuarioAutenticado.idEmpresa); // Llama a la función y pasa el idEmpresa
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [fetchWithAuth, user, API_URL, fetchModulosAndEjecuciones]); // Añade fetchModulosAndEjecuciones como dependencia

  const handleMonthChange = (event) => {
    setSelectedMonth(event.target.value);
  };

  const handleModuleChange = (event) => {
    setSelectedModule(parseInt(event.target.value));
  };

  //Estados para guardar el costo del mes actual y pasado independientemente del select del gráfico
  const [costoMesActualFijo, setCostoMesActualFijo] = useState(0);
  const [costoMesPasadoFijo, setCostoMesPasadoFijo] = useState(0);

  useEffect(() => {
    // Calcular y establecer el costo del mes actual
    const fechaActual = new Date();
    const mes = String(fechaActual.getMonth() + 1).padStart(2, '0');
    const anio = fechaActual.getFullYear();
    const mesAnioActual = `${anio}-${mes}`;
    setCostoMesActualFijo(obtenerCostoDelMes(costosMensuales, mesAnioActual));

    // Calcular y establecer el costo del mes pasado
    const fechaMesPasado = new Date();
    fechaMesPasado.setMonth(fechaMesPasado.getMonth() - 1);
    const mesPasado = String(fechaMesPasado.getMonth() + 1).padStart(2, '0');
    const anioPasado = fechaMesPasado.getFullYear();
    const mesAnioPasado = `${anioPasado}-${mesPasado}`;
    setCostoMesPasadoFijo(obtenerCostoDelMes(costosMensuales, mesAnioPasado));
  }, [costosMensuales, obtenerCostoDelMes]);

  // Filtro de ejecuciones por mes
  const [ejecucionesFiltradas, setEjecucionesFiltradas] = useState([]);

  useEffect(() => {
    if (ejecuciones.length > 0) {
      const filtered = selectedMonth === "all"
        ? ejecuciones
        : ejecuciones.filter(ejecucion => {
          const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
          const mes = String(fechaEjecucion.getMonth() + 1).padStart(2, '0');
          const anio = fechaEjecucion.getFullYear();
          const mesAnio = `${anio}-${mes}`;
          return mesAnio === selectedMonth;
        });
      setEjecucionesFiltradas(filtered);
    }
  }, [ejecuciones, selectedMonth]);

  // Calcular el porcentaje de uso de cada módulo
  const [modulosConPorcentajeUso, setModulosConPorcentajeUso] = useState([]);

  useEffect(() => {
    if (ejecuciones.length > 0 && modulos.length > 0) {
      const totalEjecuciones = ejecuciones.length;

      const modulosConPorcentaje = modulos.map(modulo => {
        const ejecucionesModulo = ejecuciones.filter(ejecucion => ejecucion.moduloId === modulo.id);
        const porcentajeUso = (ejecucionesModulo.length / totalEjecuciones) * 100;
        return { ...modulo, porcentajeUso };
      });

      setModulosConPorcentajeUso(modulosConPorcentaje);
    }
  }, [ejecuciones, modulos]);

  const calcularPorcentajeAumento = useCallback(() => {
    if (costoMesPasadoFijo === 0) return 0;
    return (((costoMesActualFijo - costoMesPasadoFijo) / costoMesPasadoFijo) * 100).toFixed(2);
  }, [costoMesActualFijo, costoMesPasadoFijo]);

  const topUsuarios = () => {
    const usuariosCount = ejecuciones.reduce((acc, row) => {  //Usar ejecuciones en lugar de data
      acc[row.nombreUsuario] = (acc[row.nombreUsuario] || 0) + 1;
      return acc;
    }, {});
    return Object.entries(usuariosCount)
      .sort((a, b) => b[1] - a[1])
      .slice(0, 3)
      .map(([usuario]) => usuario);
  };

  const costoMesActual = costoMesActualFijo;
  const costoMesPasado = costoMesPasadoFijo;
  const porcentajeAumento = calcularPorcentajeAumento();

  const [availableModules, setAvailableModules] = useState([]);
  useEffect(() => {
    // Set Available Modules for Select
    setAvailableModules(modulos.map(modulo => ({
      value: modulo.id,
      label: modulo.name
    })));
  }, [modulos]);

  // useEffect para calcular dataGrafico
  useEffect(() => {
    const calculateChartData = async () => {
      if (ejecuciones.length > 0 && selectedMonth) {
        const [year, month] = selectedMonth.split('-');
        const anioSeleccionado = parseInt(year, 10);
        const mesSeleccionado = parseInt(month, 10) - 1;

        const ultimoDiaMesSeleccionado = new Date(anioSeleccionado, mesSeleccionado + 1, 0).getDate();
        const acumuladoDiario = [];

        // Crear un array de fechas para todo el mes
        let fechasDelMes = [];
        for (let i = 1; i <= ultimoDiaMesSeleccionado; i++) {
          fechasDelMes.push(new Date(anioSeleccionado, mesSeleccionado, i));
        }

        // Ordenar ejecuciones por fecha
        const ejecucionesFiltradas = ejecuciones.filter(ejecucion => {
          const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
          const esDelMes = fechaEjecucion.getFullYear() === anioSeleccionado && fechaEjecucion.getMonth() === mesSeleccionado;
          const esDelModulo = selectedModule ? ejecucion.moduloId === selectedModule : true; // Filtrar por módulo si está seleccionado

          return esDelMes && esDelModulo;
        }).sort((a, b) => new Date(a.fechaHoraLanzamiento) - new Date(b.fechaHoraLanzamiento));


        let acumulado = 0;
        const fechaActual = new Date(); // Obtener la fecha actual
        for (let fecha of fechasDelMes) {
          const fechaFormateada = `${fecha.getFullYear()}-${String(fecha.getMonth() + 1).padStart(2, '0')}-${String(fecha.getDate()).padStart(2, '0')}`;

          // Sumar los costos de ejecuciones hasta este día
          const costoHastaEsteDia = ejecucionesFiltradas.filter(ejecucion => {
            const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
            const fechaEjecucionFormateada = `${fechaEjecucion.getFullYear()}-${String(fechaEjecucion.getMonth() + 1).padStart(2, '0')}-${String(fechaEjecucion.getDate()).padStart(2, '0')}`;
            return fechaEjecucionFormateada <= fechaFormateada;
          }).reduce((total, ejecucion) => total + ejecucion.precioTotal, 0);

          // Si la fecha es posterior a la fecha actual, establecer el costo en null
          if (fecha > fechaActual) {
            acumuladoDiario.push({
              x: `${fecha.toLocaleDateString('default', { month: 'short', day: 'numeric' })}`,
              y: null, // Mostrar en blanco despues de la fecha actual
            });
          } else {
            acumuladoDiario.push({
              x: `${fecha.toLocaleDateString('default', { month: 'short', day: 'numeric' })}`,
              y: costoHastaEsteDia,
            });
          }
        }
        setDataGrafico(acumuladoDiario);
      }
    };

    calculateChartData();
  }, [ejecuciones, selectedMonth, selectedModule]);

  return {
    loading,
    error,
    dataGrafico,
    selectedMonth,
    availableMonths,
    availableModules,
    handleMonthChange,
    handleModuleChange,
    costoMesActual,
    costoMesPasado,
    porcentajeAumento,
    histogramData,
    modulos,
    costosMensuales,
    usuariosData,
    selectedModule,
    modulosConPorcentajeUso,
    ejecucionesFiltradas,
    topUsuarios,
    allUsersWithExecutions, 
    nombreEmpresa 
  };
};