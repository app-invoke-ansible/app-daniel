import React, { useState } from 'react';
import { BalanceData } from './BalanceData';
import Loading from '../../Base/Loading';
import CostChart from './CostChart';
import DonutChart from '../../DonutChart';
import HistogramChart from '../../HistogramChart';
import TabFacturacion from './TabFacturacion';
import { Link } from 'react-router-dom';
import ModuleRankingList from './ModuleRankingList';
import ModuleTableDetails from './ModuleTableDetails';
import ModulePorcentajeList from './ModulePorcentajeList';
import InfoPopup from './InfoPopup';

const BalanceResumen = () => {

    const {
        loading,
        error,
        dataGrafico,
        selectedMonth,
        ejecuciones,
        availableMonths,
        availableModules,
        handleMonthChange,
        handleModuleChange,
        costoMesActual,
        costoMesPasado,
        porcentajeAumento,
        histogramData,
        modulos,
        costosMensuales,
        usuariosData,
        selectedModule,
        modulosConPorcentajeUso,
        ejecucionesFiltradas,
        nombreEmpresa
    } = BalanceData();

    const [isPopupOpen, setPopupOpen] = useState(false);
    const [popupContent, setPopupContent] = useState(null); 
    const [popupTitle, setPopupTitle] = useState('');


    if (loading) {
        return <Loading />;
    }

    if (error) {
        return <div className="text-red-500">Error: {error.message}</div>;
    }

    const handleOpenPopup = (title,content) => {
        setPopupTitle(title);
        setPopupContent(content); // Establecer el contenido dinámico
        setPopupOpen(true); // Abrir el popup
    };

    return (
            <div className="p-2">
                <h2 className="text-xl text-sky-900 font-bold mb-2 text-center uppercase">{`Balance General de ${nombreEmpresa}`}</h2>
                <div className="grid grid-cols-1 lg:grid-cols-3 gap-6">
                    {/* Filtros */}
                    <div className="col-span-1 md:col-span-2 bg-white shadow-md rounded-3xl border border-sky-950 p-4 flex flex-col">
                        <div className="flex flex-wrap items-center gap-4 p-4">
                            {/* Selector de Mes */}
                            <div className="flex flex-wrap sm:flex-nowrap items-center w-auto">
                                <label htmlFor="monthSelect" className="mr-2 font-semibold text-gray-700">Seleccionar Mes:</label>
                                <select 
                                    id="monthSelect" 
                                    value={selectedMonth} 
                                    onChange={handleMonthChange} 
                                    className="rounded-3xl border border-sky-950 px-3 py-2 text-gray-700 focus:ring-sky-500 focus:border-sky-500 w-auto"
                                >
                                    <option className="w-auto" value="all">Todos los Meses</option>
                                    {availableMonths.map(month => (
                                        <option className="w-auto" key={month.value} value={month.value}>{month.label}</option>
                                    ))}
                                </select>
                            </div>
                            
                            {/* Selector de Modulos */}
                            <div className="flex flex-wrap sm:flex-nowrap items-center w-auto">
                                <label htmlFor="moduleSelect" className="mr-2 font-semibold text-gray-700">Seleccionar Módulo:</label>
                                <select 
                                    id="moduleSelect" 
                                    value={selectedModule || ''} 
                                    onChange={handleModuleChange} 
                                    className="rounded-3xl border border-sky-950 px-3 py-2 text-gray-700 focus:ring-sky-500 focus:border-sky-500 w-auto"
                                >
                                    <option value="" className="w-auto">Todos los Módulos</option>
                                    {availableModules.map(module => (
                                        <option className="w-auto" key={module.value} value={module.value}>{module.label}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        {/* Cost Chart */}
                        <CostChart data={[{ id: 'selected', data: dataGrafico }]} title={`Balance Mensual  ${selectedMonth}`} />
                        {/* Botón*/}
                        <div className="text-right">
                            <button 
                                onClick={() => handleOpenPopup(
                                    "Información de Balance",
                                    <div>
                                        <p>El gráfico muestra el balance de costos para el mes de {selectedMonth}.</p>
                                        <p>El costo total para este mes es de <strong>${costoMesActual} USD</strong>.</p>
                                        <p>En comparación con el mes pasado, hubo un {porcentajeAumento > 0 ? "aumento" : "disminución"} del <strong>{Math.abs(porcentajeAumento)}%</strong>.</p>
                                        <p>Posibles causas de esta variación incluyen cambios en la demanda, ajustes en los precios o modificaciones en los módulos utilizados.</p>
                                    </div>
                                )} 
                                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 w-fit flex-shrink-0">
                                Más información
                            </button>
                        </div>
                    </div>
                    {/* Resumen General */}
                    <div className="col-span-1 bg-gradient-to-br from-white to-gray-50 rounded-3xl border border-sky-950 shadow-lg p-6 ">
                        <h3 className="text-3xl text-center font-bold mb-6 text-gray-800 md:mt-[15%]">Resumen General</h3>
                        <div className="grid grid-cols-1 gap-6">
                            <div className="p-4">
                                <h3 className="text-lg font-medium text-gray-600 mb-2">Balance del Mes Actual</h3>
                                <p className="text-4xl font-bold text-green-600">${costoMesActual} USD</p>
                                <p className="text-sm text-gray-500 mt-2">
                                    <span className="font-semibold">Aumento</span> del {porcentajeAumento}% con el mes pasado
                                </p>
                            </div>
                            <div className="p-4">
                                <p className="text-lg font-medium text-gray-600 mb-2">Balance total mes pasado</p>
                                <p className="text-4xl font-bold text-green-600">${costoMesPasado} USD</p>
                            </div>
                        </div>
                        {/* Botón*/}
                        <div className="text-right">
                            <button 
                                onClick={() => handleOpenPopup(
                                    "Información de Resumen",
                                    <div>
                                        <p>El balance del mes actual es de <strong>${costoMesActual} USD</strong>.</p>
                                        <p>En comparación con el mes pasado, que tuvo un balance de <strong>${costoMesPasado} USD</strong>, hubo un {porcentajeAumento > 0 ? "aumento" : "disminución"} del <strong>{Math.abs(porcentajeAumento)}%</strong>.</p>
                                        <p>Este cambio puede deberse a factores como la adopción de nuevos módulos, cambios en la demanda de los usuarios.</p>
                                    </div>
                                )} 
                                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 w-fit flex-shrink-0">
                                Más información
                            </button>
                        </div>
                    </div>
                    {/* Module Porcentaje List */}
                    <div className="col-span-1 rounded-3xl border border-sky-950 shadow-md px-4 py-2">
                        <ModulePorcentajeList modules={modulosConPorcentajeUso} />
                        <div className="text-right">
                            <Link to="/reportesmodulos" className="text-blue-600 font-semibold hover:underline">Ver más</Link>
                        </div>
                    </div>
                    {/* Donut Chart */}
                    <div className="col-span-1 md:col-span-2 bg-white rounded-3xl border border-sky-950 shadow-md p-4 px-4 py-2">
                        <h3 className="text-xl text-center font-bold mb-4 text-gray-800">Porcentaje de Uso de Módulos</h3>
                        <DonutChart modulos={modulosConPorcentajeUso} />
                        <div className="text-right">
                            <Link to="/reportesmodulos" className="text-blue-600 font-semibold hover:underline">Ver más</Link>
                        </div>
                    </div>
                    {/* Tab Facturacion */}
                    <div className="col-span-1 md:col-span-2 rounded-3xl border border-sky-950 shadow-md p-4 px-4 py-2">
                        <TabFacturacion data={ejecuciones} />
                        {/* Botón*/}
                        <div className="text-right">
                            <button 
                                onClick={() => handleOpenPopup(
                                    "Información de Facturación",
                                    <div>
                                        <p>La tabla muestra las ejecuciones (facturaciones) realizadas en el mes de {selectedMonth}.</p>
                                        <p>Total de ejecuciones: <strong>{ejecuciones?.length || 0}</strong>.</p>
                                        {ejecuciones?.length > 0 ? (
                                            <>
                                                <p>Las ejecuciones más costosas fueron:</p>
                                                <ul>
                                                    {ejecuciones.slice(0, 3).map((ejecucion, index) => (
                                                        <li key={index}>
                                                            <strong>{ejecucion.nombre}:</strong> ${ejecucion.costo} USD
                                                        </li>
                                                    ))}
                                                </ul>
                                            </>
                                        ) : (
                                            <p>No hay ejecuciones registradas para este mes.</p>
                                        )}
                                        <p>Se recomienda revisar las ejecuciones recurrentes para identificar oportunidades de optimización.</p>
                                    </div>
                                )} 
                                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 w-fit flex-shrink-0">
                                Más información
                            </button>
                        </div>
                    </div>
                    {/* Module Ranking List */}
                    <div className="col-span-1 rounded-3xl border border-sky-950 p-4 shadow-md px-4 py-2">
                        <ModuleRankingList modules={modulos} />
                        <div className="text-right">
                            <Link to="/reportesmodulos" className="text-blue-600 font-semibold hover:underline">Ver más</Link>
                        </div>
                    </div>
                </div>
                <br></br>
                {/* Module Table Details */}
                <div className="grid grid-cols-1 rounded-3xl border border-sky-950 shadow-md p-4 px-4 py-2">
                        <ModuleTableDetails modulos={modulos} ejecuciones={ejecucionesFiltradas} usuariosData={usuariosData} selectedMonth={selectedMonth} />
                </div>
                <br></br>
                {/* Histogram Chart */}
                <div className="col-span-1 bg-white rounded-3xl border border-sky-950 shadow-md p-4 px-4 py-2">
                    <HistogramChart costosMensuales={histogramData} />
                </div>
                
                {/* Popup */}
                <InfoPopup 
                    isOpen={isPopupOpen} 
                    onClose={() => setPopupOpen(false)} 
                    title={popupTitle} // Título dinámico
                    >
                    {popupContent} {/* Contenido dinámico */}
                </InfoPopup>
            </div>
    );
};

export default BalanceResumen;