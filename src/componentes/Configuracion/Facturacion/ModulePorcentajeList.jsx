import React from 'react';

const ModuleUsageList = ({ modules }) => {
  return (
    <div className="p-4">
      <h2 className="text-xl text-center font-bold mb-2 text-gray-800">Uso de Módulos</h2>
      <p className="text-sm text-gray-600 mb-4">Distribución del uso de módulos.</p>
      <ul className="list-none p-0">
        {modules.map(module => (
          <li key={module.id} className="mb-3">
            <div className="text-gray-700 font-medium">{module.name}</div>
            <div className="bg-gray-100 rounded-full h-5 relative overflow-hidden flex items-center">
              {module.porcentajeUso > 0 ? (
                <>
                  <div
                    className="bg-pink-400 h-full rounded-full transition-width duration-500 flex items-center justify-end"
                    style={{ width: `${Math.min(module.porcentajeUso, 100)}%` }}
                  >
                    {module.porcentajeUso >= 5 ? (
                      <span className="px-2 text-white text-xs font-bold">{module.porcentajeUso.toFixed(0)}%</span>
                    ) : null} 
                  </div>
                  {module.porcentajeUso < 5 && (
                    <span className="ml-2 text-gray-900 text-xs">{module.porcentajeUso.toFixed(1)}%</span>
                  )}
                </>
              ) : (
                <div className="h-full flex items-center justify-center text-gray-400 text-xs italic">
                  Sin uso
                </div>
              )}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ModuleUsageList;