import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';
import Loading from '../../Base/Loading';

const ModuleUsageList = ({ selectedMonth, ejecuciones }) => {
  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);

        // Filtrar ejecuciones por mes si es necesario
        const ejecucionesFiltradas = selectedMonth === "all"
          ? ejecuciones
          : ejecuciones?.filter(ejecucion => {  
            const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
            const mes = String(fechaEjecucion.getMonth() + 1).padStart(2, '0');
            const anio = fechaEjecucion.getFullYear();
            const mesAnio = `${anio}-${mes}`;
            return mesAnio === selectedMonth;
          }) || []; 


        // Agrupar las ejecuciones por moduloId
        const ejecucionesPorModulo = ejecucionesFiltradas.reduce((acc, ejecucion) => {
          const { moduloId, nombreUsuario } = ejecucion;

          if (!acc[moduloId]) {
            acc[moduloId] = {
              moduloId: moduloId,
              usuarios: new Set()
            };
          }
          acc[moduloId].usuarios.add(nombreUsuario);

          return acc;
        }, {});

        // Convierte la info en un array
        const formattedData = Object.values(ejecucionesPorModulo).map(item => ({
          moduloId: item.moduloId,
          usuarios: Array.from(item.usuarios)
        }));

        setData(formattedData);
      } catch (error) {
        console.error('Error al obtener datos de la API:', error.message);
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [ejecuciones, selectedMonth, fetchWithAuth, user, API_URL]);


  if (loading) {
    return <Loading message="Cargando datos..." />;
  }

  if (error) {
    return <div className="text-red-500">Error: {error}</div>;
  }

  return (
    <div className="bg-white border border-gray-200 rounded-lg p-4 shadow-md">
      <h2 className="text-lg font-semibold mb-2 text-gray-800">Módulos y Usuarios</h2>
      <p className="text-sm text-gray-600 mb-4">Lista de módulos y usuarios que los han ejecutado.</p>
      {data.length === 0 ? (
        <div className="text-center text-gray-500">No hay ejecuciones de módulos.</div>
      ) : (
        <ul className="list-none p-0">
          {data.map(item => (
            <li key={item.moduloId} className="mb-4">
              <h3 className="text-md font-semibold text-gray-700">Módulo {item.moduloId}</h3>
              {item.usuarios.length > 0 ? (
                <ul className="list-none p-0 ml-4">
                  {item.usuarios.map((usuario, index) => (
                    <li key={index} className="text-sm text-gray-600">• {usuario}</li>
                  ))}
                </ul>
              ) : (
                <div className="text-sm text-gray-500 italic ml-4">Sin usuarios.</div>
              )}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ModuleUsageList;