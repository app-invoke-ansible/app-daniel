import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';
import Loading from '../../Base/Loading';

const TabFacturacion = () => {
  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;
  const [data, setData] = useState([]);
  const [nombreEmpresa, setNombreEmpresa] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await response.json();
        const usuarioAutenticado = usuarios.find((u) => u.username === user.name);

        if (usuarioAutenticado) {
          const ejecucionesResponse = await fetchWithAuth(`${API_URL}/ejecuciones/empresa/${usuarioAutenticado.idEmpresa}`);
          const ejecucionesData = await ejecucionesResponse.json();

          setNombreEmpresa(ejecucionesData.nombre);
          setData(ejecucionesData.ejecuciones);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [fetchWithAuth, user, API_URL]);

  // Agrupar datos por usuario
  const groupedData = data.reduce((acc, row) => {
    const nombreUsuario = row.nombreUsuario;
    if (!acc[nombreUsuario]) {
      acc[nombreUsuario] = {
        nombreUsuario: nombreUsuario,
        ejecuciones: 0,
      };
    }
    acc[nombreUsuario].ejecuciones++;
    return acc;
  }, {});

  // Convertir el objeto agrupado a un array
  const groupedArray = Object.values(groupedData);

  // Calcular el total de ejecuciones
  const totalEjecuciones = groupedArray.reduce((sum, user) => sum + user.ejecuciones, 0);

  // Calcular el porcentaje de ejecuciones por usuario
  const usersWithPercentage = groupedArray.map(user => ({
    ...user,
    percentage: ((user.ejecuciones / totalEjecuciones) * 100).toFixed(2),
  }));

  // Ordenar a los usuarios por la cantidad de ejecuciones
  const sortedUsers = [...usersWithPercentage].sort((a, b) => b.ejecuciones - a.ejecuciones);

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 5;
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = sortedUsers.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className="p-4">
      {loading ? (
        <Loading message="Cargando datos..." />
      ) : (
        <>
          <div className="">
            <h2 className="text-xl text-center font-bold mb-2 text-gray-800">Ejecuciones de Usuarios</h2>
            <p className="text-sm text-gray-600 mb-4">Porcentaje de ejecuciones por usuarios de {nombreEmpresa}.</p>
            <ul className="list-none p-0">
              {currentItems.map(user => (
                <li key={user.nombreUsuario} className="mb-3 flex items-center">
                  <div className="flex items-center w-full">
                    <div className="w-10 h-10 rounded-full bg-gray-300 flex items-center justify-center mr-3">
                      {/* Puedes reemplazar esto con un icono de usuario */}
                      <span>{user.nombreUsuario.charAt(0).toUpperCase()}</span>
                    </div>
                    <div className="flex-grow">
                      <div className="text-gray-700 font-medium">{user.nombreUsuario}</div>
                      <div className="text-sm text-gray-500">
                        Ejecuciones: {user.ejecuciones} ({user.percentage}%)
                      </div>
                      {/* Barra de porcentaje */}
                      <div className="bg-gray-200 rounded-full h-2">
                        <div
                          className="bg-sky-500 rounded-full h-2"
                          style={{ width: `${user.percentage}%` }}
                        ></div>
                      </div>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
    </div>
  );
};

export default TabFacturacion;