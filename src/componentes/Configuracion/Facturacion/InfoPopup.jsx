import React from 'react';

const InfoPopup = ({ isOpen, onClose, title, children }) => {
    if (!isOpen) return null;

    return (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
            <div className="bg-white p-6 rounded-lg shadow-lg w-2/3 flex flex-wrap lg:w-1/3">
            <h2 className="text-lg font-bold">{title}</h2>  
                <div>{children}

                </div>
                <button 
                    onClick={onClose} 
                    className="mt-4 bg-red-500 text-white px-4 py-2 rounded-md hover:bg-red-600">
                    Cerrar
                </button>
            </div>
        </div>
    );
};

export default InfoPopup;