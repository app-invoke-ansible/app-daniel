import React from 'react';

const MenuSuperior = ({ activeTab, setActiveTab }) => {
  const options = ["Inicio", "Formas de pago", "Reportes"];

  return (
    <nav className="bg-white dark:bg-black dark:text-white flex justify-center space-x-6 px-6 py-4 border-b border-gray-200 shadow-sm w-full">
      {options.map((option) => (
        <button
          key={option}
          onClick={() => setActiveTab(option)}
          className={`mt-5 md:mt-0 text-xl font-semibold tracking-tight block cursor-pointer p-2 hover:text-blue-500 transition-colors duration-300
            ${activeTab === option ? "text-black font-bold underline" : "text-sky-950 hover:underline"}`}
        >
          {option}
        </button>
      ))}
    </nav>
  );
};

export default MenuSuperior;
