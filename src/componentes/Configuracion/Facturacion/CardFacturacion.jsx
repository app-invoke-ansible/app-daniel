import React, { useState } from 'react';
import MenuSuperior from './MenuSuperior';
import BalanceResumen from './BalanceResumen';

const CardFacturacion = () => {
  const [activeTab, setActiveTab] = useState("Inicio");

  const renderContent = () => {
    switch (activeTab) {
      case "Inicio":
        return <BalanceResumen />;
      case "Formas de pago":
        return <div>Información sobre formas de pago disponibles.</div>;
      case "Reportes":
        return <div>Información sobre los Reportes.</div>;
      default:
        return <div>Selecciona una opción del menú.</div>;
    }
  };

  return (
    <div className="flex flex-col">
      <MenuSuperior activeTab={activeTab} setActiveTab={setActiveTab} />
      <div className="p-4">{renderContent()}</div>
    </div>
  );
};

export default CardFacturacion;
