import React, { useState, useEffect } from 'react';
import { IoStar, IoStarHalf, IoStarOutline } from 'react-icons/io5';
import { useAuth } from '../../../providers/AuthProvider';

const ModuleRankingList = ({ modules }) => {
    const { fetchWithAuth } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;
    const [moduleRatings, setModuleRatings] = useState({}); // Objeto para guardar la valoracion promedio de cada modulo

    useEffect(() => {
        const fetchModuleRatings = async () => {
            const ratings = {};
            for (const module of modules) {
                try {
                    const response = await fetchWithAuth(`${API_URL}/comentarios/modulo/${module.id}/resenas`);
                    const data = await response.json();
                    if (response.ok) {
                        ratings[module.id] = data.valoracionPromedio || 0; 
                    } else {
                        console.error(`Error al obtener las reseñas del módulo ${module.name}:`, data);
                        ratings[module.id] = 0; 
                    }
                } catch (error) {
                    console.error(`Error al realizar la solicitud GET para el módulo ${module.name}:`, error);
                    ratings[module.id] = 0;
                }
            }
            setModuleRatings(ratings);
        };

        if (modules && modules.length > 0) {
            fetchModuleRatings();
        }
    }, [modules, fetchWithAuth, API_URL]);

    const renderStars = (valoracionPromedio) => {
        const stars = [];
        const fullStars = Math.floor(valoracionPromedio);
        const halfStar = valoracionPromedio % 1 >= 0.5 ? 1 : 0;
        const emptyStars = 5 - fullStars - halfStar;

        for (let i = 0; i < fullStars; i++) {
            stars.push(<IoStar key={`full-${i}`} className="inline text-orange-500 h-4 w-4" />);
        }

        if (halfStar) {
            stars.push(<IoStarHalf key="half" className="inline text-orange-500 h-4 w-4" />);
        }

        for (let i = 0; i < emptyStars; i++) {
            stars.push(<IoStarOutline key={`empty-${i}`} className="inline text-gray-400 h-4 w-4" />);
        }

        return stars;
    };

    return (
        <div className="p-4">
            <h2 className="text-xl text-center font-bold mb-2 text-gray-800">Ranking de Módulos</h2>
            <p className="text-sm text-gray-600 mb-4">Valoración promedio de los módulos.</p>
            <ul className="list-none p-0">
                {modules.map(module => (
                    <li key={module.id} className="mb-3 flex items-center justify-between">
                        <div className=" text-sky-900 px-3 py-1 rounded font-medium">{module.name}</div>
                        <div className="flex items-center">
                            <span className="text-sky-900 text-lg font-bold">{renderStars(moduleRatings[module.id] || 0)}</span>
                            <span className="ml-2 text-sm text-gray-500">({(moduleRatings[module.id] || 0).toFixed(1)})</span>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ModuleRankingList;