import React from 'react';

const ModuleTableDetails = ({ modulos, ejecuciones, selectedMonth }) => {

    //Filtrar ejecuciones por mes
    const ejecucionesFiltradas = selectedMonth === "all"
        ? ejecuciones
        : ejecuciones.filter(ejecucion => {
            const fechaEjecucion = new Date(ejecucion.fechaHoraLanzamiento);
            const mes = String(fechaEjecucion.getMonth() + 1).padStart(2, '0');
            const anio = fechaEjecucion.getFullYear();
            const mesAnio = `${anio}-${mes}`;
            return mesAnio === selectedMonth;
        });

    const modulosConDetalles = modulos.map(modulo => {
        //Filtrar ejecuciones por modulo
        const ejecucionesModulo = ejecucionesFiltradas.filter(ejecucion => ejecucion.moduloId === modulo.id);
        const cantidadEjecuciones = ejecucionesModulo.length;
        const valorTotal = cantidadEjecuciones * modulo.precioTotal;

        // Extraer usuarios que ejecutaron el modulo sin duplicados
        const usuarios = [...new Set(ejecucionesModulo.map(ejecucion => ejecucion.nombreUsuario))];

        return {
            ...modulo,
            cantidadEjecuciones,
            valorTotal,
            usuarios
        };
    });

    return (
        <div className="p-6 overflow-x-auto">
            <h2 className="text-xl text-center font-bold mb-4 text-gray-800">Detalles de Módulos</h2>
            <p className="text-sm text-gray-600 mb-6"></p>
            <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gradient-to-r from-blue-50 to-blue-100">
                    <tr>
                        <th className="px-6 py-3 text-left text-sm font-semibold text-blue-800 uppercase tracking-wider">Módulo</th>
                        <th className="px-6 py-3 text-left text-sm font-semibold text-blue-800 uppercase tracking-wider">Ejecuciones</th>
                        <th className="px-6 py-3 text-left text-sm font-semibold text-blue-800 uppercase tracking-wider">Valor Individual</th>
                        <th className="px-6 py-3 text-left text-sm font-semibold text-blue-800 uppercase tracking-wider">Valor Total</th>
                        {/* <th className="px-6 py-3 text-left text-sm font-semibold text-blue-800 uppercase tracking-wider">Usuarios</th> */}
                    </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                    {modulosConDetalles.map(modulo => (
                        <tr key={modulo.id} className="hover:bg-gray-50 transition duration-200">
                            <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{modulo.name}</td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{modulo.cantidadEjecuciones}</td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">${modulo.precioTotal}</td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">${modulo.valorTotal.toFixed(2)}</td>
                            {/* <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{modulo.usuarios.join(', ')}</td> */}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ModuleTableDetails;