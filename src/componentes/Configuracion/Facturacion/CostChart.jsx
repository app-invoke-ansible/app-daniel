import React, { useMemo, useState } from "react";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, Filler } from "chart.js";

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, Filler);

const CostChart = ({ data, title }) => {
  const [selectedCost, setSelectedCost] = useState(null);

  const chartData = useMemo(() => {
    const labels = data.length > 0 && data[0].data ? data[0].data.map(item => item.x) : [];
    const datasets = [
      {
        label: 'Balance mensual',
        data: data.length > 0 && data[0].data ? data[0].data.map(item => item.y) : [],
        borderColor: 'rgba(75, 192, 192, 1)', // Verde más vibrante
        backgroundColor: (context) => {
          const ctx = context.chart.ctx;
          const gradient = ctx.createLinearGradient(0, 0, 0, 400);
          backgroundColor: 'rgba(75, 192, 192, 0.48)', // Verde más translúcido
          gradient.addColorStop(0, 'rgba(75, 192, 192, 0.6)');
          gradient.addColorStop(1, 'rgba(75, 192, 192, 0.1)');
          return gradient;
        },
        fill: true,
        tension: 0.4,
        pointRadius: (context) => {
          const dataset = context.dataset.data;
          const index = context.dataIndex;

          if (index === 0 || dataset[index] !== dataset[index - 1]) {
            return dataset[index] > 0 ? 4 : 0; // Puntos un poco más grandes
          } else {
            return 0;
          }
        },
        pointHoverRadius: 10, // Hover más grande
        pointBackgroundColor: 'rgb(49, 128, 128)', // Color de los puntos
      }
    ];

    return {
      labels,
      datasets,
    };
  }, [data, title]);

  const options = useMemo(() => ({
    responsive: true,
    maintainAspectRatio: false,
    onClick: (event, elements) => {
      if (elements.length > 0) {
        const index = elements[0].index;
        const datasetIndex = elements[0].datasetIndex;
        const dataset = chartData.datasets[datasetIndex];
        const day = chartData.labels[index];
        const cost = dataset.data[index];

        if (cost > 0) {
          setSelectedCost({
            day: day,
            cost: cost,
            dataset: dataset.label
          });
        } else {
          setSelectedCost(null);
        }
      } else {
        setSelectedCost(null);
      }
    },
    plugins: {
      legend: {
        display: true, // Mostrar la leyenda
        position: 'bottom', // Posición de la leyenda
        labels: {
          font: {
            size: 14 // Tamaño de fuente de la leyenda
          }
        }
      },
      title: {
        display: true,
        text: title,
        padding: {
          top: 0, // Más espacio arriba
          bottom: 30, // Más espacio abajo
        },
        font: {
          size: 25, // Título más grande
          color: "black",
        },
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.8)', // Fondo más oscuro
        titleColor: '#fff', // Título blanco
        bodyColor: '#fff', // Cuerpo blanco
        borderColor: '#ddd', // Borde gris claro
        borderWidth: 1,
        callbacks: {
          label: (context) => {
            let label = context.dataset.label || '';

            if (label) {
              label += ': ';
            }
            if (context.parsed.y !== null) {
              label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y);
            }
            return label;
          }
        }
      }
    },
    scales: {
      x: {
        display: true,
        grid: {
          display: false,
        },
        title: {
          display: false,
        },
        ticks: {
          color: "rgba(0, 0, 0, 0.5)",
          font: {
            size: 12 // Tamaño de fuente en el eje X
          }
        },
      },
      y: {
        type: 'linear',
        display: true,
        position: 'left',
        grid: {
          color: 'rgba(0, 0, 0, 0.1)',
        },
        title: {
          display: false,
        },
        ticks: {
          min: 0,
          max: 200,
          stepSize: 10,
          color: "rgba(0, 0, 0, 0.5)",
          font: {
            size: 14 // Tamaño de fuente en el eje Y
          },
          callback: function (value) {
            return '$' + value;
          }
        }
      },
    },
    elements: {
      line: {
        borderWidth: 4, // Líneas más gruesas
        tension: 0.4,
      },
    },
    animation: {
      duration: 1000, // Animación más lenta
      easing: 'easeInOutQuad', // Easing suave
    },  
  }), [chartData, title]);

  return (
    <div className="w-full h-full p-6"> {/* Más padding */}
        <div className="bg-white p-4 rounded-lg h-80 md:h-96 lg:h-96"> {/* Ajuste de altura y sombra */}
          <Line data={chartData} options={options} />
        </div>
        {selectedCost && (
          <div className="mt-4 text-center bg-gray-100 py-6 rounded-lg shadow"> {/* Más padding y sombra */}
            Costo de {selectedCost.day} ({selectedCost.dataset}):{" "}
            <span className="font-semibold">
              {new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(selectedCost.cost)}
            </span>
          </div>
        )}
    </div>
  );
};

export default CostChart;