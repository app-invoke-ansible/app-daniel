import React from 'react';
import DonutChart from '../../DonutChart';

const TopModules = ({ top3Modulos, usuariosData }) => {
    return (
        <div className="bg-white px-2 rounded-lg sm:w-full max-w-96 sm:max-w-full h-full grid sm:grid-cols-3 grid-cols-1 gap-4 mb-6">
            {top3Modulos.map(modulo => (
                <div key={modulo.id} className='border border-sky-950 rounded-3xl p-6 flex flex-col'>
                    <h2 className="text-base font-semibold text-gray-800">{modulo.name}</h2>
                    <DonutChart modulos={[modulo]} />
                    <div>Total de ejecuciones: {modulo.totalEjecuciones}</div>
                    <div>
                        Usuarios: {usuariosData && modulo.usuarios.map(usuarioId => {
                            const usuario = usuariosData.find(u => u.id === usuarioId);
                            // console.log(usuariosData)
                            return <span key={usuarioId}>{usuario ? usuario.username : 'Usuario Desconocido'}, </span>;
                        })}
                    </div>
                </div>
            ))}
        </div>
    );
};

export default TopModules;