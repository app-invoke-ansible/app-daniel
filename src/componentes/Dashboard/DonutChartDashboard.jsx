import React, { useState, useEffect } from 'react';
import { Chart } from 'react-google-charts';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

const DonutChartDashboard = ({ id }) => {
  const { fetchWithAuth, user } = useAuth();
  const [loading, setLoading] = useState(true);
  const [ejecucionesExitosas, setEjecucionesExitosas] = useState(0);
  const [ejecucionesFallidas, setEjecucionesFallidas] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      if (!id || id === "more") {
        setLoading(false);
        return;
      }

      try {
        setLoading(true);

        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
            console.error("Usuario autenticado no encontrado en la API.");
            setData({ labels: [], datasets: [] });
            return;
        }

        const idUsuario = usuarioAutenticado.id;

        // Obtener los módulos globales
        const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
        const dataGlobal = await responseGlobal.json();

        // Obtener los módulos adquiridos por el usuario
        const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
        const dataUsuario = await responseUsuario.json();

        // Obtener los IDs de los módulos adquiridos por el usuario
        const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

        // Filtrar los módulos globales por los módulos adquiridos
        const modulosAdquiridos = dataGlobal.filter((modulo) =>
          modulosUsuarioIds.includes(modulo.id)
        );

        // Filtrar módulos por ID de categoría
        const filteredModules = modulosAdquiridos.filter(
          (modulo) => modulo.categoria.id === parseInt(id)
        );
        const ejecuciones = filteredModules.flatMap((modulo) => modulo.ejecuciones || []);

        // Calcular estadísticas de ejecuciones
        const totalEjecucionesExitosas = ejecuciones.filter(
          (ejec) => ejec.status === "success"
        ).length;
        const totalEjecucionesFallidas = ejecuciones.filter(
          (ejec) => ejec.status !== "success"
        ).length;

        setEjecucionesExitosas(totalEjecucionesExitosas);
        setEjecucionesFallidas(totalEjecucionesFallidas);
      } catch (error) {
        console.error("Error al obtener datos de la API:", error.message);
        setEjecucionesExitosas(0);
        setEjecucionesFallidas(0);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [id, fetchWithAuth, user?.name]);

  // Datos para el gráfico
  const data = [
    ['Tipo de ejecución', 'Cantidad'],
    ['Ejecuciones Exitosas', ejecucionesExitosas],
    ['Ejecuciones Fallidas', ejecucionesFallidas],
  ];

  // Opciones para el gráfico
  const options = {
    title: 'Análisis de Ejecuciones',
    pieHole: 0.4, // Estilo de gráfico Donut
    is3D: false, // Desactivar 3D para mantener el diseño plano
    titleTextStyle: {
      fontSize: 16,
      bold: true,
      color: '#0c4a6e',
    },
    slices: {
      0: { color: '#2ecc71' }, // Verde para exitosas
      1: { color: '#e74c3c' }, // Rojo para fallidas
    },
  };

  return (
    <div className="w-full bg-white p-4 rounded-3xl shadow-lg">
      {loading ? (
        // Mostrar estado de carga
        <div className="text-center">
          <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
        </div>
      ) : (
        // Mostrar gráfico si hay datos
        <Chart
          chartType="PieChart"
          width="100%"
          height="300px"
          data={data}
          options={options}
        />
      )}
    </div>
  );
};

export default DonutChartDashboard;