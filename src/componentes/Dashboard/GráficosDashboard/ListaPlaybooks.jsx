import React, { useState, useEffect } from 'react';
import { useAuth } from '../../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

export const ListaPlaybooks = ({ id }) => {
    const { fetchWithAuth, user } = useAuth();
    const [stats, setStats] = useState({ totalPlaybooks: 0 });
    const [loading, setLoading] = useState(true);
    const [filteredModules, setFilteredModules] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            if (!id) {
                setLoading(false);
                return;
            }

            try {
                setLoading(true);

                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    setData({ labels: [], datasets: [] });
                    return;
                }

                const idUsuario = usuarioAutenticado.id;
                
                // Realizar la solicitud autenticada para obtener los módulos globales
                const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
                const dataGlobal = await responseGlobal.json();

                // Realizar la solicitud para obtener los módulos adquiridos por el usuario
                const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
                const dataUsuario = await responseUsuario.json();

                // Obtener los IDs de los módulos adquiridos por el usuario
                const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

                // Filtrar los módulos globales por los módulos adquiridos
                const modulosAdquiridos = dataGlobal.filter((modulo) =>
                    modulosUsuarioIds.includes(modulo.id)
                );

                // Filtrar los módulos por ID de categoría
                const filteredModules = modulosAdquiridos.filter(
                    (modulo) => modulo.categoria.id === parseInt(id)
                );

                // Calcular estadísticas
                const totalPlaybooks = filteredModules.length;

                setFilteredModules(filteredModules);
                setStats({ totalPlaybooks });
            } catch (error) {
                console.error("Error al obtener datos de la API:", error.message);
                setFilteredModules([]);
                setStats({ totalPlaybooks: 0 });
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [id, fetchWithAuth, user?.name]);

    return (
        <div className="grid grid-cols-4 gap-4 mb-6">
            {loading ? (
                // Mostrar estado de carga
                <div className="col-span-4 text-center">
                    <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
                </div>
            ) : (
                <>
                    {filteredModules.length > 0 ? (
                        // Mostrar lista de módulos filtrados
                        <ul className="col-span-4 grid grid-cols-2 gap-4">
                            {filteredModules.map((modulo) => (
                                <li
                                    key={modulo.id}
                                    className="p-2 bg-white shadow-lg rounded-lg transform transition duration-300 hover:scale-105 hover:shadow-2xl"
                                >
                                    <h3 className="text-xs text-center font-semibold text-sky-950 mb-2">{modulo.name}</h3>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        // Mostrar mensaje si no se encuentran módulos
                        <div className="col-span-4 text-center">
                            <p className="text-base font-bold text-sky-950">No se encontraron módulos para el ID especificado.</p>
                        </div>
                    )}
                </>
            )}
        </div>
    );
};

export default ListaPlaybooks;
