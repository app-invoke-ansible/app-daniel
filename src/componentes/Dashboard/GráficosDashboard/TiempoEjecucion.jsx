import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Tooltip, Legend } from 'chart.js';
import { useAuth } from '../../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

ChartJS.register(CategoryScale, LinearScale, BarElement, Tooltip, Legend);

export const TiempoEjecucion = ({ id }) => {
    const { fetchWithAuth, user } = useAuth();
    const [filteredModules, setFilteredModules] = useState([]);
    const [loading, setLoading] = useState(true);

    // Efecto para cargar datos al montar o cuando cambia el ID
    useEffect(() => {
        const fetchData = async () => {
            if (!id) {
                setLoading(false);
                return;
            }

            try {
                setLoading(true);

                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    setData({ labels: [], datasets: [] });
                    return;
                }

                const idUsuario = usuarioAutenticado.id;

                // Obtener los módulos globales
                const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
                const dataGlobal = await responseGlobal.json();

                // Obtener los módulos adquiridos por el usuario
                const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
                const dataUsuario = await responseUsuario.json();

                // Obtener los IDs de los módulos adquiridos por el usuario
                const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

                // Filtrar los módulos globales por los módulos adquiridos
                const modulosAdquiridos = dataGlobal.filter((modulo) =>
                    modulosUsuarioIds.includes(modulo.id)
                );

                // Filtrar módulos por ID de categoría
                const filteredModules = modulosAdquiridos.filter(
                    (modulo) => modulo.categoria.id === parseInt(id)
                );

                setFilteredModules(filteredModules);
            } catch (error) {
                console.error("Error al obtener datos de la API:", error.message);
                setFilteredModules([]);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [id, fetchWithAuth, user?.name]);

    // Colores para el gráfico
    const colors = ['rgba(75, 192, 192, 0.8)'];

    // Configuración de datos para el gráfico
    const data = {
        labels: filteredModules.map(modulo => modulo.name),
        datasets: [
            {
                label: 'Tiempo de Ejecución Promedio (en segundos)',
                data: filteredModules.map(modulo => {
                    // Calcular tiempos de ejecución promedio
                    const executionTimes = modulo.ejecuciones.map(ejec => {
                        const startTime = new Date(ejec.startTime).getTime();
                        const endTime = new Date(ejec.endTime).getTime();
                        return (endTime - startTime) / 1000; // Convertir a segundos
                    });
                    return executionTimes.length > 0
                        ? executionTimes.reduce((acc, time) => acc + time, 0) / executionTimes.length
                        : 0; // Promedio o 0 si no hay ejecuciones
                }),
                backgroundColor: filteredModules.map((_, index) => colors[index % colors.length]),
                borderColor: filteredModules.map((_, index) => colors[index % colors.length].replace('0.8', '1')),
                borderWidth: 1,
            },
        ],
    };

    // Opciones para el gráfico
    const options = {
        responsive: true,
        indexAxis: 'y', // Gráfico horizontal
        plugins: {
            legend: {
                display: false, // Ocultar leyenda
            },
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Tiempo promedio (segundos)',
                    color: '#0c4a6e',
                    font: {
                        size: 14,
                        weight: 'bold',
                    },
                },
                beginAtZero: true,
                ticks: {
                    color: '#0c4a6e',
                },
            },
            y: {
                ticks: {
                    color: '#0c4a6e',
                    font: {
                        size: 12,
                    },
                },
            },
        },
    };

    return (
        <div className="w-full h-full max-w-4xl mx-auto bg-white rounded-lg p-6">
            {loading ? (
                // Mostrar estado de carga
                <div className="text-center">
                    <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
                </div>
            ) : (
                filteredModules.length > 0 ? (
                    // Mostrar gráfico si hay datos
                    <Bar data={data} options={options} />
                ) : (
                    // Mensaje si no hay datos disponibles
                    <div className="text-center">
                        <p className="text-base font-bold text-sky-950">No se encontraron módulos para el ID especificado.</p>
                    </div>
                )
            )}
        </div>
    );
};

export default TiempoEjecucion;