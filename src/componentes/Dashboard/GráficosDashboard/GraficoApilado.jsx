import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { useAuth } from '../../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

const GraficoApilado = ({ id }) => {
  const { fetchWithAuth, user } = useAuth();
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      if (!id) {
        setLoading(false);
        return;
      }

      try {
        setLoading(true);

        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
          console.error("Usuario autenticado no encontrado en la API.");
          setData({ labels: [], datasets: [] });
          return;
        }

        const idUsuario = usuarioAutenticado.id;

        const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
        const dataGlobal = await responseGlobal.json();

        const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
        const dataUsuario = await responseUsuario.json();

        const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

        const modulosAdquiridos = dataGlobal.filter((modulo) =>
          modulosUsuarioIds.includes(modulo.id)
        );

        const filteredModules = modulosAdquiridos.filter(
          (modulo) => modulo.categoria.id === parseInt(id)
        );

        // Obtener los sistemas únicos
        const sistemas = [
          ...new Set(filteredModules.map((modulo) => modulo.sistema)),
        ];

        // Calcular las ejecuciones exitosas, canceladas y fallidas por sistema
        const exitosas = sistemas.map((sistema) =>
          filteredModules
            .filter((modulo) => modulo.sistema === sistema)
            .reduce(
              (count, modulo) =>
                count +
                modulo.ejecuciones.filter((ejec) => ejec.status === "success")
                  .length,
              0
            )
        );

        const fallidas = sistemas.map((sistema) =>
          filteredModules
            .filter((modulo) => modulo.sistema === sistema)
            .reduce(
              (count, modulo) =>
                count +
                modulo.ejecuciones.filter((ejec) => ejec.status === "failed")
                  .length,
              0
            )
        );

        const canceladas = sistemas.map((sistema) =>
          filteredModules
            .filter((modulo) => modulo.sistema === sistema)
            .reduce(
              (count, modulo) =>
                count +
                modulo.ejecuciones.filter((ejec) => ejec.status === "canceled")
                  .length,
              0
            )
        );

        // Crear los datos para el gráfico
        setData({
          labels: sistemas,
          datasets: [
            {
              label: "Exitosas",
              data: exitosas,
              backgroundColor: "rgba(0, 123, 255, 0.6)", // Azul
            },
            {
              label: "Canceladas",
              data: canceladas,
              backgroundColor: "rgba(255, 206, 86, 0.6)", // Amarillo
            },
            {
              label: "Fallidas",
              data: fallidas,
              backgroundColor: "rgba(255, 0, 0, 0.6)", // Rojo
            },
          ],
        });
      } catch (error) {
        console.error("Error al obtener datos de la API:", error.message);
        setData({
          labels: [],
          datasets: [],
        }); // Vaciar datos en caso de error
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [id, fetchWithAuth, user?.name]);

  // Opciones de configuración para el gráfico
  const chartOptions = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
        labels: {
          color: '#0c4a6e', // Color de las etiquetas de la leyenda
        },
      },
      title: {
        display: false,
      },
    },
    scales: {
      y: {
        beginAtZero: true,
        title: {
          display: true,
          text: 'Cantidad de Ejecuciones',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e', // Color de las etiquetas del eje Y
        },
      },
      x: {
        title: {
          display: true,
          text: 'Sistemas',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e', // Color de las etiquetas del eje X
        },
      },
    },
  };

  return (
    <div>
      {loading ? (
        <div className="text-center">
          <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
        </div>
      ) : (
        data && (
          <Bar
            data={data}
            options={chartOptions}
          />
        )
      )}
    </div>
  );
};

export default GraficoApilado;