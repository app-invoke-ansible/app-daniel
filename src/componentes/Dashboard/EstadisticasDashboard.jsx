import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

export const EstadisticasDashboard = ({ id }) => {
    const { fetchWithAuth, user } = useAuth();
    const [stats, setStats] = useState({
        totalEjecuciones: 0,
        mediaEnSegundos: 0,
        totalPlaybooks: 0,
        tasaDeExito: 0,
    }); // Estado para estadísticas
    const [loading, setLoading] = useState(true);

    // Efecto para cargar estadísticas al montar o cuando cambia el ID
    useEffect(() => {
        const fetchData = async () => {
            if (!id) {
                setLoading(false);
                return;
            }

            try {
                setLoading(true);

                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    setData({ labels: [], datasets: [] });
                    return;
                }

                const idUsuario = usuarioAutenticado.id;

                // Obtener los módulos globales
                const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
                const dataGlobal = await responseGlobal.json();

                // Obtener los módulos adquiridos por el usuario
                const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
                const dataUsuario = await responseUsuario.json();

                // Obtener los IDs de los módulos adquiridos por el usuario
                const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

                // Filtrar los módulos globales por los módulos adquiridos
                const modulosAdquiridos = dataGlobal.filter((modulo) =>
                    modulosUsuarioIds.includes(modulo.id)
                );

                // Filtrar módulos por ID de categoría
                const filteredModules = modulosAdquiridos.filter(
                    (modulo) => modulo.categoria.id === parseInt(id)
                );

                // Calcular estadísticas
                const totalPlaybooks = filteredModules.length;

                const ejecuciones = filteredModules.flatMap(
                    (modulo) => modulo.ejecuciones || []
                );
                const totalEjecuciones = ejecuciones.length;

                const duracionesEnSegundos = ejecuciones.map((ejec) => {
                    const start = new Date(ejec.startTime);
                    const end = new Date(ejec.endTime);
                    return (end - start) / 1000; // Convertir a segundos
                });

                const mediaEnSegundos = duracionesEnSegundos.length
                    ? duracionesEnSegundos.reduce((a, b) => a + b, 0) / duracionesEnSegundos.length
                    : 0;

                const ejecucionesExitosas = ejecuciones.filter(
                    (ejec) => ejec.status === "success"
                ).length;
                const tasaDeExito =
                    totalEjecuciones > 0
                        ? (ejecucionesExitosas / totalEjecuciones) * 100
                        : 0;

                // Actualizar el estado con estadísticas calculadas
                setStats({
                    totalEjecuciones,
                    mediaEnSegundos: mediaEnSegundos.toFixed(2),
                    totalPlaybooks,
                    tasaDeExito: tasaDeExito.toFixed(2),
                });
            } catch (error) {
                console.error("Error al obtener datos de la API:", error.message);
                // Establecer estadísticas vacías en caso de error
                setStats({
                    totalEjecuciones: 0,
                    mediaEnSegundos: "0.00",
                    totalPlaybooks: 0,
                    tasaDeExito: "0.00",
                });
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [id, fetchWithAuth, user?.name]);

    return (
        <div className="grid grid-cols-4 gap-4 mb-6">
            {loading ? (
                // Mostrar estado de carga
                <div className="col-span-4 text-center">
                    <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
                </div>
            ) : (
                // Mostrar estadísticas si hay datos
                <>
                    <StatItem label="Cantidad total de Playbooks" value={stats.totalPlaybooks} />
                    <StatItem label="Total de ejecuciones" value={stats.totalEjecuciones} />
                    <StatItem label="Media en segundos" value={stats.mediaEnSegundos} />
                    <StatItem label="Porcentaje de éxito" value={`${stats.tasaDeExito} %`} />
                </>
            )}
        </div>
    );
};

// Componente para mostrar un ítem de estadística
export const StatItem = ({ label, value }) => (
    <div className="bg-white p-2 rounded-3xl border border-sky-950 text-center flex flex-col">
        <p className="text-gray-800 text-xs">{label}</p>
        <p className="text-base font-bold text-sky-950">{value}</p>
    </div>
);

export default EstadisticasDashboard;