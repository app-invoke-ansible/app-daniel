import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend } from 'chart.js';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

// Registrar componentes de Chart.js
ChartJS.register(LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend);

const LineChart = ({ id }) => {
  const { fetchWithAuth, user } = useAuth(); // Obtener función fetch autenticada
  const [loading, setLoading] = useState(true); // Estado para mostrar carga
  const [chartData, setChartData] = useState({ labels: [], data: [] }); // Estado para datos del gráfico

  // Efecto para obtener datos al montar o cuando cambia el ID
  useEffect(() => {
    const fetchData = async () => {
      if (!id) {
        setLoading(false); // Desactivar estado de carga si no hay ID
        return;
      }

      try {
        setLoading(true);

        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
            console.error("Usuario autenticado no encontrado en la API.");
            setData({ labels: [], datasets: [] });
            return;
        }

        const idUsuario = usuarioAutenticado.id;

        // Obtener los módulos globales
        const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
        const dataGlobal = await responseGlobal.json();

        // Obtener los módulos adquiridos por el usuario
        const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
        const dataUsuario = await responseUsuario.json();

        // Obtener los IDs de los módulos adquiridos por el usuario
        const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);

        // Filtrar los módulos globales por los módulos adquiridos
        const modulosAdquiridos = dataGlobal.filter((modulo) =>
          modulosUsuarioIds.includes(modulo.id)
        );

        // Filtrar módulos por ID de categoría
        const filteredModules = modulosAdquiridos.filter(
          (modulo) => modulo.categoria.id === parseInt(id)
        );

        // Calcular tiempos de ejecución
        const ejecuciones = filteredModules.flatMap(
          (modulo) => modulo.ejecuciones || []
        );

        // Mapear las fechas y tiempos de ejecución
        const labels = ejecuciones
          .map((ejec) => new Date(ejec.startTime).toLocaleDateString())
          .sort(); // Ordenar las fechas
        const executionTimes = ejecuciones.map((ejec) => {
          const startTime = new Date(ejec.startTime).getTime();
          const endTime = new Date(ejec.endTime).getTime();
          return (endTime - startTime) / 1000; // Tiempo en segundos
        });

        // Actualizar el estado con los datos para el gráfico
        setChartData({ labels, data: executionTimes });
      } catch (error) {
        console.error("Error al obtener datos de la API:", error.message);
        setChartData({ labels: [], data: [] }); // Vaciar los datos del gráfico en caso de error
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [id, fetchWithAuth, user?.name]);

  // Configuración de datos para Chart.js
  const data = {
    labels: chartData.labels,
    datasets: [
      {
        label: 'Tiempo de Ejecución (segundos)',
        data: chartData.data,
        fill: true,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        pointBackgroundColor: 'rgba(75, 192, 192, 1)',
        tension: 0.3, // Suavizar la línea del gráfico
      },
    ],
  };

  // Configuración de opciones para Chart.js
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        position: 'top',
        labels: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
      title: {
        display: true,
        text: 'Tiempo de Ejecución por Fecha',
        color: '#0c4a6e',
        font: {
          size: 18,
          weight: 'bold',
        },
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: 'Fecha de Ejecución',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
      y: {
        title: {
          display: true,
          text: 'Tiempo (segundos)',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
    },
  };

  return (
    <div className="w-full h-96">
      {loading ? (
        // Mostrar estado de carga
        <div className="text-center">
          <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
        </div>
      ) : (
        // Mostrar gráfico si hay datos
        <Line data={data} options={options} />
      )}
    </div>
  );
};

export default LineChart;
