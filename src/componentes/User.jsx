import React from 'react';
import { LuUser, LuEye, LuTrash2, LuPen, LuUserPlus } from 'react-icons/lu';
import logoInvokeIcono from '../img/logoInvokeIcono.png'; 

const UserCard = ({ index }) => {
  return (
    <div className="bg-white shadow-md rounded-lg p-4 flex items-center relative w-full">

      <img
        src={logoInvokeIcono}
        alt="Logo Invoke"
        className="absolute top-2 right-2 w-8 h-8" // Ajusta el tamaño de la imagen aquí
      />
      <div className="w-24 h-24 flex items-center justify-center rounded-full border border-sky-950 mr-4">
        <LuUser className="w-16 h-16 text-gray-600" />
      </div>
      <div className="flex-grow">
        <h2 className="text-xl font-semibold">Usuario {index + 1}</h2>
        <p className="text-gray-500">Tipo de Usuario</p>
        <p className="text-gray-500">Estado</p>
        <div className="flex items-center mt-2">
          <span
            className={`ml-2 w-44 h-2 rounded-3xl ${
              index % 2 === 0 ? 'bg-green-500' : 'bg-red-500'
            }`}
          ></span>
        </div>
      </div>
      <div className="flex flex-col items-end mt-14">
        <div className="flex space-x-2">
          <button className="text-sky-950 hover:text-gray-600 rounded-full border border-sky-950">
            <LuEye className="w-6 h-6" />
          </button>
          <button className="text-sky-950 hover:text-gray-600 rounded-full border border-sky-950">
            <LuPen className="w-6 h-6" />
          </button>
          <button className="text-sky-950 hover:text-gray-600 rounded-full border border-sky-950">
            <LuTrash2 className="w-6 h-6" />
          </button>
        </div>
      </div>
    </div>
  );
};

const UserManagement = () => {
  return (
    <div className="flex justify-center items-center min-h-screen bg-gray-100">
      <div className="w-full max-w-6xl p-6">
        {/* Contenedor para botón, dropdown y input alineados a la derecha */}
        <div className="flex justify-end items-center mb-8 space-x-32">
          <button className="bg-sky-900 text-white px-8 py-2 rounded-lg hover:bg-gray-600 flex items-center">
            <LuUserPlus className="w-6 h-6 mr-2" />
            Crear Usuario
          </button>

          <select className="border rounded-lg px-4 py-2">
            <option>Tipo de Usuario</option>
            <option>Administrador</option>
            <option>Editor</option>
            <option>Visualizador</option>
          </select>

          <div className="relative">
            <input
              type="text"
              className="border rounded-lg pl-8 pr-10 py-2"
              placeholder="Buscar usuario..."
            />
            <span className="absolute inset-y-0 right-0 flex items-center pr-3 text-gray-400">
              <i className="fas fa-search"></i>
            </span>
          </div>
        </div>

        {/* Cards de usuarios */}
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          {[...Array(10)].map((_, index) => (
            <UserCard key={index} index={index} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default UserManagement;