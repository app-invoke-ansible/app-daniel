import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';

ChartJS.register(ArcElement, Tooltip, Legend);

function GraficoDonut({ modulo1Exitos, modulo1Fallos, modulo2Exitos, modulo2Fallos }) {
    const data = {
        labels: ['Módulo Healthcheck Éxitos', 'Módulo Healthcheck Fallos', 'Módulo cambio-zona-horario-linux Éxitos', 'Módulo cambio-zona-horario-linux Fallos'],
        datasets: [
            {
                data: [modulo1Exitos, modulo1Fallos, modulo2Exitos, modulo2Fallos],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.8)', 
                    'rgba(255, 99, 132, 0.8)', 
                    'rgba(54, 162, 235, 0.8)', 
                    'rgba(255, 99, 132, 0.8)', 
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)', 
                    'rgba(255, 99, 132, 1)', 
                    'rgba(54, 162, 235, 1)', 
                    'rgba(255, 99, 132, 1)', 
                ],
                borderWidth: 1,
            },
        ],
    };

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Éxitos vs Fallos por Módulo',
                font: {
                    size: 16,
                    weight: 'bold'
                }
            },
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    };

    return (
        <div className="bg-white rounded-lg shadow-lg p-6 hover:shadow-xl transition-shadow duration-300">
            <h2 className="text-xl font-bold mb-4 text-gray-800">Éxitos vs Fallos por Módulo</h2>
            <div className="relative" style={{ height: '300px' }}>
                <Doughnut data={data} options={options} />
            </div>
        </div>
    );
}

export default GraficoDonut;
