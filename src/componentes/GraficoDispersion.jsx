import React from 'react';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

const GraficoDispersion = ({ ejecuciones }) => {
    if (!ejecuciones || !Array.isArray(ejecuciones) || ejecuciones.length === 0) {
        return <p>No hay datos disponibles para mostrar.</p>;
    }

    const data = ejecuciones.map((ejecucion, index) => ({
        x: index + 1,
        y: ejecucion.tiempoEjecucion,
        exitoso: ejecucion.exitoso
    }));

    return (
        <ResponsiveContainer width="100%" height={300}>
            <ScatterChart
                margin={{
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 20,
                }}
            >
                <CartesianGrid />
                <XAxis type="number" dataKey="x" name="Ejecución" />
                <YAxis type="number" dataKey="y" name="Tiempo de ejecución" />
                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                <Scatter name="Ejecuciones" data={data} fill="#8884d8" />
            </ScatterChart>
        </ResponsiveContainer>
    );
};

export default GraficoDispersion;
