import React from "react";
import { FaFileDownload } from "react-icons/fa";

// Contenido inicial del archivo CSV (puedes reemplazarlo con una importación si lo deseas)
const csvTemplate = `
name;inventory;group;ip_address;port;user;os_name
Prueba1;{inventarioId};2;123.456.7.89;22;{username};Ejemplo_Linux
Prueba2;{inventarioId};2;123.456.8.89;22;{username};Ejemplo_Windows
`.trim();

const DescargarFormato = ({ inventarioId, usuarioAutenticado }) => {
    const handleDownload = () => {
        // Reemplazar las variables dinámicas en el archivo CSV
        const updatedCsv = csvTemplate
            .replace(/{inventarioId}/g, inventarioId)
            .replace(/{username}/g, usuarioAutenticado);

        // Crear un blob para el archivo CSV modificado
        const blob = new Blob([updatedCsv], { type: "text/csv;charset=utf-8;" });
        const url = URL.createObjectURL(blob);

        // Crear un enlace para descargar el archivo
        const link = document.createElement("a");
        link.href = url;
        link.download = "Ejemplo.csv";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    return (
        <div
            className="flex items-center justify-center cursor-pointer text-sky-950 hover:text-gray-600 transition"
            onClick={handleDownload}
            title="Descargar formato .csv"
        >
            <FaFileDownload className="text-2xl" />
        </div>
    );
};

export default DescargarFormato;