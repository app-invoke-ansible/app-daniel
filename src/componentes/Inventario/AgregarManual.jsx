import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import Loading from '../Base/Loading';

const AgregarManual = ({ fetchInventario, inventarioUser }) => {
    const { fetchWithAuth, user } = useAuth();
    const [isOpen, setIsOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState({
        hostName: '',
        ipAddress: '',
        port: '',
        user: user.name,
        inventory: '',
        osName: '',
        groupId: '',
    });
    const [errorMessage, setErrorMessage] = useState({
        hostName: '',
        ipAddress: '',
    });
    const [hosts, setHosts] = useState([]);
    const [groups, setGroups] = useState([]);
    const API_URL = process.env.REACT_APP_API_URL;

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);


    // Efecto para actualizar el nombre del inventario cuando esté disponible
    useEffect(() => {
        if (inventarioUser.nombreInventario) {
            setFormData((prevData) => ({
                ...prevData,
                inventory: inventarioUser.nombreInventario,
            }));
        }
    }, [inventarioUser.nombreInventario]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        const trimmedValue = value.trim();

        if (name === "port" && !/^\d*$/.test(value)) return;
        if (name === "ipAddress" && !/^[0-9.]*$/.test(value)) return;

        setFormData((prevData) => ({ ...prevData, [name]: trimmedValue }));

        if (name === 'hostName' || name === 'ipAddress') {
            validateField(name, trimmedValue);
        }
    };

    const validateField = (field, value) => {
        let error = '';
        const containsSpaces = /\s/.test(value);

        if (field === 'hostName') {
            if (containsSpaces) {
                error = 'El nombre del host no puede contener espacios.';
            } else if (hosts.some(host => host.hostname === value)) {
                error = 'El nombre de host ya está en uso.';
            }
        } else if (field === 'ipAddress') {
            if (containsSpaces) {
                error = 'La dirección IP no puede contener espacios.';
            } else if (hosts.some(host => host.ipAddress === value)) {
                error = 'La dirección IP ya está en uso.';
            }
            const ipRegex = /^(\d{1,3}\.){3}\d{1,3}$/;
            if (value && !ipRegex.test(value)) {
                error = 'Formato de dirección IP no válido.';
            }
        }
        setErrorMessage((prev) => ({ ...prev, [field]: error }));
    };

    const extractHosts = (inventoryData) => {
        return inventoryData.flatMap(inventario =>
            inventario.groups.flatMap(group => group.hosts)
        );
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            if (errorMessage.hostName || errorMessage.ipAddress) {
                alert('Corrige los errores antes de agregar un nuevo Host.');
                return;
            }

            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await responseUsuarios.json();
            const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

            if (!usuarioAutenticado) {
                alert('Usuario autenticado no encontrado en la API.');
                return;
            }

            const idUsuario = usuarioAutenticado.id;
            const url = `${API_URL}/hosts/${idUsuario}/create?inventoryId=${inventarioUser.idInventario}&groupId=${formData.groupId}&hostName=${formData.hostName}&ipAddress=${formData.ipAddress}&port=${formData.port}&user=${formData.user}&osName=${formData.osName}`;

            const postResponse = await fetchWithAuth(url, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
            });

            if (postResponse.ok) {
                alert('Host agregado exitosamente.');
                fetchInventario();
                setFormData({
                    hostName: '',
                    ipAddress: '',
                    port: '',
                    user: user.name,
                    inventory: inventarioUser.nombreInventario,
                    osName: '',
                    groupId: '',
                });
                setIsOpen(false);
                setErrorMessage({ hostName: '', ipAddress: '' });
            } else {
                alert('Hubo un problema al agregar el host.');
            }
        } catch (error) {
            console.error('Error al agregar el host:', error.message);
            setIsOpen(false);
        }
    };

    const fetchGroups = async () => {
        try {
            const response = await fetchWithAuth(`${API_URL}/inventarios/${inventarioUser.idInventario}`);
            const data = await response.json();
            if (data && data.groups) {
                const formattedGroups = data.groups.map(group => ({
                    id: group.id,
                    groupName: group.groupName,
                }));
                setGroups(formattedGroups);
            } else {
                setGroups([]);
                console.warn(`No se encontraron grupos en el inventario ${inventarioUser.idInventario} o el formato de respuesta es inesperado.`);
            }
        } catch (error) {
            console.error('Error al obtener los grupos', error);
        }
    };

    const handleCancel = () => {
        setIsOpen(false);
        setFormData({
            hostName: '',
            ipAddress: '',
            port: '',
            user: user.name,
            inventory: inventarioUser.nombreInventario,
            osName: '',
            groupId: '',
        });
        setErrorMessage({ hostName: '', ipAddress: '' });
    };

    const handleOpen = async () => {
        setIsLoading(true);
        setIsOpen(true);

        try {
            const response = await fetchWithAuth(`${API_URL}/inventarios`);
            const data = await response.json();
            const allHosts = extractHosts(data);
            setHosts(allHosts);
            await fetchGroups();
        } catch (error) {
            console.error('Error al obtener los hosts', error.message);
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div className="flex justify-center h-full">
            <button
                className="block mt-2 ml-[1rem] bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white"
                onClick={handleOpen}>
                Agregar manual
            </button>
            {isOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white rounded-lg p-6 shadow-lg w-full h-full md:w-96 md:h-auto md:rounded-lg">
                        <h2 className="text-xl font-bold mb-4 text-sky-950 pt-10 sm:pt-0">Agregar Host Manualmente</h2>
                        {isLoading ? (
                            <Loading message="Cargando..." />
                        ) : (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-4">
                                    <label className="block text-sm font-semibold mb-1">Nombre del Host</label>
                                    <input
                                        type="text"
                                        name="hostName"
                                        value={formData.hostName}
                                        onChange={handleChange}
                                        className={`mt-1 p-1 border ${errorMessage.hostName ? 'border-red-500' : 'border-gray-300'
                                            } rounded-md w-full text-xs focus:outline-none`}
                                        required
                                    />
                                    {errorMessage.hostName && (
                                        <p className="text-red-600 text-xs mt-1">{errorMessage.hostName}</p>
                                    )}
                                </div>
                                <div className="grid grid-cols-2 gap-4">
                                    <div className="mb-4">
                                        <label className="block text-sm font-semibold mb-1">Dirección IP</label>
                                        <input
                                            type="text"
                                            name="ipAddress"
                                            value={formData.ipAddress}
                                            onChange={handleChange}
                                            className={`mt-1 p-1 border ${errorMessage.ipAddress ? 'border-red-500' : 'border-gray-300'
                                                } rounded-md w-full text-xs focus:outline-none`}
                                            required
                                        />
                                        {errorMessage.ipAddress && (
                                            <p className="text-red-600 text-xs mt-1">{errorMessage.ipAddress}</p>
                                        )}
                                    </div>
                                    <div className="mb-4">
                                        <label className="block text-sm font-semibold mb-1">Puerto</label>
                                        <input
                                            type="text"
                                            name="port"
                                            value={formData.port}
                                            onChange={handleChange}
                                            className="mt-1 p-1 border border-gray-300 rounded-md w-full text-xs focus:outline-none"
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="mb-4">
                                    <label className="block text-sm font-semibold mb-1">Inventario</label>
                                    <input
                                        type="text"
                                        name="inventory"
                                        value={formData.inventory}
                                        onChange={handleChange}
                                        className="mt-1 p-1 border border-gray-300 rounded-md w-full text-xs focus:outline-none cursor-not-allowed"
                                        disabled
                                    />
                                </div>

                                <div className="mb-4">
                                    <label className="block text-sm font-semibold mb-1">Sistema Operativo</label>
                                    <input
                                        type="text"
                                        name="osName"
                                        value={formData.osName}
                                        onChange={handleChange}
                                        className="mt-1 p-1 border border-gray-300 rounded-md w-full text-xs focus:outline-none"
                                        required
                                    />
                                </div>
                                <div className="mb-4">
                                    <label className="block text-sm font-semibold mb-1">Infraestructura</label>
                                    <select
                                        name="groupId"
                                        value={formData.groupId}
                                        onChange={handleChange}
                                        className="mt-1 p-1 border border-gray-300 rounded-md w-full text-xs focus:outline-none"
                                        required
                                    >
                                        <option value="" disabled>Selecciona un grupo</option>
                                        {groups.map(group => (
                                            <option key={group.id} value={group.id}>
                                                {group.groupName}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="flex flex-row justify-center gap-4 mt-4">
                                    <button
                                        type="button"
                                         className="px-3 py-1 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                        onClick={handleCancel}>
                                        Cancelar
                                    </button>
                                    <button
                                        type="submit"
                                        className="px-3 py-1 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105">
                                        Agregar
                                    </button>
                                </div>
                            </form>
                        )}
                    </div>
                </div>
            )}
        </div>
    );
};

export default AgregarManual;