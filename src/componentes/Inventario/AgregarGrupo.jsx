import React, { useState, useEffect } from 'react';
import { FaPlus } from 'react-icons/fa';
import { useAuth } from '../../providers/AuthProvider';

const AgregarGrupo = ({ fetchInventario, inventarioId }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [formData, setFormData] = useState({ groupName: '', descripcion: '' });
    const { fetchWithAuth } = useAuth();
    const [errorMessage, setErrorMessage] = useState('');

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    const API_URL = process.env.REACT_APP_API_URL;

    const handleInputChange = (e) => {
        const { id, value } = e.target;
        setFormData((prev) => ({ ...prev, [id]: value }));
    };

    const openModal = () => {
        setErrorMessage('');
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
        setFormData({ groupName: '', descripcion: '' });
        setErrorMessage('');
    };

    const stopPropagation = (e) => {
        e.stopPropagation();
    };

    const handleSave = async () => {
        if (!formData.groupName.trim()) {
            setErrorMessage('El nombre del grupo es obligatorio.');
            return;
        }

        setErrorMessage('');
        const { groupName, descripcion } = formData;
        try {
            const response = await fetchWithAuth(
                `${API_URL}/grupos?inventoryId=${inventarioId}&groupName=${groupName}&descripcion=${descripcion}`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
            );

            if (response.ok) {
                alert('Grupo creado correctamente');
                closeModal();
                fetchInventario();
            } else {
                const errorData = await response.json();
                if (errorData.message.includes("already exists")) {
                    setErrorMessage('Ya existe un grupo con ese nombre.');
                } else {
                    alert(`Error al crear el grupo: ${errorData.message || response.statusText}`);
                }
            }
        } catch (error) {
            console.error('Error al crear el grupo:', error);
            alert('Error al crear el grupo. Por favor, inténtalo de nuevo.');
        }
    };

    return (
        <li
            className="relative group block p-4 text-center bg-sky-950 transition cursor-pointer "
            onClick={openModal}
        >
            <div className="flex flex-col items-center justify-center p-2 transition-colors duration-100 transform hover:scale-105">
                <FaPlus className="text-3xl text-white" />
                <h3 className="mt-2 text-sm font-semibold text-white">Agregar Grupo</h3>
            </div>

            {isOpen && (
                <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto">
                    {/* Fondo del modal */}
                    <div className="fixed inset-0 bg-black/30" aria-hidden="true" onClick={closeModal} />

                    {/* Contenido del modal */}
                    <div
                        className="relative w-full h-full bg-white rounded-xl shadow-lg p-6 sm:p-6 sm:max-w-xs sm:h-auto"
                        onClick={(e) => e.stopPropagation()}
                    >
                        <h2 className="text-center text-1xl font-semibold text-sky-950 mb-4">Agregar Grupo</h2>

                        <form className="space-y-6 text-start">
                            {/* Nombre del Grupo */}
                            <div>
                                <label
                                    htmlFor="groupName"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Nombre del Grupo
                                </label>
                                <input
                                    type="text"
                                    id="groupName"
                                    value={formData.groupName}
                                    onChange={handleInputChange}
                                    className={`w-full mt-1 p-3 border rounded-md focus:outline-none focus:ring-2 ${errorMessage ? 'border-red-500' : 'border-gray-300'
                                        }`}
                                />
                                {errorMessage && (
                                    <p className="mt-1 text-sm text-red-500">{errorMessage}</p>
                                )}
                            </div>

                            {/* Descripción */}
                            <div>
                                <label
                                    htmlFor="descripcion"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Descripción
                                </label>
                                <textarea
                                    id="descripcion"
                                    value={formData.descripcion}
                                    onChange={handleInputChange}
                                    className="w-full mt-1 p-3 border border-gray-300 rounded-md focus:outline-none focus:ring-2"
                                    rows="6"
                                />
                            </div>

                            <div className="flex flex-row justify-center gap-4 mt-4">
                                <button
                                    type="button"
                                    onClick={closeModal}
                                    className="px-3 py-1 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                                >
                                    Cancelar
                                </button>
                                <button
                                    type="button"
                                    onClick={handleSave}
                                    className="px-3 py-1 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105"
                                >
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </li>
    );
};

export default AgregarGrupo;