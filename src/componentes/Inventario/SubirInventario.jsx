import React, { useState, useEffect } from "react";
import { FiUpload } from "react-icons/fi";
import { LuRefreshCcw } from "react-icons/lu";
import { useAuth } from "../../providers/AuthProvider";
import DescargarFormato from "./DescargarFormato"

const SubirInventario = ({ usuarioAutenticado, inventarioId }) => {
    const [file, setFile] = useState(null);
    const [errorMessage, setErrorMessage] = useState("");
    const [loading, setLoading] = useState(false);
    const [modalMessage, setModalMessage] = useState("");
    const [showModal, setShowModal] = useState(false);
    const { fetchWithAuth, user } = useAuth();

    const API_URL = process.env.REACT_APP_API_URL;

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (showModal) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [showModal]);

    const handleFileChange = (event) => {
        const selectedFile = event.target.files[0];

        if (!selectedFile.name.endsWith(".csv")) {
            setErrorMessage("Solo se permiten archivos .csv");
            return;
        }

        setErrorMessage("");
        setFile(selectedFile);
    };

    const resetComponent = () => {
        setFile(null);
        setErrorMessage("");
        setLoading(false);
        setModalMessage("");
        setShowModal(false);
        document.getElementById("uploadInventario").value = "";
    };

    const handleUpload = async () => {
        if (!file) {
            setErrorMessage("Seleccione un archivo .csv antes de subir.");
            return;
        }

        setLoading(true);

        const formData = new FormData();
        formData.append("file", file);

        try {
            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await responseUsuarios.json();

            const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

            if (!usuarioAutenticado) {
                alert("Usuario autenticado no encontrado en la API.");
                setLoading(false);
                return;
            }

            const idUsuario = usuarioAutenticado.id;

            const response = await fetchWithAuth(`${API_URL}/hosts/${idUsuario}/import-hosts`, {
                method: "POST",
                body: formData,
            });

            const responseData = await response.text();

            if (response.ok) {
                if (responseData.includes("Algunos hosts no se pudieron importar")) {
                    // Dividir mensaje en partes antes de "Detalles:"
                    const formattedMessage = responseData
                        .split("Detalles:")
                        .map((part, index) => (index === 0 ? part : ` ${part.trim()}`)); // Reinsertar "Detalles:" en las partes posteriores

                    setModalMessage(formattedMessage);
                    setShowModal(true);
                } else {
                    alert("Inventario subido exitosamente.");
                    resetComponent();
                }
            } else {
                setErrorMessage(`Error al importar los hosts: ${responseData}`);
            }
        } catch (error) {
            setErrorMessage(`Error en la solicitud: ${error.message}`);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="px-8 flex flex-col items-start w-full space-y-4">
            <div className="flex items-center justify-between w-full gap-4">
                {/* Input para cargar archivo */}
                <div className="relative w-64">
                    <div className="block w-full border-b-2 border-sky-950 py-1.5 text-gray-400 placeholder-gray-400 focus-within:border-sky-950">
                        <input
                            className="w-full text-sm outline-none bg-transparent"
                            type="file"
                            accept=".csv"
                            id="uploadInventario"
                            onChange={handleFileChange}
                        />
                    </div>
                    {errorMessage && (
                        <p className="text-red-600 text-xs text-right">{errorMessage}</p>
                    )}
                </div>

                {/* Botón de subir archivo */}
                <div className="flex justify-end">
                    <button
                        onClick={handleUpload}
                        disabled={loading}
                        className={`flex items-center gap-2 bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white ${loading ? "opacity-50 cursor-not-allowed" : ""
                            }`}
                    >
                        {loading ? (
                            <LuRefreshCcw className="animate-spin text-white" />
                        ) : (
                            <FiUpload className="text-white" />
                        )}
                        {loading ? "Subiendo..." : "Subir"}
                    </button>
                </div>

                {/* Botón de descarga */}
                <DescargarFormato usuarioAutenticado={usuarioAutenticado} inventarioId={inventarioId} />
            </div>

            {/* Modal */}
            {showModal && (
                <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-50">
                    <div className="bg-white rounded-lg p-6 sm:w-1/2 sm:h-auto w-full h-full">
                        <h2 className="text-xl font-semibold mb-4 text-gray-700 text-center">Ocurrio un error</h2>
                        {/* Renderizar cada parte del mensaje */}
                        {Array.isArray(modalMessage) ? (
                            modalMessage.map((msg, idx) => (
                                <p key={idx} className="text-red-600 mb-4">
                                    {msg}
                                </p>
                            ))
                        ) : (
                            <p className="text-gray-700 mb-6">{modalMessage}</p>
                        )}
                        <div className="flex justify-end">
                            <button
                                onClick={() => setShowModal(false)}
                                className="px-3 py-1 bg-sky-950 text-white hover:bg-sky-900 rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105"
                            >
                                Aceptar
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default SubirInventario;
