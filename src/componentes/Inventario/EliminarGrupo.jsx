import React, { useState, useEffect } from 'react';
import { Dialog } from '@headlessui/react';
import { useAuth } from '../../providers/AuthProvider';

const EliminarGrupo = ({ groupId, fetchInventario, onClose }) => {
    const [isOpen, setIsOpen] = useState(true); // Modal abierto por defecto
    const { fetchWithAuth } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    const closeModal = () => {
        setIsOpen(false);
        if (onClose) {
            onClose(); // Ejecutar la función onClose si se proporciona
        }
    };

    useEffect(() => {
        console.log("ID del grupo en EliminarGrupo:", groupId); // Log para verificar el ID
    }, [groupId])


    const handleDelete = async () => {
        console.log("handleDelete se ha llamado");
        try {
            const response = await fetchWithAuth(`${API_URL}/grupos/${groupId}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                alert('Grupo eliminado correctamente');
                closeModal();
                fetchInventario();
            } else {
                const errorData = await response.json();
                console.log("Respuesta de error de la API:", errorData);
                alert(`Error al eliminar el grupo: ${errorData.message || response.statusText}`);
            }
        } catch (error) {
            console.error('Error al eliminar el grupo:', error);
            alert('Error al eliminar el grupo. Por favor, inténtalo de nuevo.');
        }
    };


    return (
        <Dialog open={isOpen} onClose={closeModal} className="relative z-50">
            <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
            <div className="fixed inset-0 flex items-center justify-center sm:p-4 ">
                <Dialog.Panel className="sm:mx-auto  bg-white p-6 rounded-xl w-full h-full sm:w-auto sm:h-auto">
                    <Dialog.Title className="text-lg font-medium leading-6 text-gray-900">
                        Confirmar Eliminación
                    </Dialog.Title>
                    <div className="mt-4">
                        <p className="text-sm text-gray-700">
                            ¿Estás seguro de que deseas eliminar este grupo?
                        </p>
                    </div>
                    <div className="mt-6 flex justify-end space-x-2">
                        <button
                            type="button"
                            onClick={closeModal}
                            className="px-3 py-1 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-sm transition-colors duration-300 transform hover:scale-105"
                        >
                            Cancelar
                        </button>
                        <button
                            type="button"
                            onClick={handleDelete}
                            className="mr-2 bg-red-500 hover:bg-red-600 rounded-md px-3 py-1 text-sm text-white transition-colors duration-300 transform hover:scale-105"
                        >
                            Eliminar
                        </button>
                    </div>
                </Dialog.Panel>
            </div>
        </Dialog>
    );
};

export default EliminarGrupo;