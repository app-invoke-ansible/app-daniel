import React, { useState, useEffect, useRef } from 'react';
import { CiMenuKebab } from "react-icons/ci";
import { FaTrashAlt } from "react-icons/fa";
import EliminarGrupo from './EliminarGrupo';
import IconOtros from '../../img/iconos/Iconos_Otros.png';


const CardGrupo = ({ grupo, isSelected, onSelect, fetchInventario }) => {
    const [showOptions, setShowOptions] = useState(false);
    const [showEliminarModal, setShowEliminarModal] = useState(false);
    const dropdownRef = useRef(null);

    const handleOptionsClick = (e) => {
        e.stopPropagation();
        setShowOptions(!showOptions);
    };

    const closeDropdown = () => {
        setShowOptions(false);
    };

    const handleEliminarClick = (e) => {
        e.stopPropagation();
        setShowEliminarModal(true);
        closeDropdown();
    };

    const handleCloseModal = () => {
        setShowEliminarModal(false);
    };

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                closeDropdown();
            }
        };

        if (showOptions) {
            document.addEventListener('mousedown', handleClickOutside);
        } else {
            document.removeEventListener('mousedown', handleClickOutside);
        }

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [showOptions]);

    return (
        <li
            key={grupo.id}
            className="border border-sky-950 col-span-1 flex flex-col rounded-3xl bg-white text-center p-2 relative"
        >
            <div className="absolute top-2 right-2 z-10" ref={dropdownRef}>
                <div className="relative inline-block text-left">
                    <button
                        type="button"
                        className="inline-flex justify-center text-gray-700 hover:text-gray-900"
                        onClick={handleOptionsClick}
                    >
                        <CiMenuKebab />
                    </button>
                    {showOptions && (
                        <div className="absolute top-0 right-full mt-0 mr-2 w-32 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                            <button
                                onClick={handleEliminarClick}
                                className="flex items-center px-4 py-2 text-sm text-red-600 hover:bg-gray-200 w-full text-left"
                            >
                                <FaTrashAlt className="mr-2" />
                                Eliminar
                            </button>
                        </div>
                    )}
                </div>
            </div>

            {showEliminarModal && (
                <EliminarGrupo groupId={grupo.id} fetchInventario={fetchInventario} onClose={handleCloseModal} />
            )}

            <div className="flex flex-1 p-2 items-center justify-center">
                {/* Primera columna (Imagen) */}
                <div className="flex-shrink-0">
                    <img src={IconOtros} alt="Icono grupo" className="w-16 h-16 object-contain" />
                </div>

                {/* Segunda columna (Texto) */}
                <div className="ml-4 flex flex-col items-start justify-center">
                    <h3 className="text-sm text-sky-900 font-bold">{grupo.name}</h3>
                    <span className="text-xs text-gray-400">ID: {grupo.id}</span>
                </div>
            </div>

            <div className="flex items-center justify-center">
                <div className="flex items-center justify-center w-4">
                    <button
                        onClick={onSelect}
                        className={`relative inline-flex items-center justify-center py-1 px-3 text-xs
                            ${isSelected
                                ? 'bg-white text-sky-950 border-sky-950'
                                : 'bg-sky-950 text-white border-transparent hover:bg-gray-600'
                            }
                            rounded-lg border`}
                        type="button"
                    >
                        Seleccionar
                    </button>
                </div>
            </div>
        </li>
    );
};

export default CardGrupo;
