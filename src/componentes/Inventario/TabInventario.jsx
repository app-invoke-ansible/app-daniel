import React, { useEffect, useState } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import BotonActualizarFila from './BotonActualizarFila';

const TabInventario = ({ selectedGroup, searchTerm }) => {
    const [inventario, setInventario] = useState([]);
    const [loading, setLoading] = useState(true);
    const [loadingHost, setLoadingHost] = useState(false);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [inventarioId, setInventarioId] = useState(null);
    const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);

    const API_URL = process.env.REACT_APP_API_URL;
    const { fetchWithAuth, isAuthenticated, user } = useAuth();

    useEffect(() => {
        if (isAuthenticated && user) {
            const fetchUserData = async () => {
                setLoading(true);
                try {
                    const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                    const usuarios = await responseUsuarios.json();
                    const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);
                    if (usuarioAutenticado) {
                        setTipoUsuario(usuarioAutenticado.tipoUsuario);
                        setInventarioId(usuarioAutenticado.inventarioId);

                    } else {
                        console.warn('Usuario no encontrado en la API');
                    }
                } catch (error) {
                    console.error('Error al obtener datos de usuario:', error);
                } finally {
                    setLoadingTipoUsuario(false);
                    setLoading(false);
                }
            };
            fetchUserData();
        }
    }, [isAuthenticated, user]);

    useEffect(() => {
        if (inventarioId) {
            fetchInventario();
        }
    }, [inventarioId]);

    const fetchInventario = async () => {
        if (isAuthenticated && inventarioId) {
            setLoading(true);
            try {
                const response = await fetchWithAuth(`${API_URL}/inventarios/${inventarioId}`);
                if (response.ok) {
                    const data = await response.json();
                    setInventario(data.groups || []);
                    // console.log("Datos de inventario asociado al usuario",data);
                } else {
                    throw new Error("El formato de la respuesta no es JSON");
                }
            } catch (error) {
                console.error('Error al cargar el inventario:', error.message);
                alert('Hubo un problema al cargar el inventario.');
            } finally {
                setLoading(false);
            }
        }
    };

    useEffect(() => {
        fetchInventario();
    }, [isAuthenticated, inventarioId]);

    const handleUpdateHost = (updatedHost) => {
        setInventario((prevInventario) => prevInventario.map((group) => ({
            ...group,
            hosts: group.hosts.map((host) => host.id === updatedHost.id ? { ...host, ...updatedHost } : host)
        })));
    };

    const handleDeleteHost = (hostId) => {
        setInventario((prevInventario) => prevInventario.map((group) => ({
            ...group,
            hosts: group.hosts.filter((host) => host.id !== hostId)
        })));
    };

    const filteredGroups = selectedGroup
        ? inventario.filter((group) => group.groupName === selectedGroup)
        : inventario;

    const filteredHostsBySearch = filteredGroups.map((group) => ({
        ...group,
        hosts: group.hosts.filter((host) => host.hostname.toLowerCase().includes(searchTerm.toLowerCase()))
    }));

    const hasHosts = filteredHostsBySearch.some((group) => group.hosts.length > 0);


    return (
        <div className="border-sky-950 border rounded-lg mb-8">
            <div className="mx-auto rounded-lg overflow-x-auto">
                <table className="min-w-full divide-y divide-gray-300  ">
                    <thead>
                    <tr className="text-center font-semibold text-xs text-gray-900">
                            <th className="px-3 py-3.5 sm:table-cell">Host</th>
                            <th className="px-3 py-3.5 sm:table-cell">IP</th>
                            <th className="hidden px-3 py-3.5 sm:table-cell">Grupo</th>
                            <th className="hidden px-3 py-3.5 sm:table-cell">Sistema Operativo</th>
                            <th className="hidden px-3 py-3.5 sm:table-cell">Usuario</th>
                            <th className="px-3 py-3.5 sm:table-cell">Puerto</th>
                            <th className=" px-3 py-3.5 sm:table-cell">Actualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {!hasHosts ? (
                            <tr>
                                <td colSpan="7" className="text-center py-4 text-gray-500">No existe el host</td>
                            </tr>
                        ) : (
                            filteredHostsBySearch.map((group) =>
                                group.hosts.map((host, index) => (
                                    <tr key={host.id} className={loadingHost === host.id ? 'bg-gray-200 animate-pulse' : index % 2 === 1 ? 'bg-white' : 'bg-gray-200'}>
                                        <td className="px-3 py-4 text-xs text-center sm:table-cell">{host.hostname}</td>
                                        <td className="px-3 py-4 text-xs font-bold text-black text-center sm:table-cell">{host.ipAddress}</td>
                                        <td className="hidden px-3 py-4 text-xs text-black text-center sm:table-cell">{group.groupName.charAt(0).toUpperCase() + group.groupName.slice(1)}</td>
                                        <td className="hidden px-3 py-4 text-xs text-black text-center sm:table-cell">{host.sistemaOperativo}</td>
                                        <td className="hidden px-3 py-4 text-xs text-center sm:table-cell">{user?.name}</td>
                                        <td className="px-3 py-4 text-xs text-center sm:table-cell">{host.port}</td>
                                        <td className="px-3 py-4 text-xs text-center sm:table-cell">
                                            <div className="flex flex-col items-center justify-center">
                                                <BotonActualizarFila
                                                    host={host}
                                                    setLoadingHost={setLoadingHost}
                                                    onDelete={handleDeleteHost}
                                                    onUpdate={handleUpdateHost}
                                                />
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            )
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default TabInventario;
