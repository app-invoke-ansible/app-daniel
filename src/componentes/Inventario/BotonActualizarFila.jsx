import React, { useState,useEffect } from "react";
import { AiOutlineEdit } from "react-icons/ai";
import { useAuth } from "../../providers/AuthProvider";

const BotonActualizarFila = ({ host, onDelete, onUpdate }) => {
    const { fetchWithAuth, user } = useAuth();
    const [isModalOpen, setModalOpen] = useState(false);
    const [modalData, setModalData] = useState(host);
    const [loading, setLoading] = useState({ update: false, delete: false });
    const [errorMessage, setErrorMessage] = useState({});
    const [allHosts, setAllHosts] = useState([]);
    const [inventoryData, setInventoryData] = useState(null); // Nuevo estado para guardar datos de inventario

    const API_URL = process.env.REACT_APP_API_URL;

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isModalOpen]);


    const openModal = async () => {
        setModalData(host);
        setModalOpen(true);

        try {
            const response = await fetchWithAuth(`${API_URL}/inventarios`);
            const data = await response.json();

            // Aseguramos de que data no esté vacío
            if (data && data.length > 0) {
                // Guardar los datos completos del inventario
                setInventoryData(data[0]);
                console.log("Datos completos de la API de inventarios:", data);
                // Extraer todos los hosts
                const allHosts = data.flatMap((inventory) =>
                    inventory.groups.flatMap((group) => group.hosts)
                );
                setAllHosts(allHosts);
            } else {
                console.error("No se encontraron datos de inventario.");
            }
        } catch (error) {
            console.error("Error al obtener los hosts:", error.message);
        }
    };

    const closeModal = () => {
        setModalOpen(false);
        setErrorMessage({});
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        const trimmedValue = value.trim();

        if (name === "port" && !/^\d*$/.test(value)) {
            return;
        }

        // Condición para ipAddress: solo números y puntos
        if (name === "ipAddress" && !/^[0-9.]*$/.test(value)) {
            return;
        }

        setModalData((prevData) => ({ ...prevData, [name]: trimmedValue }));

        if (name === "hostname" || name === "ipAddress") {
            validateField(name, trimmedValue);
        }
    };

    const validateField = (field, value) => {
        let error = "";
        const containsSpaces = /\s/.test(value);

        if (containsSpaces) {
            error = "El campo no puede contener espacios.";
        }
        else if (field === "hostname" && allHosts.some((h) => h.hostname === value && h.id !== modalData.id)) {
            error = "El nombre de host ya está en uso.";
        } else if (field === "ipAddress" && allHosts.some((h) => h.ipAddress === value && h.id !== modalData.id)) {
            error = "La dirección IP ya está en uso.";
        }

        // Validar formato IP
        if (field === 'ipAddress') {
            const ipRegex = /^(\d{1,3}\.){3}\d{1,3}$/;
            if (value && !ipRegex.test(value)) {
                error = 'Formato de dirección IP no válido.';
            }
        }
        setErrorMessage((prev) => ({ ...prev, [field]: error }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (Object.values(errorMessage).some((err) => err)) {
            alert("Corrige los errores antes de actualizar el Host.");
            return;
        }
        setLoading((prev) => ({ ...prev, update: true }));

        const { id, hostname, ipAddress, sistemaOperativo, port, userMachine } = modalData;

        // Verificar que inventoryData esté disponible y que no sea null
        if (!inventoryData) {
            alert("Error: No se han cargado los datos del inventario.");
            setLoading((prev) => ({ ...prev, update: false }));
            return;
        }

        // Encontrar el grupo correspondiente al host actual
        const currentGroup = inventoryData.groups.find(group =>
            group.hosts.some(h => h.id === host.id)
        );
        if (!currentGroup) {
            alert("Error: No se pudo encontrar el grupo para este host.");
            setLoading((prev) => ({ ...prev, update: false }));
            return;
        }
        const inventoryId = inventoryData.idInventario;
        const groupId = currentGroup.ansibleGroupId;

        const params = new URLSearchParams({
            Id: id,
            inventoryId: inventoryId,
            groupId: groupId,
            hostName: hostname,
            ipAddress,
            port,
            user: userMachine,
            osName: sistemaOperativo,
        });

        try {
            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await responseUsuarios.json();

            const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

            if (!usuarioAutenticado) {
                alert("Usuario no encontrado");
                return;
            }

            const idUsuario = usuarioAutenticado.id;

            const url = `${API_URL}/hosts/${idUsuario}/modificar?${params.toString()}`;
            const response = await fetchWithAuth(url, {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
            });

            if (response.ok) {
                alert("Host actualizado exitosamente.");
                closeModal();
                onUpdate(modalData);
            } else {
                alert("Error: No se pudo actualizar el host.");
            }
        } catch (error) {
            console.error("Error al actualizar el host:", error.message);
        } finally {
            setLoading((prev) => ({ ...prev, update: false }));
        }
    };

    const handleDelete = async () => {
        if (!modalData.id) {
            alert("Error: No se pudo obtener el ID del host para eliminar.");
            return;
        }

        setLoading((prev) => ({ ...prev, delete: true }));

        try {
            const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
            const usuarios = await responseUsuarios.json();

            const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

            if (!usuarioAutenticado) {
                alert("Usuario no encontrado");
                return;
            }

            const idUsuario = usuarioAutenticado.id;

            // Construir la URL para la solicitud DELETE
            const url = `${API_URL}/hosts/${idUsuario}/${modalData.id}`;
            const response = await fetchWithAuth(url, { method: "DELETE" });

            if (response.ok) {
                alert("Host eliminado exitosamente.");
                closeModal();
                onDelete(host.id);
            } else {
                alert("Error: No se pudo eliminar el host.");
            }
        } catch (error) {
            console.error("Error al eliminar el host:", error.message);
        } finally {
            setLoading((prev) => ({ ...prev, delete: false }));
        }
    };


    return (
        <div className="flex flex-col items-center justify-center">
            <button
                onClick={openModal}
                className="flex items-center justify-center rounded-full border-2 border-sky-950 text-sky-950 hover:text-white hover:bg-sky-950 h-8 w-8 mb-1 transition-colors duration-100 transform hover:scale-105"
            >
                <AiOutlineEdit className="h-4 w-4" />
            </button>
            <span className="text-xs">Editar</span>

            {isModalOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white rounded-lg p-6 shadow-lg w-full h-full md:w-96 md:h-auto md:rounded-lg">
                        <form onSubmit={handleSubmit}>
                            <h2 className="text-xl font-bold mb-4 text-sky-950">Actualizar Host</h2>
                            {["hostname", "ipAddress", "sistemaOperativo", "port"].map((field) => (
                                <div className="mb-4" key={field}>
                                    <label
                                        className="block text-m font-medium text-gray-700 text-left"
                                        htmlFor={field}
                                    >
                                        {field === "hostname"
                                            ? "Nombre del Host"
                                            : field === "ipAddress"
                                                ? "Dirección IP"
                                                : field === "sistemaOperativo"
                                                    ? "Sistema Operativo"
                                                    : "Puerto"}
                                    </label>
                                    <input
                                        type="text"
                                        name={field}
                                        id={field}
                                        value={modalData[field] || ""}
                                        onChange={handleChange}
                                        placeholder={
                                            field === "hostname"
                                                ? "Ej: Servidor-1"
                                                : field === "ipAddress"
                                                    ? "Ej: 192.168.0.1"
                                                    : field === "sistemaOperativo"
                                                        ? "Ej: Windows 10"
                                                        : "Ej: 22"
                                        }
                                        className={`mt-1 p-2 border rounded-md w-full text-m ${errorMessage[field] ? "border-red-500" : "border-gray-300"
                                            }`}
                                        required
                                    />
                                    {errorMessage[field] && (
                                        <p className="text-red-500 text-xs mt-1 text-left">
                                            {errorMessage[field]}
                                        </p>
                                    )}
                                </div>
                            ))}
                            <div className="flex justify-between mt-4">
                                <button
                                    type="button"
                                    className="mr-2 bg-red-500 hover:bg-red-600 rounded-md px-3 py-1 text-m text-white transition-colors duration-300 transform hover:scale-105"
                                    onClick={handleDelete}
                                    disabled={loading.delete}
                                >
                                    {loading.delete ? "Eliminando..." : "Eliminar"}
                                </button>
                                <div className="flex gap-4">
                                    <button
                                        type="button"
                                        className="px-3 py-1 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-m transition-colors duration-300 transform hover:scale-105"
                                        onClick={closeModal}
                                    >
                                        Cancelar
                                    </button>
                                    <button
                                        type="submit"
                                        className={`px-3 py-1 bg-sky-950 text-white hover:bg-sky-900 rounded text-m flex items-center justify-center gap-2 transition-colors duration-300 transform hover:scale-105 ${loading.update
                                            ? "bg-gray-400 cursor-not-allowed"
                                            : "bg-sky-950 hover:bg-gray-600"
                                            }`}
                                        disabled={loading.update}
                                    >
                                        {loading.update ? "Actualizando..." : "Actualizar"}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
};

export default BotonActualizarFila;