import React, { useState } from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';

const BuscarModulo = ({ modules, setFilteredModules }) => {
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearchChange = (e) => {
        const term = e.target.value.toLowerCase();
        setSearchTerm(term);
        const filtered = modules.filter(module =>
            module.name.toLowerCase().includes(term)
        );
        setFilteredModules(filtered);  // Actualizamos los módulos filtrados
    };

    return (
        <div className="px-8 flex items-center justify-between w-full">
            <div className="relative w-80">
                <input
                    type="search"
                    name="search"
                    id="searchModulo"
                    className="pl-2 pr-10 block w-full border-b-2 border-sky-950 py-1.5 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
                    placeholder="Buscar módulo"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />
            </div>
        </div>
    );
};

export default BuscarModulo;
