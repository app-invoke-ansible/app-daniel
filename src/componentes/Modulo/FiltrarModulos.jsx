import React, { useState } from "react";
import { FaFilter } from "react-icons/fa";

const FiltrarModulos = ({ iconMap, categorias, onFilter }) => {
  const [showDropdowns, setShowDropdowns] = useState(false);
  const [tipoSeleccionado, setTipoSeleccionado] = useState("");
  const [opcionSeleccionada, setOpcionSeleccionada] = useState("");

  const handleReset = () => {
    setTipoSeleccionado("");
    setOpcionSeleccionada("");
    onFilter(null, null); // Restaurar a mostrar todos los módulos
  };

  const opciones = tipoSeleccionado === "Categoría" ? categorias : iconMap;

  return (
    <div className="px-4 sm:px-8 flex mt-4 justify-center items-center sm:justify-end gap-4">
      {showDropdowns && (
        <div className="flex flex-col sm:flex-row md:flex-row gap-2 md:gap-4">
          {/* Dropdown para seleccionar tipo */}
          <select
            className="bg-white border border-gray-300 rounded-lg py-1 px-2 text-sm mb-2 sm:mb-0 md:mb-0"
            value={tipoSeleccionado}
            onChange={(e) => {
              setTipoSeleccionado(e.target.value);
              setOpcionSeleccionada(""); // Limpiar selección de opciones
            }}
          >
            <option value="" disabled>
              Seleccionar tipo...
            </option>
            <option value="Categoría">Categoría</option>
            <option value="Sistema">Sistema</option>
          </select>

          {/* Dropdown para seleccionar opciones */}
          <select
            className="bg-white border border-gray-300 rounded-lg py-1 px-2 text-sm"
            value={opcionSeleccionada}
            onChange={(e) => {
              const value = e.target.value;
              setOpcionSeleccionada(value);
              if (value) {
                onFilter(tipoSeleccionado, value); // Aplicar filtro automáticamente
              }
            }}
            disabled={!tipoSeleccionado}
          >
            <option value="" disabled>
              {tipoSeleccionado
                ? `Seleccionar ${tipoSeleccionado.toLowerCase()}...`
                : "Selecciona un tipo primero"}
            </option>
            {opciones.map((opcion) => (
              <option key={opcion.name} value={opcion.name}>
                {opcion.name}
              </option>
            ))}
          </select>
        </div>
      )}

      {/* Botón para alternar filtros */}
      <button
        className="flex items-center bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-2 px-4 text-xs font-semibold text-white whitespace-nowrap gap-2"
        onClick={() => {
          if (showDropdowns) handleReset(); // Restaurar si se cierra el filtro
          setShowDropdowns(!showDropdowns);
        }}
      >
        <FaFilter />
        {showDropdowns ? "Cerrar filtro" : "Filtrar módulos"}
      </button>
    </div>
  );
};

export default FiltrarModulos;