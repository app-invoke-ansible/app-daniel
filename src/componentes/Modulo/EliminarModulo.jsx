import React from 'react';
import { FaTrashAlt } from "react-icons/fa";
import { useAuth } from '../../providers/AuthProvider';
import { fetchUsuarios, deleteModulo } from '../../funciones/funcionesUsuario';

const EliminarModulo = ({ module, onDelete }) => {
  const { fetchWithAuth, user } = useAuth();

  const handleDelete = async () => {
    try {
      const usuariosData = await fetchUsuarios(fetchWithAuth);
      const usuarioAutenticado = usuariosData.find((u) => u.username === user?.name);

      if (!usuarioAutenticado) {
        console.warn('Usuario autenticado no encontrado en la API');
        return;
      }

      await deleteModulo(fetchWithAuth, usuarioAutenticado.id, module.id);

      alert("Módulo eliminado exitosamente.");
      onDelete(module.id);
    } catch (error) {
      console.error("Error al eliminar el módulo:", error.message);
      alert("Error: No se pudo eliminar el módulo.");
    }
  };

  return (
    <button
      onClick={handleDelete}
      className="flex items-center justify-center px-4 py-2 text-xs text-red-600 hover:bg-gray-300 w-full"
    >
      <FaTrashAlt className="mr-2" />
      Eliminar
    </button>
  );
};

export default EliminarModulo;