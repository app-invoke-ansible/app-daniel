import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { IoPlay } from "react-icons/io5";
import { useAuth } from '../../providers/AuthProvider';
import BuscarModulo from './BuscarModulo';
import { CiMenuKebab } from "react-icons/ci";
import SegmentoTabContent from '../Home/SegmentoTabContent';
import EliminarModulo from './EliminarModulo';
import Loading from '../Base/Loading';
import FiltrarModulos from './FiltrarModulos';
import { IoMdClose, IoIosNotifications } from "react-icons/io";
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';
import { fetchUsuarios } from '../../funciones/funcionesUsuario';
import { fetchModulos, fetchModulosUsuario } from '../../funciones/funcionesModulo';

function TabListaModulos() {
    const [isOn, setIsOn] = useState({});
    const [modulos, setModulos] = useState([]);
    const [filteredModules, setFilteredModules] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [currentModulo, setCurrentModulo] = useState(null);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
    const [filteredCategorias, setFilteredCategorias] = useState([]);
    const [filteredIconMap, setFilteredIconMap] = useState([]);
    const { fetchWithAuth, user } = useAuth();
    const [expandedModule, setExpandedModule] = useState(null);
    const [notificaciones, setNotificaciones] = useState({});
    const [usuarioAutenticado, setUsuarioAutenticado] = useState(null);
    const [modulosUsuario, setModulosUsuario] = useState(null);
    const API_URL = process.env.REACT_APP_API_URL;

    const channels = {
        email: 'Correo Electrónico',
        whatsapp: 'WhatsApp',
        teams: 'Microsoft Teams',
        slack: 'Slack',
        sms: 'SMS',
        pushNotifications: 'Notificaciones Push',
    };

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isModalOpen]);

    useEffect(() => {
        const fetchDatosIniciales = async () => {
            setLoading(true);
            try {
                const usuariosData = await fetchUsuarios(fetchWithAuth);
                const usuarioAutenticado = usuariosData.find((u) => u.username === user?.name);
                const modulosUsuario = await fetchModulosUsuario(fetchWithAuth, usuarioAutenticado.id)

                if (usuarioAutenticado) {
                    setTipoUsuario(usuarioAutenticado.tipoUsuario);
                    setUsuarioAutenticado(usuarioAutenticado);
                    setModulosUsuario(modulosUsuario)
                } else {
                    console.warn('Usuario autenticado no encontrado en la API');
                }

                if (usuarioAutenticado?.id) {
                    const [dataGlobal, dataUsuario] = await Promise.all([
                        fetchModulos(fetchWithAuth),
                        fetchModulosUsuario(fetchWithAuth, usuarioAutenticado.id)
                    ]);

                    const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);
                    const modulosAdquiridos = dataGlobal.filter((modulo) =>
                        modulosUsuarioIds.includes(modulo.id)
                    );

                    console.log(modulosUsuario)
                    // Inicializar los valores de notificación y estado del toggle
                    const initialIsOn = {};
                    modulosUsuario.modulos.forEach(modulo => {
                        initialIsOn[modulo.id] = modulo.estadoCorreo === 1;
                    });

                    console.log(initialIsOn)
                    setIsOn(initialIsOn);
                    setModulos(modulosAdquiridos);
                    setFilteredModules(modulosAdquiridos);
                }

            } catch (error) {
                console.error('Error al obtener los datos iniciales:', error.message);
                setError(error);
            } finally {
                setLoading(false);
                setLoadingTipoUsuario(false);
            }
        };

        fetchDatosIniciales();
    }, [fetchWithAuth, user?.name]);



    const handlePlay = (module) => {
        setCurrentModulo(module);
        setIsModalOpen(true);
    };

    // al eliminar se quita la fila de la tabla automaticamente
    const handleDelete = async (moduloId) => {
        setFilteredModules((prevModules) =>
            prevModules.filter((modulo) => modulo.id !== moduloId)
        );
        setModulos((prevModules) =>
            prevModules.filter((modulo) => modulo.id !== moduloId)
        );
    };

    const handleModalClose = () => {
        setIsModalOpen(false);
    };

    const formatDateTime = (dateString) => {
        if (!dateString || isNaN(new Date(dateString).getTime())) return 'N/A';
        const date = new Date(dateString);
        const optionsDate = { day: '2-digit', month: '2-digit', year: '2-digit' };
        const optionsTime = { hour: '2-digit', minute: '2-digit', hour12: true };
        return `${date.toLocaleDateString('es-CL', optionsDate)} ${date.toLocaleTimeString('es-CL', optionsTime)}`;
    };

    const toggleMenu = (module) => {
        setExpandedModule(expandedModule === module.id ? null : module.id);
    };

    const toggleNotification = async (moduleId, channel) => {
        if (channel !== 'email') {
            alert('Función no implementada');
            return;
        }

        try {
            const module = modulos.find(m => m.id === moduleId);
            if (!module) {
                console.error(`Módulo con ID ${moduleId} no encontrado`);
                return;
            }

            const newEstadoCorreo = module.estadoCorreo === 1 ? 0 : 1;

            // Actualización en backend
            const response = await fetchWithAuth(
                `${API_URL}/usuario/modulos/${module.id}/estado-correo?estadoCorreo=${newEstadoCorreo}&usuarioId=${usuarioAutenticado.id}`,
                { method: 'PUT' }
            );

            if (!response.ok) {
                throw new Error('Error al actualizar el estado del correo');
            }

            // Actualizar estado local
            const updatedModulos = modulos.map(m =>
                m.id === moduleId ? { ...m, estadoCorreo: newEstadoCorreo } : m
            );

            setModulos(updatedModulos);
            setFilteredModules(updatedModulos);

            // Actualizar UI
            setIsOn(prevState => ({
                ...prevState,
                [moduleId]: newEstadoCorreo === 1 // Actualiza solo el módulo afectado
            }));
        } catch (error) {
            console.error('Error:', error);
            alert('Error al actualizar el estado');
        }
    };

    const aplicarFiltro = (tipo, opcion) => {
        if (!tipo || !opcion) {
            setFilteredModules(modulos);
            return;
        }

        const filtered = modulos.filter((modulo) => {
            if (tipo === "Categoría") {
                return modulo.categoria.name === opcion;
            }
            if (tipo === "Sistema") {
                return modulo.sistema === opcion;
            }
            return false;
        });

        setFilteredModules(filtered);
    };


    if (error) return <div>Error: {error.message}</div>;

    return (
        <div>
            {/* Barra de búsqueda */}
            <div className="flex flex-col md:flex-row items-end justify-between w-full mt-4 mb-4 ">
                <BuscarModulo modules={modulos} setFilteredModules={setFilteredModules} />
                <FiltrarModulos iconMap={filteredIconMap} categorias={filteredCategorias} onFilter={aplicarFiltro} />
            </div>

            {/* Lista de módulos */}
            {loading ? (
                <Loading message="Cargando módulos adquiridos..." />
            ) : (

                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 gap-6 p-8">
                    {filteredModules.length === 0 ? (
                        <div className="text-center text-sm">El usuario no tiene módulos asociados.</div>
                    ) : (
                        filteredModules.map((module) => {
                            const lastStartTime = module.ejecuciones?.[0]?.startTime || 'N/A';
                            const isExpanded = expandedModule === module.id;
                            return (
                                <div
                                    key={module.id}
                                    className="border border-sky-950 rounded-lg py-3 shadow-md hover:shadow-lg transition-shadow duration-300 ease-in-out"
                                >
                                    <div className="flex items-center justify-between mb-4">
                                        <div className="flex-1">
                                            <h1 className="text-xs text-gray-800 text-center">Categoría {module.categoria.name}</h1>
                                            <h2 className="text-sm text-sky-900 font-bold text-center">{module.name}</h2>
                                        </div>
                                        {loadingTipoUsuario ? (
                                            <div></div>
                                        ) : (
                                            <>
                                                {(tipoUsuario === 'admin' || tipoUsuario === 'editor') && (
                                                    <div className="relative mx-1">
                                                        <button
                                                            onClick={() => toggleMenu(module)}
                                                            className="focus:outline-none"
                                                        >
                                                            {isExpanded ? <IoMdClose /> : <CiMenuKebab />}
                                                        </button>
                                                        {currentModulo === module.id && (
                                                            <div className="absolute top-0 left-full ml-2 w-40 bg-white border border-gray-200 rounded-md shadow-lg">
                                                                <Link
                                                                    to="/notificacionesusuario"
                                                                    className="flex items-center px-4 py-2 text-sm text-blue-600 hover:bg-gray-200 w-full text-left"
                                                                >
                                                                    <IoIosNotifications className="mr-2" />
                                                                    Notificaciones
                                                                </Link>
                                                                <EliminarModulo module={module} onDelete={handleDelete} />
                                                            </div>
                                                        )}
                                                    </div>
                                                )}
                                            </>
                                        )}
                                    </div>

                                    {isExpanded ? (
                                        <div>
                                            <h2 className="text-sm font-bold text-center">Notificaciones</h2>
                                            {Object.entries(channels).map(([key, label]) => {
                                                // Determinar estado activo basado en el canal
                                                const isActive = key === 'email'
                                                    ? isOn[module.id]
                                                    : notificaciones[module.id]?.[key];

                                                return (
                                                    <div key={key} className="flex justify-between items-center px-4 py-2 border-b">
                                                        <span className='text-xs'>{label}</span>
                                                        <button
                                                            onClick={() => toggleNotification(module.id, key)}
                                                            className={`w-8 h-4 flex items-center rounded-full duration-300 ease-in-out p-1 ${isActive ? "bg-green-400" : "bg-gray-300"
                                                                }`}
                                                        >
                                                            <div
                                                                className={`bg-white w-3 h-3 rounded-full shadow-md transform duration-300 ease-in-out ${isActive ? "translate-x-3" : ""
                                                                    }`}
                                                            ></div>
                                                        </button>
                                                    </div>
                                                );
                                            })}

                                            <div className="text-sm text-gray-700 bg-gray-200 flex flex-col justify-end">
                                                <Link
                                                    to="/notificacionesusuario"
                                                    className="flex items-center justify-center px-4 py-2 text-xs text-blue-600 hover:bg-gray-300 w-full"
                                                >
                                                    <IoIosNotifications className="mr-2" />
                                                    Notificaciones Generales
                                                </Link>
                                                <EliminarModulo module={module} onDelete={handleDelete} />
                                            </div>
                                        </div>
                                    ) : (
                                        <div className="mb-4">
                                            <div className="text-sm mb-2 px-3">
                                                <p className="font-semibold text-xs text-gray-500">Última Ejecución:</p>
                                                <p className="ml-4 mb-1 text-sky-900"><strong>{formatDateTime(lastStartTime)}</strong></p>
                                                <p className="font-semibold text-xs text-gray-500">Ejecuciones:</p>
                                                <p className="ml-4 mb-1 text-sky-900 font-semibold"> {module.ejecuciones.length}</p>
                                                <p className="font-semibold text-xs text-gray-500">Sistema Operativo:</p>
                                                <p className="ml-4 mb-1 text-sky-900 font-semibold">{module.sistema}</p>
                                                <div className="text-right">
                                                    <Link
                                                        to={`/detallemodulo/${module.id}`}
                                                        className="text-blue-500 hover:underline text-xs"
                                                    >
                                                        Más información...
                                                    </Link>
                                                </div>
                                            </div>

                                            <div className="flex justify-around items-center border-t border-b border-sky-800">
                                                {/* Imagen de categoría */}
                                                <div className="w-10 h-10 m-2">
                                                    <CategoriaImagen categoriaName={module.categoria.name} />
                                                </div>

                                                {/* Nivel de impacto */}
                                                <div className="flex flex-col items-center justify-center">
                                                    <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                                                        <div className="text-sky-500 text-xs font-bold bg-white px-1">
                                                            {module.nivelImpacto}
                                                        </div>
                                                    </div>
                                                </div>

                                                {/* Imagen de sistema */}
                                                <div className="w-10 h-10 m-2">
                                                    <SistemaImagen sistemaName={module.sistema} />
                                                </div>
                                            </div>
                                            {loadingTipoUsuario ? (
                                                <div></div>
                                            ) : (
                                                <>
                                                    {tipoUsuario && (
                                                        <div className="flex justify-center items-center mt-2">
                                                            <button
                                                                onClick={() => handlePlay(module)}
                                                                className="text-green-600 hover:text-green-900 flex flex-col items-center"
                                                            >
                                                                <IoPlay className="h-5 w-5" />
                                                                <span className="text-green-600 hover:text-green-900 text-xs">
                                                                    Ejecutar
                                                                </span>
                                                            </button>
                                                        </div>
                                                    )}
                                                </>
                                            )}
                                        </div>
                                    )}

                                </div>
                            );
                        })
                    )}
                </div>
            )}

            {/* Modal */}
            {isModalOpen && currentModulo && (
                <div className="fixed inset-0 bg-gray-800 bg-opacity-50 flex justify-center items-center z-50">
                    <div className="bg-white p-6 rounded-lg shadow-lg sm:w-5/6 sm:h-auto w-full h-full mx-0 overflow-auto">
                        <h2 className="text-lg font-semibold mb-4">Ejecutar módulo {currentModulo.name}</h2>
                        <SegmentoTabContent id={currentModulo.id} />
                        <div className="flex justify-end">
                            <button
                                onClick={handleModalClose}
                                className="text-white bg-red-500 hover:bg-red-600 px-4 py-1 rounded-lg shadow-md font-semibold transition-colors duration-300"
                            >
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default TabListaModulos;
