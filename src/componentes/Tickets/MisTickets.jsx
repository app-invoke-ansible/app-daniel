import React, { useState, useEffect, useCallback } from "react";
import { useAuth } from "../../providers/AuthProvider";
import { fetchUsuarios } from "../../funciones/funcionesUsuario";
import { fetchTickets, fetchZammadIdByUsername } from "../../funciones/funcionesZammad";
import Loading from "../Base/Loading";

const ITEMS_PER_PAGE = 5;

const MisTickets = ({ reloadTrigger }) => {
  const { fetchWithAuth, user } = useAuth();
  const [tickets, setTickets] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [zammadIdUser, setZammadIdUser] = useState(null);

  const cargarDatosIniciales = useCallback(async () => {
    try {
      setLoading(true);
      const [usuariosData, ticketsData] = await Promise.all([
        fetchUsuarios(fetchWithAuth),
        fetchTickets(fetchWithAuth),
      ]);

      const usuarioLogueado = usuariosData.find((u) => u.username === user.name);
      if (!usuarioLogueado) throw new Error("Usuario no encontrado");

      const zammadId = await fetchZammadIdByUsername(fetchWithAuth, usuarioLogueado.username);
      setZammadIdUser(zammadId);

      const userTickets = ticketsData
        .filter((ticket) => ticket.customer_id === zammadId)
        .sort((b, a) => new Date(a.created_at) - new Date(b.created_at));

      setTickets(userTickets);
    } catch (error) {
      console.error("Error:", error);
      setError(error.message);
    } finally {
      setLoading(false);
    }
  }, [fetchWithAuth, user.name]);

  useEffect(() => {
    cargarDatosIniciales();
  }, [cargarDatosIniciales]);

  if (error) {
    return <div className="text-center p-4 text-red-500">{error}</div>;
  }

  const totalPages = Math.ceil(tickets.length / ITEMS_PER_PAGE);
  const paginatedTickets = tickets.slice(
    (currentPage - 1) * ITEMS_PER_PAGE,
    currentPage * ITEMS_PER_PAGE
  );

  useEffect(() => {
    cargarDatosIniciales();
  }, [cargarDatosIniciales, reloadTrigger]);

  return (
    <div className="h-full flex flex-col">
      <h2 className="text-xl font-bold text-sky-950 text-center mb-2">Mis Tickets</h2>
      <div className="border-sky-950 bg-sky-950 border rounded-lg mb-8 pb-2 flex-grow">
        <div className="mx-auto sm:px-6 lg:px-0 rounded-lg h-full flex flex-col">
          <div className="flex-1 overflow-auto">
            <table className="min-w-full divide-y divide-gray-300">
              <thead>
                <tr>
                  <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">
                    Número
                  </th>
                  <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">
                    Título
                  </th>
                  <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">
                    Fecha de Creación
                  </th>
                  <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">
                    Estado
                  </th>
                  <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">
                    Prioridad
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white">
                {loading ? (
                  <tr>
                    <td colSpan="5" className="h-80">
                      <div className="flex justify-center items-center w-full h-full">
                        <Loading message="Cargando tickets..." />
                      </div>
                    </td>
                  </tr>
                ) : tickets.length === 0 ? (
                  <tr>
                    <td colSpan="5" className="h-80">
                      <p className="flex justify-center items-center w-full h-full">
                        No hay tickets registrados.
                      </p>
                    </td>
                  </tr>
                ) : (
                  paginatedTickets.map((ticket, index) => (
                    <tr
                      key={`${ticket.id}-${index}`}
                      className={index % 2 === 1 ? "bg-gray-200" : "bg-white"}
                    >
                      <td className="px-3 py-4 text-xs text-center">{ticket.number}</td>
                      <td className="px-3 py-4 text-xs text-center font-bold text-black">
                        {ticket.title}
                      </td>
                      <td className="px-3 py-4 text-xs text-center">
                        {formatFecha(ticket.created_at)}
                      </td>
                      <td className="px-3 py-4 text-xs text-center">
                        {getEstado(ticket.state_id)}
                      </td>
                      <td className="px-3 py-4 text-xs text-center">
                        {getPrioridad(ticket.priority_id)}
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
          </div>

          <div className="bg-white p-2 border-t border-sky-950">
            {!loading && (
              <div className="flex justify-center">
                {Array.from({ length: totalPages }, (_, i) => (
                  <button
                    key={i + 1}
                    onClick={() => setCurrentPage(i + 1)}
                    className={`mx-1 px-3 py-1 rounded ${currentPage === i + 1
                        ? "bg-sky-950 border rounded-xl text-white border-sky-950"
                        : "bg-gray-200 border rounded-xl border-sky-950"
                      }`}
                  >
                    {i + 1}
                  </button>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const formatFecha = (fecha) => {
  const fechaObj = new Date(fecha);
  return (
    <>
      {fechaObj.toLocaleDateString()} <br /> {fechaObj.toLocaleTimeString()}
    </>
  );
};

const getEstado = (stateId) => {
  const estados = {
    1: "Nuevo",
    2: "Abierto",
    3: "Pendiente",
    4: "Resuelto",
    5: "Cerrado",
  };
  return estados[stateId] || "Desconocido";
};

const getPrioridad = (priorityId) => {
  const prioridades = {
    1: "Baja",
    2: "Media",
    3: "Alta",
  };
  return prioridades[priorityId] || "Desconocida";
};

export default MisTickets;
