import React, { useState } from "react";
import { GoStack } from "react-icons/go";
import Zammad from "../../img/tickets/Zammad.png";
import ServiceNow from "../../img/tickets/ServiceNow.png";
import JiraService from "../../img/tickets/Jira.png";
import Zendesk from "../../img/tickets/Zendesk.png";
import { FaCheckCircle } from "react-icons/fa";

const StackTickets = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState("Zammad");

  const toggleMenu = () => setIsMenuOpen(!isMenuOpen);

  return (
    <div className="w-full">
      {/* Header */}
      <div className="w-full flex items-center bg-gray-100 px-4 border-b">
        <button
          onClick={toggleMenu}
          className="hover:bg-gray-200 p-2 rounded-lg transition-colors flex items-center space-x-2"
        >
          <GoStack className="text-xl text-sky-900" />
          <p className="text-xs text-sky-900">Stack Sistema de Tickets</p>
        </button>
      </div>

      {/* Menú horizontal */}
      <div
        className={`w-full bg-white shadow-md flex px-4 space-x-4 transition-all duration-300 overflow-hidden ${isMenuOpen ? "max-h-40 opacity-100" : "max-h-0 opacity-0"}`}
      >
        {/* Zammad */}
        <div
          className={`relative px-4 py-2 rounded cursor-pointer flex flex-col items-center justify-center border ${selectedItem === "Zammad" ? "border-sky-950" : "border-transparent"}`}
          onClick={() => setSelectedItem("Zammad")}
        >
          <img src={Zammad} alt="Zammad" className="w-8 h-auto mb-1" />
          <span className="text-xs text-center">Zammad</span>
          <FaCheckCircle className="absolute top-0 right-0 text-green-500 text-sm m-1" />
        </div>

        {/* Jira Service */}
        <div
          className={`px-4 py-2 rounded cursor-pointer flex flex-col items-center justify-center border ${selectedItem === "Jira Service" ? "border-sky-950" : "border-transparent"}`}
          onClick={() => setSelectedItem("Jira Service")}
        >
          <img src={JiraService} alt="Jira Service" className="w-6 h-auto mb-1" />
          <span className="text-xs text-center">Jira Service</span>
        </div>

        {/* ServiceNow */}
        <div
          className={`px-4 py-2 rounded cursor-pointer flex flex-col items-center justify-center border ${selectedItem === "ServiceNow" ? "border-sky-950" : "border-transparent"}`}
          onClick={() => setSelectedItem("ServiceNow")}
        >
          <img src={ServiceNow} alt="ServiceNow" className="w-7 h-auto mb-1" />
          <span className="text-xs text-center">ServiceNow</span>
        </div>

        {/* Zendesk */}
        <div
          className={`px-4 py-2 rounded cursor-pointer flex flex-col items-center justify-center border ${selectedItem === "Zendesk" ? "border-sky-950" : "border-transparent"}`}
          onClick={() => setSelectedItem("Zendesk")}
        >
          <img src={Zendesk} alt="Zendesk" className="w-7 h-auto mb-1" />
          <span className="text-xs text-center">Zendesk</span>
        </div>
      </div>
    </div>
  );
};

export default StackTickets;