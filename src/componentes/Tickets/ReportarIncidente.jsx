import React, { useState, useEffect } from 'react';
import { CgCheckO } from 'react-icons/cg';
import { useAuth } from "../../providers/AuthProvider";
import { fetchUsuarios } from '../../funciones/funcionesUsuario';
import { fetchZammadIdByUsername, createTicket } from '../../funciones/funcionesZammad';

const ReportarIncidente = ({ onReportSuccess }) => {
  const { fetchWithAuth, user } = useAuth();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [priority, setPriority] = useState('1 low');
  const [zammadIdUser, setZammadIdUser] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [isCreating, setIsCreating] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    const fetchDatosIniciales = async () => {
      try {
        const usuariosData = await fetchUsuarios(fetchWithAuth);
        const usuarioLogueado = usuariosData.find((u) => u.username === user.name);
        if (!usuarioLogueado) throw new Error("Usuario no encontrado");

        const zammadId = await fetchZammadIdByUsername(fetchWithAuth, usuarioLogueado.username);
        setZammadIdUser(zammadId);

      } catch (error) {
        console.error('Error cargando datos iniciales:', error);
        setErrorMessage({ general: error.message });
      }
    };
    fetchDatosIniciales();
  }, [fetchWithAuth, user.name]);

  const validateForm = () => {
    if (!title || !description) {
      setErrorMessage({ general: 'Todos los campos son requeridos' });
      return false;
    }
    return true;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!validateForm() || !zammadIdUser) return;

    setIsCreating(true);
    setErrorMessage(null);

    const priorityMap = {
      'Baja': '1 low',
      'Media': '2 normal',
      'Alta': '3 high'
    };

    const ticketData = {
      title: title,
      group: "Users",
      customer_id: zammadIdUser,
      priority: priorityMap[priority],
      state: "new",
      article: {
        subject: title,
        body: description,
        type: "web",
        internal: false
      }
    };

    try {
      await createTicket(fetchWithAuth, ticketData);
      setShowModal(true);
      setTitle('');
      setDescription('');
      setPriority('Baja');
      if (onReportSuccess) onReportSuccess();
    } catch (error) {
      console.error('Error creando ticket:', error);
      setErrorMessage({ general: error.message || 'Error al crear el ticket. Intente nuevamente.' });
    } finally {
      setIsCreating(false);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <div className="h-full flex flex-col">
      <h2 className="text-xl font-bold text-sky-950 text-center mb-2">Reportar incidente</h2>
      <div className="border-sky-950 border rounded-lg mb-8 pb-2 flex-grow p-6">
        {errorMessage?.general && (
          <div className="mb-4 p-2 bg-red-100 text-red-700 rounded text-sm">
            {errorMessage.general}
          </div>
        )}

        <form onSubmit={handleSubmit} className="space-y-6">
          <div>
            <label className="block text-sm font-semibold mb-1">Título del Incidente:</label>
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              required
              className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none"
              maxLength="100"
            />
          </div>
          <div>
            <label className="block text-sm font-semibold mb-1">
              Descripción:
              <span className="text-gray-500 font-normal text-xs block mt-1">
                Describe detalladamente el problema, incluyendo pasos para reproducirlo y cualquier error específico.
              </span>
            </label>
            <textarea
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
              className="w-full p-2 border border-gray-300 rounded text-xs focus:outline-none h-24"
              placeholder="Ejemplo: Al ejecutar el módulo 'deploy.yml', obtengo el error..."
              maxLength="500"
            />
          </div>
          <div>
            <label className="block text-sm font-semibold mb-1">Prioridad:</label>
            <select
              value={priority}
              onChange={(e) => setPriority(e.target.value)}
              required
              className="w-full px-4 py-2 bg-gray-300 border border-transparent hover:border-gray-400 rounded text-xs"
            >
              <option value="Baja">Baja</option>
              <option value="Media">Media</option>
              <option value="Alta">Alta</option>
            </select>
          </div>
          <div className='flex justify-center items-center pt-4'>
            <button
              type="submit"
              disabled={isCreating}
              className={`w-1/2 px-4 py-2 text-white rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 ${isCreating
                ? 'bg-gray-400 cursor-not-allowed'
                : 'bg-sky-950 hover:bg-sky-900 transform hover:scale-105'
                }`}
            >
              {isCreating ? 'Reportando...' : 'Reportar Incidente'}
            </button>
          </div>
        </form>

        {/* Modal de confirmación */}
        {showModal && (
          <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
            <div className="bg-white w-full h-full sm:h-auto sm:w-11/12 sm:max-w-2xl p-4 sm:p-6 rounded-lg shadow-lg flex flex-col overflow-y-auto items-center">
              <CgCheckO className="w-12 h-12 text-green-500" />
              <h3 className="text-xl font-semibold text-center mb-4">¡Reporte Enviado!</h3>
              <p className="text-center text-gray-700 mb-6">
                Hemos recibido tu reporte.<br />
                Nuestro equipo lo revisará y te contactará pronto.
              </p>
              <button
                onClick={handleCloseModal}
                className="w-full px-4 py-2 text-white rounded text-sm flex items-center justify-center gap-2 transition-colors duration-300 bg-sky-950 hover:bg-sky-900 transform hover:scale-105"
              >
                Cerrar
              </button>
            </div>
          </div>
        )
        }
      </div >
    </div >
  );
};

export default ReportarIncidente;