import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { fetchServidor } from '../../funciones/funcionesZammad';

const EstadoServidor = () => {
  const { fetchWithAuth } = useAuth();
  const [estado, setEstado] = useState({
    healthy: false,
    message: 'Conectando...',
    error: false,
  });

  useEffect(() => {
    const fetchDatos = async () => {
      try {
        const servidorData = await fetchServidor(fetchWithAuth);

        if (servidorData && 'healthy' in servidorData) {
          setEstado({
            healthy: true,
            message: 'Activo',
            error: false,
          });
        } else {
          setEstado({
            healthy: false,
            message: 'Inactivo',
            error: true,
          });
        }
      } catch (error) {
        console.error('Error al conectar con el servidor:', error);
        setEstado({
          healthy: false,
          message: 'Error al conectar con el servidor.',
          error: true,
        });
      }
    };

    fetchDatos();

    const interval = setInterval(fetchDatos, 5000);

    return () => clearInterval(interval);
  }, [fetchWithAuth]);

  return (
    <div className="flex py-2 px-6 items-center justify-end">
      <span
        className={`w-3 h-3 rounded-full transition ${
          estado.healthy ? 'bg-green-500 animate-pulse' : 'bg-red-500 animate-pulse'
        }`}
      ></span>
      <p className="ml-2 text-xs">{estado.message}</p>
    </div>
  );
};

export default EstadoServidor;
