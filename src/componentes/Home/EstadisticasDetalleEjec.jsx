import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

const EstadisticasDetalleEjec = ({ modulo }) => {
    // Estado para manejar estadísticas y carga
    const [stats, setStats] = useState({
        totalEjecuciones: 0,
        mediaEnSegundos: 0,
        tasaDeExito: 0,
    });
    const [loading, setLoading] = useState(true);

    // Extraer fetchWithAuth del contexto de autenticación
    const { fetchWithAuth } = useAuth();

    useEffect(() => {
        // Función para obtener y calcular estadísticas de ejecuciones
        const fetchEjecuciones = async () => {
            if (modulo) {
                setLoading(true);
                try {
                    // Realizar solicitud autenticada para obtener los módulos
                    const response = await fetchWithAuth(`${API_URL}/modulos`);
    
                    // Validar el tipo de contenido de la respuesta
                    const contentType = response.headers.get("content-type");
                    if (contentType && contentType.includes("application/json")) {
                        const data = await response.json();
    
                        // Filtrar el módulo específico por ID
                        const filteredModules = data.filter((mod) => mod.id === modulo.id);
    
                        // Extraer ejecuciones de los módulos filtrados
                        const ejecuciones = filteredModules.flatMap((mod) => mod.ejecuciones || []);
    
                        // Calcular estadísticas
                        const totalEjecuciones = ejecuciones.length;
    
                        // Calcular la duración promedio en segundos
                        const duracionesEnSegundos = ejecuciones.map((ejec) => {
                            if (ejec.startTime && ejec.endTime) {
                                const start = new Date(ejec.startTime);
                                const end = new Date(ejec.endTime);
                                return (end - start) / 1000; // Convertir a segundos
                            }
                            return 0; // Manejar ejecuciones sin 'endTime'
                        });
    
                        const mediaEnSegundos =
                            duracionesEnSegundos.reduce((a, b) => a + b, 0) / duracionesEnSegundos.length || 0;
    
                        // Calcular porcentaje de éxito
                        const ejecucionesExitosas = ejecuciones.filter((ejec) => ejec.status === 'success').length;
                        const tasaDeExito = totalEjecuciones > 0 ? (ejecucionesExitosas / totalEjecuciones) * 100 : 0;
    
                        // Actualizar el estado con las estadísticas calculadas
                        setStats({
                            totalEjecuciones,
                            mediaEnSegundos: mediaEnSegundos.toFixed(2),
                            tasaDeExito: tasaDeExito.toFixed(2),
                        });
                    } else {
                        throw new Error("El formato de la respuesta no es JSON");
                    }
                } catch (error) {
                    console.error("Error al obtener datos de la API:", error.message);
                    setStats({
                        totalEjecuciones: 0,
                        mediaEnSegundos: "0.00",
                        tasaDeExito: "0.00",
                    }); // Establecer estadísticas vacías en caso de error
                } finally {
                    setLoading(false);
                }
            } else {
                setLoading(false);
            }
        };
    
        fetchEjecuciones();
    }, [modulo, fetchWithAuth]);    

    // Renderizado del componente
    return (
        <div className="grid grid-cols-3 gap-4 mb-6">
            {loading ? (
                <div className="col-span-4 text-center">
                    <p className="text-base font-bold text-sky-950">Cargando datos...</p>
                </div>
            ) : (
                <>
                    <StatItem label="Total de ejecuciones" value={stats.totalEjecuciones} />
                    <StatItem label="Media en segundos" value={stats.mediaEnSegundos} />
                    <StatItem label="Porcentaje de éxito" value={`${stats.tasaDeExito} %`} />
                </>
            )}
        </div>
    );
};

// Componente para mostrar un elemento estadístico
export const StatItem = ({ label, value }) => (
    <div className="bg-white p-2 rounded-3xl border border-sky-950 text-center flex flex-col">
        <p className="text-gray-800 text-xs">{label}</p>
        <p className="text-base font-bold text-sky-950">{value}</p>
    </div>
);

export default EstadisticasDetalleEjec;
