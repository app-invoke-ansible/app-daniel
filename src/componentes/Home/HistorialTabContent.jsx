import React from 'react';
import TabHistEjec from '../../componentes/Home/TabHistEjec';

const HistorialTabContent = ({ id }) => (
  <div className="mx-auto">
    <h1 className="text-base text-sky-900 font-bold mb-2">Historial de Ejecución</h1>
    <div className="mx-auto">
      <TabHistEjec id={id} />
    </div>
    <div className="mt-5 mx-9"></div>
  </div>
);

export default HistorialTabContent;