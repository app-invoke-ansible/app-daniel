import React, { useState, useEffect, useRef } from 'react';
import { ImSpinner } from 'react-icons/im';
import { FcCheckmark } from 'react-icons/fc';
import { FiX } from 'react-icons/fi';
import { BsExclamationCircle } from 'react-icons/bs';
import { CgSandClock } from 'react-icons/cg';
import { useAuth } from '../../providers/AuthProvider';
import BuscarHost from './BuscarHost';
import { LuLoaderCircle, LuExpand, LuX } from "react-icons/lu";
import ModuloSurvey from './ModuloSurvey';
import Loading from '../Base/Loading';

// Variables de entorno
const API_URL = process.env.REACT_APP_API_URL;
const WEBSOCKET_URL = process.env.REACT_APP_WEBSOCKET_URL;

const SegmentoTabContent = ({ id }) => {
  // Estados principales
  const [modules, setModules] = useState([]);
  const [filteredModules, setFilteredModules] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [moduleStates, setModuleStates] = useState({});
  const [logs, setLogs] = useState([]);
  const [selectedInventario, setSelectedInventario] = useState(null);
  const [selectedGrupo, setSelectedGrupo] = useState(null);
  const [selectedHosts, setSelectedHosts] = useState([]);
  const [currentModuloId, setCurrentModuloId] = useState(id);
  const [inventario, setInventario] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filteredHosts, setFilteredHosts] = useState([]);
  const [tipoUsuario, setTipoUsuario] = useState(null);
  const [currentJobIds, setCurrentJobIds] = useState(null);
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
  const [inventarioId, setInventarioId] = useState(null);
  const [surveyData, setSurveyData] = useState({});
  const [currentJobId, setCurrentJobId] = useState(null);
  const [correoUsuario, setCorreoUsuario] = useState(null);
  const [usuarioId, setUsuarioId] = useState(null);

  const logsEndRef = useRef(null);

  // Extraer fetchWithAuth desde el contexto de autenticación
  const { fetchWithAuth, isAuthenticated, user } = useAuth();

  // Efecto para cargar módulos, inventarios y conectar WebSocket al montar
  useEffect(() => {
    fetchModules();
    fetchInventario();
    connectWebSocket();
  }, []);

  // Efecto para actualizar el módulo actual cuando cambia el ID
  useEffect(() => {
    setCurrentModuloId(id);
  }, [id]);

  // Función para obtener módulos
  const fetchModules = async () => {
    setLoading(true);
    setError(null); // Limpiar errores previos
    try {
      const response = await fetchWithAuth(`${API_URL}/modulos`);
      const contentType = response.headers.get("content-type");
      if (!contentType || !contentType.includes("application/json")) {
        throw new Error("El formato de la respuesta no es JSON");
      }
      const data = await response.json();
      setModules(data);
      setFilteredModules(data);
    } catch (error) {
      console.error("Error al obtener los módulos:", error.message);
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  // Función para obtener usuarios desde la API
  const fetchUsuarios = async () => {
    try {
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await responseUsuarios.json();

      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (usuarioAutenticado) {
        setUsuarioId(usuarioAutenticado.id);
        setTipoUsuario(usuarioAutenticado.tipoUsuario);
        setInventarioId(usuarioAutenticado.inventarioId);
        setCorreoUsuario(usuarioAutenticado.email);
      } else {
        console.warn('Usuario autenticado no encontrado en la API.');
      }
      return usuarios;
    } catch (error) {
      console.error('Error al cargar los permisos:', error);
      return [];
    } finally {
      setLoadingTipoUsuario(false);
    }
  };

  useEffect(() => {
    if (inventarioId, usuarioId) {
      fetchInventario();
      connectWebSocket();
    }
  }, [inventarioId, usuarioId]);

  const fetchInventario = async () => {
    if (isAuthenticated && inventarioId) {
      setLoading(true);
      try {
        const response = await fetchWithAuth(`${API_URL}/inventarios/${inventarioId}`);
        if (response.ok) {
          const data = await response.json();
          setInventario(data);
          setSelectedGrupo(null);
          setSelectedHosts([]);
        } else {
          throw new Error("El formato de la respuesta no es JSON");
        }
      } catch (error) {
        console.error('Error al cargar el inventario:', error.message);
        alert('Hubo un problema al cargar el inventario.');
      } finally {
        setLoading(false);
      }
    }
  };

  useEffect(() => {
    fetchInventario();
  }, [isAuthenticated, inventarioId]);


  // Función para conectar al WebSocket
  const connectWebSocket = () => {
    const socket = new WebSocket(`${WEBSOCKET_URL}/${usuarioId}`);
    let lastLog = "";
    let firstLogAdded = false;

    socket.onmessage = (event) => {
      const message = event.data;

      // Buscar el JobId en el mensaje
      const jobIdMatch = message.match(/JobId:\s*(\d+)/);
      if (jobIdMatch) {
        const jobId = parseInt(jobIdMatch[1], 10);
        setCurrentJobId(jobId); // Guardar el JobId
        return;
      }

      // "Logs del trabajo:"
      if (message.startsWith("Logs del trabajo:")) {
        const logMessage = message.replace("Logs del trabajo:", "").trim();

        if (!firstLogAdded) {
          // Si es el primer "Logs del trabajo:", agregar el mensaje completo
          lastLog = logMessage;
          setLogs((prevLogs) => [...prevLogs, `Logs del trabajo:\n${lastLog}`]);
          firstLogAdded = true; // Marcar que ya se ha agregado el primer log
        } else {
          // Si ya se ha agregado el primer log, comparar con el último y agregar solo la diferencia
          const diff = getDifference(lastLog, logMessage);
          if (diff) {
            setLogs((prevLogs) => [...prevLogs, diff]);
            lastLog = logMessage; // Actualizar el último log
          }
        }
        return;
      }

      // Detectar mensajes que comienzan con "Estado del trabajo:"
      if (message.startsWith("Estado del trabajo:")) {
        const status = message.split(":")[1].trim(); // Obtener el estado después de ":"

        // Mapear el estado recibido al estado del módulo
        switch (status) {
          case "running":
            setModuleStates((prev) => ({ ...prev, [currentModuloId]: "running" }));
            break;
          case "canceled":
            setModuleStates((prev) => ({ ...prev, [currentModuloId]: "canceled" }));
            break;
          case "successful":
            setModuleStates((prev) => ({ ...prev, [currentModuloId]: "successful" }));
            break;
          case "failed":
            setModuleStates((prev) => ({ ...prev, [currentModuloId]: "failed" }));
            break;
          case "pending":
            setModuleStates((prev) => ({ ...prev, [currentModuloId]: "pending" }));
            break;
          default:
            console.warn(`Estado desconocido: ${status}`);
            break;
        }
        return;
      }

      setLogs((prevLogs) => [...prevLogs, message]);
    };

    socket.onerror = (error) => {
      console.error("Error en WebSocket:", error);
    };

    return () => socket.close();
  };

  // Función para obtener la diferencia entre dos logs
  const getDifference = (lastLog, currentLog) => {
    let diff = '';
    let i = 0;
    while (i < lastLog.length && i < currentLog.length && lastLog[i] === currentLog[i]) {
      i++;
    }

    if (i < currentLog.length) {
      diff = currentLog.slice(i).trim();
    }

    return diff;
  };

  useEffect(() => {
    if (inventario) {
      setSelectedInventario(inventario);
    }
  }, [inventario]);

  const onSurveySubmit = (data) => {
    // console.log('Survey data submitted:', data);
    setSurveyData(data.extra_vars);
  };

  const handleModalExecute = async () => {
    if (!selectedInventario) {
      console.error('Inventario no seleccionado');
      console.log(selectedInventario);
      return;
    }

    if (!currentModuloId) {
      console.error('currentModuloId no está definido');
      return;
    }

    try {
      setLogs([]);
      setModuleStates((prev) => ({ ...prev, [currentModuloId]: "running" }));

      const usuarios = await fetchUsuarios();
      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (!usuarioAutenticado) {
        console.warn('Usuario autenticado no encontrado en la API.');
        return;
      }

      const idUsuario = usuarioAutenticado.id;

      // Construir la URL
      let url = `${API_URL}/modulos/ejecutar/${currentModuloId}?usuarioId=${idUsuario}&inventarioId=${selectedInventario.idInventario}`;

      if (selectedGrupo?.id) url += `&grupoId=${selectedGrupo.id}`;
      if (selectedHosts.length > 0) {
        const hostIds = selectedHosts.map((host) => host.id).join(',');
        url += `&hostIds=${hostIds}`;
      }

      // console.log('URL generada para la ejecución:', url);

      // Cuerpo de la solicitud
      const body = {
        extra_vars: surveyData || {},
      };

      // Registrar el cuerpo de la solicitud
      console.log('Cuerpo de la solicitud:', JSON.stringify(body, null, 2));

      const response = await fetchWithAuth(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      });

      // console.log('Resultado de la ejecución:', response);

      await fetchModules(); // Refrescar módulos después de ejecutar
    } catch (error) {
      console.error('Error al ejecutar el módulo:', error);
      await fetchModules();
    }
  };

  // Detener la ejecución del módulo
  const handleCancelExecution = async () => {
    if (!currentJobId) {
      console.error('No hay un Job ID actual para cancelar');
      return;
    }
    console.log(`Intentando cancelar el Job ID ${currentJobId}`);

    try {
      // Hacer la solicitud a la API para detener la ejecución
      const response = await fetchWithAuth(`${API_URL}/modulos/detener-ejecucion/${currentJobId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        console.log('Ejecución detenida exitosamente');
        const timestamp = new Date().toISOString();
        setLogs((prevLogs) => [
          ...prevLogs,
          { timestamp, message: `Ejecución detenida (Job ID: ${currentJobId})` },
        ]);
        setCurrentJobId(null);
      } else {
        console.error('La cancelación no fue exitosa. Estado de la respuesta:', response.status);
      }
    } catch (error) {
      console.error('Error al cancelar el job:', error.message);
    }
  };

  // Inicializamos los hosts filtrados con todos los hosts del grupo seleccionado
  useEffect(() => {
    if (selectedGrupo) {
      setFilteredHosts(selectedGrupo.hosts);
    }
  }, [selectedGrupo]);

  useEffect(() => {
    fetchUsuarios();
  }, []);

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  return (
    <div className="mx-auto py-2">
      <div className="flex flex-col md:flex-row gap-4">
        {/* Primera columna */}
        <div className="w-full md:w-1/3 h-auto">
          <h2 className="text-sm text-sky-900 font-bold mb-2 text-center">Configurar ejecución del módulo</h2>

          <div className="border border-sky-950 rounded-3xl py-4 bg-gray-200">
            {loading ? (
              <div className="flex justify-center items-center py-6">
                <Loading message="Cargando..." />
              </div>
            ) : (
              <>
                <div>
                  <div>
                    {/* Columna Inventario */}
                    <div className="flex flex-row md:flex-col lg:flex-row items-center text-center justify-start lg:mx-2 lg:space-x-4 sm:mx-2 sm:space-x-4 md:mx-0 md:space-x-0">
                      <h3 className="font-semibold text-sm text-gray-900 p-3">Inventario</h3>
                      <select
                        value={selectedInventario?.idInventario || ''}
                        className="w-11/12 lg:w-full border border-gray-300 rounded px-2 py-1 bg-gray-100 cursor-not-allowed"
                        disabled
                      >
                        {inventario && (
                          <option key={inventario.idInventario} value={inventario.idInventario}>
                            {inventario.nombreInventario}
                          </option>
                        )}
                      </select>
                    </div>

                    {/* Columna Grupo */}
                    <div className="flex flex-row md:flex-col lg:flex-row items-center text-center justify-start lg:mx-2 lg:space-x-4 sm:mx-2 sm:space-x-4 md:mx-0 md:space-x-0">
                      <h3 className="font-semibold text-sm text-gray-900 p-3 ml-6 md:ml-0 lg:ml-6">Grupo</h3>
                      {inventario && (
                        <select
                          value={selectedGrupo?.id || ''}
                          onChange={(e) => {
                            const grupo = inventario.groups.find(
                              (g) => g.id === parseInt(e.target.value, 10)
                            );
                            setSelectedGrupo(grupo);
                            setSelectedHosts([]);
                          }}
                          className="w-11/12 lg:w-full border border-gray-300 rounded px-2 py-1"
                        >
                          <option value="">Seleccione un grupo</option>
                          {inventario.groups.map((grupo) => (
                            <option key={grupo.id} value={grupo.id}>
                              {grupo.groupName}
                            </option>
                          ))}
                        </select>
                      )}
                    </div>

                    {/* Columna Host */}
                    <div className="flex flex-row md:flex-col lg:flex-row items-center justify-start lg:mx-2 lg:space-x-4 sm:mx-2 sm:space-x-4 md:mx-0 md:space-x-0 md:flex-grow">
                      <h3 className="font-semibold text-sm text-gray-900 p-3 ml-8 md:ml-0 lg:ml-8">Host</h3>
                      <button
                        onClick={() => setIsModalOpen(true)}
                        className={`bg-sky-800 hover:bg-blue-950 text-white font-medium py-1 px-3 rounded text-sm whitespace-nowrap ${!selectedGrupo ? 'opacity-50 cursor-not-allowed' : ''}`}
                        disabled={!selectedGrupo}
                      >
                        Seleccionar Host
                      </button>
                    </div>
                    <div className="flex  px-6 items-center justify-center">
                      {!selectedGrupo ? (
                        <p className="text-xs text-orange-500">Seleccionar un grupo para ver los hosts.</p>
                      ) : (
                        <>
                          {selectedGrupo.hosts.length === 0 && (
                            <p className="text-xs text-yellow-500">Este grupo no tiene hosts.</p>
                          )}
                          {selectedHosts.length === 0 && selectedGrupo.hosts.length > 0 && (
                            <p className="text-xs text-yellow-500">Se ejecutará en todos los hosts.</p>
                          )}
                          {selectedHosts.length > 0 && (
                            <p className="text-xs text-gray-500">
                              Se ejecutará en: {selectedHosts.map((h) => h.hostname).join(', ')}
                            </p>
                          )}
                        </>
                      )}
                    </div>
                  </div>
                </div>

                <ModuloSurvey currentModuloId={currentModuloId} correoUsuario={correoUsuario} onSurveySubmit={onSurveySubmit} usuarioId={usuarioId} />
              </>
            )}
            {/* Modal de selección de Hosts */}
            {isModalOpen && (
              <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
                <div className="bg-white p-6 rounded-lg shadow-lg sm:w-2/3 w-full h-full sm:h-auto">
                  <h2 className="text-lg font-bold mb-2 sm:text-sm">Seleccionar Host</h2>
                  <BuscarHost
                    hosts={selectedGrupo.hosts}
                    onFilterHosts={setFilteredHosts}
                  />
                  <div className="space-y-2">
                    {filteredHosts.map((host) => (
                      <div key={host.id} className="flex items-center">
                        <input
                          type="checkbox"
                          value={host.id}
                          checked={selectedHosts.some((h) => h.id === host.id)}
                          onChange={(e) => {
                            if (e.target.checked) {
                              setSelectedHosts([...selectedHosts, host]);
                            } else {
                              setSelectedHosts(selectedHosts.filter((h) => h.id !== host.id));
                            }
                          }}
                          className="mr-2"
                        />
                        <label>{host.hostname}</label>
                      </div>
                    ))}
                  </div>
                  <div className="mt-4 flex justify-end space-x-4">
                    <button
                      onClick={() => setIsModalOpen(false)}
                      className="bg-green-600 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded"
                    >
                      Seleccionar
                    </button>
                    <button
                      onClick={() => setIsModalOpen(false)}
                      className="bg-red-500 hover:bg-gray-400 text-white font-semibold py-2 px-4 rounded"
                    >
                      Cancelar
                    </button>
                  </div>
                </div>
              </div>
            )}

            {/* Botón de Ejecución */}
            <div className="mt-2 flex flex-col items-center space-y-2">
              {loadingTipoUsuario ? (
                <Loading message="Cargando permisos..." />
              ) : (
                <>
                  {moduleStates[currentModuloId] && (
                    <ModuleStateIndicator state={moduleStates[currentModuloId]} />
                  )}

                  {/* Mostrar solo el botón de ejecutar si no está ejecutando */}
                  {moduleStates[currentModuloId] !== 'running' && (
                    <button
                      onClick={handleModalExecute}
                      className={`py-1 px-4 rounded font-semibold ${tipoUsuario
                        ? 'bg-green-600 hover:bg-green-500 text-white'
                        : 'bg-gray-400 text-gray-700 cursor-not-allowed'
                        }`}
                      disabled={!tipoUsuario}
                    >
                      Ejecutar
                    </button>
                  )}

                  {/* Botón para cancelar ejecución */}
                  {moduleStates[currentModuloId] === 'running' && (
                    <button
                      onClick={handleCancelExecution}
                      className="mt-2 py-1 px-4 rounded bg-red-500 hover:bg-red-600 text-white font-semibold"
                    >
                      Cancelar
                    </button>
                  )}
                </>
              )}
            </div>
          </div>
        </div>

        {/* Segunda columna */}
        <div className="w-full md:w-2/3 h-auto">{/* Ancho completo en móviles, 2/3 en pantallas medianas */}
          <h2 className="text-sm text-sky-900 font-bold mb-2 text-center">Logs de la ejecución</h2>
          <div
            ref={logsEndRef}
            className="font-mono text-sm overflow-y-auto bg-black text-green-400 p-4 rounded-3xl max-h-96"
          >
            {logs.length > 0 ? (
              <pre className="whitespace-pre-wrap">{logs.join('\n')}</pre>
            ) : (
              <p className="text-gray-400">No existe información del log.</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

// Componente para mostrar el estado del módulo
const ModuleStateIndicator = ({ state }) => {
  const states = {
    running: (
      <span className="text-blue-600 text-xl flex flex-col items-center">
        <ImSpinner className="animate-spin" />
        <span className="text-xs mt-1">Ejecutando...</span>
      </span>
    ),
    successful: (
      <span className="text-green-600 text-xl flex flex-col items-center">
        <FcCheckmark />
        <span className="text-xs mt-1">Completado</span>
      </span>
    ),
    failed: (
      <span className="text-red-600 text-xl flex flex-col items-center">
        <FiX />
        <span className="text-xs mt-1">Fallido</span>
      </span>
    ),
    canceled: (
      <span className="text-yellow-600 text-xl flex flex-col items-center">
        <BsExclamationCircle />
        <span className="text-xs mt-1">Cancelado</span>
      </span>
    ),
    pending: (
      <span className="text-yellow-600 text-xl flex flex-col items-center">
        <CgSandClock className="animate-spin" />
        <span className="text-xs mt-1">Pendiente...</span>
      </span>
    ),
  };

  return states[state] || null;
};

export default SegmentoTabContent;