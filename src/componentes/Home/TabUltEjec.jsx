import React, { useEffect, useState } from 'react';
import { FiEye, FiTerminal } from "react-icons/fi";
import { Link } from 'react-router-dom';
import LogModal from './LogModal';
import { useAuth } from '../../providers/AuthProvider';
import Loading from '../Base/Loading';

const API_URL = process.env.REACT_APP_API_URL;

const TabUltEje = ({ selectedCategory }) => {
  const [ejecucion, setEjecuciones] = useState([]);
  const [loading, setLoading] = useState(true);
  const [logs, setLogs] = useState({});
  const [visibleLogId, setVisibleLogId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { fetchWithAuth, user } = useAuth();

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  useEffect(() => {
    const fetchEjecuciones = async () => {
      try {
        setLoading(true);
        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
          setEjecuciones([]);
          return;
        }

        const idUsuario = usuarioAutenticado.id;
        const response = await fetchWithAuth(`${API_URL}/ejecuciones/ultimas/${idUsuario}`);

        const contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          const data = await response.json();
          const sortedData = Array.isArray(data) ? [...data].sort((a, b) => new Date(b.fechaHoraLanzamiento) - new Date(a.fechaHoraLanzamiento)) : [];
          setEjecuciones(sortedData);
        } else {
          throw new Error("El formato de la respuesta no es JSON");
        }
      } catch (error) {
        console.error("Error al obtener las últimas ejecuciones:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchEjecuciones();
  }, [fetchWithAuth, user?.name]);

  const formatDateTime = (dateString) => {
    if (!dateString || isNaN(new Date(dateString).getTime())) {
      return 'N/A';
    }

    const date = new Date(dateString);
    const optionsDate = { day: '2-digit', month: '2-digit', year: '2-digit' };
    const optionsTime = { hour: '2-digit', minute: '2-digit', hour12: true };

    return (
      <>
        {date.toLocaleDateString('es-CL', optionsDate)}
        <span style={{ display: 'block' }}>
          {date.toLocaleTimeString('es-CL', optionsTime)}
        </span>
      </>
    );
  };

  const renderEjecuciones = selectedCategory
    ? ejecucion.filter((modulo) => modulo.categoria === selectedCategory)
    : ejecucion;

  const openLogModal = (jobId, modulo) => {
    setVisibleLogId(jobId);
    setIsModalOpen(true);
  };

  const closeLogModal = () => {
    setIsModalOpen(false);
    setVisibleLogId(null);
  };

  if (loading) {
    return <Loading message="Cargando últimas ejecuciones..." />;
  }

  if (!Array.isArray(renderEjecuciones) || renderEjecuciones.length === 0) {
    return (
      <p className="text-center py-4 text-sm text-black">
        No existen ejecuciones del usuario.
      </p>
    );
  }

  return (
    <div className="border-sky-950 bg-sky-950 border rounded-lg mb-8 pb-2">
      <div className="mx-auto sm:px-6 lg:px-0 rounded-lg">
        <table className="min-w-full divide-y divide-gray-300">
          <thead>
            <tr>
              <th className="hidden px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Usuario</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Fecha y Hora<br />de lanzamiento</th>
              <th className="hidden px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Duración</th>
              <th className="hidden px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Sistema Operativo</th>
              <th className="hidden px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Categoría</th>
              <th className="hidden px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Estado</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Módulo</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Detalle</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-white sm:table-cell">Log</th>
            </tr>
          </thead>

          <tbody>
            {renderEjecuciones.map((modulo, index) => (
              <React.Fragment key={`${modulo.modulo}-${index}`}>
                <tr className={`${index % 2 === 1 ? 'bg-white' : 'bg-gray-200'}`}>
                  <td className="hidden px-3 py-4 text-xs text-center sm:table-cell">{user?.name || 'N/A'}</td>
                  <td className="px-3 py-4 text-xs font-bold text-black text-center sm:table-cell">{formatDateTime(modulo.fechaHoraLanzamiento)}</td>
                  <td className="hidden px-3 py-4 text-xs font-bold text-black text-center sm:table-cell">{modulo.duracion || 'N/A'}</td>
                  <td className="hidden px-3 py-4 text-xs text-center sm:table-cell">{modulo.sistema || 'N/A'}</td>
                  <td className="hidden px-3 py-4 text-xs text-center sm:table-cell">{modulo.categoria || 'N/A'}</td>
                  <td className="hidden px-3 py-4 text-xs text-center sm:table-cell">{modulo.estado || 'N/A'}</td>
                  <td className="px-3 py-4 text-xs text-center sm:table-cell">{modulo.modulo || 'N/A'}</td>
                  <td className="py-4 pl-3 pr-4 text-center text-xs sm:pr-0">
                    <Link to={`/detallemodulo/${modulo.moduloId}`}>
                      <div className="flex flex-col items-center justify-center">
                        <div className="flex items-center justify-center rounded-full border-2 border-sky-950 h-8 w-8 mb-1">
                          <FiEye className="text-sky-950 h-4 w-4" />
                        </div>
                        <span className="text-xs">Detalle</span>
                      </div>
                    </Link>
                  </td>
                  <td className="py-4 pl-3 pr-4 text-center text-sm sm:pr-0">
                    <div
                      className="flex flex-col items-center justify-center cursor-pointer"
                      onClick={() => openLogModal(modulo.jobId, modulo)}
                    >
                      <div className="flex items-center justify-center rounded-full border-2 border-sky-950 h-8 w-8 mb-1">
                        <FiTerminal className="text-sky-950 h-4 w-4" />
                      </div>
                      <span className="text-xs">Ver Log</span>
                    </div>
                  </td>

                </tr>
              </React.Fragment>
            ))}
          </tbody>
        </table>
      </div>

      {/* Modal de Log */}
      <LogModal
        isOpen={isModalOpen}
        onClose={closeLogModal}
        jobId={visibleLogId}
        modulo={renderEjecuciones.find((mod) => mod.jobId === visibleLogId)}
      />
    </div>
  );
};

export default TabUltEje;
