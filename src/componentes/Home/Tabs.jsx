import React, { useRef } from 'react';

const Tabs = ({ activeTab, setActiveTab, tabs }) => {
  const tabsContainerRef = useRef(null);

  const handleScroll = (e) => {
    const target = e.currentTarget;
    if (target && tabsContainerRef.current) {
      tabsContainerRef.current.scrollLeft = target.scrollLeft;
    }
  };

  return (
    <div className="px-2 sm:px-6 lg:px-8 mx-6">
      <div className="flex w-full">
        <div className="w-full">
          <nav className="flex -mb-px w-full relative">
            <div
              className="flex w-full"
              ref={tabsContainerRef}
              onScroll={handleScroll}
            >
              {tabs.map((tab, index) => (
                <button
                  key={tab.id}
                  onClick={() => setActiveTab(tab.id)}
                  className={`flex-1 py-1 px-2 sm:px-6 focus:outline-none
                            font-medium text-sm sm:text-base text-center border border-sky-950
                            ${index === 0 ? 'rounded-tl-xl' : ''}
                            ${index === tabs.length - 1 ? 'rounded-tr-xl' : ''}
                            ${activeTab === tab.id
                      ? 'bg-gray-800 text-white'
                      : 'hover:bg-gray-100'
                    }
                `}
                >
                  {tab.label}
                </button>
              ))}
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default Tabs;