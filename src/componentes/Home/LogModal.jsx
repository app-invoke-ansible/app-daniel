import React, { useEffect, useState } from 'react';
import { FiX } from 'react-icons/fi';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

const LogModal = ({ isOpen, onClose, jobId, modulo }) => {
    const [logEntries, setLogEntries] = useState(null);
    const [loading, setLoading] = useState(true);
    const [displayedText, setDisplayedText] = useState('');
    const { fetchWithAuth } = useAuth();

    // Obtener los logs cuando el modal está abierto
    useEffect(() => {
        if (isOpen) {
            setLoading(true);
            setDisplayedText('');
            setLogEntries(null); // Reinicia los logs al cambiar el jobId o al abrir el modal

            if (jobId) {
                const fetchLogs = async () => {
                    try {
                        const response = await fetchWithAuth(`${API_URL}/ejecuciones/${jobId}/log`);
                        const contentType = response.headers.get('content-type');

                        if (contentType && contentType.includes('application/json')) {
                            const jsonResponse = await response.json();
                            console.error('La API devolvió JSON inesperado:', jsonResponse);
                            setLogEntries('Formato inesperado de logs.');
                        } else {
                            const text = await response.text();
                            setLogEntries(text);
                        }
                    } catch (error) {
                        console.error(`Error al obtener los logs para jobId ${jobId}:`, error.message);
                        setLogEntries(null);
                    } finally {
                        setLoading(false);
                    }
                };

                fetchLogs();
            } else {
                setLoading(false);
            }
        }
    }, [isOpen, jobId, fetchWithAuth]);

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (isOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [isOpen]);

    // Efecto para simular la escritura del log
    useEffect(() => {
        if (!loading && logEntries) {
            setDisplayedText(''); // Resetea el texto mostrado
            const words = logEntries.split(' ');
            let currentWordIndex = 0;

            const typeEffect = setInterval(() => {
                setDisplayedText((prev) => prev + words[currentWordIndex] + ' ');
                currentWordIndex += 1;
                if (currentWordIndex === words.length) {
                    clearInterval(typeEffect);
                }
            }, 10);

            return () => clearInterval(typeEffect);
        }
    }, [loading, logEntries]);



    return (
        isOpen && jobId ? (
            <div className="fixed inset-0 bg-black bg-opacity-50 flex items-center justify-center z-50">
                <div className="bg-white p-8 rounded-lg shadow-lg max-w-6xl sm:w-full sm:h-5/6 h-full w-full relative flex flex-col">
                    <h2 className="text-lg text-sky-950 font-bold mb-4">Logs del módulo {modulo?.modulo || 'N/A'}</h2>
                    <div className="font-mono text-sm overflow-y-auto bg-black text-green-400 p-4 rounded-lg flex-1 max-h-[calc(100%-60px)]">
                        {loading ? (
                            <p className="animate-pulse text-gray-500">Cargando...</p>
                        ) : logEntries ? (
                            <pre className="whitespace-pre-wrap">{displayedText}</pre>
                        ) : (
                            <p className="text-gray-400">No existe información del log.</p>
                        )}
                    </div>

                    <button
                        className="absolute top-4 right-4 p-1 text-gray-600 hover:text-white hover:bg-red-600 transition hover:scale-105"
                        onClick={onClose}
                        aria-label="Cerrar"
                    >
                        <FiX className="h-6 w-6" />
                    </button>

                </div>
            </div>
        ) : null
    );
};

export default LogModal;
