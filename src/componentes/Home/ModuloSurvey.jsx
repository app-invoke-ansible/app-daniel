import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';

const ModuloSurvey = ({ currentModuloId, correoUsuario, onSurveySubmit, usuarioId}) => {
    const [moduloData, setModuloData] = useState(null);
    const [surveyData, setSurveyData] = useState(null);
    const [responses, setResponses] = useState({});
    const [errors, setErrors] = useState({});

    const { fetchWithAuth } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const fetchModulosSurvey = async () => {
            try {
                // Obtener datos del módulo
                const moduloResponse = await fetchWithAuth(`${API_URL}/modulos/${currentModuloId}`);
                const moduloData = await moduloResponse.json();
                setModuloData(moduloData);

                // Obtener estado del correo del usuario en el módulo
                const modulosUsuarioResponse = await fetchWithAuth(`${API_URL}/usuario/${usuarioId}/modulos`);
                const modulosUsuarioData = await modulosUsuarioResponse.json();

                const moduloSeleccionado = modulosUsuarioData.modulos.find(
                    (modulo) => modulo.id === currentModuloId
                );

                if (!moduloSeleccionado) {
                    console.error("Módulo no encontrado");
                    return;
                }

                const estadoCorreo = moduloSeleccionado.estadoCorreo || 0;
                //console.log("Estado Correo del módulo seleccionado:", estadoCorreo);

                // Si el módulo tiene una encuesta asociada, obtenerla
                if (moduloData.survey) {
                    const surveyResponse = await fetchWithAuth(`${API_URL}/modulos/survey/${currentModuloId}`);
                    const surveyData = await surveyResponse.json();

                    const initialResponses = {};
                    const filteredSpec = surveyData.spec.filter(question => {
                        if (question.variable === 'h_correo') {
                            initialResponses[question.variable] = estadoCorreo;
                            //console.log("Estado Correo en ModuloSurvey:", estadoCorreo);
                            return false;
                        }
                        if (question.variable === 'e_correo') {
                            initialResponses[question.variable] = correoUsuario || '';
                            //console.log("Correo en ModuloSurvey:", correoUsuario);
                            return false;
                        }
                        initialResponses[question.variable] = question.default || '';
                        return true;
                    });

                    setSurveyData({ ...surveyData, spec: filteredSpec });
                    setResponses(initialResponses);
                }
            } catch (error) {
                console.error('Error fetching survey data:', error);
            }
        };

        fetchModulosSurvey();
    }, [currentModuloId, fetchWithAuth, API_URL, correoUsuario]);

    useEffect(() => {
        // Solo se ejecuta cuando `responses` y `surveyData` han sido establecidos
        if (responses && surveyData) {
            onSurveySubmit({ extra_vars: responses });
        }
    }, [responses, surveyData, onSurveySubmit]);

    const handleChange = (variable, value) => {
        const question = surveyData.spec.find(q => q.variable === variable);
        let parsedValue = value;

        if (question && ['integer', 'float'].includes(question.type)) {
            parsedValue = question.type === 'integer' ? parseInt(value, 10) : parseFloat(value);
            if (isNaN(parsedValue)) parsedValue = '';
        }

        const updatedResponses = { ...responses, [variable]: parsedValue };
        setResponses(updatedResponses);
        setErrors(prev => ({ ...prev, [variable]: !value }));
    };

    if (!moduloData || !surveyData) {
        return null;
    }

    return (
        <div>
            {moduloData.survey && (
                <div className="bg-gray-200 px-4 pb-2 rounded-lg">
                    {surveyData.spec.map((question, index) => (
                        <div key={index}>
                            <div className="flex items-center p-2">
                                <label className="font-semibold text-sm text-gray-900">
                                    {question.question_name}
                                </label>
                                <p className="text-xs text-gray-500 italic mx-2">
                                    {question.question_description}
                                </p>
                            </div>

                            {question.type === 'text' && (
                                <input
                                    type="text"
                                    className={`w-full border ${errors[question.variable] ? 'border-red-500' : 'border-gray-300'} rounded px-2 py-1`}
                                    placeholder={question.question_description}
                                    name={question.variable}
                                    value={responses[question.variable] || ''}
                                    onChange={e => handleChange(question.variable, e.target.value)}
                                />
                            )}

                            {question.type === 'multiplechoice' && (
                                <select
                                    className={`w-full p-2 border ${errors[question.variable] ? 'border-red-500' : 'border-gray-300'} rounded-md`}
                                    name={question.variable}
                                    value={responses[question.variable] || ''}
                                    onChange={e => handleChange(question.variable, e.target.value)}
                                >
                                    <option value="">Seleccione una opción</option>
                                    {question.choices.map((choice, idx) => (
                                        <option key={idx} value={choice}>{choice}</option>
                                    ))}
                                </select>
                            )}

                            {question.type === 'multiselect' && (
                                <div className="space-y-2">
                                    {question.choices.map((choice, idx) => (
                                        <div key={idx} className="flex items-center">
                                            <input
                                                type="checkbox"
                                                className="mr-2"
                                                checked={responses[question.variable]?.includes(choice) || false}
                                                onChange={e => {
                                                    const currentChoices = responses[question.variable] || [];
                                                    const updatedChoices = e.target.checked
                                                        ? [...currentChoices, choice]
                                                        : currentChoices.filter(c => c !== choice);
                                                    handleChange(question.variable, updatedChoices);
                                                }}
                                            />
                                            <label>{choice}</label>
                                        </div>
                                    ))}
                                </div>
                            )}

                            {['integer', 'float'].includes(question.type) && (
                                <input
                                    type="number"
                                    className={`w-full border ${errors[question.variable] ? 'border-red-500' : 'border-gray-300'} rounded px-2 py-1`}
                                    name={question.variable}
                                    min={question.min}
                                    max={question.max}
                                    step={question.type === 'float' ? '0.1' : '1'}
                                    value={responses[question.variable] || ''}
                                    onChange={e => handleChange(question.variable, e.target.value)}
                                    onKeyDown={e => {
                                        const invalidKeys = ['e', 'E', '+', '-'];
                                        if (invalidKeys.includes(e.key) || (question.type === 'integer' && e.key === '.')) {
                                            e.preventDefault();
                                        }
                                    }}
                                    onInput={e => {
                                        if (question.type === 'integer') {
                                            e.target.value = e.target.value.replace(/[^0-9]/g, '');
                                        } else if (question.type === 'float') {
                                            e.target.value = e.target.value.replace(/[^0-9.,]/g, '');
                                        }
                                    }}
                                />
                            )}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default ModuloSurvey;
