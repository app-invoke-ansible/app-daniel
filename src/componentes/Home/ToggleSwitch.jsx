import React from 'react';
import { GiPowerButton } from 'react-icons/gi';

// Componente de interruptor
const ToggleSwitch = ({ isOn, setIsOn }) => {
    const handleToggle = () => {
        const confirmToggle = window.confirm(
            `¿Estás seguro de que quieres ${isOn ? 'desactivar' : 'activar'} el módulo?`
        );

        if (confirmToggle) {
            setIsOn(!isOn);
        }
    };

    return (
        <div
            className={`relative w-28 h-8 bg-white border rounded-full cursor-pointer ${isOn ? 'border-sky-950' : 'border-gray-500'}`}
            onClick={handleToggle}
        >
            <div className="flex items-center justify-center h-full">
                <div
                    className={`absolute w-8 h-8 rounded-full flex items-center justify-center transition-all duration-300 ${isOn ? 'right-0 bg-sky-950' : 'left-0 bg-gray-500'}`}
                >
                    <GiPowerButton className="text-white" />
                </div>
            </div>
        </div>
    );
};

export default ToggleSwitch;

