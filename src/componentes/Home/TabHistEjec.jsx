import React, { useEffect, useState } from 'react';
import { FiEye } from "react-icons/fi";
import { useAuth } from '../../providers/AuthProvider';
import { LuLoaderCircle } from "react-icons/lu";

const API_URL = process.env.REACT_APP_API_URL;

const TabHistEjec = ({ id }) => {
    const [ejecuciones, setEjecuciones] = useState([]);
    const [loading, setLoading] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const [selectedHosts, setSelectedHosts] = useState([]);
    const { fetchWithAuth, user } = useAuth();

    // Bloquear/Restaurar scroll en el fondo del modal
    useEffect(() => {
        if (showModal) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

        return () => {
            document.body.style.overflow = "auto";
        };
    }, [showModal]);


    // Cargar Ejecuciones
    useEffect(() => {
        const fetchEjecuciones = async () => {
            setLoading(true);

            try {
                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    setEjecuciones([]);
                    return;
                }

                const idUsuario = usuarioAutenticado.id;

                const response = await fetchWithAuth(`${API_URL}/ejecuciones/modulo/${id}/${idUsuario}`);

                const contentType = response.headers.get("content-type");
                if (contentType && contentType.includes("application/json")) {
                    const data = await response.json();

                    // Ordenar las ejecuciones por fecha de inicio (descendente)
                    const sortedEjecuciones = data.sort(
                        (a, b) => new Date(b.inicio) - new Date(a.inicio)
                    );
                    setEjecuciones(sortedEjecuciones);
                } else {
                    throw new Error("El formato de la respuesta no es JSON");
                }
            } catch (error) {
                console.error("Error al obtener las ejecuciones:", error.message);
                setEjecuciones([]);
            } finally {
                setLoading(false);
            }
        };

        fetchEjecuciones();
    }, [fetchWithAuth, id, user?.name]);

    // Formatear fecha y hora para mostrar en la tabla
    const formatDateTime = (dateString) => {
        if (!dateString || isNaN(new Date(dateString).getTime())) {
            return 'N/A';
        }

        const date = new Date(dateString);
        const optionsDate = { day: '2-digit', month: '2-digit', year: '2-digit' };
        const optionsTime = { hour: '2-digit', minute: '2-digit', hour12: true };

        return (
            <>
                {date.toLocaleDateString('es-ES', optionsDate)}
                <span style={{ display: 'block' }}>
                    {date.toLocaleTimeString('es-ES', optionsTime)}
                </span>
            </>
        );
    };

    // Mostrar modal con los hosts seleccionados
    const handleShowHosts = (hosts) => {
        setSelectedHosts(hosts || []);
        setShowModal(true);
    };

    // Cerrar modal
    const closeModal = () => {
        setShowModal(false);
        setSelectedHosts([]);
    };

    // Indicador carga
    if (loading) {
        return <div className="flex justify-center items-center h-auto">
            <div className="text-center">
                <LuLoaderCircle className="animate-spin mx-auto text-sky-600 text-4xl mb-4" />
                <p className="text-sky-600 text-md font-medium">Cargando...</p>
            </div>
        </div>;
    }
    return (
        <div className="border-sky-950 border rounded-lg ml-0">
            <div className="mx-auto sm:px-6 lg:px-0 rounded-lg overflow-x-auto">
                <table className="min-w-full divide-y divide-gray-300">
                    <thead>
                        <tr>
                            <th scope="col" className="hidden px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">
                                Aprobación
                            </th>
                            <th scope="col" className="px-3 py-3.5 text-center font-bold text-xs text-gray-900 sm:table-cell">
                                Inicio
                            </th>
                            <th scope="col" className="px-3 py-3.5 text-center font-bold text-xs text-gray-900 sm:table-cell">
                                Término
                            </th>
                            <th scope="col" className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">
                                Inventario
                            </th>
                            <th scope="col" className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">
                                Categoría
                            </th>
                            <th scope="col" className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">
                                Hosts
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {ejecuciones.length > 0 ? (
                            ejecuciones.map((ejecucion, index) => (
                                <tr key={ejecucion.idEjecucion} className={index % 2 === 1 ? 'bg-white' : 'bg-gray-200'}>
                                    <td className="hidden px-3 py-4 text-xs text-gray-900 text-center sm:table-cell">
                                        admin
                                    </td>
                                    <td className="px-3 py-4 text-xs font-bold text-black text-center sm:table-cell">
                                        {formatDateTime(ejecucion.inicio)}
                                    </td>
                                    <td className="px-3 py-4 text-xs font-bold text-black text-center sm:table-cell">
                                        {formatDateTime(ejecucion.termino)}
                                    </td>
                                    <td className="px-3 py-4 text-xs text-black text-center sm:table-cell">
                                        {ejecucion.inventario || "N/A"}
                                    </td>
                                    <td className="px-3 py-4 text-xs text-black text-center sm:table-cell">
                                        {ejecucion.ambito || "N/A"}
                                    </td>
                                    <td className="py-4 pl-3 pr-4 text-center text-sm sm:pr-0">
                                        <div
                                            className="flex flex-col items-center justify-center cursor-pointer"
                                            onClick={() => handleShowHosts(ejecucion.hosts)}
                                        >
                                            <div className="flex items-center justify-center rounded-full border-2 border-sky-950 h-8 w-8 mb-1">
                                                <FiEye className="text-sky-950 h-4 w-4" />
                                            </div>
                                            <span className="text-xs">Ver Host</span>
                                        </div>
                                    </td>
                                </tr>
                            ))
                        ) : (
                            <tr>
                                <td colSpan="6" className="text-center py-4 text-gray-500">
                                    No hay ejecuciones disponibles para este módulo.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>

            {/* Modal para mostrar los hosts */}
            {showModal && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white p-6 rounded shadow-lg w-96">
                        <h1 className="text-base text-sky-900 font-bold mb-2">Host de la ejecución</h1>
                        {selectedHosts.length > 0 ? (
                            <ul className="list-disc text-sm pl-5">
                                {selectedHosts.map((host, i) => (
                                    <li key={i}>{host}</li>
                                ))}
                            </ul>
                        ) : (
                            <p>No hay hosts disponibles.</p>
                        )}
                        <div className="flex justify-end mt-4">
                            <button
                                onClick={closeModal}
                                className="bg-red-500 text-white px-4 py-2 rounded text-sm hover:bg-red-700"
                            >
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default TabHistEjec;
