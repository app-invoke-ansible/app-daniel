import React, { useMemo } from 'react';
import GeneralTabContent from './GeneralTabContent';
import HistorialTabContent from './HistorialTabContent.jsx';
import SegmentoTabContent from './SegmentoTabContent';
import CircuitoTabContent from './CircuitoTabContent';
import CategoriaImagen from '../Base/CategoriaImagen.jsx';

const TabContent = ({ activeTab, modulo, isOn, setIsOn }) => {

  return (
    <div className="mx-auto px-4 sm:px-6 lg:px-8">
      <div className="bg-white border border-sky-950 rounded-3xl p-4 sm:p-8 mt-0 mb-8">
        <div className="flex items-center mb-2">
          <div className="flex items-center">
            <div className="w-10 h-10 object-cover rounded mr-4">
              <CategoriaImagen categoriaName={modulo.categoria.name} />
              </div>
            <div>
              <h1 className="text-xl text-sky-900 font-bold">{modulo.name}</h1>
              <span className="text-gray-800 text-xs font-semibold block">{isOn ? 'Activo' : 'Inactivo'}</span>
            </div>
          </div>
        </div>
        <div className={`h-1 ${isOn ? 'bg-green-500' : 'bg-red-500'} w-1/4`} id="status-bar"></div>
        {activeTab === 'General' && <GeneralTabContent modulo={modulo} />}
        {activeTab === 'Historial de ejecución' && <HistorialTabContent id={modulo.id} />}
        {activeTab === 'Segmento' && <SegmentoTabContent id={modulo.id} />}
        {activeTab === 'Circuito' && <CircuitoTabContent />}
      </div>
    </div>
  );
};

export default TabContent;