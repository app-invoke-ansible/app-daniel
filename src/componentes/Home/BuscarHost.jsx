import React, { useState } from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';

const BuscarHost = ({ hosts, onFilterHosts }) => {
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearchChange = (e) => {
        const term = e.target.value.toLowerCase();
        setSearchTerm(term);
        const filteredHosts = hosts.filter(host =>
            host.hostname.toLowerCase().includes(term)
        );
        onFilterHosts(filteredHosts); // Pasamos los hosts filtrados al componente padre
    };

    return (
        <div className="flex mb-4 items-center justify-between w-full">
            <div className="relative w-80">
                <input
                    type="search"
                    name="search"
                    id="searchHost"
                    className="pl-2 pr-10 block w-full border-b-2 border-sky-950 py-1.5 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
                    placeholder="Buscar host"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />
            </div>
        </div>
    );
};

export default BuscarHost;
