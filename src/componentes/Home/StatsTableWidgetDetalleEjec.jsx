import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend } from 'chart.js';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

// Registrar componentes de Chart.js
ChartJS.register(LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend);

const LineChart = ({ modulo }) => {
  // Estado para manejar la carga y los datos del gráfico
  const [loading, setLoading] = useState(true);
  const [chartData, setChartData] = useState({ labels: [], data: [] });

  // Extraer fetchWithAuth desde el contexto
  const { fetchWithAuth } = useAuth();

  useEffect(() => {
    const fetchEjecuciones = async () => {
        if (modulo) {
            setLoading(true);
            try {
                // Realizar la solicitud autenticada para obtener los módulos
                const response = await fetchWithAuth(`${API_URL}/modulos`);

                // Validar el tipo de contenido de la respuesta
                const contentType = response.headers.get("content-type");
                if (contentType && contentType.includes("application/json")) {
                    const data = await response.json();

                    // Filtrar el módulo específico por ID
                    const filteredModules = data.filter((mod) => mod.id === modulo.id);

                    // Obtener las ejecuciones del módulo filtrado
                    const ejecuciones = filteredModules.flatMap((mod) => mod.ejecuciones || []);

                    // Procesar los datos de ejecuciones para el gráfico
                    const labels = ejecuciones
                        .map((ejec) => new Date(ejec.startTime).toLocaleDateString()).sort(); // Fechas de ejecución
                    const executionTimes = ejecuciones.map((ejec) => {
                        if (ejec.startTime && ejec.endTime) {
                            const startTime = new Date(ejec.startTime).getTime();
                            const endTime = new Date(ejec.endTime).getTime();
                            return (endTime - startTime) / 1000; // Tiempo en segundos
                        }
                        return 0; // Si no hay endTime, duración como 0
                    });

                    setChartData({ labels, data: executionTimes });
                } else {
                    throw new Error("El formato de la respuesta no es JSON");
                }
            } catch (error) {
                console.error("Error al obtener datos de la API:", error.message);
                setChartData({ labels: [], data: [] }); // Vaciar el gráfico en caso de error
            } finally {
                setLoading(false); //
            }
        } else {
            setLoading(false);
        }
    };

    fetchEjecuciones();
}, [modulo, fetchWithAuth]);

  // Configuración de los datos del gráfico
  const data = {
    labels: chartData.labels,
    datasets: [
      {
        label: 'Tiempo de Ejecución (segundos)',
        data: chartData.data,
        fill: true,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        pointBackgroundColor: 'rgba(75, 192, 192, 1)',
        tension: 0.3, // Curvatura de las líneas
      },
    ],
  };

  // Configuración de las opciones del gráfico
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        position: 'top',
        labels: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
      title: {
        display: true,
        text: 'Tiempo de Ejecución por Fecha',
        color: '#0c4a6e',
        font: {
          size: 18,
          weight: 'bold',
        },
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: 'Fecha de Ejecución',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
      y: {
        title: {
          display: true,
          text: 'Tiempo (segundos)',
          color: '#0c4a6e',
          font: {
            size: 14,
            weight: 'bold',
          },
        },
        ticks: {
          color: '#0c4a6e',
          font: {
            size: 12,
          },
        },
      },
    },
  };

  // Mostrar un mensaje de carga si los datos aún no están listos
  if (loading) {
    return (
      <div className="text-center">
        <p className="text-base font-bold text-sky-950">Cargando datos...</p>
      </div>
    );
  }

  // Renderizar el gráfico
  return (
    <div className="w-full h-96">
      <Line data={data} options={options} />
    </div>
  );
};

export default LineChart;
