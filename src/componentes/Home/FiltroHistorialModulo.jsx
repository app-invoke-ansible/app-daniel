import React from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';

export default function FiltroHistorialModulo({ setSearchTerm }) {
    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
    };

    return (
        <div className="hidden sm:flex mx-auto sm:px-6 lg:px-8 overflow-x-auto items-center justify-between w-full">
            <div className="relative w-80">
                <input
                    type="search"
                    name="search"
                    id="FiltroHistorialModulo"
                    className="pl-2 pr-10 block w-full border-b-2 border-sky-950 py-1 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
                    placeholder="Buscar ejecución"
                    onChange="#"
                />
                <MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />
            </div>
        </div>
    );
}