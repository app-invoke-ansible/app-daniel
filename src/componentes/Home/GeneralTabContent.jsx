import React, { useMemo } from 'react';
import EstadisticasDetalleEjec from '../../componentes/Home/EstadisticasDetalleEjec';
import StatsTableWidgetDetalleEjec from '../../componentes/Home/StatsTableWidgetDetalleEjec';
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';

const GeneralTabContent = ({ modulo }) => {
  // Formatear descripción del módulo
  const formattedDescription = modulo.description
    ? modulo.description.split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/).map((sentence, index) => (
      <React.Fragment key={index}>
        {sentence.trim()}
        <br />
        {index < modulo.description.split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/).length - 1 && <br />}
      </React.Fragment>
    ))
    : null;

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mt-4">
      {/* Columna izquierda: Detalles del módulo */}
      <div className="bg-white">
        <h1 className="text-base text-sky-900 font-bold">Descripción General</h1>
        <p className="text-gray-800 text-sm mb-4">
          {formattedDescription}
        </p>

        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 p-4">
          {/* Primer rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <div className="w-16">
              <CategoriaImagen categoriaName={modulo.categoria.name} />
            </div>
            <span className="text-sky-900 text-xs font-semibold mt-2 text-center">
              {modulo.categoria.name}
            </span>
          </div>

          {/* Segundo rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <div className="w-16">
              <SistemaImagen sistemaName={modulo.sistema} />
            </div>
          </div>

          {/* Tercer rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">Nivel de impacto</span>
            <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
              <div className="text-sky-500 text-xs font-bold bg-white px-1">
                {modulo.nivelImpacto}
              </div>
            </div>
          </div>

          {/* Cuarto rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">Total Ejecuciones</span>
            <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
              <div className="text-sky-500 text-xs font-bold bg-white px-1">
                {modulo.ejecuciones?.length || 0}
              </div>
            </div>
          </div>

          {/* Quinto rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">
              Última vez<br />ejecutado
            </span>
            {modulo.lastJobRun ? (
              <div className="text-sky-500 text-xs font-bold bg-white px-1 text-center">
                {new Date(modulo.lastJobRun).toLocaleDateString()} <br />
                {new Date(modulo.lastJobRun).toLocaleTimeString()}
              </div>
            ) : (
              <div className="text-sky-500 text-xs font-bold bg-white px-1">Nunca</div>
            )}
          </div>

          {/* Sexto rectángulo */}
          <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center p-4">
            <span className="text-sky-900 text-xs font-semibold text-center">Estado</span>
            <div className="text-sky-500 text-xs font-bold bg-white px-1">
              {modulo.status}
            </div>
          </div>
        </div>
      </div>

      {/* Columna derecha: Estadísticas y tabla */}
      <div className="bg-white w-full rounded-3xl border border-sky-950 p-4 sm:p-6">
        <div className="px-2 sm:px-6">
          <EstadisticasDetalleEjec modulo={modulo} />
        </div>
        <StatsTableWidgetDetalleEjec modulo={modulo} />
      </div>
    </div>
  );
};

export default GeneralTabContent;
