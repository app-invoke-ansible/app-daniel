import React from 'react';

const CardCategorias = ({ categoria, isSelected, onSelect }) => {
    return (
        <li
            key={categoria.name}
            className="border border-sky-950 col-span-1 flex flex-col rounded-3xl bg-white text-center p-2"
        >
            <div className="flex flex-1 flex-col p-2 ">
                <img className="mx-auto h-14 w-14 flex-shrink-0" src={categoria.imageUrl} alt="" />
                <h3 className="mt-2 text-xs font-semibold text-sky-950">{categoria.name}</h3>
            </div>
            <div>
                <div className="flex items-center justify-center">
                    <div className="flex items-center justify-center w-4">
                        <button
                            onClick={onSelect}
                            className={`relative inline-flex items-center justify-center py-1 px-3 text-xs
                                ${isSelected
                                    ? 'bg-white text-sky-950 border-sky-950'
                                    : 'bg-sky-950 text-white border-transparent hover:bg-gray-600'} 
                                rounded-lg border`}
                            type="button"
                        >
                            Seleccionar
                        </button>


                    </div>
                </div>
            </div>
        </li>
    );
};

export default CardCategorias;
