import React from 'react';
import { useEffect, useState } from 'react';
// import { useKeycloak } from '@react-keycloak/web'; // Import Keycloak

const TabHistorialUltimasEjec = ({ moduloId }) => {
    const [ejecuciones, setEjecuciones] = useState([]);
    const [moduloSeleccionado, setModuloSeleccionado] = useState('');
    // const { keycloak, initialized } = useKeycloak(); // Keycloak hook

    useEffect(() => {
        // if (initialized && keycloak.authenticated) { // Check Keycloak authentication
        //     const token = keycloak.token; // Get Keycloak token
        //     console.log("Token:", token); 

        if (moduloId) {
            setModuloSeleccionado(moduloId);

            fetch(`https://api-v-1-2-git-tdemt.apps.bddfgx0h.eastus.aroapp.io/ejecuciones/modulo/${moduloId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${token}`, // Set authorization header with Keycloak token
                },
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    setEjecuciones(data);
                })
                .catch(error => {
                    console.error('Error al hacer la solicitud:', error);
                });
        }
        // }
    }, [moduloId]); // Removed Keycloak dependencies

    const convertToCSV = (data) => {
        const header = "Aprovación,Fecha,Ambito,Inicio,Estado\n";
        const rows = data.map(ejec => 
            `${ejec.nombreModulo},${ejec.fechaEjecucion},${ejec.iDetalles.dominioMaquina},${ejec.fechaEjecucion},${ejec.estadoEjecucion}`
        ).join("\n");
        return header + rows;
    };

    const downloadCSV = () => {
        const csvData = convertToCSV(ejecuciones);
        const blob = new Blob([csvData], { type: 'text/csv' });
        const url = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = 'historial_ejecuciones.csv';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        URL.revokeObjectURL(url);
    };

    return (
            <div className='mt-8'>
            <div className="relative w-80">
            <input
            type="search"
            name="search"
            id="searchModulo"
            className="pl-2 pr-10 block w-full border-b-2 border-gray-300 py-1.5 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
            placeholder="Buscar modulo o descripción"
            /*onChange={handleSearchChange}*/
            />
            {/*<MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />*/}
          </div>
            <div className='mt-10'>
            <h1>Ultimas Ejecuciones:</h1>
            </div> 
            <div className="">
                <div className="">
                    <div className="-mx-4 mt-2 sm:-mx-0">
                        <table className="min-w-full divide-y divide-gray-300 rounded-lg border border-sky-950 ">
                            <thead>
                                <tr>
                                    <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">
                                        Aprobación
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell"
                                    >
                                        Fecha
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Ambito
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Inicio
                                    </th>
                                    <th
                                        scope="col"
                                        className="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell"
                                    >
                                        Termino
                                    </th>
                                    <th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-0">
                                        <span className="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="divide-y divide-gray-600 bg-white">
                                {ejecuciones?.map((ejec) => (
                                    <tr key={ejec.idEjecucion}>
                                        <td className="w-full max-w-0 py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:w-auto sm:max-w-none sm:pl-0">
                                            {ejec.nombreModulo}
                                            <dl className="font-normal lg:hidden">
                                                <dt className="sr-only">Fecha y hora</dt>
                                                <dd className="mt-1 truncate text-gray-700">{ejec.fechaEjecucion}</dd>
                                                <dt className="sr-only sm:hidden">{ejec.iDetalles.dominioMaquina}</dt>
                                                <dd className="mt-1 truncate text-gray-500 sm:hidden">{ejec.fechaEjecucion}</dd>
                                                <dd className="mt-1 truncate text-gray-700">{ejec.estadoEjecucion}</dd>
                                            </dl>
                                        </td>

                                        <td className="hidden px-3 py-4 text-sm text-gray-900 font-bold lg:table-cell">{ejec.fechaEjecucion}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 lg:table-cell">{ejec.iDetalles.dominioMaquina}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">{ejec.fechaEjecucion}</td>
                                        <td className="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">{ejec.estadoEjecucion}</td>
                                        <td className="py-4 pl-3 pr-4 text-center text-sm font-medium sm:pr-0">
                                            {/* Aquí puedes añadir botones de acción si es necesario */}
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="flex justify-between">
                    <button onClick={downloadCSV} className="mt-4 px-4 py-2 bg-sky-950 text-white rounded-md">
                        Descargar Historial
                    </button>
                    <button className="mt-4 px-4 py-2 bg-sky-950 text-white rounded-md">
                         Guardar
                    </button>
                    </div>                   
                </div>
            </div>
        </div>
    );
}

export default TabHistorialUltimasEjec;
