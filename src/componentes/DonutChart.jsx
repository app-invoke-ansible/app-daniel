import React, { useState, useEffect } from 'react';
import { Chart } from 'react-google-charts';
import { useAuth } from '../providers/AuthProvider';

const DonutChart = ({ modulos }) => {
  const { fetchWithAuth, user } = useAuth();
  const [loading, setLoading] = useState(true);
  const [chartData, setChartData] = useState([['Módulo', 'Costo Total']]);
  const [showChart, setShowChart] = useState(false);

  // Obtener el mes actual
  const fechaActual = new Date();
  const mes = String(fechaActual.getMonth() + 1).padStart(2, '0');
  const anio = fechaActual.getFullYear();
  const mesActual = `${anio}-${mes}`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);

        const modulosConCosto = modulos.map(modulo => {
          const costoTotalModulo = modulo.ejecuciones && modulo.ejecuciones.length > 0 ? modulo.ejecuciones
            .filter(ejecucion => {
              if (!ejecucion.startTime) return false;
              const fechaEjecucion = new Date(ejecucion.startTime);
              const mesEjecucion = String(fechaEjecucion.getMonth() + 1).padStart(2, '0');
              const anioEjecucion = fechaEjecucion.getFullYear();
              const mesAnioEjecucion = `${anioEjecucion}-${mesEjecucion}`;
              return mesAnioEjecucion === mesActual;
            })
            .reduce((total, ejecucion) => total + (modulo.precio), 0)
            : modulo.precio
          return {
            ...modulo,
            costoTotal: costoTotalModulo
          }
        });


        const top5Modulos = modulosConCosto
          .sort((a, b) => b.costoTotal - a.costoTotal)
          .slice(0, 5);

        const chartDataFormatted = [
          ['Módulo', 'Costo Total'],
          ...top5Modulos.map(modulo => [
            `${modulo.name}\n$${Number(modulo.costoTotal)}`,
            Number(modulo.costoTotal)
          ]),
        ];


        setChartData(chartDataFormatted);
        setShowChart(true);
      } catch (error) {
        console.error("Error al obtener datos de la API:", error.message);
        setChartData([['Módulo', 'Costo Total']]);
        setShowChart(false);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [modulos, fetchWithAuth, user?.name, mesActual]);

  const defaultColors = ['#5DADE2', '#5499C7', '#48C9B0', '#45B39D', '#3498DB']; // Tonos de azul y celeste

  const options = {
    pieHole: 0.4,
    is3D: false,
    titleTextStyle: {
      fontSize: 18,
      bold: true,
      color: '#0c4a6e',
    },
    chartArea: {
      width: '80%',
      height: '80%',
    },
    slices: chartData.slice(1).reduce((acc, _, index) => {
        acc[index] = { color: defaultColors[index % defaultColors.length] };
        return acc;
    }, {}),
    tooltip: {
      trigger: 'selection',
      textStyle: {
        fontSize: 16,
        fontName: 'Arial',
        color: '#0c4a6e',
      },
      showColorCode: false,
    },
  };

  return (
    <div className="w-full">
      {loading ? (
        <div className="text-center">
          <p className="text-base font-bold text-sky-950 animate-pulse">Cargando datos...</p>
        </div>
      ) : (
        showChart && chartData.length > 1 ? (
          <Chart className="w-full h-full pr-6"
            chartType="PieChart"
            data={chartData}
            options={options}
          />
        ) : (
          <div className="text-center">
            <p className="text-base font-bold text-sky-950 ">No hay datos disponibles para el gráfico.</p>
          </div>
        )
      )}
    </div>
  );
};

export default DonutChart;