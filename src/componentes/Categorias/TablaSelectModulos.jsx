import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FiEye } from "react-icons/fi";
import { useAuth } from '../../providers/AuthProvider';
import { useLocation } from 'react-router-dom';
import { IoPlay } from "react-icons/io5";
import SegmentoTabContent from '../Home/SegmentoTabContent';
import Loading from '../Base/Loading';

const TablaSelectModulos = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentModuloId, setCurrentModuloId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [modulosFiltrados, setModulosFiltrados] = useState([]);
  const [tipoUsuario, setTipoUsuario] = useState(null);
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState();

  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const categoria = params.get('categoria');
  const sistema = params.get('sistema');

  const { fetchWithAuth, user } = useAuth();

  // Variables de entorno
  const API_URL = process.env.REACT_APP_API_URL;

  const fetchModulos = async () => {
    try {

      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await responseUsuarios.json();

      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (usuarioAutenticado) {
        setTipoUsuario(usuarioAutenticado.tipoUsuario);
        setModulosFiltrados([]);
      } else {
        console.warn('Usuario autenticado no encontrado en la API');
      }

      const idUsuario = usuarioAutenticado.id;

      const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
      const dataUsuario = await responseUsuario.json();

      const modulosFiltradosUsuario = dataUsuario.modulos.filter(modulo =>
        modulo.categoria.toLowerCase() === categoria.toLowerCase() &&
        modulo.sistema.toLowerCase() === sistema.toLowerCase()
      );

      const modulosIds = modulosFiltradosUsuario.map(modulo => modulo.id);

      const responseModulos = await fetchWithAuth(`${API_URL}/modulos`);
      const dataModulos = await responseModulos.json();

      const modulosFinales = dataModulos.filter(modulo =>
        modulosIds.includes(modulo.id) &&
        modulo.categoria.name.toLowerCase() === categoria.toLowerCase() &&
        modulo.sistema.toLowerCase() === sistema.toLowerCase()
      );

      const modulosConCalculos = modulosFinales.map((modulo) => {
        const ejecuciones = modulo.ejecuciones || [];

        const tiempos = ejecuciones.map((ejec) => {
          const start = new Date(ejec.startTime);
          const end = new Date(ejec.endTime);
          return (end - start) / 1000;
        });

        const tiempoPromedio = tiempos.length > 0
          ? tiempos.reduce((a, b) => a + b, 0) / tiempos.length
          : 0;

        const exitosas = ejecuciones.filter(
          (ejec) => ejec.status === "success"
        ).length;

        const nivelExito = ejecuciones.length > 0
          ? ((exitosas / ejecuciones.length) * 100).toFixed(2)
          : "0.00";

        const factorImpacto = ejecuciones.length * tiempoPromedio;

        return {
          ...modulo,
          tiempoPromedio,
          nivelExito,
          factorImpacto,
        };
      });

      setModulosFiltrados(modulosConCalculos);

    } catch (error) {
      console.error('Error al obtener los módulos:', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchModulos();
  }, [fetchWithAuth, API_URL, categoria, sistema, user?.name]);


  const handlePlay = (module) => {
    setCurrentModuloId(module);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isModalOpen]);

  return (
    <div>
      <div className="border-sky-950 border rounded-lg my-4">
        <div className="mx-auto rounded-lg overflow-x-auto">
          <table className="min-w-full divide-y divide-gray-300">
            <thead>
              <tr>
                <th className="px-3 py-3.5 text-center font-bold text-xs text-gray-900 sm:table-cell">Nombre</th>
                <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Servicio</th>
                <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Tiempo de Ejecución</th>
                <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Factor de Impacto</th>
                <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Niveles de Éxito</th>
                {loadingTipoUsuario ? (
                  <div></div>
                ) : (
                  <>
                    {tipoUsuario !== 'visualizador' && (
                      <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Acciones</th>
                    )}
                  </>
                )}
                <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900 sm:table-cell">Detalle</th>
              </tr>
            </thead>
            <tbody>
              {loading ? (
                <tr>
                  <td colSpan="8" className="text-center">
                    <Loading message="Cargando datos..." />
                  </td>
                </tr>
              ) : modulosFiltrados.length > 0 ? (
                modulosFiltrados.map((modulo, index) => (
                  <tr
                    className={`${index % 2 === 1 ? 'bg-white' : 'bg-gray-200'} items-center`}
                    key={modulo.id}
                  >
                    <td className="px-3 py-3 text-xs font-bold text-sky-950 text-center sm:table-cell">
                      <Link to="#" className="font-bold text-sky-950 hover:underline">
                        {modulo.name}
                      </Link>
                    </td>
                    <td className="px-3 py-3 text-xs text-center sm:table-cell">
                      {modulo.categoria.name}
                    </td>
                    <td className="px-3 py-3 text-xs text-center sm:table-cell">
                      {modulo.tiempoPromedio ? modulo.tiempoPromedio.toFixed(2) : 'N/A'} s
                    </td>
                    <td className="px-3 py-3 text-xs text-center sm:table-cell">
                      {modulo.factorImpacto ? modulo.factorImpacto.toFixed(2) : 'N/A'}
                    </td>
                    <td className="px-3 py-3 text-xs text-center sm:table-cell">
                      {modulo.nivelExito ? modulo.nivelExito : 'N/A'}%
                    </td>
                    {loadingTipoUsuario ? (
                      <div></div>
                    ) : (
                      <>
                        {tipoUsuario !== 'visualizador' && (
                          <td className="px-3 py-3 text-xs text-center sm:table-cell">
                            <div className="flex justify-center items-center mt-2">
                              <button
                                onClick={() => handlePlay(modulo)}
                                className="text-green-600 hover:text-green-900 flex flex-col items-center"
                              >
                                <IoPlay className="h-5 w-5" />
                                <span className="text-green-600 hover:text-green-900 text-xs">
                                  Ejecutar
                                </span>
                              </button>
                            </div>
                          </td>
                        )}
                      </>
                    )}
                    <td className="px-3 py-3 text-xs text-center sm:table-cell">
                      <Link to={`/detallemodulo/${modulo.id}`}>
                        <div className="flex flex-col items-center justify-center">
                          <div className="flex items-center justify-center rounded-full border-2 border-sky-950 h-8 w-8 mb-1">
                            <FiEye className="text-sky-950 h-4 w-4" />
                          </div>
                          <span className="text-xs">Detalle</span>
                        </div>
                      </Link>
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan="8" className="px-3 py-4 text-xs text-center">
                    No hay módulos disponibles
                  </td>
                </tr>
              )}
            </tbody>

          </table>
        </div>
      </div>

      {/* Modal */}
      {isModalOpen && currentModuloId && (
        <div className="fixed inset-0 bg-gray-800 bg-opacity-50 flex justify-center items-center">
          <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-5xl">
            <h2 className="text-lg font-semibold mb-4">Ejecutar módulo {currentModuloId.name}</h2>
            <SegmentoTabContent id={currentModuloId.id} />
            <div className="flex justify-end">
              <button
                onClick={handleModalClose}
                className="text-white bg-red-500 hover:bg-red-600 px-4 py-1 rounded-lg shadow-md font-semibold transition-colors duration-300"
              >
                Cerrar
              </button>
            </div>

          </div>
        </div>
      )}

    </div>
  );
};

export default TablaSelectModulos;
