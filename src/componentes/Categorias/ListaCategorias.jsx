import React, { useEffect, useState } from 'react';
import IconSeguridad from '../../img/iconos/Iconos_Seguridad.png';
import IconMonitoreo from '../../img/iconos/Iconos_Monitoreo.png';
import IconInfraestructura from '../../img/iconos/Iconos_Infraestructura .png';
import IconBD from '../../img/iconos/Iconos_BaseDeDatos.png';
import IconRedes from '../../img/iconos/Iconos_Redes.png';
import IconDeployments from '../../img/iconos/Iconos_Deployments.png';
import IconOtros from '../../img/iconos/Iconos_Otros.png';
import { useAuth } from '../../providers/AuthProvider';
import { Link } from 'react-router-dom';
import Loading from '../Base/Loading';

// Mapeo de íconos para las categorías
const iconMap = {
    Seguridad: IconSeguridad,
    Monitoreo: IconMonitoreo,
    Infraestructura: IconInfraestructura,
    'Base Datos': IconBD,
    Redes: IconRedes,
    Deployments: IconDeployments,
    Transversalidad: IconOtros,
    Aprovisionamiento: IconOtros,
};

export default function ListaCategorias({ searchTerm }) {
    const [categorias, setCategorias] = useState([]);
    const [categoriaMap, setCategoriaMap] = useState({});
    const [loading, setLoading] = useState(true);
    const { fetchWithAuth, user } = useAuth();
    const API_URL = process.env.REACT_APP_API_URL;

    // Fetch categorías y mapearlas a un objeto
    useEffect(() => {
        const fetchCategorias = async () => {
            try {
                const response = await fetchWithAuth(`${API_URL}/categorias`);
                const categoriasData = await response.json();

                const categoriasMapped = categoriasData.reduce((acc, categoria) => {
                    acc[categoria.name] = categoria.id;
                    return acc;
                }, {});

                setCategoriaMap(categoriasMapped);
            } catch (error) {
                console.error("Error al obtener las categorías:", error);
                setCategoriaMap({});
            } finally {
                setLoading(false); // Asegurarse de detener el estado de carga
            }
        };

        fetchCategorias();
    }, [fetchWithAuth, API_URL]);

    // Fetch módulos del usuario y mapear categorías
    useEffect(() => {
        const fetchModulos = async () => {
            try {
                setLoading(true); // Iniciar estado de carga antes de la solicitud

                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find(
                    (usuario) => usuario.username === user?.name
                );

                if (!usuarioAutenticado) {
                    setCategorias([]);
                    return;
                }

                const idUsuario = usuarioAutenticado.id;
                const response = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
                const data = await response.json();

                const categoriasSet = new Set();
                data.modulos.forEach((modulo) => {
                    categoriasSet.add(modulo.categoria);
                });

                const mappedCategorias = Array.from(categoriasSet).map((categoria) => ({
                    name: categoria,
                    imageSrc: iconMap[categoria] || IconOtros,
                    href: categoriaMap[categoria] ? `/sistemas?categoria=${categoria}` : '#',
                }));

                setCategorias(mappedCategorias);
            } catch (error) {
                console.error("Error al obtener los módulos del usuario:", error);
                setCategorias([]);
            } finally {
                setLoading(false); // Detener el estado de carga tras finalizar
            }
        };

        fetchModulos();
    }, [fetchWithAuth, API_URL, categoriaMap, user?.name]);

    if (loading) {
        return <Loading message="Cargando Categorías..." />;
    }

    if (!Array.isArray(categorias) || categorias.length === 0) {
        return (
            <div>
                <p className="text-center py-4 text-sm text-black">
                    El usuario no tiene categorías asignadas.
                </p>
            </div>
        );
    }


    return (
        <div className="bg-white">
            <div className="mx-auto max-w-2xl px-4 m-10 sm:px- lg:max-w-6xl lg:px-10">
                <div className="grid grid-cols-1 gap-x-6 gap-y-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 xl:gap-x-8">
                    {categorias.map((categoria) => (
                        <div key={categoria.name} className="group">
                            <div className="flex items-center border border-sky-950 rounded-3xl overflow-hidden p-3 h-32">
                                <div className="flex-shrink-0 w-24 mx-auto">
                                    <img
                                        src={categoria.imageSrc}
                                        alt={categoria.name}
                                        className="h-24 w-24 object-contain"
                                    />
                                </div>
                                <div className="w-40 flex flex-col items-center justify-center text-center">
                                    <h3 className="text-l text-sky-900 font-bold">{categoria.name}</h3>
                                    <div className="mt-2">
                                        <Link to={categoria.href}>
                                            <button className="block bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white">
                                                Seleccionar
                                            </button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}
