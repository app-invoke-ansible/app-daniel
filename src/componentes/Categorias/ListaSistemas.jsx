import React, { useEffect, useState } from 'react';
import { useLocation, Link } from 'react-router-dom';  // Importa Link desde react-router-dom
import { useAuth } from '../../providers/AuthProvider';
import Loading from '../Base/Loading';

// Mapeo de íconos por sistema
import IconLinux from '../../img/ambito/linux.png';
import IconAws from '../../img/ambito/aws.png';
import IconGCP from '../../img/ambito/gcp.png';
import IconAzure from '../../img/ambito/azure.png';
import IconCisco from '../../img/ambito/cisco.png';
import IconOtros from '../../img/iconos/Iconos_Otros.png';

const iconMap = {
    Aws: { image: IconAws, name: 'Amazon Web Services' },
    Azure: { image: IconAzure, name: 'Microsoft Azure' },
    Cisco: { image: IconCisco, name: 'Cisco Systems' },
    Gcp: { image: IconGCP, name: 'Google Cloud Platform' },
    Linux: { image: IconLinux, name: 'Linux Operating System' },
    Onpremise: { image: IconGCP, name: 'On-Premise Infrastructure' },
    Transversalidad: { image: IconOtros, name: 'Transversalidad' },
};

function ListaSistemas() {
    const [sistemas, setSistemas] = useState([]);
    const { fetchWithAuth, user } = useAuth();
    const [loading, setLoading] = useState(true);
    const API_URL = process.env.REACT_APP_API_URL;
    const location = useLocation();

    // Obtener la categoría desde la URL
    const categoria = new URLSearchParams(location.search).get('categoria');

    useEffect(() => {
        const fetchModulos = async () => {
            try {
                const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                const usuarios = await responseUsuarios.json();

                const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                if (!usuarioAutenticado) {
                    console.error("Usuario autenticado no encontrado en la API.");
                    setData({ labels: [], datasets: [] });
                    return;
                }

                const idUsuario = usuarioAutenticado.id;

                const response = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
                const contentType = response.headers.get("content-type");

                if (contentType && contentType.includes("application/json")) {
                    const data = await response.json();

                    const modulosFiltrados = data.modulos.filter((modulo) =>
                        modulo.categoria.toLowerCase() === categoria.toLowerCase()
                    );

                    const sistemasSet = new Set();
                    modulosFiltrados.forEach((modulo) => {
                        sistemasSet.add(modulo.sistema);
                    });

                    setSistemas(Array.from(sistemasSet));
                } else {
                    throw new Error("El formato de la respuesta no es JSON");
                }
            } catch (error) {
                console.error("Error al obtener los módulos:", error.message);
            } finally {
                setLoading(false);
            }
        };

        fetchModulos();

    }, [fetchWithAuth, API_URL, categoria, user?.name]);

    if (loading) {
        return <Loading message="Cargando sistemas disponibles..." />;
    }

    if (sistemas.length === 0) {
        return (
            <div className="bg-white">
                <p className="text-center py-4 text-sm text-black">No hay sistemas para mostrar en esta categoría</p>
            </div>
        );
    }

    return (
        <div className="bg-white">
            <div className="mx-auto max-w-2xl px-4 m-10 sm:px- lg:max-w-6xl lg:px-10">
                <div className="grid grid-cols-1 gap-x-6 gap-y-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 xl:gap-x-8">
                    {sistemas.map((sistema) => (
                        <div key={sistema} className="group">
                            <div className="flex items-center border border-sky-950 rounded-3xl overflow-hidden p-3 h-32">
                                <div className="flex-shrink-0 w-24 mx-auto">
                                    <img
                                        src={iconMap[sistema]?.image}
                                        alt={iconMap[sistema]?.name}
                                        className="h-16 w-16 object-contain mb-2"
                                    />
                                </div>
                                <div className="w-40 flex flex-col items-center justify-center text-center">
                                    <h3 className="text-base text-sky-900 font-semibold">{sistema}</h3>
                                    <div className="mt-2">
                                        <Link to={`/selectmodulos?categoria=${categoria}&sistema=${sistema}`}>
                                            <button
                                                className="block bg-sky-950 hover:bg-gray-600 rounded-2xl border border-transparent py-1 px-6 text-xs font-semibold text-white"
                                            >
                                                Seleccionar
                                            </button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default ListaSistemas;
