import React, { useEffect, useState } from 'react';
import axios from 'axios';
// import { useKeycloak } from '@react-keycloak/web'; // Import Keycloak

const TabInventarioMaquinas = ({ moduloId }) => {
    const [inventarios, setInventarios] = useState([]);
    const [detalleInventario, setDetalleInventario] = useState(null);
    const [moduloSeleccionado, setModuloSeleccionado] = useState('');
    // const { keycloak, initialized } = useKeycloak(); // Keycloak hook

    // CARGAR LOS INVENTARIOS CUANDO LA ID DEL MODULO CAMBIA
    useEffect(() => {
        // if (initialized && keycloak.authenticated) { // Check Keycloak authentication
        //     const token = keycloak.token; // Get Keycloak token
        //     console.log("Token:", token); // Asegúrense de que el token se está obteniendo correctamente
        if (moduloId) {
            setModuloSeleccionado(moduloId);
            fetchInventariosPorModulo(moduloId); // Fetch inventories by module
        }
        // }
    }, [moduloId]); // Removed Keycloak dependencies

    // CARGAR INVENTARIOS POR MODULO
    const fetchInventariosPorModulo = async (moduloId) => {
        // console.log("Token:", token); // Asegúrense de que el token se está 
        try {
            const response = await axios.get(`https://api-v-1-2-git-tdemt.apps.bddfgx0h.eastus.aroapp.io/inventarios/modulo/${moduloId}`, {
                headers: {
                    // 'Authorization': `Bearer ${token}`, // Set authorization header with Keycloak token
                },
            });
            setInventarios(response.data);
        } catch (error) {
            console.error('Error al cargar los inventarios por módulo:', error);
        }
    };

    // DESPLIEGUE DE INVENTARIOS
    const handleInventarioSeleccionado = async (e) => {
        // if (initialized && keycloak.authenticated) { // Check Keycloak authentication
        //     const token = keycloak.token; // Get Keycloak token
        //     console.log("Token:", token); // Asegúrense de que el token se está 
        const inventarioId = e.target.value;
        try {
            const response = await axios.get(`https://api-v-1-2-git-tdemt.apps.cluster-gjswj.dynamic.redhatworkshops.io/inventarios/${inventarioId}/detalles`, {
                headers: {
                    // 'Authorization': `Bearer ${token}`, // Set authorization header with Keycloak token
                },
            });
            setDetalleInventario(response.data);
        } catch (error) {
            console.error('Error al obtener detalles del inventario:', error);
        }
    };

    return (
        <div className="container mx-auto">
            <div className="my-4">
                <h1 className="text-2xl font-bold mb-2">Inventarios del Módulo</h1>
                <select
                    className="border border-gray-300 rounded px-3 py-1"
                    onChange={handleInventarioSeleccionado}
                    disabled={!moduloSeleccionado}
                >
                    <option value="">Selecciona un inventario</option>
                    {inventarios.map(inventario => (
                        <option key={inventario.idInventario} value={inventario.idInventario}>
                            {inventario.nombreInventario}
                        </option>
                    ))}
                </select>
            </div>

            {detalleInventario && (
                <div>
                    <h2 className="text-lg font-semibold mt-4 mb-2">Detalles del Inventario</h2>
                    <table className="min-w-full divide-y divide-gray-300">
                        <thead className="bg-gray-100">
                            <tr>
                                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">host</th>
                                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Detalles</th>
                                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Número de Máquina</th>
                                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Dominio de Máquina</th>
                                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Host de Máquina</th>
                            </tr>
                        </thead>
                        <tbody className="bg-white divide-y divide-gray-200">
                            {detalleInventario.map(detalle => (
                                <tr key={detalle.idDetalle}>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{detalle.idDetalle}</td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{detalle.detalles}</td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{detalle.numeroMaquina}</td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{detalle.dominioMaquina}</td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">{detalle.hostMaquina}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )}
        </div>
    );
};

export default TabInventarioMaquinas;
