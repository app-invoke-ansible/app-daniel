import React from 'react';
import { BsArrowReturnLeft } from 'react-icons/bs';

const Return = () => {
  const handleBack = () => {
    window.history.back();
  };

  return (
    <header>
      <button
        onClick={handleBack}
        className="flex items-center justify-center w-7 h-7 rounded-full bg-white border border-sky-950"
      >
        <BsArrowReturnLeft className="text-sky-950 w-4 h-4" />
      </button>
    </header>
  );
};

export default Return;

