import React from 'react';
import logoFaceOps from '../../img/logoBlanco-FaceOps.png';
import logoInvoke from '../../img/logoBlanco-invoke.png';
import { FaFacebook, FaLinkedin } from "react-icons/fa";
import { SiGmail } from "react-icons/si";
import { Link } from 'react-router-dom';

//const navigation = {
//    solutions: [
//        { name: 'Marketing', href: '#' },
//        { name: 'Analytics', href: '#' },
//        { name: 'Commerce', href: '#' },
//        { name: 'Insights', href: '#' },
//    ],
//    support: [
//        { name: 'Pricing', href: '#' },
//        { name: 'Documentation', href: '#' },
//        { name: 'Guides', href: '#' },
//        { name: 'API Status', href: '#' },
//    ],
//    company: [
//        { name: 'About', href: '#' },
//        { name: 'Blog', href: '#' },
//        { name: 'Jobs', href: '#' },
//        { name: 'Press', href: '#' },
//        { name: 'Partners', href: '#' },
//    ],
//    legal: [
//        { name: 'Claim', href: '#' },
//        { name: 'Privacy', href: '#' },
//        { name: 'Terms', href: '#' },
//    ],
//
//    social: [
//        {
//            name: 'Facebook',
//            href: '#',
//            icon: (props) => (
//                <FaFacebook className="h-8 w-8" />
//            ),
//        },
//        {
//            name: 'Linkedin',
//            href: '#',
//            icon: (props) => (
//                <FaLinkedin className="h-8 w-8" />
//            ),
//        },
//        {
//            name: 'Gmail',
//            href: '#',
//            icon: (props) => (
//                <SiGmail className="h-8 w-8" />
//            ),
//        },
//
//    ],
//}

export default function Footer() {
    return (
        <footer className="bg-gray-900 text-gray-300">
            <div className="mx-auto max-w-7xl px-6 pb-4 pt-8 sm:pt-12 lg:px-8 lg:pt-8">
                <div className="xl:grid xl:grid-cols-2 xl:gap-8">
                    <div className="flex flex-col items-center justify-center h-full space-y-4">
                        <img
                            className="h-12"
                            src={logoInvoke}
                            alt="Company name"
                        />
                        {/*<div className="flex space-x-6">
                            {navigation.social.map((item) => (
                                <a key={item.name} href={item.href} className="text-gray-500 hover:text-gray-400">
                                    <span className="sr-only">{item.name}</span>
                                    <item.icon className="h-6 w-6" aria-hidden="true" />
                                </a>
                            ))}
                        </div>*/}
                    </div>

                    <div className="flex flex-col items-center justify-center h-full space-y-4">
                        <img
                            className="h-12"
                            src={logoFaceOps}
                            alt="Company name"
                        />
                        {/*<div className="flex space-x-6">
                            {navigation.social.map((item) => (
                                <a key={item.name} href={item.href} className="text-gray-500 hover:text-gray-400">
                                    <span className="sr-only">{item.name}</span>
                                    <item.icon className="h-6 w-6" aria-hidden="true" />
                                </a>
                            ))}
                        </div>*/}
                    </div>

                    {/*<div className="mt-8 grid grid-cols-2 gap-4 xl:col-span-2 xl:mt-0">
                        <div className="md:grid md:grid-cols-2 md:gap-8">
                            <div>
                                <h3 className="text-sm font-semibold leading-6 text-white">Solutions</h3>
                                <ul className="mt-2 space-y-1">
                                    {navigation.solutions.map((item) => (
                                        <li key={item.name}>
                                            <Link to={item.href} className="text-sm leading-6 hover:text-white">
                                                {item.name}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            <div className="mt-10 md:mt-0">
                                <h3 className="text-sm font-semibold leading-6 text-white">Support</h3>
                                <ul className="mt-2 space-y-1">
                                    {navigation.support.map((item) => (
                                        <li key={item.name}>
                                            <Link to={item.href} className="text-sm leading-6 hover:text-white">
                                                {item.name}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                        <div className="md:grid md:grid-cols-2 md:gap-8">
                            <div>
                                <h3 className="text-sm font-semibold leading-6 text-white">Company</h3>
                                <ul className="mt-2 space-y-1">
                                    {navigation.company.map((item) => (
                                        <li key={item.name}>
                                            <Link to={item.href} className="text-sm leading-6 hover:text-white">
                                                {item.name}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div className="mt-10 md:mt-0">
                                <h3 className="text-sm font-semibold leading-6 text-white">Legal</h3>
                                <ul className="mt-2 space-y-1">
                                    {navigation.legal.map((item) => (
                                        <li key={item.name}>
                                            <Link to={item.href} className="text-sm leading-6 hover:text-white">
                                                {item.name}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div> */}
                </div>
                <div className="mt-2 border-t border-gray-700 pt-3 sm:mt-4 lg:mt-6 text-center">
                    <p className="text-sm leading-5">&copy; 2025 Invoke ltda, Inc. All rights reserved.</p>
                </div>
            </div>
        </footer>
    )
}