import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FiMessageSquare } from 'react-icons/fi';
import { GoGear } from 'react-icons/go';
import { LuShoppingCart } from 'react-icons/lu';
import { BsSliders } from 'react-icons/bs';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';
import { useAuth } from '../../providers/AuthProvider';

const SearchForm = () => {
  const [tipoUsuario, setTipoUsuario] = useState(null);
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
  const { fetchWithAuth, isAuthenticated, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;


  // Cargar el tipo de usuario desde localStorage al inicio
  useEffect(() => {
    const storedUserType = localStorage.getItem('userType');
    if (storedUserType) {
      setTipoUsuario(storedUserType);
      setLoadingTipoUsuario(false); // Indica que ya cargó el tipo de usuario
    }
  }, []);


  useEffect(() => {
    const fetchTipoUsuario = async () => {
      try {
        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (usuarioAutenticado) {
          const userType = usuarioAutenticado.tipoUsuario;
          setTipoUsuario(userType);
          localStorage.setItem('userType', userType);
        } else {
          console.warn('Usuario autenticado no encontrado en la API.');
        }
      } catch (error) {
        console.error('Error al cargar los permisos:', error);
      } finally {
        setLoadingTipoUsuario(false);
      }
    };
    if (isAuthenticated && user) {
      fetchTipoUsuario();
    }
  }, [isAuthenticated, user, fetchWithAuth, API_URL]);

  return (
    <div className="flex items-center gap-x-2 lg:gap-x-6  bg-white rounded-lg">
      <form className="relative flex flex-1 max-w-md" action="#" method="GET">
        <label htmlFor="search-field" className="sr-only">
          Buscar
        </label>
        <MagnifyingGlassIcon
          className="pointer-events-none absolute inset-y-0 left-0 h-full w-7 text-gray-400 lg:block hidden"
          aria-hidden="true"
        />
        <input
          id="search-field"
          className="h-full w-full rounded-xl py-3 pl-10 pr-10 text-gray-900 placeholder:text-gray-400 focus:ring-0 focus:outline-none sm:text-sm
            sm:w-auto md:w-80 lg:w-96 lg:flex hidden"
          placeholder="Buscar"
          type="search"
          name="search"
          style={{ maxWidth: '100%' }}
        />
        <button
          type="button"
          className="lg:-m-2.5 lg:p-2.5 text-sky-950 hover:text-gray-500 lg:hidden flex items-center"
        >
          <MagnifyingGlassIcon className="h-6 w-6" aria-hidden="true" />
        </button>
        <button type="button" className="absolute inset-y-0 right-0 items-center pr-3 hidden lg:flex">
          <BsSliders className="h-6 w-6 text-gray-400" aria-hidden="true" />
        </button>
      </form>
      {!loadingTipoUsuario && tipoUsuario !== 'visualizador' && (
        <div className='flex items-center space-x-2'>
          <Link to="/ConfiguracionMenu" className="lg:-m-2.5 lg:p-2.5 text-sky-950 hover:text-gray-500">
            <span className="sr-only">Configuración</span>
            <GoGear className="h-6 w-6" aria-hidden="true" />
          </Link>
          <button type="button" className="lg:-m-2.5 lg:p-2.5 text-sky-950 hover:text-gray-500 lg:flex">
            <span className="sr-only">Notificaciones</span>
            <FiMessageSquare className="h-6 w-6" aria-hidden="true" />
          </button>
          <button type="button" className="lg:-m-2.5 lg:p-2.5 text-sky-950 hover:text-gray-500 lg:flex">
            <span className="sr-only">Carrito de compras</span>
            <LuShoppingCart className="h-6 w-6" aria-hidden="true" />
          </button>
        </div>
      )}
    </div>
  );
};

export default SearchForm;