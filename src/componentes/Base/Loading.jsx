import React from 'react';
import { LuLoaderCircle } from 'react-icons/lu';

/**
 * Componente de carga reutilizable.
 * @param {string} [message="Cargando..."] - Mensaje que se muestra durante la carga.
 */
const Loading = ({ message = "Cargando..." }) => {
    return (
        <div className="flex justify-center items-center my-8">
            <div className="text-center">
                <LuLoaderCircle className="animate-spin mx-auto text-sky-600 text-2xl mb-2" />
                <p className="text-sky-600 text-xs">{message}</p>
            </div>
        </div>
    );
};

export default Loading;