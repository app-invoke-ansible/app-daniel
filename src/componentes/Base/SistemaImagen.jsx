import React from 'react';
import ImgOtros from '../../img/iconos/Iconos_Otros.png';
import IconLinux from '../../img/ambito/linux.png';
import IconAws from '../../img/ambito/aws.png';
import IconGCP from '../../img/ambito/gcp.png';
import IconAzure from '../../img/ambito/azure.png';
import IconCisco from '../../img/ambito/cisco.png';
import IconDellos from '../../img/ambito/dellos.png';
import IconOtros from '../../img/iconos/Iconos_Otros.png';


const sistemas = [
  { name: 'Aws', imageUrl: IconAws },
  { name: 'Azure', imageUrl: IconAzure },
  { name: 'Cisco', imageUrl: IconCisco },
  { name: 'Gcp', imageUrl: IconGCP },
  { name: 'Linux', imageUrl: IconLinux },
  { name: 'Onpremise', imageUrl: IconGCP },
  { name: 'Dellos10', imageUrl: IconDellos },
  { name: 'Transversalidad', imageUrl: ImgOtros },
];

const getImagenPorSistema = (sistemaName) => {
  const sistema = sistemas.find((cat) => cat.name === sistemaName);
  return sistema ? sistema.imageUrl : ImgOtros;
};

const SistemaImagen = ({ sistemaName }) => {
  const imageUrl = getImagenPorSistema(sistemaName);

  return (
    <div className="sistema-imagen">
      <img src={imageUrl} alt={sistemaName} />
    </div>
  );
};

export default SistemaImagen;
