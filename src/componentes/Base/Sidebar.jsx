import React, { Fragment, useEffect, useState, useRef } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import {
    ChartPieIcon, CloudIcon, ComputerDesktopIcon,
    HomeIcon, ShoppingCartIcon, ChartBarIcon,
    XMarkIcon, BanknotesIcon, DocumentTextIcon
} from '@heroicons/react/24/outline';
import { MdOutlineLogin } from "react-icons/md";
import { useNavigate, useLocation } from 'react-router-dom';
import { useAuth } from '../../providers/AuthProvider';
import LogoFaceops from '../../img/LogoFaceopsVertical.png';
import fotoperfil from '../../img/perfil/fotoPerfil.jpg';
import { Link } from 'react-router-dom';

// Variables de entorno
const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_URL;
const REALM_API = process.env.REACT_APP_REALM_API;
const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;

// Navegación con rutas y nombres
const navigation = [
    { id: 1, name: 'Home', href: '/home', icon: HomeIcon, current: false },
    //    { id: 2, name: 'Categorías', href: '/categorias', icon: CloudIcon, current: false },
    { id: 3, name: 'Modulos', href: '/modulos', icon: ChartBarIcon, current: false },
    { id: 4, name: 'Dashboard', href: '/dashboard', icon: ChartPieIcon, current: false },
    { id: 5, name: 'Inventario', href: '/inventario', icon: DocumentTextIcon, current: false },
    { id: 6, name: 'Marketplace', href: '/marketplace', icon: ShoppingCartIcon, current: false },
//    { id: 7, name: 'Facturación', href: '/facturacion', icon: BanknotesIcon, current: false },
    { id: 8, name: 'Tickets', href: '/tickets', icon: ComputerDesktopIcon, current: false },

];

const adminOnlyCards = [7, 8];
const editorOnlyCards = [1, 2, 3, 4, 5, 6];
const visualizadorOnlyCards = [1, 3, 4];


// Información del usuario que aparece en el perfil
const userProfile = {
    name: 'Usuario 1',
    role: 'administrador',
    imageSrc: fotoperfil,
};

function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
}

const Sidebar = ({ sidebarOpen, setSidebarOpen }) => {
    const navigate = useNavigate();
    const location = useLocation();
    const { fetchWithAuth, isAuthenticated, user } = useAuth();
    const [activeNav, setActiveNav] = useState(navigation);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [filteredNav, setFilteredNav] = useState([]); // Estado para la navegación filtrada
    const API_URL = process.env.REACT_APP_API_URL;


    // Función para filtrar la navegación
    const filterNavigation = (userType) => {
        if (!userType) return navigation; // Mostrar todo si aún no se tiene el tipo de usuario

        if (userType === 'admin') {
            return navigation;
        }
        if (userType === 'editor') {
            return navigation.filter(item => editorOnlyCards.includes(item.id));
        }
        if (userType === 'visualizador') {
            return navigation.filter(item => visualizadorOnlyCards.includes(item.id));
        }
        return [];
    };

    // Actualiza la ruta activa
    useEffect(() => {
        const updatedNavigation = activeNav.map((item) => ({
            ...item,
            current: item.href === location.pathname,
        }));
        setActiveNav(updatedNavigation);
    }, [location.pathname]);

    // Cargar el tipo de usuario desde localStorage al inicio
    useEffect(() => {
        const storedUserType = localStorage.getItem('userType');
        if (storedUserType) {
            setTipoUsuario(storedUserType);
            setFilteredNav(filterNavigation(storedUserType));
        }
    }, []);

    // Filtrar el usuario autenticado de la API
    useEffect(() => {
        if (isAuthenticated && user) {
            const fetchUserData = async () => {
                try {
                    const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
                    const usuarios = await responseUsuarios.json();

                    // Encuentra el usuario autenticado
                    const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

                    if (usuarioAutenticado) {
                        const userType = usuarioAutenticado.tipoUsuario;
                        setTipoUsuario(userType); // Guarda el tipoUsuario
                        setFilteredNav(filterNavigation(userType)); // Guarda la navegacion filtrada
                        localStorage.setItem('userType', userType); // Almacenar en localStorage
                    } else {
                        console.warn('Usuario autenticado no encontrado en la API');
                    }
                } catch (error) {
                    console.error('Error al obtener datos de la API:', error);
                }
            };
            fetchUserData();
        }
        // Solo se ejecuta si se modifica isAuthenticated y user
    }, [isAuthenticated, user, fetchWithAuth, API_URL]);


    const handleLogout = () => {
        localStorage.removeItem('userType');

        const redirectUri = encodeURIComponent(window.location.origin);
        const logoutUrl = `${KEYCLOAK_URL}/realms/${REALM_API}/protocol/openid-connect/logout?post_logout_redirect_uri=${redirectUri}&client_id=${CLIENT_ID}`;

        window.location.href = logoutUrl;
    };


    const handleMenuItemClick = (item) => {
        if (item.name === 'Cerrar sesión') {
            handleLogout();
        } else {
            navigate(item.href);
        }
    };

    const handleProfileClick = () => { // Nueva función para manejar el click
        navigate('/ConfiguracionMenu');
    };
    return (
        <>
            {/* Sidebar para dispositivos móviles */}
            <Transition show={sidebarOpen} as={Fragment}>
                <Dialog onClose={setSidebarOpen} className="relative z-50 lg:hidden">
                    <div className="fixed inset-0 flex">
                        <Transition.Child
                            as={Fragment}
                            enter="transition ease-in-out duration-300 transform"
                            enterFrom="-translate-x-full"
                            enterTo="translate-x-0"
                            leave="transition ease-in-out duration-300 transform"
                            leaveFrom="translate-x-0"
                            leaveTo="-translate-x-full"
                        >
                            <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                                <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-gray-900 px-6 pb-2">
                                    <div className="mt-auto flex flex-col items-center">
                                        <img className="h-8 w-auto my-4" src={LogoFaceops} alt="Logo" />
                                    </div>
                                    <div className="flex flex-col items-center mb-4">
                                        <img className="h-10 w-10 rounded-full cursor-pointer" onClick={handleProfileClick} src={user?.imageSrc || fotoperfil} alt="User Profile" />
                                        <h3 className="text-sm font-semibold text-white mt-2 cursor-pointer" onClick={handleProfileClick}>{user?.name || 'Usuario Desconocido'}</h3>
                                        <p className="text-xs text-gray-400">{tipoUsuario}</p>
                                    </div>
                                    <nav className="flex flex-1 flex-col">
                                        <ul role="list" className="-mx-2 flex-1 space-y-1">
                                            {filteredNav.map((item) => (
                                                <li key={item.name}>
                                                    <Link to={item.href}
                                                        onClick={(e) => {
                                                            e.preventDefault();
                                                            handleMenuItemClick(item);
                                                        }}
                                                        className={classNames(
                                                            item.current ? 'bg-gray-800 text-white' : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                                            'group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold'
                                                        )}
                                                    >
                                                        <item.icon className="h-6 w-6 shrink-0" aria-hidden="true" />
                                                        {item.name}
                                                    </Link>
                                                </li>
                                            ))}
                                        </ul>
                                        <div className="mt-auto flex flex-col items-center pb-4">
                                            <button
                                                onClick={() => handleMenuItemClick({ name: 'Cerrar sesión' })}
                                                className="flex items-center text-sm text-gray-400 hover:text-white hover:bg-gray-800 rounded-md p-2"
                                            >
                                                <MdOutlineLogin className="h-6 w-6 mr-2" aria-hidden="true" />
                                                <span className='text-gray-400 font-semibold'>Cerrar Sesión</span>
                                            </button>
                                        </div>
                                    </nav>
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </Dialog>
            </Transition>

            {/* Sidebar para escritorio */}
            <div className="hidden lg:fixed lg:inset-y-0 lg:z-50 lg:flex lg:w-32 lg:flex-col bg-gray-900">
                <div className="flex grow flex-col gap-y-3 overflow-y-auto px-2">
                    <div className="mt-auto flex flex-col items-center">
                        <img className="h-5 w-auto my-3" src={LogoFaceops} alt="Logo" />
                        <img className="h-10 w-10 rounded-full cursor-pointer" onClick={handleProfileClick} src={userProfile.imageSrc} alt="User Profile" />
                        <h3 className="text-xs font-semibold text-white mt-2 cursor-pointer" onClick={handleProfileClick}>{user?.name || 'Usuario Desconocido'}</h3>
                        <p className="text-xs text-gray-400">{tipoUsuario}</p>
                    </div>
                    <nav className="flex flex-1 flex-col">
                        <ul className="flex flex-col items-center space-y-1">
                            {filteredNav.map((item) => (
                                <li key={item.name}>
                                    <Link to={item.href}
                                        onClick={(e) => {
                                            e.preventDefault();
                                            handleMenuItemClick(item);
                                        }}
                                        className={classNames(
                                            item.current ? 'bg-gray-800 text-white' : 'text-gray-400 hover:text-white hover:bg-gray-800',
                                            'group flex flex-col items-center gap-y-1 rounded-md p-2 text-xs leading-5 font-semibold'
                                        )}
                                    >
                                        <item.icon className="h-6 w-6" aria-hidden="true" />
                                        <span className="text-xs">{item.name}</span>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </nav>
                    <div className="mt-auto flex flex-col items-center pb-4">
                        <button
                            onClick={() => handleMenuItemClick({ name: 'Cerrar sesión' })}
                            className="flex items-center text-sm text-gray-400 hover:text-white hover:bg-gray-800 rounded-md p-2"
                        >
                            <MdOutlineLogin className="h-5 w-5" aria-hidden="true" />
                            <span className='text-gray-400 font-semibold'>Cerrar Sesión</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Sidebar;