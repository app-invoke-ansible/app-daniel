import React from 'react';

import ImgSeguridad from '../../img/iconos/Iconos_Seguridad.png';
import ImgMonitoreo from '../../img/iconos/Iconos_Monitoreo.png';
import ImgInfraestructura from '../../img/iconos/Iconos_Infraestructura .png';
import ImgRedes from '../../img/iconos/Iconos_Redes.png';
import ImgBD from '../../img/iconos/Iconos_BaseDeDatos.png';
import ImgDeployments from '../../img/iconos/Iconos_Deployments.png';
import ImgOtros from '../../img/iconos/Iconos_Otros.png';
import ImgInforme from '../../img/iconos/Iconos_Informe.png';

const categorias = [
  { name: 'Seguridad', imageUrl: ImgSeguridad },
  { name: 'Monitoreo', imageUrl: ImgMonitoreo },
  { name: 'Infraestructura', imageUrl: ImgInfraestructura },
  { name: 'Redes', imageUrl: ImgRedes },
  { name: 'Basededatos', imageUrl: ImgBD },
  { name: 'Deployments', imageUrl: ImgDeployments },
  { name: 'Aprovisionamiento', imageUrl: ImgOtros },
  { name: 'Informe', imageUrl: ImgInforme }
];

const getImagenPorCategoria = (categoriaName) => {
  const categoria = categorias.find((cat) => cat.name === categoriaName);
  return categoria ? categoria.imageUrl : ImgOtros;
};

const CategoriaImagen = ({ categoriaName }) => {
  const imageUrl = getImagenPorCategoria(categoriaName);

  return (
    <div className="categoria-imagen">
      <img src={imageUrl} alt={categoriaName} />
    </div>
  );
};

export default CategoriaImagen;
