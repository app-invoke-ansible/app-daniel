import React, { useEffect, useState } from 'react';
import BuscarMarketplace from './BuscarMarketplace';
import Card from './Card';
import Filter from './Filter';
import { useAuth } from '../../providers/AuthProvider';
import ActualizarMarketplace from './ActualizarMarketplace';
import Loading from '../../componentes/Base/Loading'

const CardMarket = () => {
  const [modulos, setModulos] = useState([]);
  const [modulosUsuario, setModulosUsuario] = useState([]);
  const [categoriasSeleccionadas, setCategoriasSeleccionadas] = useState([]);
  const [modulosFiltrados, setModulosFiltrados] = useState([]);
  const [modulosDestacadosFiltrados, setModulosDestacadosFiltrados] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [modalClosed, setModalClosed] = useState(false);

  const { fetchWithAuth, user } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;

  // Fetch módulos y datos del usuario
  const fetchModulos = async () => {
    setLoading(true);
    try {
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await responseUsuarios.json();
      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);
      const idUsuario = usuarioAutenticado.id;

      const responseGlobal = await fetchWithAuth(`${API_URL}/modulos`);
      const dataGlobal = await responseGlobal.json();

      const responseUsuario = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos`);
      const dataUsuario = await responseUsuario.json();
      processFetchedData(dataGlobal, dataUsuario);
    } catch (err) {
      handleFetchError(err.message);
    } finally {
      setLoading(false);
    }
  };

  // Maneja errores en la obtención de datos
  const handleFetchError = (errorMessage) => {
    console.error("Error al obtener los módulos:", errorMessage);
    setError(new Error(errorMessage));
    clearModulesData();
  };

  // Limpia los estados relacionados con los módulos
  const clearModulesData = () => {
    setModulos([]);
    setModulosUsuario([]);
    setModulosFiltrados([]);
    setModulosDestacadosFiltrados([]);
  };

  // Procesa los datos obtenidos de la API
  const processFetchedData = (dataGlobal, dataUsuario) => {
    if (!dataUsuario.modulos || dataUsuario.modulos.length === 0) {
      setModulosUsuario([]);
      setModulosFiltrados([]);
      setModulosDestacadosFiltrados(dataGlobal);
    } else {
      const modulosUsuarioIds = dataUsuario.modulos.map((modulo) => modulo.id);
      const modulosAdquiridos = dataGlobal.filter((modulo) => modulosUsuarioIds.includes(modulo.id));
      const modulosNoAdquiridos = dataGlobal.filter((modulo) => !modulosUsuarioIds.includes(modulo.id));

      setModulosUsuario(modulosAdquiridos);
      setModulosFiltrados(modulosAdquiridos);
      setModulosDestacadosFiltrados(modulosNoAdquiridos);
    }
    setModulos(dataGlobal);
  };

  useEffect(() => {
    fetchModulos();
  }, [fetchWithAuth, API_URL, user?.name]);

  // Filtros de módulos según categorías
  const filteredModulos = categoriasSeleccionadas.length
    ? modulosFiltrados.filter((modulo) => categoriasSeleccionadas.includes(modulo.categoria.name))
    : modulosFiltrados;

  const filteredDestacados = categoriasSeleccionadas.length
    ? modulosDestacadosFiltrados.filter((modulo) => categoriasSeleccionadas.includes(modulo.categoria.name))
    : modulosDestacadosFiltrados;

  const modulosNoAdquiridos = modulos.filter(
    (modulo) => !modulosUsuario.some((moduloUsuario) => moduloUsuario.id === modulo.id)
  );

  return (
    <>
      {loading ? (
        <Loading message="Cargando Marketplace..." />
      ) : (
        <div className="flex flex-col min-h-screen">
          <header className="bg-white flex justify-between items-center p-4">
            <BuscarMarketplace
              modulos={modulosUsuario}
              setModulosFiltrados={setModulosFiltrados}
              modulosDestacados={modulosNoAdquiridos}
              setModulosDestacadosFiltrados={setModulosDestacadosFiltrados}
            />
            <div className="w-3/4 md:w-1/4 lg:w-1/6 px-2 mb-4 md:mb-0 sm:hidden">
              <Filter
                categoriasSeleccionadas={categoriasSeleccionadas}
                setCategoriasSeleccionadas={setCategoriasSeleccionadas}
              />
            </div>
            <ActualizarMarketplace onActualizar={fetchModulos} setLoading={setLoading} />
          </header>

          <div className="flex flex-wrap p-4 ">
            <div className="w-full md:w-1/4 lg:w-1/6 px-2 mb-4 md:mb-0 hidden sm:block">
              <Filter
                categoriasSeleccionadas={categoriasSeleccionadas}
                setCategoriasSeleccionadas={setCategoriasSeleccionadas}
              />
            </div>

            <div className="w-full md:w-3/4 lg:w-5/6 flex flex-col space-y-6">
              {/* Módulos Adquiridos */}
              <section>
                <h1 className="text-base text-sky-900 font-bold mb-4">Módulos adquiridos</h1>
                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                  {filteredModulos.length === 0 ? (
                    <div className="text-center py-4 text-sm text-black">No hay módulos para mostrar.</div>
                  ) : (
                    filteredModulos.map((modulo) => {
                      return (
                        <Card
                          id={modulo.id}
                          key={modulo.id}
                          titlesecond={`Módulo ${modulo.categoria.name}`}
                          title={modulo.name}
                          description="Descripción"
                          contenido={modulo.description}
                          categoriaName={modulo.categoria.name}
                          sistemaName={modulo.sistema}
                          isModulosAdquirido={modulosUsuario.some((mod) => mod.id === modulo.id)}
                          setModalClosed={setModalClosed}
                          fetchModulos={fetchModulos}
                          precio={modulo.precio}
                          nivelImpacto={modulo.nivelImpacto}
                        />
                      );
                    })

                  )}
                </div>
              </section>

              {/* Módulos Destacados */}
              <section>
                <h1 className="text-base text-sky-900 font-bold mb-4">Destacados</h1>
                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                  {filteredDestacados.length === 0 ? (
                    <div className="text-center py-4 text-sm text-black">No hay módulos destacados.</div>
                  ) : (
                    filteredDestacados.map((modulo) => (
                      <Card
                        id={modulo.id}
                        key={modulo.id}
                        titlesecond={`Módulo ${modulo.categoria.name}`}
                        title={modulo.name}
                        description="Descripción"
                        contenido={modulo.description}
                        categoriaName={modulo.categoria.name}
                        sistemaName={modulo.sistema}
                        setModalClosed={setModalClosed}
                        fetchModulos={fetchModulos}
                        precio={modulo.precio}
                        nivelImpacto={modulo.nivelImpacto}
                      />
                    ))
                  )}
                </div>
              </section>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CardMarket;
