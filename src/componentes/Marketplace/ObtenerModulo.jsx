import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import CalculadoraCotizacion from './CalculadoraCotizacion';
import Reviews from './Reviews';
import CategoriaImagen from '../Base/CategoriaImagen';
import SistemaImagen from '../Base/SistemaImagen';
import { motion } from 'framer-motion';

const ObtenerModulo = ({ isOpen, onClose, modulo, isModulosAdquirido, fetchModulos, categoriaName, sistemaName }) => {
  if (!isOpen) return null;

  const { id } = modulo;
  const [activeTab, setActiveTab] = useState('Descripción General');
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
  const [tipoUsuario, setTipoUsuario] = useState(null);
  const [totalEjecuciones, setTotalEjecuciones] = useState(null);

  const { fetchWithAuth, user } = useAuth();

  const API_URL = process.env.REACT_APP_API_URL;

  useEffect(() => {
    const fetchTipoUsuario = async () => {
      try {
        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (usuarioAutenticado) {
          setTipoUsuario(usuarioAutenticado.tipoUsuario);
        } else {
          console.warn('Usuario autenticado no encontrado en la API.');
        }
      } catch (error) {
        console.error('Error al cargar los permisos:', error);
      } finally {
        setLoadingTipoUsuario(false);
      }
    };

    if (user) {
      fetchTipoUsuario();
    }
  }, [user, fetchWithAuth, API_URL]);

  useEffect(() => {
    const getEjecuciones = async () => {
      try {
        const response = await fetchWithAuth(`${API_URL}/modulos/${id}/ejecuciones`);
        if (response.ok) {
          const data = await response.json();
          setTotalEjecuciones(data);
        }
      } catch (error) {
        console.error("Error al obtener las ejecuciones:", error);
      }
    };

    getEjecuciones();
  }, [fetchWithAuth, API_URL, id]);

  const tabs = ['Descripción General', 'Cotización', 'Reviews'];

  const tabVariants = {
    initial: { opacity: 0, x: -20 },
    animate: { opacity: 1, x: 0, transition: { duration: 0.2, ease: "easeInOut" } },
    exit: { opacity: 0, x: 20, transition: { duration: 0.2, ease: "easeInOut" } }
  };
  const formattedContenido = modulo.contenido
    .split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/)
    .map((sentence, index) => (
      <React.Fragment key={index}>
        {sentence.trim()}
        <br />
        {index < modulo.contenido.split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/).length - 1 && (
          <br />
        )}
      </React.Fragment>
    ));

  const renderContent = () => {
    switch (activeTab) {
      case 'Descripción General':
        return (
          <motion.div
            variants={tabVariants}
            initial="initial"
            animate="animate"
            exit="exit"
            className="mx-4"
          >
            {/* Tu contenido de Descripción General aquí */}
            <h2 className="text-base font-bold text-sky-900 px-4">Descripción</h2>
            <div className="text-xs px-4">
              <br />{formattedContenido}
            </div>
            <br />

            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 p-2">
              {/* Primer cuadrado */}
              <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center">
                <div className="w-16">
                  <CategoriaImagen categoriaName={categoriaName} />
                </div>
                <span className="text-sky-900 text-xs font-semibold mt-2">{categoriaName}</span>
              </div>

              {/* Segundo cuadrado */}
              <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center">
                <div className="w-16">
                  <SistemaImagen sistemaName={sistemaName} />
                </div>
              </div>

              {/* Tercer cuadrado */}
              <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center">
                <span className="text-sky-900 text-xs font-semibold">Nivel de impacto</span>
                <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                  <div className="text-sky-500 text-xs font-bold bg-white px-1">
                    {modulo.nivelImpacto}
                  </div>
                </div>
              </div>

              {/* Cuarto cuadrado */}
              <div className="border border-sky-900 rounded-xl w-48 h-28 mx-auto flex flex-col items-center justify-center">
                <span className="text-sky-900 text-xs font-semibold">Total ejecuciones</span>
                <span className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                  <div className="text-sky-500 text-xs font-bold bg-white px-1">
                    {totalEjecuciones}
                  </div>
                </span>
              </div>
            </div>
          </motion.div>
        );
      case 'Cotización':
        return (
          <motion.div
            variants={tabVariants}
            initial="initial"
            animate="animate"
            exit="exit"
          >
            {/* Tu contenido de Cotización aquí */}
            <CalculadoraCotizacion precio={modulo.precio} nivelImpacto={modulo.nivelImpacto} />
          </motion.div>
        );
      case 'Reviews':
        return (
          <motion.div
            variants={tabVariants}
            initial="initial"
            animate="animate"
            exit="exit"
          >
            {/* Tu contenido de Reviews aquí */}
            <Reviews idModulo={modulo.id} isModuloAdquirido={isModulosAdquirido} />
          </motion.div>
        );
      default:
        return null;
    }
  };

  const handleContratar = async () => {
    try {
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await responseUsuarios.json();

      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (!usuarioAutenticado) {
        alert('Usuario autenticado no encontrado en la API.');
        return;
      }

      const idUsuario = usuarioAutenticado.id;

      const response = await fetchWithAuth(`${API_URL}/usuario/${idUsuario}/modulos/${id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        alert(`Módulo ${modulo.name} adquirido exitosamente`);
        await fetchModulos();
        onClose();
      } else {
        alert('Ocurrió un error al intentar adquirir el módulo.');
      }
    } catch (error) {
      alert('Ocurrió un error inesperado al adquirir el módulo. Error:', error);
    }
  };

  return (
    <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center z-50">
      <div className="bg-white rounded-lg p-4 sm:p-6 w-full h-full sm:w-3/4 sm:h-5/6 shadow-lg flex flex-col overflow-hidden">
        <div className="flex items-start mb-4">
          <div className="w-12 h-12 sm:w-16 sm:h-16 mr-4">
            <CategoriaImagen categoriaName={categoriaName} />
          </div>
          <div className="flex flex-grow items-start justify-between">
            <div className="flex flex-col">
              <span className="text-xs text-gray-600">{modulo.categoria}</span>
              <h2 className="text-base sm:text-lg font-bold text-sky-900">{modulo.name}</h2>
            </div>
            <div className="flex flex-col space-y-2 ml-4">
              {loadingTipoUsuario ? null : (tipoUsuario === 'admin' || 'editor') && !isModulosAdquirido && (
                <button
                  className="bg-green-500 text-white text-sm font-semibold py-2 px-4 rounded-3xl hover:bg-green-600 transition hover:scale-105"
                  onClick={handleContratar}
                >
                  CONTRATAR
                </button>
              )}
            </div>
          </div>
        </div>

        <div className="w-full flex-1 overflow-y-auto">
          <div className="flex border-b border-gray-300">
            {tabs.map((tab) => (
              <button
                key={tab}
                onClick={() => setActiveTab(tab)}
                className={`px-4 py-2 text-xs ${activeTab === tab
                  ? 'text-black font-bold border-b-2 border-black'
                  : 'text-gray-500'
                  }`}
              >
                {tab}
              </button>
            ))}
          </div>
          <div className="mt-4 py-4 border border-gray-300 rounded-lg">
            <motion.div key={activeTab} variants={tabVariants} initial="initial" animate="animate" exit="exit" transition={{ mode: "out" }}>
              {renderContent()}
            </motion.div>
          </div>
        </div>

        <div className="flex justify-end mt-4">
          <button
            onClick={onClose}
            className="bg-sky-950 text-white text-sm font-semibold py-2 px-4 rounded-3xl hover:bg-sky-800 hover:scale-105"
          >
            CERRAR
          </button>
        </div>
      </div>
    </div>
  );
};

export default ObtenerModulo;
