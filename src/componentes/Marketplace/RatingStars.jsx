import React, { useState } from 'react';
import { IoStar } from 'react-icons/io5';

const RatingStars = ({ onRatingChange }) => {
  const [rating, setRating] = useState(0);

  const handleStarClick = (index) => {
    setRating(index);
    onRatingChange(index);
  };

  return (
    <div className="flex items-center pb-4 ">
      {Array.from({ length: 5 }, (_, index) => (
        <IoStar
          key={index + 1}
          className={`h-4 w-4 cursor-pointer transition hover:scale-150  ${
            index + 1 <= rating ? 'text-sky-900' : 'text-gray-400'
          }`}
          onClick={() => handleStarClick(index + 1)}
        />
      ))}
      <p className="ml-2 text-xs font-bold text-sky-900">( {rating} )</p>
    </div>
  );
};

export default RatingStars;
