import React, { useState, useEffect } from 'react';
import RatingStars from './RatingStars';
import { IoStar } from 'react-icons/io5';
import { useAuth } from '../../providers/AuthProvider';
import { AiOutlineLike, AiFillLike } from "react-icons/ai";
import { FaRegComment, FaHashtag } from "react-icons/fa";
import { MdNavigateBefore, MdNavigateNext } from "react-icons/md";

const API_URL = process.env.REACT_APP_API_URL;

const Reviews = ({ idModulo, isModuloAdquirido }) => {
  const [selectedRating, setSelectedRating] = useState(0);
  const [reviewText, setReviewText] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [valoracionPromedio, setValoracionPromedio] = useState(0);
  const [comentarios, setComentarios] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [liked, setLiked] = useState(false);
  const [sortOption, setSortOption] = useState('fecha');
  const [currentPage, setCurrentPage] = useState(1);
  const [commentsPerPage] = useState(10);

  const { fetchWithAuth, user } = useAuth();

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (showModal) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [showModal]);

  // Manejar cambios en la calificación
  const handleRatingChange = (rating) => {
    setSelectedRating(rating);
  };

  // Manejar cambios en el textarea
  const handleReviewTextChange = (e) => {
    setReviewText(e.target.value);
  };

  // Función para obtener las reseñas desde la API
  const fetchResenas = async () => {
    try {
      const response = await fetchWithAuth(`${API_URL}/comentarios/modulo/${idModulo}/resenas`);
      const data = await response.json();

      if (response.ok) {
        setValoracionPromedio(data.valoracionPromedio);
        setComentarios(data.comentarios);
      } else {
        console.error('Error al obtener las reseñas:', data);
      }
    } catch (error) {
      console.error('Error al realizar la solicitud GET:', error);
    }
  };

  useEffect(() => {
    fetchResenas(); // Obtener las reseñas cuando el componente se monta
  }, [idModulo]);

  const handleSortChange = (e) => {
    setSortOption(e.target.value);
  };

  // Ordenar los comentarios según la opción seleccionada
  const sortedComentarios = [...comentarios].sort((a, b) => {
    if (sortOption === 'fecha') {
      return new Date(b.fechaCreacion) - new Date(a.fechaCreacion);
    }
    if (sortOption === 'mejor') {
      return b.valoracion - a.valoracion;
    }
    if (sortOption === 'peor') {
      return a.valoracion - b.valoracion;
    }
    return 0;
  });

  // Pagination
  const indexOfLastComment = currentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  const currentComments = sortedComentarios.slice(indexOfFirstComment, indexOfLastComment);

  // Función para manejar el cambio de página
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const calcularPorcentajes = () => {
    const totalComentarios = comentarios.length;

    if (totalComentarios === 0) {
      return [0, 0, 0, 0, 0];
    }

    const conteoValoraciones = [5, 4, 3, 2, 1].map((valor) => {
      return comentarios.filter((comentario) => comentario.valoracion === valor).length;
    });

    return conteoValoraciones.map((conteo) => Math.round((conteo / totalComentarios) * 100));
  };

  const porcentajes = calcularPorcentajes();

  // Manejar la publicación de la reseña
  const handlePublish = async () => {
    try {
      // Obtener el usuario autenticado
      const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await responseUsuarios.json();
      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (!usuarioAutenticado) {
        console.error('Usuario no autenticado encontrado.');
        return;
      }

      const idUsuario = usuarioAutenticado.id;

      // Crear el cuerpo de la solicitud
      const body = {
        texto: reviewText,
        valoracion: selectedRating,
      };

      // Realizar la solicitud POST
      const response = await fetchWithAuth(
        `${API_URL}/comentarios/modulo/${idModulo}/usuario/${idUsuario}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        }
      );

      if (response.ok) {
        console.log('Reseña publicada con éxito.');
        setReviewText('');
        setSelectedRating(0);
        setSuccessMessage('¡Reseña publicada correctamente!');
        setShowModal(true);

        // Ocultar el mensaje y cerrar el modal después de unos segundos
        setTimeout(() => {
          setSuccessMessage('');
          setShowModal(false);
        }, 3000);
      } else {
        console.error('Error al publicar la reseña:', response.statusText);
      }
    } catch (error) {
      console.error('Error en la solicitud POST:', error);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
    fetchResenas();
  };

  const handleLike = (id) => {
    setLiked((prevLikes) => ({
      ...prevLikes,
      [id]: !prevLikes[id],
    }));
  };
  return (
    <div className="w-full px-4">
      <div className="flex flex-col sm:flex-row items-center mb-6">
        <div className="w-full sm:w-1/3 mb-4 sm:mb-0">
          <h1 className="text-base text-sky-900 font-bold mb-2">Valoración</h1>
          {['5', '4', '3', '2', '1'].map((valor, index) => (
            <div key={valor} className="flex items-center">
              <span className="text-blue-600 text-lg mr-2">
                {[...Array(Number(valor))].map((_, i) => (
                  <IoStar key={i} className="inline text-sky-900 h-4 w-4" />
                ))}
                {[...Array(5 - Number(valor))].map((_, i) => (
                  <IoStar key={i + 5} className="inline text-gray-400 h-4 w-4" />
                ))}
              </span>
              <span className="ml-2 text-xs font-bold text-sky-900">({porcentajes[index]}%)</span>
            </div>
          ))}
        </div>

        {isModuloAdquirido ? (
          <div className="w-full sm:w-2/3">
            <h1 className="text-base text-sky-900 font-bold mb-2 ">Su calificación</h1>
            <RatingStars className="transition hover:scale-105" onRatingChange={handleRatingChange} />
            <h1 className="text-base text-sky-900 font-bold mb-2">Su reseña</h1>
            <textarea
              className="w-full p-2 border border-gray-300 rounded mb-2"
              rows="4"
              placeholder="Escribir su opinión..."
              value={reviewText}
              onChange={handleReviewTextChange}
            ></textarea>
            <div className="flex justify-end py-2">
              <button
                className="bg-sky-950 text-white text-xs font-semibold py-1 px-6 rounded-3xl hover:bg-sky-800 transition hover:scale-105"
                onClick={handlePublish}
              >
                PUBLICAR
              </button>
            </div>
          </div>
        ) : (
          <p className="text-center text-sm text-gray-500">
            Debe adquirir este módulo para dejar reseña.
          </p>
        )}
      </div>

      {/* Modal para mensaje de éxito */}
      {showModal && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <div className="bg-white p-6 rounded shadow-md text-center sm:w-auto sm:h-auto w-full h-full">
            <h2 className="text-lg font-bold text-green-600 mb-4">{successMessage}</h2>
            <button
              className="bg-sky-950 text-white text-xs font-semibold py-1 px-4 rounded hover:bg-sky-800 transition:hover-scale-105"
              onClick={handleCloseModal}
            >
              CERRAR
            </button>
          </div>
        </div>
      )}

      <div className="flex flex-col sm:flex-row justify-between items-center mb-6">
        <div className="flex items-center mb-4 sm:mb-0">
          <button onClick={() => paginate(currentPage - 1)} className="bg-gray-200 text-sky-900 p-2 mx-1 rounded-full" disabled={currentPage === 1}>
            <MdNavigateBefore />
          </button>
          <span className="text-gray-600 text-xs">
            Página {currentPage} de {Math.ceil(comentarios.length / commentsPerPage)}
          </span>
          <button onClick={() => paginate(currentPage + 1)} className="bg-gray-200 text-sky-900 p-2 mx-1 rounded-full" disabled={currentPage === Math.ceil(comentarios.length / commentsPerPage)}>
            <MdNavigateNext />
          </button>
        </div>

        <div className="flex items-center text-xs">
          <h1 className="text-sky-900 font-bold mr-2">Ordenar por:</h1>
          <select
            value={sortOption}
            onChange={handleSortChange}
            className="p-2 border border-gray-300 rounded text-xs"
          >
            <option value="fecha">Más recientes</option>
            <option value="mejor">Mejor puntuación</option>
            <option value="peor">Peor puntuación</option>
          </select>
        </div>
      </div>

      {currentComments.length > 0 ? (
        currentComments.map((comentario) => (
          <div key={comentario.id} className="mb-4">
            <div className="grid grid-cols-1 sm:grid-cols-3 gap-4 bg-gray-200 rounded-lg p-4">
              {/* Primera columna */}
              <div className="flex items-center mb-4 sm:mb-0">
                <img
                  src="https://placehold.co/50x50"
                  alt="User profile picture"
                  className="w-12 h-12 rounded-full mr-4"
                />
                <div>
                  <div className="flex items-center">
                    {[...Array(comentario.valoracion)].map((_, i) => (
                      <IoStar key={i} className="inline text-sky-900" />
                    ))}
                    {[...Array(5 - comentario.valoracion)].map((_, i) => (
                      <IoStar key={i + 5} className="inline text-gray-400" />
                    ))}
                  </div>
                  <div className="text-gray-600 text-xs py-1">{new Date(comentario.fechaCreacion).toLocaleString()}</div>
                  <div className="text-gray-600 text-xs">{comentario.usuarioNombre}</div>
                </div>
              </div>

              {/* Segunda columna */}
              <div className="col-span-1 sm:col-span-2">
                <p className="text-gray-800 mb-4">{comentario.texto}</p>
                <div className="flex items-center justify-between text-gray-600">
                  <button
                    className="flex items-center mr-4 text-xs w-full text-left"
                    onClick={() => handleLike(comentario.id)}
                  >
                    {liked[comentario.id] ? (
                      <AiFillLike className="mr-1 text-sky-900" />
                    ) : (
                      <AiOutlineLike className="mr-1" />
                    )}
                    Marcar como útil
                  </button>
                  <button className="flex items-center mr-4 text-xs w-full text-left">
                    <FaRegComment className="mr-1" />
                    Comentario
                  </button>
                  <button className="flex items-center text-xs w-full text-left">
                    <FaHashtag className="mr-1" />
                    Informar comentario
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="text-gray-600 text-center pt-2 pb-4 text-sm">Este módulo no tiene reseñas</div>
      )}

      {/* Paginación */}
      <div className="flex justify-center items-center mb-6">
        <button onClick={() => paginate(currentPage - 1)} className="bg-gray-200 text-sky-900 p-3 mx-1 rounded-full" disabled={currentPage === 1}>
          <MdNavigateBefore />
        </button>
        <span className="text-gray-600 text-sm">
          Página {currentPage} de {Math.ceil(comentarios.length / commentsPerPage)}
        </span>
        <button onClick={() => paginate(currentPage + 1)} className="bg-gray-200 text-sky-900 p-3 mx-1 rounded-full" disabled={currentPage === Math.ceil(comentarios.length / commentsPerPage)}>
          <MdNavigateNext />
        </button>
      </div>
    </div>

  );
};

export default Reviews;
