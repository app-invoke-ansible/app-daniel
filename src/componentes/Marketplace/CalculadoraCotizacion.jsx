import React, { useEffect, useState } from 'react';
import { useAuth } from '../../providers/AuthProvider';

const API_URL = process.env.REACT_APP_API_URL;

const CalculadoraCotizacion = ({ precio, nivelImpacto }) => {
  const { fetchWithAuth, user } = useAuth();
  const [empresa, setEmpresa] = useState(null);
  const [loading, setLoading] = useState(false);
  const [nodos, setNodos] = useState(0);
  const [repeticiones, setRepeticiones] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    const fetchModulo = async () => {
      try {
        setLoading(true);

        const responseUsuarios = await fetchWithAuth(`${API_URL}/usuario`);
        const usuarios = await responseUsuarios.json();

        const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

        if (!usuarioAutenticado) {
          console.error('Usuario autenticado no encontrado en la API.');
          return;
        }

        const idEmpresa = usuarioAutenticado.idEmpresa;

        if (idEmpresa) {
          const responseEmpresas = await fetchWithAuth(`${API_URL}/empresas`);
          const empresas = await responseEmpresas.json();

          const empresaFiltrada = empresas.find((empresa) => empresa.id === idEmpresa);
          if (empresaFiltrada) {
            setEmpresa(empresaFiltrada);
          }
        }
      } catch (error) {
        console.error('Error al obtener datos:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchModulo();
  }, [fetchWithAuth, user?.name]);

  useEffect(() => {
    const totalCalculado = nodos * 10 + repeticiones * precio;
    setTotal(totalCalculado);
  }, [nodos, repeticiones, precio]);

  const handleInputChange = (e, setter) => {
    const value = e.target.value;
    if (/^\d*$/.test(value)) {
      setter(Number(value));
    }
  };

  return (
    <div>
      <h1 className="text-base text-sky-900 font-bold text-center mb-4">Calculadora de Cotización</h1>

      <div className="hidden lg:block">
        {/* Tabla horizontal para pantallas grandes */}
        <table className="min-w-full divide-y divide-gray-300 border">
          <thead>
            <tr>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900">Usuario</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900">Empresa</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900">Nodos × RHEL</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900">Criterio<br />(Nivel de impacto)</th>
              <th className="px-3 py-3.5 text-center font-semibold text-xs text-gray-900">Repeticiones</th>
            </tr>
          </thead>
          <tbody>
            <tr className="bg-gray-200">
              <td className="px-3 py-4 text-xs text-center">{user?.name || 'N/A'}</td>
              <td className="px-3 py-4 text-xs text-center">
                {loading ? 'Cargando...' : (empresa && empresa.nombre ? empresa.nombre : 'Sin empresa asignada')}
              </td>
              <td className="px-3 py-4 text-xs text-center">
                <input
                  type="number"
                  className="border border-black rounded-lg bg-white p-1 mr-2 text-center w-24"
                  placeholder="Cantidad"
                  min="0"
                  value={nodos}
                  onFocus={() => setNodos('')}
                  onChange={(e) => handleInputChange(e, setNodos)}
                  onKeyDown={(e) => {
                    if (
                      !/^[0-9]$/.test(e.key) &&
                      e.key !== "Backspace" &&
                      e.key !== "ArrowLeft" &&
                      e.key !== "ArrowRight" &&
                      e.key !== "Tab"
                    ) {
                      e.preventDefault();
                    }
                  }}
                />
                × 10$ USD
              </td>
              <td className="px-3 py-4 text-xs text-center">
                ${precio} USD ({nivelImpacto} NDI)
              </td>
              <td className="px-3 py-4 text-xs text-center">
                <input
                  type="number"
                  className="border border-black rounded-lg bg-white p-1 mr-2 text-center w-24"
                  placeholder="Cantidad"
                  min="0"
                  value={repeticiones}
                  onFocus={() => setRepeticiones('')}
                  onChange={(e) => handleInputChange(e, setRepeticiones)}
                  onKeyDown={(e) => {
                    if (
                      !/^[0-9]$/.test(e.key) &&
                      e.key !== "Backspace" &&
                      e.key !== "ArrowLeft" &&
                      e.key !== "ArrowRight" &&
                      e.key !== "Tab"
                    ) {
                      e.preventDefault();
                    }
                  }}
                />
                × mensuales
              </td>
            </tr>
          </tbody>
        </table>
        <h1 className="text-large text-sky-900 font-bold text-end pr-8 p-2">Total = ${total} USD</h1>
      </div>

      <div className="block lg:hidden">
        {/* Diseño vertical para pantallas pequeñas */}
        <div className="border rounded-lg bg-gray-100 p-4 flex flex-col gap-4">
          <div className="grid grid-cols-2 sm:grid-cols-1 gap-2">
            <span className="font-semibold text-sm text-gray-700">Usuario:</span>
            <span className="text-sm text-gray-900">{user?.name || 'N/A'}</span>
          </div>
          <div className="grid grid-cols-2 sm:grid-cols-1 gap-2">
            <span className="font-semibold text-sm text-gray-700">Empresa:</span>
            <span className="text-sm text-gray-900">
              {loading ? 'Cargando...' : empresa?.nombre || 'Sin empresa asignada'}
            </span>
          </div>
          <div className="grid grid-cols-2 sm:grid-cols-1 gap-2">
            <span className="font-semibold text-sm text-gray-700">Nodos × RHEL:</span>
            <div className="flex items-center gap-2">
              <input
                type="number"
                className="border border-gray-300 rounded-lg p-1 text-sm text-center w-20"
                placeholder="Cantidad"
                min="0"
                value={nodos}
                onFocus={() => setNodos('')}
                onChange={(e) => handleInputChange(e, setNodos)}
              />
              <span className="text-sm text-gray-700">× 10$ USD</span>
            </div>
          </div>
          <div className="grid grid-cols-2 sm:grid-cols-1 gap-2">
            <span className="font-semibold text-sm text-gray-700">Criterio (Nivel de impacto):</span>
            <span className="text-sm text-gray-900">${precio} USD ({nivelImpacto} NDI)</span>
          </div>
          <div className="grid grid-cols-2 sm:grid-cols-1 gap-2">
            <span className="font-semibold text-sm text-gray-700">Repeticiones:</span>
            <div className="flex items-center gap-2">
              <input
                type="number"
                className="border border-gray-300 rounded-lg p-1 text-sm text-center w-20"
                placeholder="Cantidad"
                min="0"
                value={repeticiones}
                onFocus={() => setRepeticiones('')}
                onChange={(e) => handleInputChange(e, setRepeticiones)}
              />
              <span className="text-sm text-gray-700">× mensuales</span>
            </div>
          </div>
        </div>
        <div className="text-right mt-4">
          <h1 className="text-lg text-sky-900 font-bold pr-2">Total = ${total} USD</h1>
        </div>
      </div>
    </div>
  );
};

export default CalculadoraCotizacion;
