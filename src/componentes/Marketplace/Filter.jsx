import React from 'react';

const Filter = ({ categoriasSeleccionadas, setCategoriasSeleccionadas }) => {
  const categorias = [
    { name: 'Seguridad', displayName: 'Seguridad' },
    { name: 'Monitoreo', displayName: 'Monitoreo' },
    { name: 'Infraestructura', displayName: 'Infraestructura' },
    { name: 'Redes', displayName: 'Redes' },
    { name: 'Basededatos', displayName: 'Base de Datos' },
    //    { name: 'Deployments', displayName: 'Deployments' },
    { name: 'Aprovisionamiento', displayName: 'Aprovisionamiento' },
    { name: 'Informe', displayName: 'Informe' },
  ];

  const handleChange = (e) => {
    const { value, checked } = e.target;
    setCategoriasSeleccionadas((prevSelected) =>
      checked ? [...prevSelected, value] : prevSelected.filter((category) => category !== value)
    );
  };
  const handleChangeSelect = (e) => {
    const selectedValue = e.target.value;
    setCategoriasSeleccionadas(selectedValue === "all" ? [] : [selectedValue]);
 };

  return (
    <div>
      <div className="border-r border-gray-300 bg-white hidden sm:block">
        <h3 className="text-base text-sky-900 font-bold">Categorías</h3>
        <form>
          {categorias.map(({ name, displayName }, index) => (
            <div key={index} className="mt-2 border-b border-transparent last:border-b-0 text-sm text-sky-950 ">
              <input
                type="checkbox"
                id={`checkbox-${index}`}
                className="mr-4 w-4 h-4 accent-sky-950"
                value={name}
                onChange={handleChange}
                checked={categoriasSeleccionadas.includes(name)}
              />
              <label htmlFor={`checkbox-${index}`}>
                {displayName}
              </label>
            </div>
          ))}
        </form>
      </div>
      <div className=" bg-white sm:hidden">
        <div className="mt-2">
          <select
            className="text-sm text-gray-500 border border-gray-300 rounded-md p-2 w-full"
            onChange={handleChangeSelect}

          >
            <option value="all">Todas las categorías</option>
            {categorias.map(({ name, displayName }, index) => (
              <option key={index} value={name}>
                {displayName}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};

export default Filter;
