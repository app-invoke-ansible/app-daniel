import React, { useState, useEffect } from 'react';
import ObtenerModulo from './ObtenerModulo';
import PromedioRating from './PromedioRating';
import { useAuth } from '../../providers/AuthProvider';
import CategoriaImagen from '../Base/CategoriaImagen';
import { IoPlay } from "react-icons/io5";
import SegmentoTabContent from '../Home/SegmentoTabContent';

const Card = ({ id, titlesecond, title, description, contenido, precio, nivelImpacto, isModulosAdquirido, fetchModulos, categoriaName, sistemaName }) => {
  const [mostrarTodaDescripcion, setMostrarTodaDescripcion] = useState(false);
  const [totalEjecuciones, setTotalEjecuciones] = useState(null);
  const [currentModulo, setCurrentModulo] = useState(null);
  const [isObtenerModalOpen, setIsObtenerModalOpen] = useState(false);
  const [isPlayModalOpen, setIsPlayModalOpen] = useState(false);
  const { fetchWithAuth, user } = useAuth();

  const API_URL = process.env.REACT_APP_API_URL;

  // Bloquear/Restaurar scroll en el fondo del modal
  useEffect(() => {
    if (isObtenerModalOpen || isPlayModalOpen) { document.body.style.overflow = "hidden"; } else { document.body.style.overflow = "auto"; }

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [isObtenerModalOpen, isPlayModalOpen]);

  useEffect(() => {
    const getEjecuciones = async () => {
      try {
        const response = await fetchWithAuth(`${API_URL}/modulos/${id}/ejecuciones`);
        if (response.ok) {
          const data = await response.json();
          setTotalEjecuciones(data);
        }
      } catch (error) {
        console.error("Error al obtener las ejecuciones:", error);
      }
    };

    getEjecuciones();
  }, [fetchWithAuth, API_URL, id]);

  const toggleDescription = () => setMostrarTodaDescripcion(!mostrarTodaDescripcion);

  // Funciones para el modal de ObtenerModulo
  const handleOpenObtenerModal = () => setIsObtenerModalOpen(true);
  const handleCloseObtenerModal = () => setIsObtenerModalOpen(false);

  // Funciones para el modal de IoPlay
  const handleOpenPlayModal = (module) => {
    setCurrentModulo(module);
    setIsPlayModalOpen(true);
  };

  const handleClosePlayModal = () => {
    setIsPlayModalOpen(false);
    setCurrentModulo(null);
  };

  const modulo = {
    id,
    name: title,
    categoria: titlesecond,
    description,
    contenido,
    precio,
    nivelImpacto
  };

  const formattedContenido = contenido.split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/).map((sentence, index) => (
    <React.Fragment key={index}>
      {sentence.trim()}
      <br />
      {index < contenido.split(/(?<=\. )|(?=Resumen de tareas: )|(?<=Resumen de tareas: )/).length - 1 && <br />}
    </React.Fragment>
  ));

  return (
    <div className="flex sm:border sm:border-sky-900 border-b border-sky-950 sm:rounded-lg sm:shadow-sm hover:shadow-lg transition-shadow duration-300 ease-in-out w-full flex-col justify-between">
      {/* Contenido de la tarjeta */}
      <div>
        <div className="flex p-4 my-2">
          <div className="w-auto h-full max-w-24 object-cover p-2 mr-2 sm:mr-4 border rounded-lg border-sky-950 sm:w-12 sm:h-12 sm:border-hidden sm:p-0">
            <CategoriaImagen categoriaName={categoriaName} />
          </div>
          <div className="flex flex-col items-start mr-4">
            <h6 className="text-xs text-gray-800 hidden sm:block">{titlesecond}</h6>
            <h2 className="text-sm text-sky-900 font-bold w-28">{title}</h2>
            <div className="sm:hidden">
              <p className={`text-gray-600 text-xs ${mostrarTodaDescripcion ? '' : 'line-clamp-3'}`}>
                {formattedContenido}
              </p>
              <div className="flex justify-end items-end">
                <button onClick={toggleDescription} className="text-sky-950 font-semibold mt-1 text-xs hover:underline">
                  {mostrarTodaDescripcion ? 'Ver menos' : 'Leer más...'}
                </button>
              </div>
            </div>
          </div>
          {/* Nivel de impacto */}
          <div className="flex flex-col items-end sm:hidden">
            <div className="flex flex-row justify-between items-center">
              <div className="flex flex-col items-center justify-end sm:mr-1 mr-2">
                <div className="w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                  <div className="text-sky-500 text-sm font-bold bg-white">
                    {nivelImpacto}
                  </div>
                </div>
                <span className="text-sky-900 text-sm font-semibold">Impacto</span>
              </div>
              {/* Total de ejecuciones */}
              {totalEjecuciones !== null && (
                <div className="flex flex-col items-center justify-end">
                  <span className="w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                    <div className="text-sky-500 text-sm font-bold bg-white">
                      {totalEjecuciones}
                    </div>
                  </span>
                  <span className="text-sky-900 text-sm font-semibold">Ejecucion</span>
                </div>
              )}
            </div>
            {/* modulos informacion */}
            <div className="flex-row items-center sm:hidden">
              {isModulosAdquirido ? (
                <PromedioRating className="flex justify-end" id={id} />
              ) : (
                <div className="flex justify-end my-2">
                  <span className="text-sky-900 font-bold text-lg">{precio}</span>
                  <span className="text-sm text-sky-900 ml-1 mt-1">USD</span>
                </div>
              )}
            </div>
            <div className="sm:hidden">
              {isModulosAdquirido ? (
                <div className="w-full">
                  <div className="flex justify-end">
                    <button className="text-sky-950 text-xs font-semibold py-1 hover:underline transition hover:scale-105" onClick={handleOpenObtenerModal}>
                      Ver información...
                    </button>
                  </div>
                  <div className="mt-2 sm:flex px-6 justify-center items-center transition hover:scale-105">
                    <button
                      onClick={() => handleOpenPlayModal(modulo)}
                      className="text-green-600 hover:text-green-900 rounded-full flex flex-col items-center"
                    >
                      <IoPlay className="h-8 w-8" />
                      <span className="text-green-600 hover:text-green-900 text-xs">
                        Ejecutar
                      </span>
                    </button>
                  </div>
                </div>
              ) : (
                <div className="flex flex-col items-center justify-center">
                  <PromedioRating className="flex justify-end" id={id} />
                  <button className="bg-sky-950 text-white text-xs font-semibold py-1 px-4 mt-2 rounded-3xl hover:bg-sky-800 transition hover:scale-105" onClick={handleOpenObtenerModal}>
                    OBTENER
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>

        <div className="px-4 hidden sm:block">
          <p className={`text-gray-800 text-xs ${mostrarTodaDescripcion ? '' : 'line-clamp-2'}`}>{description}</p>
          <p className={`mt-2 text-gray-600 text-xs ${mostrarTodaDescripcion ? '' : 'line-clamp-2'}`}>
            {formattedContenido}
          </p>
          <div className="flex justify-end">
            <button onClick={toggleDescription} className="text-sky-950 font-semibold mt-1 text-xs hover:underline">
              {mostrarTodaDescripcion ? 'Ver menos' : 'Leer más...'}
            </button>
          </div>
        </div>
      </div>

      <div className="pb-4 sm:flex flex-col justify-end hidden">
        <div className="flex items-center justify-between my-2 px-4">
          <PromedioRating id={id} />
          <div className="flex items-start ml-auto">
            <span className="text-sky-900 font-bold text-lg">{precio}</span>
            <span className="text-xs text-sky-900 ml-1 mt-1">USD</span>
          </div>
        </div>

        {/* Nivel de impacto */}
        <div className="sm:flex justify-center items hidden">
          {isModulosAdquirido ? (
            <div className="w-full">
              <div className="border-t border-b border-sky-950 py-2 sm:flex px-6 hidden justify-center items-center mt-2 ">
                <button
                  onClick={() => handleOpenPlayModal(modulo)}
                  className="text-green-600 hover:text-green-900 flex flex-col items-center transition hover:scale-105"
                >
                  <IoPlay className="h-5 w-5" />
                  <span className="text-green-600 hover:text-green-900 text-xs">
                    Ejecutar
                  </span>
                </button>
              </div>
              <span className="bg-sky-950 text-white text-xs font-semibold text-center py-1 w-full block">
                COMPRADO
              </span>
              <div className="flex justify-end mt-2">
                <button className="text-sky-950 text-xs font-semibold py-1 px-4 hover:underline transition hover:scale-105" onClick={handleOpenObtenerModal}>
                  Ver información...
                </button>
              </div>
            </div>
          ) : (
            <div className="w-full">
              <div className="border-t border-b border-sky-950 py-2 sm:flex justify-between items-center px-6 hidden">
                <div className="flex flex-col items-center justify-center">
                  <span className="text-sky-900 text-xs font-semibold">Nivel de impacto</span>
                  <div className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                    <div className="text-sky-500 text-xs font-bold bg-white px-1">
                      {nivelImpacto}
                    </div>
                  </div>
                </div>

                {/* Total de ejecuciones */}
                {totalEjecuciones !== null && (
                  <div className="flex flex-col items-center justify-center">
                    <span className="text-sky-900 text-xs font-semibold">Total ejecuciones</span>
                    <span className="mt-1 w-8 h-8 flex items-center justify-center rounded-full border border-sky-950 bg-white">
                      <div className="text-sky-500 text-xs font-bold bg-white px-1">
                        {totalEjecuciones}
                      </div>
                    </span>
                  </div>
                )}
              </div>

              <div className="flex justify-center items-center mt-4">
                <button className="bg-sky-950 text-white text-xs font-semibold py-1 px-4 rounded-3xl hover:bg-sky-800 transition hover:scale-105" onClick={handleOpenObtenerModal}>
                  OBTENER
                </button>
              </div>
            </div>
          )}
        </div>
      </div>

      {/* Modal de ObtenerModulo */}
      <ObtenerModulo
        isOpen={isObtenerModalOpen}
        onClose={handleCloseObtenerModal}
        modulo={modulo}
        isModulosAdquirido={isModulosAdquirido}
        fetchModulos={fetchModulos}
        categoriaName={categoriaName}
        sistemaName={sistemaName}
      />

      {/* Modal de IoPlay */}
      {isPlayModalOpen && currentModulo && (
        <div className="fixed inset-0 bg-gray-800 bg-opacity-50 flex justify-center items-center z-50">
          <div className="bg-white p-6 rounded-lg shadow-lg sm:w-5/6 sm:h-auto w-full h-full mx-0 overflow-auto">
            <h2 className="text-lg font-semibold mb-4">Ejecutar módulo {currentModulo.name}</h2>
            <SegmentoTabContent id={currentModulo.id} />
            <div className="flex justify-end">
              <button
                onClick={handleClosePlayModal}
                className="text-white bg-red-500 hover:bg-red-600 px-4 py-1 rounded-lg shadow-md font-semibold transition-colors duration-300"
              >
                Cerrar
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Card;