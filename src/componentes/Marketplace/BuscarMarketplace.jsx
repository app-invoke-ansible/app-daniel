import React, { useState } from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';

const BuscarMarketplace = ({ modulos, setModulosFiltrados, modulosDestacados, setModulosDestacadosFiltrados }) => {
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearchChange = (e) => {
        const term = e.target.value.toLowerCase();
        setSearchTerm(term);

        // Filtrar módulos adquiridos
        const filteredAdquiridos = modulos.filter(modulo =>
            modulo.name.toLowerCase().includes(term)
        );
        setModulosFiltrados(filteredAdquiridos);

        // Filtrar módulos destacados
        const filteredDestacados = modulosDestacados.filter(modulo =>
            modulo.name.toLowerCase().includes(term)
        );
        setModulosDestacadosFiltrados(filteredDestacados);
    };

    return (
        <div className="sm:flex mx-auto sm:px-6 lg:px-8 overflow-x-auto items-center justify-between w-full">
            <div className="relative w-full sm:w-64 md:w-72 lg:w-80">
                <input
                    type="search"
                    name="search"
                    id="searchMarketplace"
                    className="pl-2 pr-10 block w-full border-b-2 border-sky-950 py-1.5 text-gray-900 placeholder-gray-400 focus:border-sky-950 focus:outline-none sm:text-sm sm:leading-6"
                    placeholder="Buscar en Marketplace"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <MagnifyingGlassIcon className="absolute right-2 top-1/2 transform -translate-y-1/2 h-5 w-5 text-sky-950" />
            </div>
        </div>
    );
}

export default BuscarMarketplace;
