import React, { useState, useEffect } from 'react';
import { IoStar, IoStarHalf, IoStarOutline } from 'react-icons/io5';
import { useAuth } from '../../providers/AuthProvider';

const PromedioRating = ({ id }) => {
  const [valoracionPromedio, setValoracionPromedio] = useState(0);
  const { fetchWithAuth } = useAuth();
  const API_URL = process.env.REACT_APP_API_URL;

  const fetchResenas = async () => {
    try {
      const response = await fetchWithAuth(`${API_URL}/comentarios/modulo/${id}/resenas`);
      const data = await response.json();

      if (response.ok) {
        setValoracionPromedio(data.valoracionPromedio);
      } else {
        console.error('Error al obtener las reseñas:', data);
      }
    } catch (error) {
      console.error('Error al realizar la solicitud GET:', error);
    }
  };

  useEffect(() => {
    fetchResenas();
  }, [id]);

  // Función para renderizar las estrellas
  const renderStars = () => {
    const stars = [];
    const fullStars = Math.floor(valoracionPromedio);
    const halfStar = valoracionPromedio % 1 >= 0.5 ? 1 : 0;
    const emptyStars = 5 - fullStars - halfStar;

    // Añadir las estrellas completas
    for (let i = 0; i < fullStars; i++) {
      stars.push(<IoStar key={`full-${i}`} className="inline text-orange-500 h-4 w-4" />);
    }

    // Añadir la estrella media si corresponde
    if (halfStar) {
      stars.push(<IoStarHalf key="half" className="inline text-orange-500 h-4 w-4" />);
    }

    // Añadir las estrellas vacías
    for (let i = 0; i < emptyStars; i++) {
      stars.push(<IoStarOutline key={`empty-${i}`} className="inline text-gray-400 h-4 w-4" />);
    }

    return stars;
  };

  return (
    <div className="flex items-center">
      <span className="text-sky-900 text-lg font-bold">{renderStars()}</span>
    </div>
  );
};

export default PromedioRating;
