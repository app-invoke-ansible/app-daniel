import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js';

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

const CostBalanceHistogram = ({ costosMensuales }) => {
    const monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    const data = {
        labels: monthNames,  // Muestra todos los meses, no solo los meses con datos
        datasets: [
            {
                label: 'Balance de Costo',
                data: costosMensuales, // Los datos deben tener la misma longitud que labels
                backgroundColor: 'rgba(144, 180, 249, 0.8)', // Azul Claro
                borderColor: 'rgba(144, 180, 249, 1)',  // Azul Claro
                borderWidth: 1,  // Borde delgado
                hoverBackgroundColor: 'rgba(100, 149, 237, 0.9)', // Cornflower Blue (hover)
                hoverBorderColor: 'rgba(100, 149, 237, 1)',  // Cornflower Blue (hover)
            },
        ],
    };

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            y: {
                beginAtZero: true,
                grid: {
                    display: true,
                    color: 'rgba(0, 0, 0, 0.1)',  // Rejilla más sutil
                },
                title: {
                    display: false,
                },
                ticks: {
                    color: '#555',  // Ticks más legibles
                    callback: function (value) {
                        return '$' + value;
                    },
                },
            },
            x: {
                grid: {
                    display: false,
                },
                title: {
                    display: false,
                },
                ticks: {
                    color: '#555',  // Ticks más legibles
                },
            },
        },
        plugins: {
            legend: {
                display: false, // Ocultar la leyenda
            },
            title: {
                display: true,
                text: 'Balance de Costos Mensual',
                font: {
                    size: 18,
                },
                color: '#333',  // Título más visible
            },
            tooltip: {
                backgroundColor: 'rgba(0, 70, 100, 0.9)', // Fondo más azulado
                titleColor: '#fff',  // Texto blanco
                bodyColor: '#fff',  // Texto blanco
                borderColor: 'rgba(220, 220, 220, 0.8)', // Borde más suave
                borderWidth: 1,
                callbacks: {
                    label: (context) => {
                        let label = context.dataset.label || '';
                        if (label) {
                            label += ': ';
                        }
                        if (context.parsed.y !== null) {
                            label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y);
                        }
                        return label;
                    }
                }
            },
        },
    };

    return (
        <div style={{ height: '400px', width: 'auto' }} className="p-2"> 
            <Bar data={data} options={options} />
        </div>
    );
};

export default CostBalanceHistogram;