import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ImgLogoInvoke from '../img/logoColor-InvokeGroup.png';

const RegisterForm = () => {
  const [formData, setFormData] = useState({
    nombre: '',
    apellido: '',
    email: '',
    empresa: '',
    telefono: '',
    tipoUsuario: '',
    contraseña: '',
    confirmarContraseña: '',
    aceptoPoliticas: false,
  });

  const navigate = useNavigate(); 

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setFormData({
      ...formData,
      [name]: type === 'checkbox' ? checked : value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (formData.contraseña !== formData.confirmarContraseña) {
      alert('Las contraseñas no coinciden');
      return;
    }
    if (formData.contraseña.length < 8) {
      alert('La contraseña debe tener al menos 8 caracteres');
      return;
    }
    try {
      await sendDataToServer();
      alert('Registro exitoso');
      setFormData({
        nombre: '',
        apellido: '',
        email: '',
        empresa: '',
        telefono: '',
        tipoUsuario: '',
        contraseña: '',
        confirmarContraseña: '',
        aceptoPoliticas: false,
      });
      navigate('/login');
    } catch (error) {
      alert('Error al registrar');
    }
  };

  const sendDataToServer = async () => {
    const response = await fetch('/api/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (!response.ok) {
      throw new Error('Error en la solicitud');
    }
    return await response.json();
  };

  const handleCancel = () => {
    navigate('/login');
  };

  return (
    <div className="flex justify-center items-center max-h-screen px-4">
      <form onSubmit={handleSubmit} className="bg-white p-6 rounded-lg shadow-md w-full ">
        <div className="mb-4 text-center">
          <img src={ImgLogoInvoke} alt="Empresa Logo" className="mx-auto mb-4 w-auto h-auto"/>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="nombre">
            Nombre
          </label>
          <input
            type="text"
            name="nombre"
            id="nombre"
            value={formData.nombre}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="apellido">
            Apellido
          </label>
          <input
            type="text"
            name="apellido"
            id="apellido"
            value={formData.apellido}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="email">
            Dirección de Correo
          </label>
          <input
            type="email"
            name="email"
            id="email"
            value={formData.email}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="empresa">
            Empresa
          </label>
          <input
            type="text"
            name="empresa"
            id="empresa"
            value={formData.empresa}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="telefono">
            Número de Teléfono
          </label>
          <input
            type="tel"
            name="telefono"
            id="telefono"
            value={formData.telefono}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="tipoUsuario">
            Tipo de Usuario
          </label>
          <select
            name="tipoUsuario"
            id="tipoUsuario"
            value={formData.tipoUsuario}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            required
          >
            <option value="">Selecciona el tipo de usuario</option>
            <option value="admin">Administrador</option>
            <option value="user">Usuario</option>
          </select>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="contraseña">
            Contraseña
          </label>
          <input
            type="password"
            name="contraseña"
            id="contraseña"
            value={formData.contraseña}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            minLength="8"
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="confirmarContraseña">
            Confirmar Contraseña
          </label>
          <input
            type="password"
            name="confirmarContraseña"
            id="confirmarContraseña"
            value={formData.confirmarContraseña}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            minLength="8"
            required
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-bold mb-1" htmlFor="aceptoPoliticas">
            <input
              type="checkbox"
              name="aceptoPoliticas"
              id="aceptoPoliticas"
              checked={formData.aceptoPoliticas}
              onChange={handleChange}
              className="mr-2 leading-tight"
              required
            />
            Acepto las políticas de privacidad
          </label>
        </div>
        <div className="flex items-center justify-between">
          <button
            type="submit"
            className="bg-sky-950 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline"
          >
            Registrar
          </button>
        </div>
      </form>
    </div>
  );
};

export default RegisterForm;
