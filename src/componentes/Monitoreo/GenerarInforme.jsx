import React, { useState } from "react";
import { useAuth } from '../../providers/AuthProvider';
import { LuDownload } from "react-icons/lu";  // Importamos el icono de descarga

const API_URL = process.env.REACT_APP_API_URL;

const GenerarInforme = () => {
    const [loading, setLoading] = useState(false);
    const { fetchWithAuth } = useAuth();

    const descargarPDF = async () => {
        setLoading(true);

        try {
            // Obtener los hostIds activos desde el localStorage
            const storedHostIds = localStorage.getItem("hostIds");
            
            const hostIds = storedHostIds ? JSON.parse(storedHostIds) : [];
            console.log("Id de los host que se estan ejecutando actualmente: ", hostIds);
            if (!hostIds || hostIds.length === 0) {
                console.error("No se encontraron hosts activos.");
                return;
            }

            // Hacer la solicitud a la API para generar el informe de los hosts activos
            const response = await fetchWithAuth(`${API_URL}/monitoreo/${hostIds.join(",")}/reportes`, {
                method: "POST",
            });

            if (!response.ok) {
                throw new Error("No se pudo generar el PDF");
            }

            // Obtener el PDF como un blob
            const pdfBlob = await response.blob();

            // Crear una URL de descarga para el archivo
            const pdfUrl = URL.createObjectURL(pdfBlob);

            // Crear un enlace invisible para descargar el archivo
            const link = document.createElement("a");
            link.href = pdfUrl;
            link.download = "Informe.pdf"; // Nombre del archivo PDF
            link.click();

            // Liberar la URL cuando ya no sea necesaria
            URL.revokeObjectURL(pdfUrl);
        } catch (error) {
            console.error("Error al descargar el PDF:", error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="flex justify-end">
            <button
                className="flex items-center gap-2 bg-sky-950 hover:bg-gray-600 font-medium rounded text-sm py-1 px-4 border border-transparent text-white"
                onClick={descargarPDF}
                disabled={loading}
            >
                {/* Icono de descarga */}
                <LuDownload className="text-white" size={18} />
                {loading ? "Generando informe..." : "Descargar informe"}
            </button>
        </div>
    );
};

export default GenerarInforme;
