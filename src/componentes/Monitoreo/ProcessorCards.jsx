import React, { useEffect, useState } from "react";
import { useAuth } from "../../providers/AuthProvider";
import GenerarInforme from "./GenerarInforme";
import HostData from "./HostData"; // Importamos el nuevo componente HostData

const API_URL = process.env.REACT_APP_API_URL;

const MonitoringDashboard = ({ updatedHostIds }) => {
    const [monitoringData, setMonitoringData] = useState({});
    const [hostnamesMap, setHostnamesMap] = useState({});
    const [networkData, setNetworkData] = useState({});
    const [isMonitoring, setIsMonitoring] = useState(false);
    const { fetchWithAuth } = useAuth();

    useEffect(() => {
        const monitoringStatus = localStorage.getItem("isMonitoring") === "true";
        setIsMonitoring(monitoringStatus);

        if (monitoringStatus) {
            fetchMonitoringData();
            fetchInventories();
            const interval = setInterval(fetchMonitoringData, 30000);
            return () => clearInterval(interval);
        }
    }, [updatedHostIds]);

    const fetchMonitoringData = async () => {
        try {
            const storedHostIds = localStorage.getItem("hostIds");
            const hostIds = storedHostIds ? JSON.parse(storedHostIds) : updatedHostIds;

            if (!hostIds || hostIds.length === 0) {
                console.error("No se proporcionaron IDs de host.");
                return;
            }

            const response = await fetchWithAuth(`${API_URL}/monitoreo/${hostIds.join(",")}`);
            const result = await response.json();

            const groupedData = result.monitoreo.reduce((acc, item) => {
                const { idHost, tipo, head, used, free, total, timestamp } = item;

                if (!acc[idHost]) acc[idHost] = {};
                if (!acc[idHost][tipo]) acc[idHost][tipo] = [];
                acc[idHost][tipo].push({ head, used, free, total, timestamp });
                acc[idHost][tipo] = acc[idHost][tipo].sort(
                    (a, b) => new Date(b.timestamp) - new Date(a.timestamp)
                );

                return acc;
            }, {});

            setMonitoringData(groupedData);

            if (Array.isArray(result.redes)) {
                const networkDataByHost = hostIds.reduce((acc, hostId) => {
                    const hostData = result.redes.filter((item) => item.hostId === hostId);
                    acc[hostId] = hostData;
                    return acc;
                }, {});
                setNetworkData(networkDataByHost);
            } else {
                console.error("Los datos de redes no son válidos.");
            }
        } catch (error) {
            console.error("Error al obtener los datos:", error);
        }
    };

    const fetchInventories = async () => {
        try {
            const response = await fetchWithAuth(`${API_URL}/inventarios`);
            const result = await response.json();

            const hostMap = {};
            result.forEach((inventory) => {
                inventory.groups.forEach((group) => {
                    group.hosts.forEach((host) => {
                        hostMap[host.id] = host.hostname;
                    });
                });
            });
            setHostnamesMap(hostMap);
        } catch (error) {
            console.error("Error al obtener los inventarios:", error);
        }
    };

    if (!isMonitoring) {
        return <div className="text-center">Debe seleccionar las opciones disponibles para realizar el monitoreo.</div>;
    }

    return (
        <div className="w-full">
            <div className="flex justify-end">
                <GenerarInforme />
            </div>
            <div className="grid grid-cols-1 gap-4">
                {/* Iteramos sobre los hostIds y renderizamos HostData para cada uno */}
                {updatedHostIds.map((idHost) => {
                    const hostname = hostnamesMap[idHost] || "Host no encontrado";
                    return (
                        <div key={idHost}> {/* Envolvemos HostData en un div */}
                            <HostData
                                idHost={idHost}
                                hostname={hostname}
                                monitoringData={monitoringData}
                                networkData={networkData}
                            />
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default MonitoringDashboard;