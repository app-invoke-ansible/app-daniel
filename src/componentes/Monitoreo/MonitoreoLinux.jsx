import React, { useState, useEffect, useRef } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { fetchUsuarios } from '../../funciones/funcionesUsuario';
import { fetchInventario } from '../../funciones/funcionesInventario';

const WEBSOCKET_URL = process.env.REACT_APP_WEBSOCKET_URL;

const MonitoreoLinux = ({ updatedHostIds }) => {
  const [logs, setLogs] = useState([]);
  const [idUsuario, setIdUsuario] = useState(null);
  const [inventario, setInventario] = useState(null);
  const [hostsData, setHostsData] = useState({ RHEL: null, eve: null });
  const logsEndRef = useRef(null);

  const { fetchWithAuth, isAuthenticated, user } = useAuth();

  useEffect(() => {
    const fetchDatosIniciales = async () => {
      try {
        const usuariosData = await fetchUsuarios(fetchWithAuth);
        const usuarioAutenticado = usuariosData.find((u) => u.username === user?.name);

        if (!usuarioAutenticado) {
          console.warn('Usuario no encontrado');
          return;
        }

        setIdUsuario(usuarioAutenticado.id);
        const inventarioData = await fetchInventario(fetchWithAuth, usuarioAutenticado.inventarioId);
        setInventario(inventarioData);
      } catch (error) {
        console.error('Error al obtener datos iniciales:', error);
      }
    };

    fetchDatosIniciales();
  }, [fetchWithAuth, user]);

  useEffect(() => {
    if (idUsuario && WEBSOCKET_URL) {
      const socket = new WebSocket(`${WEBSOCKET_URL}/${idUsuario}`);
      console.log("Conectando WebSocket a:", `${WEBSOCKET_URL}/${idUsuario}`);

      socket.onopen = () => {
        console.log("WebSocket conectado");
      };

      socket.onmessage = (event) => {
        const message = event.data;
        setLogs(prev => [...prev, message]);
      };

      socket.onerror = (error) => {
        console.error("Error en WebSocket:", error);
      };

      return () => socket.close();
    } else {
      console.error('No se pudo conectar al WebSocket: URL no definida o idUsuario no disponible.');
    }
  }, [idUsuario]);

  useEffect(() => {
    const joinedLogs = logs.join('\n');

    const extractData = (system) => {
      const regexPatterns = {
        hostname: `ok: \\[${system}] => \\{[^}]*?'Static hostname':\\s*'([^']+)'`,
        chassis: `ok: \\[${system}] => \\{[^}]*?\\[.*?'Chassis':\\s*'([^']+)'`,
        os: `ok: \\[${system}] => \\{[^}]*?\\[.*?'Operating System':\\s*'([^']+)'`,
        kernel: `ok: \\[${system}] => \\{[^}]*?\\[.*?'Kernel':\\s*'([^']+)'`,
        architecture: `ok: \\[${system}] => \\{[^}]*?\\[.*?'Architecture':\\s*'([^']+)'`,
        cpu: `ok: \\[${system}] => \\{[^}]*?\\[.*?'CPU\\(s\\)':\\s*'([^']+)'`,
        model: `ok: \\[${system}] => \\{[^}]*?\\[.*?'Model name|Nombre del modelo':\\s*'([^']+)'`,
        // Nuevos patrones para RAM
        ramMemTotal: `ok: \\[${system}] => \\{.*?'Ram': 'Mem',.*?'total':\\s*'([^']+)'`,
        ramMemUsed: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Mem',[^}]*?'used':\\s*'([^']+)'`,
        ramMemFree: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Mem',[^}]*?'free':\\s*'([^']+)'`,
        ramMemAvailable: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Mem',[^}]*?'available':\\s*'([^']+)'`,
        ramSwapTotal: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Swap',[^}]*?'total':\\s*'([^']+)'`,
        ramSwapUsed: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Swap',[^}]*?'used':\\s*'([^']+)'`,
        ramSwapFree: `ok: \\[${system}] => \\{[^}]*?{'Ram': 'Swap',[^}]*?'free':\\s*'([^']+)'`,
        // Patrones para filesystem raíz
        fsTamano: `ok: \\[${system}] => \\{[^}]*?{'Ficheros': '[^']*',[^}]*?'Montado en': '/',[^}]*?'Tamano':\\s*'([^']+)'`,
        fsUsados: `ok: \\[${system}] => \\{[^}]*?{'Ficheros': '[^']*',[^}]*?'Montado en': '/',[^}]*?'Usados':\\s*'([^']+)'`,
        fsDisponible: `ok: \\[${system}] => \\{[^}]*?{'Ficheros': '[^']*',[^}]*?'Montado en': '/',[^}]*?'Disp':\\s*'([^']+)'`,
        fsUso: `ok: \\[${system}] => \\{[^}]*?{'Ficheros': '[^']*',[^}]*?'Montado en': '/',[^}]*?'Uso%':\\s*'([^']+)'`,
        // Patrones para CPU
        cpuApps: `ok: \\[${system}] => \\{[^}]*?{'CPU uso Aplicaciones':\\s*'([^']+)'`,
        cpuKernel: `ok: \\[${system}] => \\{[^}]*?{'CPU uso Kernel':\\s*'([^']+)'`,
        cpuLowPriority: `ok: \\[${system}] => \\{[^}]*?{'CPU Procesos prioridad baja':\\s*'([^']+)'`,
        cpuDisponible: `ok: \\[${system}] => \\{[^}]*?{'CPU Disponible':\\s*'([^']+)'`,
        cpuIO: `ok: \\[${system}] => \\{[^}]*?{'CPU Operaciones I/O':\\s*'([^']+)'`,
        cpuHWInterrupt: `ok: \\[${system}] => \\{[^}]*?{'CPU interrupcion hardware':\\s*'([^']+)'`,
        cpuSWInterrupt: `ok: \\[${system}] => \\{[^}]*?{'CPU interrupcion software':\\s*'([^']+)'`,
        cpuHypervisor: `ok: \\[${system}] => \\{[^}]*?{'CPU uso hypervisor':\\s*'([^']+)'`,
      };

      let updatedData = {};

      // Extraer todos los valores
      Object.entries(regexPatterns).forEach(([key, pattern]) => {
        const regex = new RegExp(pattern);
        const match = joinedLogs.match(regex);
        if (match) updatedData[key] = match[1];
      });

      if (Object.keys(updatedData).length > 0) {
        setHostsData(prevData => ({
          ...prevData,
          [system]: { ...prevData[system], ...updatedData },
        }));
      }
    };

    ["RHEL", "eve"].forEach(system => {
      if (joinedLogs.includes(`ok: [${system}] => {`)) {
        extractData(system);
      }
    });
  }, [logs]);

  const filteredHosts = inventario?.groups?.[0]?.hosts?.filter(host =>
    updatedHostIds.includes(host.id)
  ) || [];

  return (
    <div className="mx-auto py-2">
      <div>
        <h2 className="text-lg font-bold">Monitoreo Linux</h2>
        <p className="text-sm text-gray-600">Host IDs: {updatedHostIds.join(', ')}</p>
      </div>

      <div className="flex flex-col md:flex-row gap-4">
        {filteredHosts.map((host) => {
          const hostData = hostsData[host.hostname] || {}; // Verifica si hay datos disponibles para el host
          return (
            <div key={host.id} className="w-full md:w-2/3 p-4 border rounded-xl shadow-lg">
              <h3 className="text-xl font-bold text-center mb-4">
                {host.hostname}
              </h3>

              <div className="font-mono text-sm overflow-y-auto bg-black text-green-400 p-4 rounded-3xl max-h-96">
                {logs.length > 0 ? (
                  <pre className="whitespace-pre-wrap">{logs.join('\n')}</pre>
                ) : (
                  <p className="text-gray-400">No hay logs disponibles.</p>
                )}
              </div>
              {hostData && (
                <div className="mt-4 space-y-2 bg-white p-4 rounded shadow-md">
                  <h4 className="text-lg font-semibold">Detalles del Sistema</h4>
                  <p><strong>Hostname:</strong> {hostData.hostname}</p>
                  <p><strong>Chassis:</strong> {hostData.chassis || 'No disponible'}</p>
                  <p><strong>Sistema Operativo:</strong> {hostData.os}</p>
                  <p><strong>Kernel:</strong> {hostData.kernel}</p>
                  <p><strong>Arquitectura:</strong> {hostData.architecture}</p>
                  <p><strong>CPU(s):</strong> {hostData.cpu}</p>
                  <p><strong>Modelo de CPU:</strong> {hostData.model}</p>

                  <div className="mt-3">
                    <h5 className="font-semibold">Memoria RAM</h5>
                    <p><strong>Total:</strong> {hostData.ramMemTotal}</p>
                    <p><strong>Usada:</strong> {hostData.ramMemUsed}</p>
                    <p><strong>Libre:</strong> {hostData.ramMemFree}</p>
                    <p><strong>Disponible:</strong> {hostData.ramMemAvailable}</p>
                  </div>

                  <div className="mt-3">
                    <h5 className="font-semibold">Swap</h5>
                    <p><strong>Total:</strong> {hostData.ramSwapTotal}</p>
                    <p><strong>Usada:</strong> {hostData.ramSwapUsed}</p>
                    <p><strong>Libre:</strong> {hostData.ramSwapFree}</p>
                  </div>

                  <div className="mt-3">
                    <h5 className="font-semibold">Sistema de Archivos (/)</h5>
                    <p><strong>Tamaño:</strong> {hostData.fsTamano}</p>
                    <p><strong>Usado:</strong> {hostData.fsUsados}</p>
                    <p><strong>Disponible:</strong> {hostData.fsDisponible}</p>
                    <p><strong>Uso%:</strong> {hostData.fsUso}</p>
                  </div>

                  <div className="mt-3">
                    <h5 className="font-semibold">Uso de CPU</h5>
                    <p><strong>Aplicaciones:</strong> {hostData.cpuApps}</p>
                    <p><strong>Kernel:</strong> {hostData.cpuKernel}</p>
                    <p><strong>Procesos baja prioridad:</strong> {hostData.cpuLowPriority}</p>
                    <p><strong>Disponible:</strong> {hostData.cpuDisponible}</p>
                    <p><strong>Operaciones I/O:</strong> {hostData.cpuIO}</p>
                    <p><strong>Interrupciones hardware:</strong> {hostData.cpuHWInterrupt}</p>
                    <p><strong>Interrupciones software:</strong> {hostData.cpuSWInterrupt}</p>
                    <p><strong>Uso hypervisor:</strong> {hostData.cpuHypervisor}</p>
                  </div>
                </div>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default MonitoreoLinux;
