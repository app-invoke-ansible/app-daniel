import React, { useState, useEffect } from 'react';
import { useAuth } from '../../providers/AuthProvider';
import { fetchUsuarios } from '../../funciones/funcionesUsuario';
import { fetchModulosMonitoreo, ejecutarMonitoreo, detenerMonitoreo, ejecutarModulo } from '../../funciones/funcionesModulo';
import { fetchInventario } from '../../funciones/funcionesInventario';
import Loading from '../Base/Loading';
import Cisco from '../../img/ambito/cisco.png';
import Linux from '../../img/ambito/linux.png';
import BuscarHost from '../Home/BuscarHost';
import ModuloSurvey from '../Home/ModuloSurvey';

// Estilos CSS para los hexágonos
const hexagonClass = `
    .hexagon {
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }
    .hexagon:hover {
        transform: scale(1.05);
        transition: all 0.3s ease;
    }
    .hexagon-disabled {
        opacity: 0.5;
        cursor: not-allowed;
        pointer-events: none;
    }`;

const ControladorMonitoreo = ({ onHostIdsUpdate, onModuleTypeChange }) => {
    const [idUsuario, setIdUsuario] = useState(null);
    const [tipoUsuario, setTipoUsuario] = useState(null);
    const [modulosMonitoreo, setModulosMonitoreo] = useState([]);
    const [inventario, setInventario] = useState([]);
    const [correoUsuario, setCorreoUsuario] = useState(null);
    const [selectedHex, setSelectedHex] = useState(null);
    const [selectedMonitoreo, setSelectedMonitoreo] = useState(null);
    const [loading, setLoading] = useState(true);
    const [selectedGrupo, setSelectedGrupo] = useState(null);
    const [selectedHosts, setSelectedHosts] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isModalSurveyOpen, setIsModalSurveyOpen] = useState(false);
    const [isMonitoring, setIsMonitoring] = useState(false);
    const [timeElapsed, setTimeElapsed] = useState(0);
    const [filteredHosts, setFilteredHosts] = useState([]);
    const [surveyData, setSurveyData] = useState({});
    const { fetchWithAuth, user } = useAuth();

    // Obtener hosts a mostrar
    const displayedHosts = selectedHosts.length > 0
        ? selectedHosts
        : (selectedGrupo?.hosts || []);

    // Recuperar estado del monitoreo y tiempo transcurrido al montar
    useEffect(() => {
        const savedMonitoring = JSON.parse(localStorage.getItem('isMonitoring')) || false;
        const savedTimeElapsed = JSON.parse(localStorage.getItem('timeElapsed')) || 0;

        setIsMonitoring(savedMonitoring);
        setTimeElapsed(savedTimeElapsed);

        if (savedMonitoring) {
            // Iniciar cronómetro si el monitoreo está activo
            const interval = setInterval(() => {
                setTimeElapsed((prevTime) => prevTime);
            }, 1000);

            return () => clearInterval(interval);
        }
    }, []);

    // Persistir estado del monitoreo y tiempo transcurrido
    useEffect(() => {
        localStorage.setItem('isMonitoring', JSON.stringify(isMonitoring));
        localStorage.setItem('timeElapsed', JSON.stringify(timeElapsed));
    }, [isMonitoring, timeElapsed]);

    // Hook para manejar el cronómetro
    useEffect(() => {
        let timer;
        if (isMonitoring) {
            timer = setInterval(() => {
                setTimeElapsed((prevTime) => prevTime + 1);
            }, 1000);
        }
        return () => clearInterval(timer);
    }, [isMonitoring]);

    // Función para formatear el tiempo en minutos y segundos
    const formatTime = (time) => {
        const minutes = Math.floor(time / 60);
        const seconds = time % 60;
        return `${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
    };

    // Obtener y actualizar datos iniciales
    useEffect(() => {
        const fetchDatosIniciales = async () => {
            setLoading(true);
            try {
                const usuariosData = await fetchUsuarios(fetchWithAuth);
                const usuarioAutenticado = usuariosData.find((u) => u.username === user?.name);

                if (!usuarioAutenticado) {
                    console.warn('Usuario no encontrado');
                    return;
                }

                const [modulos, inventarioData] = await Promise.all([
                    fetchModulosMonitoreo(fetchWithAuth, usuarioAutenticado.id),
                    fetchInventario(fetchWithAuth, usuarioAutenticado.inventarioId)
                ]);

                setIdUsuario(usuarioAutenticado.id)
                setTipoUsuario(usuarioAutenticado.tipoUsuario);
                setCorreoUsuario(usuarioAutenticado.email);
                setModulosMonitoreo(modulos);
                setInventario(inventarioData);

            } catch (error) {
                console.error('Error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchDatosIniciales();
    }, [fetchWithAuth, user]);

    //  Inicializamos los hosts filtrados con todos los hosts del grupo seleccionado
    useEffect(() => {
        if (selectedGrupo) {
            setFilteredHosts(selectedGrupo.hosts);
        }
    }, [selectedGrupo]);

    const onSurveySubmit = (data) => {
        // console.log('Survey data submitted:', data);
        setSurveyData(data.extra_vars);
    };

    // Verificar si un módulo existe en la lista de módulos
    const moduloExists = (nombre) => {
        return modulosMonitoreo.some((modulo) => modulo.name === nombre);
    };

    const getHostIds = () => {
        if (selectedHosts.length > 0) return selectedHosts.map(host => host.id);
        if (selectedGrupo && selectedGrupo.hosts) return selectedGrupo.hosts.map(host => host.id);
        return inventario.groups?.flatMap(g => g.hosts).map(h => h.id) || [];
    };


    // Ejecutar monitoreo
    const handleMonitoreoExecute = async () => {
        setIsMonitoring(true);
        try {
            const hostIds = getHostIds();
            const selectedModule = getSelectedModule();
            const moduleType = selectedModule?.title?.includes('cisco') ? 'cisco' : 'linux';

            // Validación crítica
            if (!hostIds.length) {
                throw new Error('No hay hosts disponibles para monitorear');
            }

            // Persistencia local
            localStorage.setItem('hostIds', JSON.stringify(hostIds));
            localStorage.setItem('inventario', JSON.stringify(inventario.id));
            localStorage.setItem('grupo', JSON.stringify(selectedGrupo));

            // Ejecución según tipo de módulo
            if (moduleType === 'cisco') {
                await ejecutarMonitoreo(
                    selectedModule.id,
                    idUsuario,
                    inventario.idInventario,
                    selectedGrupo?.id,
                    hostIds,
                    fetchWithAuth
                );
                setIsMonitoring(true);
            } else if (moduleType === 'linux') {
                const response = await ejecutarModulo(
                    selectedModule.id,
                    idUsuario,
                    inventario.idInventario,
                    selectedGrupo?.id,
                    hostIds,
                    fetchWithAuth,
                    surveyData
                );

                // Mostrar resultado en los divs
                const responseText = await response.text();
                const result = isJson(responseText) ? JSON.parse(responseText) : responseText;
                console.log('Respuesta del módulo Linux:', result);
                setIsMonitoring(false);
            }

            // Actualización estado
            setTimeElapsed(0);
            onHostIdsUpdate?.(hostIds);

        } catch (error) {
            console.error('Error en monitoreo:', error);
        }
    };

    const handleStopMonitoreo = async () => {
        try {
            // Usar nueva función importada
            const response = await detenerMonitoreo(idUsuario, fetchWithAuth);

            if (!response.ok) {
                throw new Error(`Error: ${response.status}`);
            }

            const responseText = await response.text();
            const result = isJson(responseText) ? JSON.parse(responseText) : responseText;

            console.log('Monitoreo detenido:', result);
            setIsMonitoring(false);
            setTimeElapsed(0);

        } catch (error) {
            console.error('Error al detener:', error);
        }
    };

    // Función para verificar si la respuesta es JSON
    const isJson = (str) => {
        try {
            JSON.parse(str);
            return true;
        } catch {
            return false;
        }
    };

    useEffect(() => {
        if (selectedMonitoreo === 1) {
            onModuleTypeChange('cisco');
        } else if (selectedMonitoreo === 2) {
            onModuleTypeChange('linux');
        }
    }, [selectedMonitoreo, onModuleTypeChange]);

    // Indicador de carga y error
    if (loading) {
        return (
            <div className="flex justify-center items-center h-auto">
                <Loading message="Cargando..." />
            </div>
        );
    }

    // Configuración de los hexágonos
    const hexagons = [
        { id: 1, color: 'bg-gray-400', title: '', disabled: true },
        { id: 2, color: 'bg-gray-400', title: '', disabled: true },
        { id: 3, color: 'bg-gray-400', title: '', disabled: true },
        { id: 4, color: 'bg-gray-400', title: '', disabled: true },
        { id: 5, color: 'bg-gray-400', title: '', disabled: true },
        { id: 6, color: 'bg-gray-400', title: '', disabled: true },
        { id: 7, color: 'bg-gray-400', title: '', disabled: true },
        { id: 8, color: 'bg-gray-400', title: '', disabled: true },
        { id: 9, color: 'bg-gray-400', title: '', disabled: true },
    ];

    // Configuración módulos monitoreo
    const monitoreo = [
        { id: 1, title: '[cisco] - monitoreo switch', disabled: !moduloExists('[cisco] - monitoreo switch') },
        { id: 2, title: '[linux] - monitoreo linux', disabled: !moduloExists('[linux] - monitoreo linux') },
    ];
    // Obtener el módulo seleccionado
    const getSelectedModule = () => {
        return modulosMonitoreo.find((modulo) => modulo.name === monitoreo.find((mon) => mon.id === selectedMonitoreo)?.title);
    };

    return (
        <div className="flex flex-col sm:flex-row">

            {/* Seleccionar tipo de monitoreo */}
            <div className="w-auto sm:w-1/5 p-2">
                <div className="flex-1 flex flex-col items-center justify-center space-y-4">
                    <div className="w-full space-y-4">
                        {monitoreo.map((mod) => (
                            <div
                                key={mod.id}
                                className={`border border-sky-950 rounded-lg p-3 cursor-pointer text-center transition-colors
                                    ${selectedMonitoreo === mod.id ? 'bg-sky-100' : ''}
                                    ${mod.disabled ? 'opacity-50 cursor-not-allowed' : ''}`}
                                onClick={() => !mod.disabled && setSelectedMonitoreo(mod.id)}
                            >
                                <div className="w-12 h-12 mx-auto mb-2">
                                    <img
                                        src={mod.title.includes('cisco') ? Cisco : Linux}
                                        alt={mod.title}
                                        className="w-full h-full object-contain"
                                    />
                                </div>
                                <span className="text-sm font-semibold text-sky-950 block">
                                    {mod.title.replace(/\[(.*?)\]\s-\s/, '')}
                                    <br />
                                    <span className="text-xs">ID: {
                                        modulosMonitoreo.find(m => m.name === mod.title)?.id || 'N/A'
                                    }</span>
                                </span>
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {/* Selección Grupo y Host */}
            <div className="w-auto sm:w-2/5 bg-gray-100 p-4 border border-sky-950 rounded-xl m-2">
                <div className="w-full">
                    <h1 className="text-base text-sky-950 font-bold text-center mb-2">Configuración del monitoreo</h1>
                    <div className="p-1">
                        <label className="text-sm text-sky-900 font-bold">Grupo</label>
                        {!selectedMonitoreo ? (
                            <p className="text-gray-500 text-xs">Selecciona un módulo para habilitar la selección de grupo</p>
                        ) : inventario?.groups ? (
                            <select
                                value={selectedGrupo?.id || ''}
                                onChange={(e) => {
                                    const grupo = inventario.groups.find(g => g.id === parseInt(e.target.value)) || null;
                                    setSelectedGrupo(grupo);
                                    setSelectedHosts([]);
                                }}
                                className="w-full border border-gray-300 rounded px-2 py-1 text-xs"
                            >
                                <option value="">Todos</option>
                                {inventario.groups.map(({ id, groupName }) => (
                                    <option key={id} value={id}>{groupName}</option>
                                ))}
                            </select>
                        ) : (
                            <p className="text-gray-500 text-xs">Cargando grupos...</p>
                        )}
                    </div>
                </div>

                <div className="w-full">
                    <div className="p-1">
                        {selectedGrupo && (
                            <div className="flex justify-between items-center">
                                <div>
                                    <label className="block text-sm text-sky-900 font-bold">Host</label>
                                    <button
                                        onClick={() => setIsModalOpen(true)}
                                        className={`bg-sky-800 hover:bg-blue-950 text-white font-medium py-1 px-2 rounded text-xs ${!selectedGrupo ? 'opacity-50 cursor-not-allowed' : ''}`}
                                        disabled={!selectedGrupo}
                                    >
                                        Seleccionar Host
                                    </button>
                                </div>

                                {selectedMonitoreo === 2 && (
                                    <div>
                                        <label className="block text-sm text-sky-900 font-bold">Survey</label>
                                        <button
                                            onClick={() => setIsModalSurveyOpen(true)}
                                            className={`bg-sky-800 hover:bg-blue-950 text-white font-medium py-1 px-2 rounded text-xs ${!selectedGrupo ? 'opacity-50 cursor-not-allowed' : ''}`}
                                            disabled={!selectedGrupo}
                                        >
                                            Insertar encuesta
                                        </button>
                                    </div>
                                )}
                            </div>
                        )}

                        {isModalOpen && (
                            <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
                                <div className="bg-white rounded-lg shadow-lg w-1/3 p-4">
                                    <h2 className="text-lg font-bold mb-2">Seleccionar Host</h2>
                                    <BuscarHost
                                        hosts={selectedGrupo.hosts}
                                        onFilterHosts={setFilteredHosts}
                                    />
                                    <div className="space-y-2">
                                        {filteredHosts.map((host) => (
                                            <div key={host.id} className="flex items-center">
                                                <input
                                                    type="checkbox"
                                                    value={host.id}
                                                    checked={selectedHosts.some((h) => h.id === host.id)}
                                                    onChange={(e) => {
                                                        if (e.target.checked) {
                                                            setSelectedHosts([...selectedHosts, host]);
                                                        } else {
                                                            setSelectedHosts(selectedHosts.filter(h => h.id !== host.id));
                                                        }
                                                    }}
                                                    className="mr-2"
                                                />
                                                <label>{host.hostname}</label>
                                            </div>
                                        ))}
                                    </div>

                                    <div className="mt-4 flex justify-end space-x-4">
                                        <button
                                            onClick={() => setIsModalOpen(false)}
                                            className="bg-green-600 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded"
                                        >
                                            Seleccionar
                                        </button>
                                        <button
                                            onClick={() => setIsModalOpen(false)}
                                            className="bg-red-500 hover:bg-gray-400 text-white font-semibold py-2 px-4 rounded"
                                        >
                                            Cancelar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}

                        {selectedGrupo && (
                            <div className="mt-2">
                                {selectedHosts.length === 0 ? (
                                    <p className="text-xs text-yellow-500">Se monitorearán todos los hosts del grupo</p>
                                ) : (
                                    <p className="text-xs text-gray-500">
                                        Hosts seleccionados: {selectedHosts.map(h => h.hostname).join(', ')}
                                    </p>
                                )}
                            </div>
                        )}

                        {isModalSurveyOpen && (
                            <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
                                <div className="bg-white rounded-lg shadow-lg w-1/3 p-4">
                                    <h2 className="text-lg font-bold mb-2">Ingresar encuesta</h2>
                                    <ModuloSurvey currentModuloId={getSelectedModule()?.id} correoUsuario={correoUsuario} onSurveySubmit={onSurveySubmit} usuarioId={idUsuario} />

                                    <div className="mt-4 flex justify-end space-x-4">
                                        <button
                                            onClick={() => setIsModalSurveyOpen(false)}
                                            className="bg-green-600 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded"
                                        >
                                            Seleccionar
                                        </button>
                                        <button
                                            onClick={() => setIsModalSurveyOpen(false)}
                                            className="bg-red-500 hover:bg-gray-400 text-white font-semibold py-2 px-4 rounded"
                                        >
                                            Cancelar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}

                        <div className="w-full justify-center items-center">
                            <div className="p-4">

                                <div className="mt-2 flex flex-col items-center space-y-2">
                                    {!isMonitoring ? (
                                        <button
                                            className={`font-medium rounded text-sm py-1 px-4 ${isMonitoring
                                                ? 'bg-red-600 hover:bg-red-500 text-white'
                                                : tipoUsuario === 'admin' || tipoUsuario === 'editor'
                                                    ? 'bg-green-600 hover:bg-green-500 text-white'
                                                    : 'bg-gray-400 text-gray-700'
                                                } ${!selectedGrupo ? 'cursor-not-allowed' : ''}`}
                                            disabled={!selectedGrupo}
                                            onClick={() => {
                                                if (!isMonitoring) {
                                                    handleMonitoreoExecute();
                                                }
                                            }}
                                        >
                                            Ejecutar monitoreo
                                        </button>
                                    ) : (
                                        <button
                                            className="font-medium rounded text-sm py-1 px-4 bg-red-600 hover:bg-red-500 text-white"
                                            onClick={handleStopMonitoreo}
                                        >
                                            Detener monitoreo
                                        </button>
                                    )}


                                    {isMonitoring && selectedMonitoreo === 1 && (
                                        <div className="text-sm text-gray-500">
                                            Cronómetro: {formatTime(timeElapsed)}
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            {/* Panal */}
            <div className="w-auto sm:w-2/5 p-4 h-48 flex flex-col items-center">
                <style>{hexagonClass}</style>
                <div className="relative">
                    {/* Primer grupo de 4 hexágonos */}
                    <div className="flex space-x-2 justify-center">
                        {hexagons.slice(0, 5).map((hex, index) => {
                            const host = displayedHosts[index];
                            const hasHost = !!host;
                            return (
                                <div
                                    key={hex.id}
                                    className={`hexagon cursor-pointer relative
                                        ${hasHost ? 'bg-sky-950' : 'bg-gray-400'}
                                        ${!hasHost ? 'hexagon-disabled' : ''}
                                        h-24 w-20 flex items-center justify-center`}
                                >
                                    <span className="text-white text-xs font-semibold text-center">
                                        {host?.hostname || ''}
                                    </span>
                                </div>
                            );
                        })}
                    </div>

                    {/* Segundo grupo de 3 hexágonos */}
                    <div className="flex justify-center mt-[-30px] space-x-2 pt-3">
                        {hexagons.slice(5).map((hex, index) => {
                            const hostIndex = 4 + index;
                            const host = displayedHosts[hostIndex];
                            const hasHost = !!host;
                            return (
                                <div
                                    key={hex.id}
                                    className={`hexagon cursor-pointer relative
                                        ${hasHost ? 'bg-sky-950' : 'bg-gray-400'}
                                        ${!hasHost ? 'hexagon-disabled' : ''}
                                        h-24 w-20 flex items-center justify-center`}
                                >
                                    <span className="text-white font-bold text-center">
                                        {host?.hostname || ''}
                                    </span>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ControladorMonitoreo;