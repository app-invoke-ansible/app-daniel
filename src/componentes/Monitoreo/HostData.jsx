import React from "react";
import DataCard from "./DataCard";
import GraficoUsoCPU from "./GraficoUsoCPU";
import NetworkTrafficChart from "./NetworkTrafficChart";
import EstadisticasMonitoreo from "./EstadisticasMonitoreo";

const HostData = ({ idHost, hostname, monitoringData, networkData }) => {
    const processorData = monitoringData[idHost]?.["Processor"] || [];
    const networkDataForHost = networkData[idHost] || [];

    return (
        <div className="mb-8 p-4  rounded-lg"> {/* Contenedor para toda la info del host */}
            <h1 className="text-xl text-sky-900 font-bold mb-4">Host: {hostname}</h1>

            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                {/* Columna 1: DataCards (CPU e I/O) y Gráfico de Uso de CPU */}
                <div>
                    <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                        <DataCard
                            idHost={idHost}
                            tipo="Processor"
                            data={monitoringData[idHost]?.["Processor"]?.[0]}
                            hostname={hostname}
                            monitoringData={monitoringData}
                        />
                        <DataCard
                            idHost={idHost}
                            tipo="I/O"
                            data={monitoringData[idHost]?.["I/O"]?.[0]}
                            hostname={hostname}
                            monitoringData={monitoringData}
                        />
                    </div>

                    {/* Gráfico de uso de CPU debajo de los DataCards */}
                    <div className="mt-4">
                        <GraficoUsoCPU cpuData={processorData} threshold={75} />
                    </div>
                </div>

                {/* Columna 2: Datos de Red */}
                <div className="flex flex-col h-full">
                    <div className="grid grid-cols-1 gap-4 ">
                        <div>
                            <NetworkTrafficChart monitoringData={monitoringData} data={networkDataForHost} title={`Tráfico de Red`} />
                        </div>
                        <div>
                            <div className="rounded-lg border border-sky-950 p-2 w-full mt-3">
                                <EstadisticasMonitoreo data={monitoringData[idHost]} />
                            </div>
                        </div>

                    </div>
                    {/*Cuadro vacio para ocupar el espacio restante */}
                    <div className="rounded-lg border border-sky-950  w-full mt-3 flex-grow">

                    </div>
                </div>
            </div>
        </div>
    );
};

export default HostData;