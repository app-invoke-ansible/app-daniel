import React from "react";
import PropTypes from "prop-types";
import { BsInfoCircle } from "react-icons/bs";

/**
 * Componente para mostrar estadísticas avanzadas de monitoreo.
 *
 * @param {Object} props
 * @param {Array} props.data Datos de monitoreo agrupados por tipo (Processor, I/O).
 */
const EstadisticasMonitoreo = ({ data }) => {
  const calculateStats = (typeData) => {
    if (!typeData || typeData.length === 0) return null;

    const totalUsed = typeData.reduce((sum, item) => sum + item.used, 0);
    const totalFree = typeData.reduce((sum, item) => sum + item.free, 0);
    const minUsed = Math.min(...typeData.map((item) => item.used));
    const maxUsed = Math.max(...typeData.map((item) => item.used));
    const averageUsed = totalUsed / typeData.length;
    const averageFree = totalFree / typeData.length;

    return { totalUsed, totalFree, minUsed, maxUsed, averageUsed, averageFree };
  };

  const renderIndicator = (currentUsed, total) => {
    const usagePercentage = (currentUsed / total) * 100;

    if (usagePercentage > 85) {
      return <span className="text-red-500">🔴 Crítico</span>;
    } else if (usagePercentage > 60) {
      return <span className="text-yellow-500">🟡 Medio</span>;
    }
    return <span className="text-green-500">🟢 Óptimo</span>;
  };

  const detectFluctuations = (typeData) => {
    if (!typeData || typeData.length < 2) return null;

    const [latest, previous] = typeData;
    const fluctuation = Math.abs(latest.used - previous.used);

    return fluctuation > latest.total * 0.1 ? (
      <span className="text-red-500">⚠️ Variación detectada</span>
    ) : (
      <span className="text-green-500">Estable</span>
    );
  };

  if (!data) {
    return <p>No hay datos disponibles para mostrar.</p>;
  }

  return (
    <div>
      {["Processor", "I/O"].map((type) => {
        const typeData = data[type];
        if (!typeData || typeData.length === 0) return null;

        const stats = calculateStats(typeData);

        const statLabels = [
          { label: 'Máximo Usado', value: stats.maxUsed },
          { label: 'Mínimo Usado', value: stats.minUsed },
          { label: 'Uso Actual', value: ((typeData[0]?.used / typeData[0]?.total) * 100).toFixed(2) + '%' },
          { label: 'Estado Actual', value: renderIndicator(typeData[0]?.used, typeData[0]?.total), info: 'Indica el estado actual según el porcentaje de uso del recurso. Crítico, Medio, Óptimo.' },
          { label: 'Variaciones', value: detectFluctuations(typeData), info: 'Indica si hubo una variación significativa en el uso de los recursos en comparación con los datos previos.' }
        ];

        return (
          <div key={type}>
            <h2 className="text-sm text-gray-800 text-center font-semibold py-1">{type}</h2>
            <div className="grid grid-cols-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-2">
              {statLabels.map(({ label, value, info }, index) => (
                <div key={index} className="border border-sky-950 p-2 rounded-lg mb-5 relative">
                  <h6 className="text-xs text-gray-800 text-center">{label}:</h6>
                  <div className="flex items-center justify-center">
                    <h2 className="text-sm text-sky-900 font-bold">{value}</h2>
                    {info && (
                      <div
                        className="absolute top-0 right-0 p-1"
                        data-tooltip={info}
                      >
                        <BsInfoCircle className="cursor-pointer text-gray-400 text-xs" title={info} />
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          </div>
        );
      })}
    </div>
  );
};

EstadisticasMonitoreo.propTypes = {
  data: PropTypes.shape({
    Processor: PropTypes.arrayOf(
      PropTypes.shape({
        used: PropTypes.number,
        free: PropTypes.number,
        total: PropTypes.number,
        timestamp: PropTypes.string,
      })
    ),
    "I/O": PropTypes.arrayOf(
      PropTypes.shape({
        used: PropTypes.number,
        free: PropTypes.number,
        total: PropTypes.number,
        timestamp: PropTypes.string,
      })
    ),
  }),
};

export default EstadisticasMonitoreo;
