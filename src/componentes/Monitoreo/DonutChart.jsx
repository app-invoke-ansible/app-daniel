import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";

ChartJS.register(ArcElement, Tooltip, Legend); // Registro aquí

const DonutChart = ({ data }) => {
    const chartData = {
        labels: ["Usado", "Libre"],
        datasets: [
            {
                data: [data.used, data.free],
                backgroundColor: ["#FF6384", "#36A2EB"],
            },
        ],
    };

    return <Doughnut data={chartData} />;
};

export default DonutChart;