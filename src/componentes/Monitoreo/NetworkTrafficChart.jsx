import React, { useState, useMemo } from "react";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend } from "chart.js";

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

const NetworkTrafficChart = ({ data, title }) => {
  // Memoizar el agrupamiento de datos
  const groupedData = useMemo(() => {
    return data.reduce((acc, item) => {
      if (!acc[item.nombre]) {
        acc[item.nombre] = {
          bytesEntrada: [],
          paquetesEntrada: [],
        };
      }
      acc[item.nombre].bytesEntrada.push(item.bytesEntrada);
      acc[item.nombre].paquetesEntrada.push(item.paquetesEntrada);
      return acc;
    }, {});
  }, [data]);

  // Obtener los nombres de las interfaces
  const interfaceNames = Object.keys(groupedData);

  // Estado para gestionar la interfaz seleccionada
  const [selectedInterface, setSelectedInterface] = useState(null);

  // Generar un color aleatorio (más eficiente)
  const getRandomColor = () => `#${Math.floor(Math.random() * 16777215).toString(16)}`;

  // Crear los datasets de manera eficiente
  const datasets = useMemo(() => {
    if (selectedInterface) {
      const selectedData = groupedData[selectedInterface];
      return [
        {
          label: `Bytes Entrada - ${selectedInterface}`,
          data: selectedData.bytesEntrada,
          borderColor: getRandomColor(),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          tension: 0.4,
          fill: false,
        },
        {
          label: `Paquetes Entrada - ${selectedInterface}`,
          data: selectedData.paquetesEntrada,
          borderColor: getRandomColor(),
          backgroundColor: "rgba(54, 162, 235, 0.2)",
          tension: 0.4,
          fill: false,
        },
      ];
    } else {
      return interfaceNames.flatMap((nombre) => [
        {
          label: `Bytes Entrada - ${nombre}`,
          data: groupedData[nombre].bytesEntrada,
          borderColor: getRandomColor(),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          tension: 0.4,
          fill: false,
        },
        {
          label: `Paquetes Entrada - ${nombre}`,
          data: groupedData[nombre].paquetesEntrada,
          borderColor: getRandomColor(),
          backgroundColor: "rgba(54, 162, 235, 0.2)",
          tension: 0.4,
          fill: false,
        },
      ]);
    }
  }, [groupedData, selectedInterface, interfaceNames]);

  const labels = Array.from({ length: 100 }, (_, i) => `Datos N°${i + 1}`);

  const chartData = {
    labels,
    datasets,
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false, // Ocultar la leyenda
      },
      title: {
        display: true,
        text: title,
        padding: {
          top: 10,
          bottom: 20,
        },
        font: {
          size: 16,
          color: "black",
        },
      },
    },
    scales: {
      x: {
        display: false,
      },
      y: {
        display: true,
      },
    },
  };

  // Función para manejar el cambio de selección en el dropdown
  const handleSelectChange = (e) => {
    setSelectedInterface(e.target.value || null);
  };

  return (
    <div className="w-full h-full">
      <div className="rounded-lg border border-sky-950 p-2 w-full">
        <div className="bg-white p-2 rounded-lg">
          {/* Dropdown para seleccionar una interfaz */}
          <div className="flex justify-end">
            <select
              id="interface-select"
              value={selectedInterface || ""}
              onChange={handleSelectChange}
              className="p-0.5 border rounded text-xs"
            >
              <option value="">Todos</option>
              {interfaceNames.map((nombre) => (
                <option key={nombre} value={nombre}>
                  {nombre}
                </option>
              ))}
            </select>
          </div>

          {/* Renderizado del gráfico */}
          <Line data={chartData} options={options} />
        </div>
      </div>
    </div>
  );
};

export default NetworkTrafficChart;
