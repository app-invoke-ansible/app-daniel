import React, { useEffect, useState } from "react";
import { Line } from "react-chartjs-2";

const GraficoUsoCPU = ({ cpuData, initialThreshold = 80 }) => {
  const [data, setData] = useState(cpuData);
  const [threshold, setThreshold] = useState(initialThreshold);
  const [isDataLoaded, setIsDataLoaded] = useState(false); // Estado para verificar si los datos están cargados
  const [isMonitoring, setIsMonitoring] = useState(false); // Estado para el monitoreo

  // Cargar el valor de isMonitoring desde localStorage
  useEffect(() => {
    const monitoringStatus = localStorage.getItem("isMonitoring") === "true";
    setIsMonitoring(monitoringStatus);
  }, []);

  // Función para actualizar los datos cada 30 segundos
  useEffect(() => {
    const interval = setInterval(() => {
      setData((prevData) => [...prevData]); // Esto puede reemplazarse con una actualización real de datos
    }, 30000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    // Verifica si hay datos de CPU y si el monitoreo está activo
    if (cpuData && cpuData.length > 0) {
      setData(cpuData);
      setIsDataLoaded(true); // Marca los datos como cargados
    } else {
      setIsDataLoaded(false); // Si no hay datos, marca como no cargado
    }
  }, [cpuData]);

  if (!isMonitoring) {
    return <div className="text-center">Debe seleccionar las opciones disponibles para realizar el monitoreo.</div>;
  }

  if (!isDataLoaded) {
    return <div className="text-center">No hay datos de CPU disponibles.</div>;
  }

  // Ordenar los datos por timestamp (por si no lo están)
  const sortedData = [...data].sort((a, b) => new Date(a.timestamp) - new Date(b.timestamp));

  // Limitar a los últimos 30 datos
  const limitedData = sortedData.slice(-30);

  // Formatear los datos para el gráfico
  const timestamps = limitedData.map((data) =>
    new Date(data.timestamp).toLocaleTimeString()
  );

  // Calcular el porcentaje de uso
  const usedPercentages = limitedData.map((data) => {
    const percentage = data.total ? ((data.used / data.total) * 100).toFixed(2) : 0;
    return parseFloat(percentage);
  });

  // Crear los datasets
  const chartData = {
    labels: timestamps,
    datasets: [
      {
        label: "Uso de CPU (%)",
        data: usedPercentages,
        fill: true,
        borderColor: "#36A2EB",
        backgroundColor: "rgba(54, 162, 235, 0.2)",
        tension: 0.4, // Línea suave
      },
      {
        label: "Umbral de uso",
        data: new Array(usedPercentages.length).fill(threshold),
        borderColor: "red",
        backgroundColor: "rgba(255, 99, 132, 0.2)",
        borderDash: [1, 1],
        fill: false,
        tension: 0,
        pointRadius: 0,
      },
    ],
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: true,
        position: "top",
      },
    },
    scales: {
      x: {
        title: {
          display: true, // Mostrar el título
          text: "Tiempo", // Título del eje X
        },
        ticks: {
          display: false, // Ocultar las etiquetas del eje X
        },
      },
      y: {
        title: {
          display: true,
          text: "Uso (%)",
        },
        min: 0,
        max: 100,
        ticks: {
          stepSize: 20, // Intervalos de 20 en 20
        },
      },
    },
  };

  // Función para manejar el cambio del slider
  const handleThresholdChange = (event) => {
    setThreshold(Number(event.target.value)); // Actualiza el umbral
  };

  return (
    <div className="w-full h-full">
      <div className="rounded-lg border border-sky-950 p-3 w-full">
        <div className="bg-white rounded-lg">
          {/* Barra deslizante para seleccionar el umbral */}
          <div className="flex justify-end">
            <div className="flex items-center py-2">
              <label htmlFor="threshold-slider" className="text-xs mr-2">
                Umbral: {threshold}%
              </label>
              <input
                type="range"
                id="threshold-slider"
                min="0"
                max="100"
                step="1"
                value={threshold}
                onChange={handleThresholdChange}
                className="w-36"
              />
            </div>
          </div>

          {/* Renderizado del gráfico */}
          <Line data={chartData} options={options} />
        </div>
      </div>
    </div>
  );
};

export default GraficoUsoCPU;
