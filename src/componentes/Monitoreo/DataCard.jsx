import React from "react";
import { IoMdArrowDropdown, IoMdArrowDropup } from "react-icons/io";
import { LuDot } from "react-icons/lu";
import PreviousDataButton from "./PreviousDataButton";
import DonutChart from "./DonutChart";

//Función para formatear bytes a unidades más legibles
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return "0 Bytes";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

const DataCard = ({ idHost, tipo, data, hostname, monitoringData }) => {
    if (!data) return null;

    const previousData = monitoringData[idHost]?.[tipo]?.slice(1, 10) || [];

    const renderArrowIndicator = (currentValue, previousValue) => {
        if (previousData.length === 0) return <LuDot className="text-sky-950" />; // No hay datos previos
        if (currentValue > previousValue) return <IoMdArrowDropup className="text-green-500" />;
        if (currentValue < previousValue) return <IoMdArrowDropdown className="text-red-500" />;
        return <LuDot className="text-sky-950" />; // No hay cambio
    };

    const renderValueWithTrend = (label, currentValue, previousValue) => (
        <div className="border border-sky-950 p-2 rounded-lg">
            <h6 className="text-xs text-gray-800 text-center">{label}:</h6>
            <div className="flex items-center justify-center">
                {renderArrowIndicator(currentValue, previousValue)}
                <h2 className="text-sm text-sky-900 font-bold">{formatBytes(currentValue)}</h2>
            </div>
            {previousData.slice(0, 1).map((prevData, index) => (
                <h6 key={`${prevData.id}-${index}`} className="text-xs text-gray-500 italic text-right mr-6">
                    {formatBytes(previousValue)}
                </h6>
            ))}
        </div>
    );

    return (
        <div className="col-span-1 p-2 rounded-lg border border-sky-950">
            {/* VERSION ESCRITORIO */}
            <div className="hidden sm:block">
                <h6 className="text-sm text-gray-800 text-center font-semibold">{tipo}</h6>
                <h6 className="text-xs text-gray-600 text-center">Head: {data.head}</h6>
                <div className="border border-sky-950 rounded-lg my-2 py-2">
                    <h6 className="text-xs text-gray-800 text-center">Total:</h6>
                    <h2 className="text-sm text-sky-900 font-bold text-center">{formatBytes(data.total)}</h2>
                </div>

                <div className="grid grid-cols-2 gap-2">
                    {renderValueWithTrend("Usado", data.used, previousData[0]?.used)}
                    {renderValueWithTrend("Libre", data.free, previousData[0]?.free)}
                </div>

                <DonutChart data={data} />
                <div className="bg-white p-4 rounded mt-2">
                    <h6 className="text-xs text-gray-800 text-center">
                        Última actualización: <p>{new Date(data.timestamp).toLocaleString()}</p>
                    </h6>
                </div>
                {previousData.length > 0 && (
                    <div className="flex justify-end">
                        <PreviousDataButton previousData={previousData} />
                    </div>
                )}
            </div>

            {/* VERSION MOBILE */}
            <div className="sm:hidden">
                <div className="flex flex-row justify-between items-center border-b border-gray-300">
                    {/* Columna centrada */}
                    <div className="flex flex-col items-start flex-grow px-2">
                        <h6 className="text-md text-gray-800">{tipo}</h6>
                        <h6 className="text-sm text-gray-600">Head: {data.head}</h6>
                    </div>

                    {/* Columna para el botón de datos previos alineado a la derecha */}
                    <div className="flex flex-row justify-end">
                        {previousData.length > 0 && (
                            <PreviousDataButton previousData={previousData} />
                        )}
                    </div>
                </div>
                {/* CARD del grafico de donuts */}
                <div className="grid grid-cols-2 gap-2">
                    {/* Información de total, usado y libre */}
                    <div className="flex flex-col p-4 ">
                        <div className="border border-sky-950 rounded-lg my-2 py-2">
                            <h6 className="text-xs text-gray-800 text-center">Total:</h6>
                            <h2 className="text-sm text-sky-900 font-bold text-center">{formatBytes(data.total)}</h2>
                        </div>
                        <div className="grid grid-cols-2 gap-1">
                            {renderValueWithTrend("Usado", data.used, previousData[0]?.used)}
                            {renderValueWithTrend("Libre", data.free, previousData[0]?.free)}
                        </div>
                        {/* Última actualización */}
                        <div className="mt-2 text-center">
                            <h6 className="text-xs text-gray-800">Última actualización:</h6>
                            <p className="text-xs text-gray-600">{new Date(data.timestamp).toLocaleString()}</p>
                        </div>
                    </div>
                    <div className="flex flex-col-2">
                        <DonutChart data={data} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DataCard;