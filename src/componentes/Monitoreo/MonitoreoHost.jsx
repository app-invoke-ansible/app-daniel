import React, { useState, useEffect } from "react";
import { useAuth } from "../../providers/AuthProvider";
import { LuLoaderCircle } from "react-icons/lu";
import ProcessorCards from "./ProcessorCards";

// Variables de entorno
const API_URL = process.env.REACT_APP_API_URL;

const MonitoreoHost = ({ updatedHostIds }) => {
  const [data, setData] = useState({ monitoreo: [] });
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [tipoUsuario, setTipoUsuario] = useState(null);
  const [loadingTipoUsuario, setLoadingTipoUsuario] = useState(true);
  const [localHostIds, setLocalHostIds] = useState([]);

  const { fetchWithAuth, user } = useAuth();

  // Obtener información del usuario autenticado
  const fetchUsuarios = async () => {
    try {
      const response = await fetchWithAuth(`${API_URL}/usuario`);
      const usuarios = await response.json();

      const usuarioAutenticado = usuarios.find((usuario) => usuario.username === user?.name);

      if (usuarioAutenticado) {
        setTipoUsuario(usuarioAutenticado.tipoUsuario);
      } else {
        console.warn("Usuario autenticado no encontrado en la API.");
      }
    } catch (error) {
      console.error("Error al cargar los permisos:", error);
    } finally {
      setLoadingTipoUsuario(false);
    }
  };

  // Obtener monitoreo de la API
  const fetchMonitoreo = async () => {
    setLoading(true);
    setError(null);

    // Usar IDs actualizados o locales si están disponibles
    const hostIds =
      updatedHostIds?.length > 0 ? updatedHostIds.join(",") : localHostIds.join(",");

    if (!hostIds) {
      setError("No se proporcionaron IDs de host.");
      setLoading(false);
      return;
    }

    try {
      const response = await fetchWithAuth(`${API_URL}/monitoreo/${hostIds}`);
      const contentType = response.headers.get("content-type");
      if (!contentType || !contentType.includes("application/json")) {
        throw new Error("El formato de la respuesta no es JSON");
      }
      const result = await response.json();
      setData(result);
    } catch (error) {
      console.error("Error al obtener el monitoreo:", error.message);
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  // Cargar `hostIds` desde localStorage al montar
  useEffect(() => {
    const storedHostIds = localStorage.getItem("hostIds");
    if (storedHostIds) {
      setLocalHostIds(JSON.parse(storedHostIds));
    }
  }, []);

  // Ejecutar llamadas a la API al cargar el componente o al cambiar updatedHostIds
  useEffect(() => {
    fetchUsuarios();
    fetchMonitoreo(); // Llamada inicial y cada vez que cambian los IDs
  }, [updatedHostIds, localHostIds]);

  if (loading) {
    return (
      <div className="flex justify-center items-center h-auto">
        <div className="text-center">
          <LuLoaderCircle className="animate-spin mx-auto text-sky-600 text-4xl mb-4" />
          <p className="text-sky-600 text-md font-medium">Cargando monitoreo...</p>
        </div>
      </div>
    );
  }

  return (
    <div className="min-h-screen p-4">
      {/* Procesadores */}
      <div>
        <ProcessorCards updatedHostIds={localHostIds} />
      </div>
    </div>
  );
};

export default MonitoreoHost;
