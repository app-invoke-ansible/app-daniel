import React, { useState } from "react";
import { IoMdClose } from "react-icons/io";

const PreviousDataModal = ({ previousData, onClose }) => {
  return (
    <div className="fixed inset-0 bg-black bg-opacity-50 flex items-center justify-center z-50">
      <div className="bg-white rounded-lg shadow-lg w-3/4 max-w-2xl p-4">
        <div className="flex justify-between items-center">
          <h4 className="text-lg font-semibold">Datos Previos</h4>
          <button onClick={onClose} className="text-red-500">
            <IoMdClose size={24} />
          </button>
        </div>
        <div className="mt-4">
          <table className="table-auto border-collapse border border-gray-300 w-full">
            <thead>
              <tr>
                <th className="border border-gray-300 sm:px-2 px-1 py-1">Fecha</th>
                <th className="border border-gray-300 sm:px-2 px-1 py-1">Usado</th>
                <th className="border border-gray-300 sm:px-2 px-1 py-1">Libre</th>
              </tr>
            </thead>
            <tbody>
              {previousData.map((data, index) => (
                <tr key={`${data.id}-${index}`}>
                  <td className="border border-gray-300 sm:px-2 px-1 py-1">
                    {new Date(data.timestamp).toLocaleString()}
                  </td>
                  <td className="border border-gray-300 sm:px-2 px-1 py-1">{data.used}</td>
                  <td className="border border-gray-300 sm:px-2 px-1 py-1">{data.free}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

const PreviousDataButton = ({ previousData }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);

  return (
    <div>
      <button
        onClick={openModal}
        className="bg-sky-950 text-xs font-semibold text-white px-4 py-2 rounded-lg hover:bg-blue-600"
      >
        Datos previos
      </button>
      {isModalOpen && (
        <PreviousDataModal previousData={previousData} onClose={closeModal} />
      )}
    </div>
  );
};

export default PreviousDataButton;
