import React from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend } from 'chart.js';


ChartJS.register(LineElement, PointElement, CategoryScale, LinearScale, Title, Tooltip, Legend);

const LineChart = () => {
  const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'], 
    datasets: [
      {
        label: 'My First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40], 
        fill: true, 
        borderColor: 'rgba(75, 192, 192, 1)', 
      },
    ],
  };

  const options = {
    plugins: {
      legend: {
        display: true,
        position: 'top',
      },
      title: {
        display: false,
        text: 'Gráfico de Líneas',
      },
    },
    scales: {
      x: {
        title: {
          display: false,
          text: 'Meses',
        },
      },
      y: {
        title: {
          display: false,
          text: 'Valor',
        },
      },
    },
  };

  return (
    <div className="w-full mx-auto">
      <Line data={data} options={options} />
    </div>
  );
};

export default LineChart;
