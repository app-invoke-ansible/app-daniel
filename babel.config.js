// babel.config.js
module.exports = {
    presets: [
        '@babel/preset-env', // Para compilar el código moderno de JavaScript
        '@babel/preset-react', // Para compilar JSX
    ],
    plugins: [
        '@babel/plugin-proposal-nullish-coalescing-operator', // Para el operador de fusión nula
        '@babel/plugin-proposal-optional-chaining', // Para el encadenamiento opcional
        '@babel/plugin-proposal-class-properties', // Para propiedades de clase
        '@babel/plugin-proposal-private-methods', // Para métodos privados
    ],
};
