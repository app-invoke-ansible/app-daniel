FROM node:21.7.2 

# Establecer el directorio de trabajo
WORKDIR /src

# Copiar los archivos de configuración de dependencias
COPY package*.json ./

# Instalar las dependencias
RUN npm install

# Copiar el resto de la aplicación
COPY . .

# Exponer el puerto en el que la aplicación escuchará
EXPOSE 3000

# Comando para iniciar la aplicación
CMD ["npm", "start"]